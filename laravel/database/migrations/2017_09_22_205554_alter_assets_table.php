<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assets', function (Blueprint $table) {
            //$table->enum('for', ['profile_pic', 'user_project'])->comment('profile_pic => For Profile Picture, user_project => For user project')->nullable()->change();
            $table->text('title')->nullable();
            $table->enum('assets_visibility', [0, 1, 2])->default(0)->comment('0 => Public, 1 => Only Friends, 2 => Private');
            $table->integer('views')->default(0);
            $table->text('category')->nullable();
            $table->text('tags')->nullable();
            $table->text('about')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->dropColumn('assets_visibility');
            $table->dropColumn('views');
            $table->dropColumn('category');
            $table->dropColumn('tags');
            $table->dropColumn('about');
            //
        });
    }
}
