<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->enum('type', ['audio', 'image', 'video'])->default('image');
            $table->enum('for', ['profile_pic'])->comment('profile_pic => For Profile Picture, 1 => Accepted, 2 => Declined, 3 => Blocked')->nullable();
            $table->enum('priority', [0, 1])->default(1)->comment('0 => Other, 1 => Default');
            $table->text('path');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
}
