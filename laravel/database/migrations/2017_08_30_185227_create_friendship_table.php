<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFriendshipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('friendship', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('first_user_id')->unsigned();
            $table->integer('second_user_id')->unsigned();
            $table->integer('action_user_id')->unsigned();
            $table->enum('status', [0, 1, 2, 3])->default(0)->comment('0 => Pending, 1 => Accepted, 2 => Declined, 3 => Blocked');
            $table->timestamp('friendship_at');
            $table->timestamps();
            $table->foreign('first_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('second_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('action_user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('friendship');
    }
}
