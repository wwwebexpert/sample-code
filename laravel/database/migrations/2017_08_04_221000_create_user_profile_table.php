<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profile', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('profile_img')->nullable();
            $table->date('dob')->nullable();
            $table->text('interest')->nullable();
            $table->text('about_user')->nullable();
            $table->string('company_name')->nullable();
            $table->string('city')->nullable();
            $table->string('timezone')->nullable();
            $table->string('website_url')->nullable();
            $table->string('facebook_link_url')->nullable();
            $table->string('instagram_link_url')->nullable();
            $table->string('twitter_link_url')->nullable();
            $table->string('youtube_link_url')->nullable();
            $table->string('dribbble_link_url')->nullable();
            $table->string('linkedin_link_url')->nullable();
            $table->string('vimeo_link_url')->nullable();
            

            $table->string('phone')->nullable();
            $table->string('title')->nullable();
            $table->string('address_1')->nullable();
            $table->string('address_2')->nullable();
            $table->string('state')->nullable();
            $table->string('postal')->nullable();
            $table->string('country')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profile');
    }
}
