var echo = require('laravel-echo-server/dist');

echo.run({    
    "authEndpoint": "/broadcasting/auth",
    "authHost": "http://talentpool.mobilytedev.com",
    "database": "redis",
    "databaseConfig": {
        "redis": {
            "port": "6379",
            "host": "localhost"
        },
        "sqlite": {
            "databasePath": "/database/laravel-echo-server.sqlite"
        }
    },
    "devMode": true,
    // "host": "10.20.1.155",
    "port": "6001",
    "sslCertPath": "",
    "sslKeyPath": ""
});