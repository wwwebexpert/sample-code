<?php

namespace App\Jobs;

use Mail;
use App\User;
use App\Mail\WelcomeWithRandomPasswordEmail;
// use Illuminate\Mail\Mailable;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendWelcomeWithRandomPasswordEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $user;
    protected $password;
    protected $provider;

    public function __construct(User $user, $password, $provider)
    {
        $this->user = $user;
        $this->password = $password;
        $this->provider = $provider;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // echo "taf".$this->password; die;
        $email = new WelcomeWithRandomPasswordEmail($this->user, $this->password, $this->provider);
        Mail::to($this->user->email)->send($email);
    }
}
