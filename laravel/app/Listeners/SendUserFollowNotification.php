<?php

namespace App\Listeners;

use App\Events\UserFollowOther;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendUserFollowNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserFollowOther  $event
     * @return void
     */
    public function handle(UserFollowOther $event)
    {
        //
    }
}
