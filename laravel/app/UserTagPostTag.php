<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTagPostTag extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_tag_post_tag';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function assets(){
    	return $this->belongsTo('App\Asset', 'asset_id');
    }
    public function user(){
    	return $this->belongsTo('App\User', 'user_id');
    }
}
