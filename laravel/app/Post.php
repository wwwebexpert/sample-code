<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Get the assiociate user as author of the post.
     */
    public function author()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
