<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follower extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'user_id', 'follower_user_id' ];
    /**
     * Get the user profile associated with the user.
     */
    public function details()
    {
        return $this->belongsTo('App\User', 'follower_user_id');
    }
    public function detailsOfFollowingUser()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function videosOfFollowingUser()
    {
        // return $this->hasMany('App\Asset', 'user_id1');
        return $this->hasMany('App\Asset', 'user_id', 'id')
            ->where('for', 'user_project')
            ->orderBy('created_at', 'DESC');
    }
}
