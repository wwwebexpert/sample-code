<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // \App\Console\Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        /*if (stripos((string) shell_exec('ps xf | grep \'[q]ueue:work\''), 'artisan queue:work') === false) {
        $schedule->command('queue:work --queue=default --sleep=2 --tries=3 --timeout=5')->everyMinute()->appendOutputTo(storage_path() . '/logs/cron.log')->emailOutputTo('sachin.kumar@mobilyte.com');
        }*/
        $schedule->exec('/usr/local/bin/node /var/www/html/talentpool/laravel-echo-server.js')->everyMinute()->sendOutputTo(storage_path() . '/logs/cron.log')->before(function () {
             // Task is about to start...
            $new = new Schedule();
            $new->command('queue:work --tries=3 --timeout=5')->everyMinute()->sendOutputTo(storage_path() . '/logs/cron.log');
         });
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
