<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function tagAssets(){
    	return $this->hasMany('App\UserTagPostTag', 'tag_id')
    	->whereNull('user_tag_post_tag.user_id');
    }
    public function tagUsers(){
    	return $this->hasMany('App\UserTagPostTag', 'tag_id')
    	->whereNull('user_tag_post_tag.asset_id');
    }

/*    public function assets(){
    	return $this->hasMany('App\UserTagPostTag', 'tag_id')
    	->whereNull('user_tag_post_tag.user_id')
    	->leftJoin('assets', 'user_tag_post_tag.asset_id', '=', 'assets.id');
    }
    public function users(){
    	return $this->hasMany('App\UserTagPostTag', 'tag_id')
    	->whereNull('user_tag_post_tag.asset_id')
    	->leftJoin('users', 'user_tag_post_tag.user_id', '=', 'users.id');
    }*/
    /**
     * Get all of the users that are assigned this tag.
     */
    public function users()
    {
        return $this->morphedByMany('App\User', 'taggable');
    }

    /**
     * Get all of the videos that are assigned this tag.
     */
    public function videos()
    {
        return $this->morphedByMany('App\Asset', 'taggable');
    }

}
