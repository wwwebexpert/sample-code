<?php

namespace App;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class AssetComment extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'asset_comment';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function asset(){
        return $this->belongsTo('App\Asset');
    }
    public function commentator(){
        return $this->belongsTo('App\User', 'user_id');
    }
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->diffForHumans();
    }
}
