<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;
use Auth;
class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
    /**
     * The channels the user receives notification broadcasts on.
     *
     * @return string
     */
    /*public function receivesBroadcastNotificationsOn()
    {
        return 'users.'.$this->id;
    }*/
    public function scopeAdminUsers($query)
    {
        return $query->where('id', '!=', Auth::id())
                    ->orderBy('created_at', 'DESC');
    }

    /**
     * Get the user profile associated with the user.
     */
    public function profile()
    {
        return $this->hasOne('App\UserProfile');
    }
    /**
     * Get the posts associated with the user.
     */
    public function posts()
    {
        return $this->hasMany('App\Post');
    }
    public function assets()
    {
        return $this->hasMany('App\Asset')
        ->where('for', 'profile_pic')
        ->orderBy('created_at', 'DESC');
    } 
    public function videos()
    {
        return $this->hasMany('App\Asset')
            ->where('for', 'user_project')
            ->orderBy('created_at', 'DESC');
    }
    public function projectView(){
        return $this->videos->sum('views');
    }
    public function followers()
    {
        return $this->hasMany('App\Follower', 'user_id');
    }
    public function following()
    {
        return $this->hasMany('App\Follower', 'follower_user_id');
    }
    public function sendRequests() {
        return $this->hasMany('App\Friend', 'first_user_id');
    }

    public function receiveRequests() {
        return $this->hasMany('App\Friend', 'second_user_id');
    }

    public function friends() {
        // return $this->sendRequests->merge($this->receiveRequests);
       // return $this->sendRequests->where('status', '1')->merge($this->receiveRequests->where('status', '1'));
        $friends = DB::table("friendship")
        ->select(DB::raw('(CASE WHEN first_user_id = ' . Auth::id() . ' THEN second_user_id ELSE first_user_id  END)
         AS friend_id,
            (SELECT name from users where id = friend_id) as name'))->where(["status"=>'1',"first_user_id"=> Auth::id()])->orwhere(["status"=>'1',"second_user_id"=> Auth::id()])->get();
        return $friends;


    }

    public function isFollowedByMe($followerId) {
        return (boolean) $this->followers()->where('follower_user_id', $followerId)->count();
    }

    public function friendshipStatus($friendId) {
        $userConnection = $this->sendRequests->where('second_user_id', $friendId)->merge($this->receiveRequests->where('first_user_id', $friendId))->first();
        // echo count($userConnection);die;
        if(count($userConnection) == "1"){
            // echo "here";die;
        return $userConnection;
        }else{
            return false;
        }
    }

    public function tags()
    {
        return $this->morphToMany('App\Tag', 'taggable');
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','email_token','verified',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

}
