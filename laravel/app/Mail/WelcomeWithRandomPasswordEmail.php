<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WelcomeWithRandomPasswordEmail extends Mailable
{
    use Queueable, SerializesModels;
    protected $user;
    protected $randPass;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $randPass, $serviceProvider)
    {
        $this->user = $user;
        $this->randPass = $randPass;
        $this->serviceProvider = $serviceProvider;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.welcomeWithRandomPassword')->subject('Welcome Email')->with([
        'randPass' => $this->randPass,
        'serviceProvider' => $this->serviceProvider,
        ]);
    }
}
