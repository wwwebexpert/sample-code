<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user() &&  Auth::user()->user_type == 1) {
            return $next($request);
        }
        if (Auth::user()){
            return redirect('/');
        }
        return redirect('/admin/login');
       /* if(!Auth::check()){
            return redirect('/admin/login');
            echo "her";die;
        }else{
            if ($request->user_type == 0) {
                return redirect('/');
            }
        }*/
        // echo $request->user_type;die;
        if ($request->user_type == 0) {
            return redirect('/admin');
        }
        return $next($request);
    }
}
