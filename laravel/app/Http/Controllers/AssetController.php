<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Collection;
use Auth;
use Response;
use Mail;
use DB;
use App\Http\Controllers\TagController;
use Storage;
use App\User;
use App\Asset;
use App\AssetLike;
use App\AssetComment;
use App\UserTagPostTag;
use App\Events\UserFollowOther;
use App\Notifications\AssetsLike;
use App\Notifications\AssetsComment;
use App\Notifications\UserConnection;

class AssetController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except'=>['incrView']]);
    }

    /**
     * Show all assets
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        event(new UserFollowOther($user));
        $notify = Auth::user()->notify(new UserConnection(Auth::user()));
        return "event fired";
        // echo $title = $postsData->title;die;
        // print_r($notify);die;
        // $postsData = Asset::orderBy('created_at', 'desc')->get();
        // return view('posts.index', ['postsData' => $postsData]);
    }
    public function allnotify(){
        foreach (Auth::user()->notifications as $notification) {
            echo $notification->type;
        }
    }
    public function singleUserTimeline($username)
    {        
        if(Auth::check() && Auth::user()->name == $username){
            $userData = User::where(['name' => $username, 'verified' => '1'])->first();
            $followingData = Auth::user()->following;
            // print_r($userData);
            $timelineData = new Collection();
            foreach ($followingData as $key => $value) {
                // print_r(User::find($value->user_id)->videos);die;
                $timelineData = $timelineData->merge(User::find($value->user_id)->videos);
            }
            // print_r($userData);
            // print_r($timelineData);die;
        return view('users.timeline', ['timelineData' => $timelineData, 'userData' => $userData]);
        }else{
            return redirect('/');
        }
    }

    public function incrView(Request $request){
        $data = $request->all();
        $assets_id = Crypt::decryptString($data['asset_id']);
        $asset = Asset::whereId($assets_id)->first();
        if(Auth::check() && Auth::id() == $asset->user_id){
            return response()->json(['success' => true]);
        }
        if($asset->increment('views')){
            return response()->json(['success' => true]);
        }else{
            return response()->json(['success' => false]);
        }
        
    }
    public function getUserFollowing()
    {
        // $usersData = Auth::User()->with('followers')->get();
        $usersData = Auth::User()->following()->with('detailsOfFollowingUser')->get();
        // dd($usersData);die;
        // print_r($usersData);die;
        return view('users.followings', ['title' => 'Following', 'usersData' => $usersData]);
    }
    public function showAsset($username, $id){
        $userData = User::where(['name' => $username, 'verified' => '1'])->first();
        if($userData){
            if($userData->friendshipStatus(Auth::id()) && $userData->friendshipStatus(Auth::id())->status == "3" && $userData->friendshipStatus(Auth::id())->action_user_id != Auth::id()){
                return redirect('/');
            }else{
                $assetData = Asset::findOrFail($id);
                // print_r($assetData);die;
                // return "here";
                return view('assets.single', ['assetData' => $assetData]);
            }
           
        }else{
            // abort(404);
            return redirect('/');
        }
        
    } 

    /**
     * Show the form for add assets
     *
     * @return void
     */
    public function addAssetsForm()
    {
        // (new TagController)->method();
        return view('assets.create');
    }

    public function likeUnlike($id, Request $request )
    {
        $data = $request->all();
        // print_r($data);die;
        if($data['action'] == 'like'){
            $asset_like = AssetLike::firstOrCreate([
                'user_id' => $data['user_id'],
                'asset_id' => $id
                ], [
                'user_id' => $data['user_id'],
                'asset_id' => $id
            ]);
           /* print_r($asset_like->asset);
            print_r($asset_like->asset->user_id);
            echo Auth::id();
            print_r($asset_like->asset->type);die;*/
            // $notify = $asset_like->user->notify(new UserConnection(Auth::user()));
           /* echo $asset_like->user->id;
            echo Auth::id();die;*/
            if($asset_like->asset->user_id != Auth::id()){
                $notify = $asset_like->asset->owner->notify(new AssetsLike($asset_like->asset, $asset_like->user, $asset_like->asset->owner->name));
            }

            if($asset_like){
                $likes = Asset::find($id)->likes;
                $likes = count($likes);
                return response()->json(['success' => true, 'action' => 'unlike', 'count' => $likes , 'color' => 'red']);
            }else{
                return response()->json(['success' => false]);
            }
        }else{
            $asset_unlike = AssetLike::where([
                'user_id' => $data['user_id'],
                'asset_id' => $id
                ])->delete();
            if($asset_unlike){
                $likes = Asset::find($id)->likes;
                $likes = count($likes);
                return response()->json(['success' => true, 'action' => 'like', 'count' => $likes , 'color' => '']);
            }else{
                return response()->json(['success' => false]);
            }            
        }
        return response()->json(['success' => false]);        
    }

    public function getLikes($id, Request $request){
        $userLikeCollection = [];
        $likes = Asset::findOrFail($id)->likes;
        foreach ($likes as $key => $value) {
            $userLikeCollection[$key]['name'] = $value->user->name;

            $userLikeCollection[$key]['profile_image_url'] = $value->user->profile->profile_img ? asset('storage/avatars/'.$value->user->profile->profile_img) : asset('uploads/Dummy_User.png');
            $userLikeCollection[$key]['user_url'] = url('/').'/'.$value->user->name;
        }
        return response()->json(['success' => true, 'usersInfo' => $userLikeCollection]);
    }
    public function getComments(Request $request){
        $data = $request->all();

        $assets_id = Crypt::decryptString($data['asset_id']);
        $commentatorCollection = [];
        $comments = Asset::findOrFail($assets_id)->comments;
        // print_r($comments);die;
        foreach ($comments as $key => $value) {
            $commentatorCollection[$key]['name'] = $value->commentator->name;
            $commentatorCollection[$key]['comment'] = $value->comment;
            $commentatorCollection[$key]['comment_at'] = $value->created_at;
            $commentatorCollection[$key]['profile_image_url'] = $value->commentator->profile->profile_img ? asset('storage/avatars/'.$value->commentator->profile->profile_img) : asset('uploads/Dummy_User.png');
            $commentatorCollection[$key]['user_url'] = url('/').'/'.$value->commentator->name;
        }
        // print_r($commentatorCollection);die;
        return response()->json(['success' => true, 'commentatorsInfo' => $commentatorCollection]);
    }

    public function addAssetComment(Request $request){
        $data = $request->all();
        // print_r($data);die;
        $commentAdded = AssetComment::create([
            'user_id' => Auth::id(),
            'asset_id' => Crypt::decryptString($data['asset_id']),
            'comment' => $data['comment_value']
            ]);
        if($commentAdded->asset->user_id != Auth::id()){
                $notify = $commentAdded->asset->owner->notify(new AssetsComment($commentAdded->asset, $commentAdded->commentator, $commentAdded->asset->owner->name));
            }
        if($commentAdded){
            $name = $commentAdded->commentator->name;
            $comment_at = $commentAdded->created_at;
            $comment = $commentAdded->comment;
            $profile_image_url = $commentAdded->commentator->profile->profile_img ? asset('storage/avatars/'.$commentAdded->commentator->profile->profile_img) : asset('uploads/Dummy_User.png');
            $user_url = url('/').'/'.$commentAdded->commentator->name;
            return response()->json(['success' => true, 'name' => $name, 'comment_at' => $comment_at, 'comment' => $comment, 'profile_image_url' => $profile_image_url, 'user_url' => $user_url]);
        }else{
            return response()->json(['success' => false]);
        }
        // print_r($commentAdded);die;    
    }
    public function unlink(Request $request)
    {
    // $path = $this->request->input('file');
    $path = $request->input('file');
    // $data = $request->all();

    if(Storage::has($path)){
        Storage::delete($path);
        echo 'true';
    }die;
    // echo !Storage::disk('public')->exists('file.jpg');
    
        /*$filePath = Storage::put('public/assets', $request->file('files'), 'public');
        $files = array(array('name' => $filePath));
        $data = array('isSuccess' => 'true','files' => $files);
        echo json_encode($data);die;*/

    }
    public function getTags(){
        return Response::json(array("ActionScript","AppleScript","Asp","BASIC","C","C++","Clojure","COBOL","ColdFusion","Erlang","Fortran","Groovy","Haskell","Java","JavaScript","Lisp","Perl","PHP","Python","Ruby","Scala","Scheme"), 200);
        // return Response::json(array([ "value"=> 2 , "text"=> "Test"]), 200);


    }
    
    /**
     * Add assets of a User.
     *
     * @param  Request  $request
     * @return Response
     */
    public function add(Request $request)
    {

        // print_r($request);
        /*print_r($request->file());
        print_r($request->input());
        die;*/
        $messages = [
            'title.required' => 'Please enter video title.',
            'title.min' => 'Minimum :min characters required for video title.',
            'title.max' => 'Only :max characters allow for video title.',           
            'about_video.required' => 'Please enter video description.',
            'about_video.min' => 'Minimum 8 characters required.',
            'about_video.max' => 'Only :max characters allow for video description.',
        ];
        $rules = [
            'title' => 'required|min:3|max:255',
            'about_video' => 'required|min:8|max:350'
        ];
        $data = $request->all();
        
        if(!empty($data['files'])){
            $rules = array_merge($rules, ['files' => 'required|mimes:mp4|max:25600']);
            $messages = array_merge($messages, ['files.required' => 'Please select video.',
            'files.mimes' => 'Only video/mp4 files are allowed to be uploaded.',
            'files.max' => 'Files that you choosed are too large! Please upload files up to 25 MB.']);
        }else{
            $rules = array_merge($rules, ['fileUrl' => 'required']);
            $messages = array_merge($messages, ['fileUrl.required' => 'Please add video url.']);
        }
        
        $v = Validator::make($request->all(), $rules, $messages);
        if ($v->fails())
        {
            return Response::json(array(
                'success' => false,
                'errors' => $v->getMessageBag()->toArray()

            ), 400);
            //return redirect()->back()->withErrors($v->errors());
        }
        
        // $filePath = Storage::putFile('public/assets', $request->file('files'), 'public');
        if(!empty($data['files'])){
            $filePath = $request->file('files')->store(
                'assets', 'public'
            );
            $upload_type = '0';
        }else{
            $filePath = $data['fileUrl'];
            $upload_type = '1';
        }
        $newAssets = Asset::Create([
            'user_id' => Auth::id(),
            'for' => 'user_project',
            'title' => $data['title'],
            'category' => $data['category'],
            'tags' => $data['tags'],
            'about' => $data['about_video'],
            'upload_type' => $upload_type,
            'assets_visibility' => $data['visibility'],
            'type' => 'video',
            'path' => $filePath
            ]);
        // echo (new TagController)->exist('again');
        $tags = explode(",",trim($data['tags']));
        foreach ($tags as $key => $value) {
            if(!empty($value)){
                DB::table('taggables')->insert(
                    [
                        'tag_id' => (new TagController)->exist($value),
                        'taggable_id' => $newAssets->id,
                        'taggable_type' => 'App\Asset'
                    ]
                );
            }
        }

           /* $comment = new App\Comment(['body' => $request->body]);

            $post = App\Post::find($post->id);

            $comment = $post->comments()->save($comment);*/

        /*$postTagAdded = UserTagPostTag::Create([
                'tag_id' => (new TagController)->exist($value),
                'asset_id' => $newAssets->id
        ]);*/
        
        return Response::json(array('success' => true), 200);
    }
    public function upload(Request $request)
    {
        /*print_r($request->file());die;
        print_r($request->input());die;*/
        $filePath = Storage::put('public/assets', $request->file('files'), 'public');
        $files = array(array('name' => $filePath));
        $data = array('isSuccess' => 'true','files' => $files);
        echo json_encode($data);die;

        // echo $path = $request->file('files')->store('public/assets');die;
        echo Storage::disk('avatars')->exists($path);
        echo $visibility = Storage::disk('avatars')->getVisibility($path).'</br>';

       /* $path = $request->file('avatar')->storeAs(
        'public/', $request->user()->id
        );*/
        echo Storage::put('public/assets', $request->file('files'), 'private').'</br>';
        // $path = $request->file('avatar')->store('avatars/'.$request->user()->name);
        // $path = $request->file('avatar')->store($request->user()->name);
        // $path = Storage::disk('local')->put($request->user()->name, $request->file('avatar'));
        // $path = Storage::disk('avatars')->put($request->user()->name, $request->file('avatar'), 'private');
        echo asset('storage/avatars/'.$path);die;
    }  

    /**
     * Edit the specified post.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function editPostForm($id)
    {    
        $postData = Post::find($id);
        if($postData){
            if($postData->author->id != Auth::user()->id){
                return redirect('posts');
            }
        return view('posts.edit', ['postData' => $postData]);
        }else{
            return redirect('posts');
        }
    } 

    /**
     * Show the specified post.
     *
     * @param  string  $id
     * @return Response
     */
    public function singlePost($id)
    {
        $postData = Post::find($id);
        if($postData){           
        return view('posts.single', ['postData' => $postData]);
        }else{
            return redirect('posts');
        }
    }    



    /**
     * Update the specified post.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $data = $request->all();
        if(Post::where('id', $id)
            ->update([
            'title' => $data['title'],
            'content' => $data['content']
            ])){
            return redirect('posts');
        }else{
            return back()->withInput();
        }
              
    }
    /**
     * Update the specified post.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function remove($id, Request $request)
    {
        echo $id;die;
        if(Post::where('id', $id)
            ->delete()){
            return redirect('posts');
        }else{
            return back()->withInput();
        }
              
    }

    /**
     * Ajax request to send email.
     *
     * @param  Request  $request
     * @return Response
     */
    public function contactPerson(Request $request)
    {
        $data = $request->all();
        $postData = Post::find($data['contactPostId']);
        $postAuthor = Post::find($data['contactPostId'])->author;
        $email_var = $postData;
        $email_var['message'] = $data['message'];
        // print_r($email_var);die;
        if($postAuthor->id != Auth::user()->id){
            $mailSent = Mail::send('emails.contactPerson', ['postData' => $email_var], function ($m) use ($postData) {
                $m->from(Auth::user()->email, Auth::user()->name);

                $m->to($postData->author->email, $postData->author->name)
                ->subject('New contact for post Title - '.$postData->title);
            });
        }
        if(count(Mail::failures()) == 0){
        echo '1';     
        }else{
            echo Mail::failures();
        }die;
    }
}
