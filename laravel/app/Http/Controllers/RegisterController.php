<?php

namespace App\Http\Controllers;

use App\User;
use App\UserProfile;
use Illuminate\Support\Facades\Crypt;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use App\Jobs\SendVerificationEmail;
use Mail;
use Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        Validator::extend('not_disallow_username', function($attribute, $value, $parameters)
        {
            // Banned words
            $disallow_username = config('auth.disallow_username');
            if(in_array($value, $disallow_username)){
                return false;
            }
        return true;
        });
        $message = array(
            'name.unique' => 'The username has already been taken.',
            'name.not_disallow_username' => 'The username has already been taken.',
            'name.regex' => 'only alphanumeric and dot symbol allowed.'
        );
        return Validator::make($data, [
            'name' => 'required|min:6|regex:/^[a-zA-Z0-9.]+$/|not_disallow_username|string|unique:users|max:255',
            // 'name' => 'required|min:6|Regex:/\A(?!.*[:;]-\))[ -~]+\z/|not_disallow_username|string|unique:users|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ], $message);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $username = $data['name'];
        if ($username == trim($username) && strpos($username, ' ') !== false) {
            $username = trim($username);
            $username = preg_replace('!\s+!', ' ', $username);
            $username = trim(preg_replace("/[^A-Za-z0-9 ]/", "", preg_replace('!\s+!', ' ', $username)));
            $username = preg_replace('/\s+/', '.', $username).".".time();
        }

        $user = User::create([
            'name' => $username,
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'email_token' => Crypt::encryptString($data['email'])
        ]);
        // $result = File::makeDirectory('/uploads/'.$user->id.'/directory', 0775, true);
        UserProfile::create([
            'user_id' => $user->id
        ]);
        return $user;
    }
    /**
    * Handle a registration request for the application.
    *
    * @param \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        $user = $this->create($request->all());
        event(new Registered($user));
        dispatch(new SendVerificationEmail($user));
        return view('auth.verification');
    }
    /**
    * Handle a registration request for the application.
    *
    * @param $token
    * @return \Illuminate\Http\Response
    */
    public function verify($token)
    {
        $user = User::where('email_token',$token)->first();
        if($user && !Auth::check()){
            $user->verified = '1';
            $user->email_token = null;
            if($user->save()){
                return view('auth.emailconfirm',['user'=>$user]);
            } 
        }else{
            return redirect('/');
        }
    }
}
