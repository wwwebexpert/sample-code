<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Session;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    // use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function loginForm(){
        return view('admin.login');
    }
    
    public function authenticate(Request $request)
    {
        // $username = $request->input('username');
        $username = $request->username;
        $password = $request->password;
        $user = User::where('name', '=', $username)
                    ->orWhere('email', '=', $username)->Where('user_type','1')->first();
        // print_r($user);die;
        // if(!$user) return Redirect::back()->withInput()->withFlashMessage('Unknown username.');
        if(!$user) return redirect()->back()->withErrors(['userNotExists' => 'User Doesn\'t exists']);
        if(!$user->verified) return redirect()->back()->withErrors(['userNotVerified' => 'Please verify your account']);
        if(!$user->user_type) return redirect()->back()->withErrors(['userNotAdmin' => 'You are not an Admin.']);

        if(filter_var($username, FILTER_VALIDATE_EMAIL)) {
            Auth::attempt(['email' => $username, 'password' => $password, 'verified' => '1']);
        } else {
            Auth::attempt(['name' => $username, 'password' => $password, 'verified' => '1']);
        }

        if ( Auth::check() ) {
            Session::flash('loginMsg', 'You Are Successfully LogIn!');
            
            return redirect()->route('AdminDashboard');
            // ->intended('/');
            // ->route('AdminDashboard', ['name' => Auth::user()->name]);
        }

        return redirect()->back()->withErrors([
            'password' => 'Please, Check Your Credentials',
            'username' => 'Please, Check Your Credentials'
        ]);
    }

}
