<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Collection;
use Auth;
use Mail;
use Hash;
use Session;
use Validator;
use DB;
use Storage;
use App\Asset;
use App\User;
use App\UserProfile;
use App\Follower;
use App\Friend;
use App\Post;
use App\SocialAccount;
use App\Notifications\UserConnection;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin', ['except' => ['singleUser', 'isUserNameInUse', 'notify']]);
    }

    /**
     * Show all posts
     *
     * @return \Illuminate\Http\Response
     */

    public function delete(Request $request){
        $data = $request->all();
        $id = Crypt::decryptString($data['user_id']);
        $user = User::find($id);
        if($user){
            $deleteUser = $user->delete();
            if($deleteUser){
                return response()->json(['success' => true]);
            }else{
                return response()->json(['success' => false]);
            }
        }else{
            return response()->json(['success' => false]);
        }    
    }
    public function allnotify(){
        foreach (Auth::user()->notifications as $notification) {
            echo $notification->type;
        }
    }
    public function index()
    {
        $usersData = User::all()->except(Auth::id())->where('verified', '1');
        print_r(count($usersData));die;
        return view('users.index', ['title' => 'All Users', 'usersData' => $usersData]);
    }
    public function getUserFriends()
    {
        $usersData = Auth::User()->friends();
        // print_r($usersData);die;
        return view('users.index', ['title' => 'Friends', 'usersData' => $usersData]);
    }
    public function getUserFollowers()
    {
        // $usersData = Auth::User()->with('followers')->get();
        $usersData = Auth::User()->followers()->with('details')->get();
        // dd($usersData);die;
        // print_r($usersData);die;
        return view('users.followers', ['title' => 'Followers', 'usersData' => $usersData]);
    }
    public function getUserFollowing()
    {
        // $usersData = Auth::User()->with('followers')->get();
        $usersData = Auth::User()->following()->with('detailsOfFollowingUser')->get();
        // dd($usersData);die;
        // print_r($usersData);die;
        return view('users.followings', ['title' => 'Following', 'usersData' => $usersData]);
    }
    public function isUserNameInUse( Request $request )
    {
        $data = $request->all();
        $username = trim($data['name']);
        $disallow_username = config('auth.disallow_username');
        if(in_array($username, $disallow_username)){
            return 'false';
        }
        if (User::where('name','=', $username)->exists()){
            return 'false';
        }

            return 'true';
    }

    public function followUnfollowUser( Request $request )
    {
        $data = $request->all();
        $checkConnectionExists = Friend::where([
            'first_user_id' => Auth::id(),
            'second_user_id' => Crypt::decryptString($data['user_id']),
            // 'action_user_id' => Auth::id()
        ])->orwhere([
            'first_user_id' => Crypt::decryptString($data['user_id']),
            'second_user_id' => Auth::id(),
            // 'action_user_id' => Crypt::decryptString($data['user_id'])
        ])->exists();
        // $frdStatus = Auth::user()->friendshipStatus(Crypt::decryptString($data['user_id']))->status;
        $frdStatus = User::find(Crypt::decryptString($data['user_id']))->friendshipStatus(Auth::id());
        if(($checkConnectionExists && $frdStatus->status != "3") || (!$checkConnectionExists)){

        if($data['action'] == 'followUser'){
            if(Follower::Create([
                'user_id' => Crypt::decryptString($data['user_id']),
                'follower_user_id' => Auth::id()
            ])){
            $reverseFollowingExists = Follower::where([
                'user_id' => Auth::id(),
                'follower_user_id' => Crypt::decryptString($data['user_id'])
            ])->exists();

            $user = User::find(Crypt::decryptString($data['user_id']));
            $notify = $user->notify(new UserConnection(Auth::user()));
        // print_r($notify);die;

                return response()->json(['success' => true, 'id' => 'unfollowUser', 'btnText' => 'Unfollow' , 'bgcolor' => 'red', 'showMsg' => $reverseFollowingExists]);
            }else{
                return response()->json(['success' => false, 'id' => 'followUser', 'btnText' => 'Follow', 'bgcolor' => 'green']);
            }
        }else{
           if(Follower::where([
                'user_id' => Crypt::decryptString($data['user_id']),
                'follower_user_id' => Auth::id()
            ])->delete()){
                return response()->json(['success' => true, 'id' => 'followUser', 'btnText' => 'Follow','bgcolor' => 'green']);
            }else{
                return response()->json(['success' => false, 'id' => 'unfollowUser', 'btnText' => 'Unfollow', 'bgcolor' => 'red']);
            } 
        }
        }else{
            return response()->json(['success' => false]);
        }
        
    }

    public function changeFriendStatus( Request $request )
    {
        $data = $request->all();
        // print_r($data);die;
        $checkConnectionExists = Friend::where([
                    'first_user_id' => Auth::id(),
                    'second_user_id' => Crypt::decryptString($data['user_id']),
                    // 'action_user_id' => Auth::id()
                ])->orwhere([
                    'first_user_id' => Crypt::decryptString($data['user_id']),
                    'second_user_id' => Auth::id(),
                    // 'action_user_id' => Crypt::decryptString($data['user_id'])
                ])->exists();
        $frdStatus = User::find(Crypt::decryptString($data['user_id']))->friendshipStatus(Auth::id());
        switch ($data['action']) {
            case 'addUser':
            if(!$checkConnectionExists){
                Friend::Create([
                'first_user_id' => Auth::id(),
                'second_user_id' => Crypt::decryptString($data['user_id']),
                'action_user_id' => Auth::id()
            ]);
                return response()->json(['success' => true, 'id' => 'cancelRequest', 'btnText' => 'Cancel Request' , 'bgcolor' => 'red']);
            }else{
                return response()->json(['success' => false, 'id' => 'addUser', 'btnText' => 'Add Friend', 'bgcolor' => 'green']);
            }
                break;

            case 'removeFriend':
                if(Friend::where([
                    'first_user_id' => Auth::id(),
                    'second_user_id' => Crypt::decryptString($data['user_id']),
                    // 'action_user_id' => Auth::id()
                ])->orwhere([
                    'first_user_id' => Crypt::decryptString($data['user_id']),
                    'second_user_id' => Auth::id(),
                    // 'action_user_id' => Crypt::decryptString($data['user_id'])
                ])->delete()){
                    return response()->json(['success' => true, 'id' => 'addUser', 'btnText' => 'Add Friend','bgcolor' => 'green']);
                }else{
                    return response()->json(['success' => false, 'id' => 'removeFriend', 'btnText' => 'Unfriend', 'bgcolor' => 'red']);
                }
                break;

            case 'unblockUser':
                if(Friend::where([
                    'first_user_id' => Auth::id(),
                    'second_user_id' => Crypt::decryptString($data['user_id']),
                    // 'action_user_id' => Auth::id(),
                    'status' => '3'
                ])->orwhere([
                    'first_user_id' => Crypt::decryptString($data['user_id']),
                    'second_user_id' => Auth::id(),
                    // 'action_user_id' => Crypt::decryptString($data['user_id']),
                    'status' => '3'
                ])->delete()){
                    return response()->json(['success' => true, 'id' => 'blockUser', 'btnText' => 'Block','bgcolor' => 'red']);
                }else{
                    return response()->json(['success' => false, 'id' => 'unblockUser', 'btnText' => 'Unblock', 'bgcolor' => 'green']);
                }
                break;

            case 'cancelRequest':
                if(Friend::where([
                    'first_user_id' => Auth::id(),
                    'second_user_id' => Crypt::decryptString($data['user_id']),
                    // 'action_user_id' => Auth::id(),
                    'status' => '0'
                ])->delete()){
                    return response()->json(['success' => true, 'id' => 'addUser', 'btnText' => 'Add Friend','bgcolor' => 'green']);
                }else{
                    return response()->json(['success' => false, 'id' => 'cancelRequest', 'btnText' => 'Cancel Request', 'bgcolor' => 'red']);
                }
                break;
            case 'acceptRequest':
                if(Friend::where([
                    'first_user_id' => Crypt::decryptString($data['user_id']),
                    'second_user_id' => Auth::id(),
                    // 'action_user_id' => Crypt::decryptString($data['user_id']),
                    'status' => '0'
                ])->update(['status' => '1', 'action_user_id'=>Auth::id()])){
                    return response()->json(['success' => true, 'id' => 'removeFriend', 'btnText' => 'Unfriend', 'bgcolor' => 'red']);
                }else{
                    return response()->json(['success' => false, 'id' => 'declineRequest', 'btnText' => 'Decline Request', 'bgcolor' => 'red']);
                }
                break;
            case 'blockUser':
            $unfollowBoth = Follower::where([
                    'user_id' => Auth::id(),
                    'follower_user_id' => Crypt::decryptString($data['user_id'])
                    ])->orwhere([
                    'user_id' => Crypt::decryptString($data['user_id']),
                    'follower_user_id' =>Auth::id()
                    ])->delete();

                if($checkConnectionExists){
                    if($frdStatus && $frdStatus->status != "3"){
                    $updateStatus = Friend::where([
                            'first_user_id' => Auth::id(),
                            'second_user_id' => Crypt::decryptString($data['user_id']),
                            // 'action_user_id' => Auth::id()
                        ])->orwhere([
                            'first_user_id' => Crypt::decryptString($data['user_id']),
                            'second_user_id' => Auth::id(),
                            // 'action_user_id' => Crypt::decryptString($data['user_id'])
                        ])->update(['status' => '3', 'action_user_id'=>Auth::id()]);
                    return response()->json(['success' => true, 'id' => 'unblockUser', 'btnText' => 'Unblock', 'bgcolor' => 'green']); 
                }else{
                    return response()->json(['success' => false, 'id' => 'blockUser', 'btnText' => 'Block', 'bgcolor' => 'red']);                }
            }else{
                    $createNew = Friend::Create([
                        'first_user_id' => Auth::id(),
                        'second_user_id' => Crypt::decryptString($data['user_id']),
                        'action_user_id' => Auth::id(),
                        'status' => '3'
                        ]);    
                    return response()->json(['success' => true, 'id' => 'unblockUser', 'btnText' => 'Unblock', 'bgcolor' => 'green']); 
                }                
                return response()->json(['success' => false, 'id' => 'blockUser', 'btnText' => 'Block', 'bgcolor' => 'red']);
                break;
            case 'declineRequest':
               if(Friend::where([
                    'first_user_id' => Crypt::decryptString($data['user_id']),
                    'second_user_id' => Auth::id(),
                    // 'action_user_id' => Crypt::decryptString($data['user_id']),
                    'status' => '0'
                ])->delete()){
                    return response()->json(['success' => true, 'id' => 'addUser', 'btnText' => 'Add Friend','bgcolor' => 'green']);
                }else{
                    return response()->json(['success' => false, 'id' => 'declineRequest', 'btnText' => 'Decline Request', 'bgcolor' => 'red']);
                }
                break;
            default:
                # code...
                break;
        }        
    } 

    /**
     * Show the form for add post
     *
     * @return void
     */
    public function addPostForm()
    {
        return view('posts.create');
    }  

    /**
     * Edit the specified post.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function editUserForm($username)
    {
        if($username == Auth::user()->name){
        $userData = Auth::user();
        $webLinks = array('facebook' => 'facebook_link_url', 'twitter' => 'twitter_link_url', 'dribbble' => 'dribbble_link_url', 'linkedin' => 'linkedin_link_url');
            // , 'vimeo' => 'vimeo_link_url', 'youtube' => 'youtube_link_url', 'instagram' => 'instagram_link_url'
        return view('users.edit', ['userData' => $userData, 'webLinks' => $webLinks]);
        }else{
            return redirect('/');
        }
    } 

    /**
     * Show the specified post.
     *
     * @param  string  $id
     * @return Response
     */
    public function singleUser($username)
    {
        $asset_id = Input::get('asset', false);
        $path = '';
        if($asset_id){
            $asset = Asset::find(Crypt::decryptString($asset_id));
            if(!$asset || $asset->user_id != Auth::id()){ return redirect('/'); }
            if(Auth::check() && Auth::id() == $asset->user_id){
                $path = asset('storage/'.$asset->path);
            }            
        }
        $userData = User::where(['name' => $username, 'verified' => '1'])->first();
        if($userData){
            if($userData->friendshipStatus(Auth::id()) && $userData->friendshipStatus(Auth::id())->status == "3" && $userData->friendshipStatus(Auth::id())->action_user_id != Auth::id()){
                return redirect('/');
            }else{

                return view('users.single', ['userData' => $userData, 'path' => $path]);
            }
           
        }else{
            // abort(404);
            return redirect('/');
        }
    }
    /**
     * Show the specified post.
     *
     * @param  string  $id
     * @return Response
     */
    public function authUserProfile()
    {
        $userData = Auth::user();
        if($userData){
        return view('users.single', ['userData' => $userData]);
        }else{
            return redirect('users');
        }
    }
    /**
     * Show the specified post.
     *
     * @param  string  $id
     * @return Response
     */
    public function singleUserTimeline($username)
    {
        if(Auth::check() && Auth::user()->name == $username){
            $userData = User::where(['name' => $username, 'verified' => '1'])->first();
            $followingData = Auth::user()->following;
            // print_r($userData);
            $timelineData = new Collection();
            foreach ($followingData as $key => $value) {
                $timelineData = $timelineData->merge(User::find($value->user_id)->videos);
            }
            // print_r($userData);
            // print_r($timelineData);die;
        return view('users.timeline', ['timelineData' => $timelineData, 'userData' => $userData]);
        }else{
            return redirect('/');
        }
    }

    /**
     * Show the specified post.
     *
     * @param  string  $id
     * @return Response
     */
    public function deactiveAccount()
    {
        $user_id = Auth::id();
        $obj_user = User::find($user_id);
        $obj_user->is_activated = '0';
        $obj_user->save();
        Session::flash('accDeactivatesuccess', 'Your account is deactive successfully');
        return redirect($obj_user->name.'/edit');
    }

    public function uploadProfileImage(Request $request)
    {
        
        $path = $request->file('avatar')->store(
            $request->user()->name, 'avatars'
        );
        echo Storage::disk('avatars')->exists($path);
        echo $visibility = Storage::disk('avatars')->getVisibility($path).'</br>';

       /* $path = $request->file('avatar')->storeAs(
        'public/', $request->user()->id
        );*/
        echo Storage::put('public/avatars', $request->file('avatar'), 'private').'</br>';
        // $path = $request->file('avatar')->store('avatars/'.$request->user()->name);
        // $path = $request->file('avatar')->store($request->user()->name);
        // $path = Storage::disk('local')->put($request->user()->name, $request->file('avatar'));
        // $path = Storage::disk('avatars')->put($request->user()->name, $request->file('avatar'), 'private');
        echo asset('storage/avatars/'.$path);die;
        // $url = Storage::url($path);
      /*  $url = Storage::disk('local')->put('sachin.jpeg', $request->file('avatar'));
        // Storage::disk('local')->put($request->user()->name, $request->file('avatar'));
        echo asset($url);
        print_r($url);echo "<br>";*/
        // print_r($path);die;
        return $path;
        $file = $request->file('image');
        $validator = Validator::make(
             ['image' => $file],
            ['image' => 'image|mimes:jpeg,jpg,png|dimensions:min_width=300,min_height=300']
        );
        if ($validator->fails())
        {
            return response()->json(['success' => false, 'errors' => $validator->messages() ]);
            // echo $messages = $validator->messages();
        }

        $file = $this->updateAvatar($file,Auth::user());
        
        return response()->json(['success' => true, 'file' => $file ]);
    }
    public function updateAvatar($request,$user)
    {
        // Move the uploaded file to public/uploads/ folder
        $path = $request->file('image')->store(
            $request->user()->name, 'avatars'
        );
       /* $destinationPath = 'uploads/';

        $filename = $user->name.'.'.$file->guessExtension();
        // $filename = $file->getClientOriginalName();
        // $filename = $file->guessExtension();
        
        $file->move($destinationPath, $filename);*/

        // save the file path on user avatar

        $userProfile = UserProfile::where('user_id',$user->id)->first();
        $userProfile->profile_img = $path;
        $userProfile->save();
        Asset::Create([
            'user_id' => Auth::id(),
            'for' => 'profile_pic',
            'path' => $path
            ]);

        return $path;
        // return asset($destinationPath.$filename);
    }

    /**
     * Show the specified post.
     *
     * @param  string  $id
     * @return Response
     */
    public function activeAccount()
    {
        $user_id = Auth::id();
        $obj_user = User::find($user_id);
        $obj_user->is_activated = '1';
        $obj_user->save();
        Session::flash('accAactivatesuccess', 'Your account is active successfully');
        return redirect($obj_user->name.'/edit');
    }
    public function changePassword(Request $request)
    {
        $user = Auth::user();

        $curPassword = $request->current_password;
        $newPassword = $request->password;

        if (Hash::check($curPassword, $user->password)) {
            $user_id = $user->id;
            $obj_user = User::find($user_id);
            // print_r($obj_user);die;
            $obj_user->password = Hash::make($newPassword);
            $obj_user->save();

            Session::flash('passChangeSuccess', 'Password Changed Successfully. Please Login Again');
            Auth::logout();
            return redirect('/');
        }
        else
        {
            Session::flash('passChangeError', 'Please provide Correct Current Password');
            return redirect($user->name.'/edit');
        }
    }    

    /**
     * Add a post.
     *
     * @param  Request  $request
     * @return Response
     */
    public function updateBasicInfo(Request $request)
    {
        $data = $request->all();
        // print_r($data);die
        $file = @$data['image'];
        if($file){
            $this->updateAvatar($request,Auth::user());
        }
        $obj_user_profile = UserProfile::where('user_id',Auth::id())->first();
        $obj_user_profile->first_name = @$data['first_name'];
        $obj_user_profile->last_name = @$data['last_name'];
        $obj_user_profile->interest = @$data['interest'];
        $obj_user_profile->dob = @$data['dob'];
        $obj_user_profile->profile_visibility = empty($data['profile_visibility']) ? 0 : $data['profile_visibility'];
        $obj_user_profile->about_user = @$data['about_user'];
        $obj_user_profile->company_name = @$data['company_name'];
        $obj_user_profile->city = @$data['city'];
        $obj_user_profile->timezone = @$data['timezone'];
        $obj_user_profile->website_url = @$data['website_url'];
        $obj_user_profile->save();
        Session::flash('updateInfoSuccess', 'User Information Is Updated Successfully');
        return redirect(Auth::user()->name.'/edit');
    }
    /**
     * Add a post.
     *
     * @param  Request  $request
     * @return Response
     */
    public function updateWebLinks(Request $request)
    {
        $data = $request->all();
        // print_r($data);die;
        $service_provider_link = $data['service_provider'];
        $arr = explode("_", $service_provider_link, 2);
        $service_provider = ucfirst($arr[0]);
        $obj_user_profile = UserProfile::where('user_id',Auth::id())->first();
        $obj_user_profile->$service_provider_link = $data[$service_provider_link];
        /*$obj_user_profile->twitter_link_url = @$data['twitter_url'];
        $obj_user_profile->dribble_link_url = @$data['facebook_url'];
        $obj_user_profile->linkeedIn_link_url = @$data['facebook_url'];
        $obj_user_profile->vimeo_link_url = @$data['facebook_url'];
        $obj_user_profile->instagram_link_url = @$data['facebook_url'];
        $obj_user_profile->youtube_link_url = @$data['facebook_url'];*/
        $obj_user_profile->save();
        return response()->json(['success' => true, 'service_provider' => $service_provider, 'service_provider_link' => $service_provider_link, 'service_provider_link_val' => $data[$service_provider_link] ]);
        // return redirect('profile/edit');
    }

    /**
     * Callback for authorize user
     *
     * @param  Request  $request
     * @return Response
     */
    public function callback() {
    $http = new GuzzleHttp\Client;
    print_r($http);die;
    $response = $http->post('http://localhost:8000/oauth/token', [
        'form_params' => [
            'grant_type' => 'authorization_code',
            'client_id' => '1',
            'client_secret' => 'bcEGzlngPbmfbxXDoTmhsqBat3L85h7Y7iS0NcZv',
            'redirect_uri' => 'http://localhost:4000/callback',
            'code' => $request->code,
        ],
    ]);

    return json_decode((string) $response->getBody(), true);
    }

    public function createOrGetUser($providerUser)
    {
        print_r($providerUser);die;
        $account = SocialAccount::whereProvider('cems')
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {

            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'cems'
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {

                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'name' => $providerUser->getName(),
                ]);
            }

            $account->user()->associate($user);
            $account->save();

            return $user;

        }

    }

    /**
     * Update the specified post.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $data = $request->all();
        $user = User::findOrFail($id);
        $user->profile->first_name = $data['fname'];
        if($user->push()){
            return redirect('users');
        }else{
            return back()->withInput();
        }
              
    }

    /**
     * Ajax request to send email.
     *
     * @param  Request  $request
     * @return Response
     */
    public function contactPerson(Request $request)
    {
        $data = $request->all();
        $postData = Post::find($data['contactPostId']);
        $postAuthor = Post::find($data['contactPostId'])->author;
        $email_var = $postData;
        $email_var['message'] = $data['message'];
        // print_r($email_var);die;
        if($postAuthor->id != Auth::user()->id){
            $mailSent = Mail::send('emails.contactPerson', ['postData' => $email_var], function ($m) use ($postData) {
                $m->from(Auth::user()->email, Auth::user()->name);

                $m->to($postData->author->email, $postData->author->name)
                ->subject('New contact for post Title - '.$postData->title);
            });
        }
        if(count(Mail::failures()) == 0){
        echo '1';     
        }else{
            echo Mail::failures();
        }die;
    }
}
