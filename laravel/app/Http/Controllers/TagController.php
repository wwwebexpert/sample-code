<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use Response;
use Mail;
use Storage;
use DB;
use App\Tag;
use App\UserProfile;
use App\UserTagPostTag;

class TagController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth',['except' => ['index']]);
    }
    /**
     * Show all assets
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::orderBy('title', 'ASC')->pluck('title')->toArray();
        return Response::json($tags);
    }
    /**
     * Add a tag.
     *
     * @param  Request  $request
     * @return Response
     */
    public function exist($tag)
    {
    	return Tag::where('title', $tag)->value('id');
    }

    /**
     * Add a tag.
     *
     * @param  Request  $request
     * @return Response
     */
    public function add(Request $request)
    {
        $data = $request->all();
        $title = trim($data['title']);
        $existThenId = $this->exist($title);
        if(!empty($data['tag']) && $data['tag'] == 'video'){
	        if(!$existThenId){
	        $tagAdded = Tag::create([
	            'title' => $data['title'],
	        ]);
	        }	
	        return Response::json(array('success' => true), 200);
	    }
        // print_r($existThenId);die;
        if(!$existThenId){
	        $tagAdded = Tag::create([
	            'title' => $data['title'],
	        ]);
	        if(!empty($data['tag']) && $data['tag'] == 'video'){

	        }
	        $userTagAdded = DB::table('taggables')->insert(
                [
                    'tag_id' => $tagAdded->id,
                    'taggable_id' => Auth::id(),
                    'taggable_type' => 'App\User'
                ]
            );

	        /*$userTagAdded = UserTagPostTag::Create([
	        	'user_id' => Auth::id(),
	        	'tag_id' => $tagAdded->id
	        ]);*/
        }else{
        	$userTagAdded = DB::table('taggables')->insert(
                [
                    'tag_id' => $existThenId,
                    'taggable_id' => Auth::id(),
                    'taggable_type' => 'App\User'
                ]
            );
        	/*$userTagAdded = UserTagPostTag::Create([
	        	'user_id' => Auth::id(),
	        	'tag_id' => $existThenId
	        ]); */       	
        }
        if($userTagAdded){
        	$obj_user_profile = UserProfile::where('user_id',Auth::id())->first();
        	$obj_user_profile->interest = $data['interest'];
        	$obj_user_profile->save();
           	return Response::json(array('success' => true), 200);
        }else{
            return Response::json(array('failure' => true), 405);
        }
    }
    /**
     * Add a tag.
     *
     * @param  Request  $request
     * @return Response
     */
    public function remove(Request $request)
    {
        $data = $request->all();
        // print_r($data);die;
        $title = trim($data['title']);
        $existThenId = $this->exist($title);
        // print_r($existThenId);die;

        $userTagRemoved = DB::table('taggables')->where([
	        	'taggable_id' => Auth::id(),
	        	'tag_id' => $existThenId
	        ])->delete();
       	/*$userTagRemoved = UserTagPostTag::where([
	        	'user_id' => Auth::id(),
	        	'tag_id' => $existThenId
	        ])->delete();*/
        if($userTagRemoved){
        	$obj_user_profile = UserProfile::where('user_id',Auth::id())->first();
        	$obj_user_profile->interest = $data['interest'];
        	$obj_user_profile->save();
           	return Response::json(array('success' => true), 200);
        }else{
            return Response::json(array('failure' => true), 405);
        }
    } 

 }