<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Collection;
use App\Http\Controllers\TagController;
use Illuminate\Support\Facades\Crypt;

use Auth;
use Response;
use Mail;
use Storage;

use App\User;
use App\Tag;
use App\Asset;
use App\AssetLike;
use App\UserTagPostTag;

class SearchController extends Controller
{
    /**
     * Show all assets
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $messages = [
            'search.required' => 'Please enter search keyword.',
            'search.min' => 'Minimum :min characters required for searching.',
            'search.max' => 'Only :max characters allow for searching.'
        ];

        $v = Validator::make($request->all(), [
            'search' => 'required|min:3|max:255'
        ], $messages);
        if ($v->fails())
        {
            return redirect()->back()->withErrors($v->errors());
        }

        $input = Input::all();
        $term = $input['search'];
        $userResults = User::select('users.*')->leftJoin('user_profile', 'users.id', '=', 'user_profile.user_id')
            // ->where('users.id', '!=', (Auth::check() ? Auth::id() : '0'))
            ->where('user_profile.first_name', 'like', '%'.$term.'%')
            ->orwhere('user_profile.last_name', 'like', '%'.$term.'%')
            ->orWhere('name', 'like', '%'.$term.'%')
            ->orWhere('email', 'like', $term)->get();
        $tagResults = Tag::where('tags.title', 'like', '%'.$term.'%')->with(['users','videos'])->get();
        $finalUsers = [];
        $finalVideos = [];
        if($tagResults){
            foreach ($tagResults as $single) {
                if($userResults){
                $finalUsers = $single->users->merge($userResults);
                }else{
                    $finalUsers = $single->users;
                }
                $finalVideos = $single->videos;
            }            
        }else{
            $finalUsers = $userResults;
        }
        
        return view('search-results', ['users' =>$finalUsers, 'videos' => $finalVideos ]);

    }
    public function advance()
    {
        $input = Input::all();
        if(!@$input['title']){
            return response()->json(['success' => true, 'videos' => [], 'users' => []]);
        }
        // print_r($input);die;
        $terms = $input['title'];
        $user_collection = new Collection();
        $video_collection = new Collection();
        foreach ($terms as $key => $term) {
            $userResults = User::select('users.*')->leftJoin('user_profile', 'users.id', '=', 'user_profile.user_id')
                // ->where('users.id', '!=', (Auth::check() ? Auth::id() : '0'))
                ->where('user_profile.first_name', 'like', '%'.$term.'%')
                ->orwhere('user_profile.last_name', 'like', '%'.$term.'%')
                ->orWhere('name', 'like', '%'.$term.'%')
                ->orWhere('email', 'like', $term)->get();
            $tagResults = Tag::where('tags.title', 'like', '%'.$term.'%')->with(['users','videos'])->get();
            $finalUsers = [];
            $finalVideos = [];
            if($tagResults){
                foreach ($tagResults as $single) {
                    if($userResults){
                    $finalUsers = $single->users->merge($userResults);
                    }else{
                        $finalUsers = $single->users;
                    }
                    $finalVideos = $single->videos;
                }            
            }else{
                $finalUsers = $userResults;
            }
            $user_collection = $user_collection->merge($finalUsers);
            $video_collection = $video_collection->merge($finalVideos);
        }
        $finalUsers = $user_collection->unique();
        $finalVideos = $video_collection->unique();
        foreach ($finalUsers as $single => $user) {
            $finalUsers[$single]['profile_pic_url'] = $user->profile->profile_img ? asset('storage/avatars/'.$user->profile->profile_img) : asset('uploads/Dummy_User.png');
            $finalUsers[$single]['encryptId'] = Crypt::encryptString($user->id);
            if(Auth::check() && $user->id != Auth::id()){
                if($user->isFollowedByMe(Auth::id())){
                    $finalUsers[$single]['followedByMe'] = true;
                }else{
                    $finalUsers[$single]['followedByMe'] = false;
                }
            }else{
                $finalUsers[$single]['followedByMe'] = 'notShown';
            }            
        }
        foreach ($finalVideos as $single => $video) {
            $finalVideos[$single]['total_likes'] = count($video->likes);
            $finalVideos[$single]['total_comments'] = count($video->comments);
            $finalVideos[$single]['encryptId'] = Crypt::encryptString($video->id);

                if($video->upload_type && (strpos($video->path, 'youtube'))){
                    preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $video->path, $matches);
                    $finalVideos[$single]['thumbnail_url'] = 'https://img.youtube.com/vi/'.$matches[1].'/sddefault.jpg';
                }elseif ($video->upload_type && (strpos($video->path, 'vimeo'))) {
                    $finalVideos[$single]['thumbnail_url'] = json_decode(file_get_contents('https://vimeo.com/api/oembed.json?url='.$video->path))->thumbnail_url;
                    # code...
                }else{
                    $finalVideos[$single]['thumbnail_url'] = '';
                }          

            if(Auth::check()){
                if($video->isLikedByMe(Auth::id())){
                    $finalVideos[$single]['likedByMe'] = true;
                }else{
                    $finalVideos[$single]['likedByMe'] = false;
                }
            }else{
                $finalVideos[$single]['likedByMe'] = 'notShown';
            }
            if($video->assets_visibility != "0"){
                if(Auth::check()){
                    if(Auth::id() != $video->user_id){
                        if(User::find($video->user_id)->isFollowedByMe(Auth::id())){
                            $finalVideos[$single]['accessibility'] = 'privateVideo';
                            $privateVideo = true;
                        }else{
                            $finalVideos->forget($single);
                        }
                    }else{
                        $finalVideos[$single]['accessibility'] = 'privateOwnVideo';
                        $privateOwnVideo = true;
                    }
                }
                else{
                            $finalVideos->forget($single);
                        }
            }else{
                $finalVideos[$single]['accessibility'] = 'publicVideo';
                $publicVideo = true;
            }            
        }
        // print_r($finalUsers);die;

        return response()->json(['success' => true, 'videos' => $finalVideos, 'users' => $finalUsers]);
    }

    public function getSearchResult(){
        return view('search-results', ['users' =>[], 'videos' => [] ]);
    } 

    /**
     * Show the form for add assets
     *
     * @return void
     */
    public function addAssetsForm()
    {

        // (new TagController)->method();
        return view('assets.create');
    }
        public function notify(Request $request){
        print_r($data = $request->all());die;
        return Auth::user()->notify(new UserConnection(Auth::user()));
    }


    public function likeUnlike($id, Request $request )
    {
        $data = $request->all();
        // print_r($data);die;
        if($data['action'] == 'like'){
            $asset_like = AssetLike::firstOrCreate([
                'user_id' => $data['user_id'],
                'asset_id' => $id
                ], [
                'user_id' => $data['user_id'],
                'asset_id' => $id
            ]);

            if($asset_like){
                $likes = Asset::find($id)->likes;
                $likes = count($likes);
                return response()->json(['success' => true, 'action' => 'unlike', 'count' => $likes , 'color' => 'red']);
            }else{
                return response()->json(['success' => false]);
            }
        }else{
            $asset_unlike = AssetLike::where([
                'user_id' => $data['user_id'],
                'asset_id' => $id
                ])->delete();
            if($asset_unlike){
                $likes = Asset::find($id)->likes;
                $likes = count($likes);
                return response()->json(['success' => true, 'action' => 'like', 'count' => $likes , 'color' => '']);
            }else{
                return response()->json(['success' => false]);
            }            
        }
        return response()->json(['success' => false]);        
    }


    public function unlink(Request $request)
    {
    // $path = $this->request->input('file');
    $path = $request->input('file');
    // $data = $request->all();

    if(Storage::has($path)){
        Storage::delete($path);
        echo 'true';
    }die;
    // echo !Storage::disk('public')->exists('file.jpg');
    
        /*$filePath = Storage::put('public/assets', $request->file('files'), 'public');
        $files = array(array('name' => $filePath));
        $data = array('isSuccess' => 'true','files' => $files);
        echo json_encode($data);die;*/

    }
    public function getTags(){
        return Response::json(array("ActionScript","AppleScript","Asp","BASIC","C","C++","Clojure","COBOL","ColdFusion","Erlang","Fortran","Groovy","Haskell","Java","JavaScript","Lisp","Perl","PHP","Python","Ruby","Scala","Scheme"), 200);
        // return Response::json(array([ "value"=> 2 , "text"=> "Test"]), 200);


    }
    
    /**
     * Add assets of a User.
     *
     * @param  Request  $request
     * @return Response
     */
    public function add(Request $request)
    {

        // print_r($request);
        /*print_r($request->file());
        print_r($request->input());
        die;*/
        $messages = [
            'title.required' => 'Please enter video title.',
            'title.min' => 'Minimum :min characters required for video title.',
            'title.max' => 'Only :max characters allow for video title.',

            'files.required' => 'Please select video.',
            'files.mimes' => 'Only video/mp4 files are allowed to be uploaded.',
            'files.max' => 'Files that you choosed are too large! Please upload files up to 25 MB.',

            'about_video.required' => 'Please enter video description.',
            'about_video.min' => 'Minimum 8 characters required.',
            'about_video.max' => 'Only :max characters allow for video description.',
        ];

        $v = Validator::make($request->all(), [
            'title' => 'required|min:3|max:255',
            'files' => 'required|mimes:mp4|max:25600',
            'about_video' => 'required|min:8|max:350'
        ], $messages);
        if ($v->fails())
        {
            return Response::json(array(
                'success' => false,
                'errors' => $v->getMessageBag()->toArray()

            ), 400);
            //return redirect()->back()->withErrors($v->errors());
        }
        $data = $request->all();
        // $filePath = Storage::putFile('public/assets', $request->file('files'), 'public');
        $filePath = $request->file('files')->store(
            'assets', 'public'
        );
        $newAssets = Asset::Create([
            'user_id' => Auth::id(),
            'for' => 'user_project',
            'title' => $data['title'],
            'category' => $data['category'],
            'tags' => $data['tags'],
            'about' => $data['about_video'],
            'assets_visibility' => $data['visibility'],
            'type' => 'video',
            'path' => $filePath
            ]);
        // echo (new TagController)->exist('again');
        $tags = explode(",",$data['tags']);
        foreach ($tags as $key => $value) {
        $postTagAdded = UserTagPostTag::Create([
                'tag_id' => (new TagController)->exist($value),
                'asset_id' => $newAssets->id
        ]);
        }
        return Response::json(array('success' => true), 200);
    }
    public function upload(Request $request)
    {
        /*print_r($request->file());die;
        print_r($request->input());die;*/
        $filePath = Storage::put('public/assets', $request->file('files'), 'public');
        $files = array(array('name' => $filePath));
        $data = array('isSuccess' => 'true','files' => $files);
        echo json_encode($data);die;

        // echo $path = $request->file('files')->store('public/assets');die;
        echo Storage::disk('avatars')->exists($path);
        echo $visibility = Storage::disk('avatars')->getVisibility($path).'</br>';

       /* $path = $request->file('avatar')->storeAs(
        'public/', $request->user()->id
        );*/
        echo Storage::put('public/assets', $request->file('files'), 'private').'</br>';
        // $path = $request->file('avatar')->store('avatars/'.$request->user()->name);
        // $path = $request->file('avatar')->store($request->user()->name);
        // $path = Storage::disk('local')->put($request->user()->name, $request->file('avatar'));
        // $path = Storage::disk('avatars')->put($request->user()->name, $request->file('avatar'), 'private');
        echo asset('storage/avatars/'.$path);die;
    }  

    /**
     * Edit the specified post.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function editPostForm($id)
    {    
        $postData = Post::find($id);
        if($postData){
            if($postData->author->id != Auth::user()->id){
                return redirect('posts');
            }
        return view('posts.edit', ['postData' => $postData]);
        }else{
            return redirect('posts');
        }
    } 

    /**
     * Show the specified post.
     *
     * @param  string  $id
     * @return Response
     */
    public function singlePost($id)
    {
        $postData = Post::find($id);
        if($postData){           
        return view('posts.single', ['postData' => $postData]);
        }else{
            return redirect('posts');
        }
    }    



    /**
     * Update the specified post.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $data = $request->all();
        if(Post::where('id', $id)
            ->update([
            'title' => $data['title'],
            'content' => $data['content']
            ])){
            return redirect('posts');
        }else{
            return back()->withInput();
        }
              
    }
    /**
     * Update the specified post.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function remove($id, Request $request)
    {
        echo $id;die;
        if(Post::where('id', $id)
            ->delete()){
            return redirect('posts');
        }else{
            return back()->withInput();
        }
              
    }

    /**
     * Ajax request to send email.
     *
     * @param  Request  $request
     * @return Response
     */
    public function contactPerson(Request $request)
    {
        $data = $request->all();
        $postData = Post::find($data['contactPostId']);
        $postAuthor = Post::find($data['contactPostId'])->author;
        $email_var = $postData;
        $email_var['message'] = $data['message'];
        // print_r($email_var);die;
        if($postAuthor->id != Auth::user()->id){
            $mailSent = Mail::send('emails.contactPerson', ['postData' => $email_var], function ($m) use ($postData) {
                $m->from(Auth::user()->email, Auth::user()->name);

                $m->to($postData->author->email, $postData->author->name)
                ->subject('New contact for post Title - '.$postData->title);
            });
        }
        if(count(Mail::failures()) == 0){
        echo '1';     
        }else{
            echo Mail::failures();
        }die;
    }
}
