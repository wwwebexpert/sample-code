<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Auth;
use App\User;
use App\Asset;

class NotificationController extends Controller
{
	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getAll($username){
    	if($username == Auth::user()->name){
        	$notifications = [];
    		foreach(Auth::user()->notifications as $notification):
                    $user = User::where('id',$notification->data['broad_user_id'])->first();
                    if($notification->type == 'App\Notifications\AssetsLike'){
                    	$asset = Asset::where('id',$notification->data['asset_id'])->first();
                    	$notification_line = trim($user->profile->first_name ?  $user->profile->first_name.' '. $user->profile->last_name : $user->name)." likes your video.";
                    	    $notification_url = route('singleUser' , Auth::user()->name).'?asset='.Crypt::encryptString($asset->id);

                    }elseif($notification->type == 'App\Notifications\AssetsComment'){
                    	$asset = Asset::where('id',$notification->data['asset_id'])->first();
                    	$notification_line = trim($user->profile->first_name ?  $user->profile->first_name.' '. $user->profile->last_name : $user->name)." comments on your video.";
                    	    $notification_url = route('singleUser' , Auth::user()->name).'?asset='.Crypt::encryptString($asset->id);

                    }elseif ($notification->type == 'App\Notifications\UserConnection') {
                    	$notification_line = trim($user->profile->first_name ?  $user->profile->first_name.' '. $user->profile->last_name : $user->name)." starting following you!";
                    	$notification_url = route('singleUser' , $user->name);
                    }
                    $notifications[] = array(
                    'created_at' => $notification->created_at->diffForHumans(),
                    'notification_line' => $notification_line,
                    'broad_user_name' => trim($user->profile->first_name ?  $user->profile->first_name.' '. $user->profile->last_name : $user->name),
                    'broad_user_image' => $user->profile->profile_img ? asset('storage/avatars/'.$user->profile->profile_img) : asset('uploads/Dummy_User.png'),
                    'notification_url' => $notification_url
                    );
                    
                endforeach;
        return view('notifications.index', ['notificationData' => $notifications]);
        }else{
            return redirect('/');
        }
    }
    public function getJsonNotifications($username){
    	if($username == Auth::user()->name){
    		$notifications = [];
    		foreach(Auth::user()->notifications as $notification):
                    $user = User::where('id',$notification->data['broad_user_id'])->first();
                    if($notification->type == 'App\Notifications\AssetsLike'){
                    	$asset = Asset::where('id',$notification->data['asset_id'])->first();
                    	$notification_line = trim($user->profile->first_name ?  $user->profile->first_name.' '. $user->profile->last_name : $user->name)." likes your video";
                    	 $notification_url = route('singleUser' , Auth::user()->name).'?asset='.Crypt::encryptString($asset->id);
                    }elseif($notification->type == 'App\Notifications\AssetsComment'){
                    	$asset = Asset::where('id',$notification->data['asset_id'])->first();
                    	$notification_line = trim($user->profile->first_name ?  $user->profile->first_name.' '. $user->profile->last_name : $user->name)." comments on your video.";
                    	    $notification_url = route('singleUser' , Auth::user()->name).'?asset='.Crypt::encryptString($asset->id);

                    }elseif ($notification->type == 'App\Notifications\UserConnection') {
                    	$notification_line = trim($user->profile->first_name ?  $user->profile->first_name.' '. $user->profile->last_name : $user->name)." starting following you!";
                    	$notification_url = route('singleUser' , $user->name);
                    }
                    $notifications[] = array(
                    'created_at' => $notification->created_at->diffForHumans(),
                    'notification_line' => $notification_line,
                    'broad_user_name' => trim($user->profile->first_name ?  $user->profile->first_name.' '. $user->profile->last_name : $user->name),
                    'broad_user_image' => $user->profile->profile_img ? asset('storage/avatars/'.$user->profile->profile_img) : asset('uploads/Dummy_User.png'),
                    'notification_url' => $notification_url
                    );
                    
                endforeach;

        return response()->json(['success' => true, 'notificationData' => $notifications]);
        // return view('notifications.index', ['notificationData' => Auth::user()->notifications]);
        }else{
            return redirect('/');
        }
    }
    public function clear(){
    	if(Auth::user()->notifications()->delete()){
    		return response()->json(['success' => true, 'notificationData' => []]);
    	}else{
    		return response()->json(['success' => false]);
    	}
    }
}
