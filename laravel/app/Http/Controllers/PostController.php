<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Mail;
use App\Post;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index','singlePost']]);
    }

    /**
     * Show all posts
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $postsData = Post::orderBy('created_at', 'desc')->get();
        return view('posts.index', ['postsData' => $postsData]);
    } 

    /**
     * Show the form for add post
     *
     * @return void
     */
    public function addPostForm()
    {
        return view('posts.create');
    }  

    /**
     * Edit the specified post.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function editPostForm($id)
    {    
        $postData = Post::find($id);
        if($postData){
            if($postData->author->id != Auth::user()->id){
                return redirect('posts');
            }
        return view('posts.edit', ['postData' => $postData]);
        }else{
            return redirect('posts');
        }
    } 

    /**
     * Show the specified post.
     *
     * @param  string  $id
     * @return Response
     */
    public function singlePost($id)
    {
        $postData = Post::find($id);
        if($postData){           
        return view('posts.single', ['postData' => $postData]);
        }else{
            return redirect('posts');
        }
    }    

    /**
     * Add a post.
     *
     * @param  Request  $request
     * @return Response
     */
    public function add(Request $request)
    {
        $data = $request->all();
        $postCreated = Post::create([
            'user_id' =>  Auth::user()->id,
            'title' => $data['title'],
            'content' => $data['content']
        ]);
        if($postCreated){
            return redirect('/');
        }else{
            return redirect('post/add');
        }
    }

    /**
     * Update the specified post.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $data = $request->all();
        if(Post::where('id', $id)
            ->update([
            'title' => $data['title'],
            'content' => $data['content']
            ])){
            return redirect('posts');
        }else{
            return back()->withInput();
        }
              
    }
    /**
     * Update the specified post.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function remove($id, Request $request)
    {
        echo $id;die;
        if(Post::where('id', $id)
            ->delete()){
            return redirect('posts');
        }else{
            return back()->withInput();
        }
              
    }

    /**
     * Ajax request to send email.
     *
     * @param  Request  $request
     * @return Response
     */
    public function contactPerson(Request $request)
    {
        $data = $request->all();
        $postData = Post::find($data['contactPostId']);
        $postAuthor = Post::find($data['contactPostId'])->author;
        $email_var = $postData;
        $email_var['message'] = $data['message'];
        // print_r($email_var);die;
        if($postAuthor->id != Auth::user()->id){
            $mailSent = Mail::send('emails.contactPerson', ['postData' => $email_var], function ($m) use ($postData) {
                $m->from(Auth::user()->email, Auth::user()->name);

                $m->to($postData->author->email, $postData->author->name)
                ->subject('New contact for post Title - '.$postData->title);
            });
        }
        if(count(Mail::failures()) == 0){
        echo '1';     
        }else{
            echo Mail::failures();
        }die;
    }
}
