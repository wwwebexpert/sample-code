<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SocialAccountService;
use Socialite;
use Session;

class SocialAuthController extends Controller
{
    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();   
    }   

    public function callback(SocialAccountService $service)
    {
        $user = $service->createOrGetUser(Socialite::driver('facebook')->user(), 'facebook');

        auth()->login($user);
        Session::flash('loginMsg', 'You Are Successfully LogIn!');
        return redirect()->to('/');
    }
    public function redirectGoogle()
    {
        return Socialite::driver('google')->redirect();   
    }   

    public function callbackGoogle(SocialAccountService $service)
    {
        $user = $service->createOrGetUser(Socialite::driver('google')->user(), 'google');
        Session::flash('loginMsg', 'You Are Successfully LogIn!');
        auth()->login($user);

        return redirect()->to('/');
    }
}