<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use Session;

class LoginController extends Controller
{
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {
        // $username = $request->input('username');
        $username = $request->username;
        $password = $request->password;
        $user = User::where('name', '=', $username)
                    ->orWhere('email', '=', $username)->first();
        // print_r($user);die;
        // if(!$user) return Redirect::back()->withInput()->withFlashMessage('Unknown username.');
        if(!$user) return redirect()->back()->withErrors(['userNotExists' => 'User Doesn\'t exists']);
        if(!$user->verified) return redirect()->back()->withErrors(['userNotVerified' => 'Please verify your account']);

        if(filter_var($username, FILTER_VALIDATE_EMAIL)) {
            Auth::attempt(['email' => $username, 'password' => $password, 'verified' => '1']);
        } else {
            Auth::attempt(['name' => $username, 'password' => $password, 'verified' => '1']);
        }

        if ( Auth::check() ) {
            Session::flash('loginMsg', 'You Are Successfully LogIn!');
            return redirect()
            // ->intended('/');
            ->route('singleUserTimeline', ['name' => Auth::user()->name]);
        }

        return redirect()->back()->withErrors([
            'password' => 'Please, Check Your Credentials',
            'username' => 'Please, Check Your Credentials'
        ]);
    }
        /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
}