<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\PrivateMessageEvent;
use Illuminate\Support\Facades\Crypt;
use App\User;
use App\ChatRoom;
use App\Message;
use App\Receiver;
use Auth;

class PrivateChatController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
	public function __construct()
	{
		$this->middleware('auth');
	}

    public function get(ChatRoom $chatroom)
    {
        return $chatroom->messages;
    }

    public function index($receiverId)
    {
        $receiverId = Crypt::decryptString($receiverId);
        $receiver = User::find($receiverId);
        if( (!$receiver->isFollowedByMe(Auth::id())) || (!Auth::user()->isFollowedByMe($receiver->id)) )
        {
            return redirect(route('singleUser' , $receiver->name));
        }
        $senderUserId = auth()->user()->id;
        if(!$receiver){
            return redirect('/');
        }
        if(!$receiver->isFollowedByMe($senderUserId)){
            return redirect('/');
        }

        $roomMembers = [$receiverId, $senderUserId];
        sort($roomMembers);
        $roomMembers = implode($roomMembers, ',');
        
        $chatRoom = ChatRoom::where('user_ids', $roomMembers)->first();
        if(is_null($chatRoom)) {
            $chatRoom = new ChatRoom;
            $chatRoom->room_type = 'private';
            $chatRoom->user_ids = $roomMembers;
            $chatRoom->save();
        }

        return view('private-chat.form', compact('chatRoom', 'receiver'));
    }

    public function check(ChatRoom $chatroom)
    {
        $senderId = auth()->user()->id;
        $roomMembers = collect(explode(',', $chatroom->user_ids));
        $roomMembers->forget($roomMembers->search($senderId));
        $receiverId = $roomMembers->first();

        $receiver = User::find($receiverId);
        if( (!$receiver->isFollowedByMe(Auth::id())) || (!Auth::user()->isFollowedByMe($receiver->id)) )
        {
            return $redirectTo = route('singleUser' , $receiver->name);
        }
    }
    public function store(ChatRoom $chatroom)
    {
        // return redirect(route('singleUser' , auth()->user()->name));

        $senderId = auth()->user()->id;
        $roomMembers = collect(explode(',', $chatroom->user_ids));
        $roomMembers->forget($roomMembers->search($senderId));
        $receiverId = $roomMembers->first();

        $receiver = User::find($receiverId);
        if( (!$receiver->isFollowedByMe(Auth::id())) || (!Auth::user()->isFollowedByMe($receiver->id)) )
        {
        // print_r($receiver);die;
            $redirectTo = route('singleUser' , $receiver->name);
        }

        $message = new Message;
        $message->chat_room_id = $chatroom->id;
        $message->sender_id = $senderId;
        $message->message = request('message');
        $message->save();

        $receiver = new Receiver;
        $receiver->message_id = $message->id;
        $receiver->receiver_id = $receiverId;

        if($receiver->save()) {
            $message = Message::with('sender')->find($message->id);
            broadcast(new PrivateMessageEvent($message))->toOthers();
            // print_r($message);die;
            return $message;
        } else {
            return 'Something went wrong!!';
        }
    }
}
