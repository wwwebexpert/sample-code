<?php 
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Request;
use LRedis;
// use L5Redis;
 
class ChatController extends Controller {

	public function __construct()
	{
		$this->middleware('auth',['except' => ['test']]);
	}

	public function index()
	{
		return view('chat');
	}
	public function test(){
		// $redis1 = Redis::connect();
		$redis2 = LRedis::connection();
		$redis2->publish('chat.message', json_encode([
            'msg'      => 'HI system here',
            'nickname' => 'System',
            'system'   => true,
        ]));
		// print_r($redis1);
		// print_r($redis2);die;
	}

	public function systemMessage()
	{
        $redis = L5Redis::connection();

        $redis->publish('chat.message', json_encode([
            'msg'      => 'HI system here',
            'nickname' => 'System',
            'system'   => true,
        ]));
	}

}