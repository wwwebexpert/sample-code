<?php

namespace App;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'assets';
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function owner()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function likes()
    {
        // return $this->hasMany('App\AssetLike', 'asset_id')->whereUserId($this->author_id)->count();
        return $this->hasMany('App\AssetLike', 'asset_id');
    }
    public function comments()
    {
        // return $this->hasMany('App\AssetLike', 'asset_id')->whereUserId($this->author_id)->count();
        return $this->hasMany('App\AssetComment', 'asset_id')->latest();
    }
    public function isLikedByMe($userId) {
        return (boolean) $this->likes()->where('user_id', $userId)->count();
    }
    public function tags()
    {
        return $this->morphToMany('App\Tag', 'taggable');
    }
    /**
     * Set the user's first name.
     *
     * @param  string  $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->diffForHumans();
    }
}
