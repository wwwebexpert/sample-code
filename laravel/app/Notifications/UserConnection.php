<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Support\Facades\Crypt;


class UserConnection extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $user;
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $name = $this->user->name;
        $url = url('/'.$name);

        return (new MailMessage)
                ->greeting('Hello!')
                ->line($name .' starting following you!')
                ->action('View User', $url)
                ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'user_id' => $this->user->id
            //
        ];
    }
    public function toDatabase($notifiable)
    {
        return [
            'broad_user_id' => $this->user->id
            //
        ];
    }
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'broad_user_name' => trim($this->user->profile->first_name ?  $this->user->profile->first_name.' '. $this->user->profile->last_name : $this->user->name),
            'broad_user_image' => $this->user->profile->profile_img ? asset('storage/avatars/'.$this->user->profile->profile_img) : asset('uploads/Dummy_User.png'),
            'notification_url' => route('singleUser' , $this->user->name),
            'notification_line' => trim($this->user->profile->first_name ?  $this->user->profile->first_name.' '. $this->user->profile->last_name : $this->user->name)." starting following you!"
        ]);
    }
}
