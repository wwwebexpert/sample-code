<?php

namespace App;

use Laravel\Socialite\Contracts\User as ProviderUser;
use Illuminate\Support\Facades\Hash;
use App\Jobs\SendWelcomeWithRandomPasswordEmail;
use App\UserProfile;

class SocialAccountService
{
    public function createOrGetUser(ProviderUser $providerUser, $provider)
    {
        $account = SocialAccount::whereProvider($provider)
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {

            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $provider
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {                
                $newPassword = str_random(8);
                $hashed_random_password = Hash::make($newPassword);
                $fullname = $providerUser->getName();

                $username = strtolower(trim($providerUser->getName()));
                if (strpos($username, ' ') !== false) {
                    $username = preg_replace('!\s+!', ' ', $username);
                    $username = trim(preg_replace("/[^A-Za-z0-9 ]/", "", preg_replace('!\s+!', ' ', $username)));
                    $username = preg_replace('/\s+/', '.', $username);
                }
                $filename = $username .=".".time();
                
               /*
               $data = file_get_contents($providerUser->avatar_original);
                $file = fopen($fileName, 'w+');
                fputs($file, $data);
                fclose($file);
                 $arrContextOptions=['ssl'=>['verify_peer'=>false,'verify_peer_name'=>false]];
                $img = Image::make(file_get_contents($file, false, stream_context_create($arrContextOptions)))->save('uploads/'.$file);*/

                /*$destinationPath = 'uploads/';
                $tempImage = tempnam($destinationPath, $username);
                copy($providerUser->avatar_original, $tempImage);*/
                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'name' => $username,
                    'password' => $hashed_random_password,
                    'verified' => '1',
                ]);
                $arr = explode(" ", $fullname, 2);
                UserProfile::create([
                    'user_id' => $user->id,
                    'first_name' => @$arr[0],
                    'last_name' => @$arr[1]
                ]);
                if($providerUser->getEmail()){
                dispatch(new SendWelcomeWithRandomPasswordEmail($user, $newPassword, $provider));
                }
            }

            $account->user()->associate($user);
            $account->save();

            return $user;

        }

    }
}