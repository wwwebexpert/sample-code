<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function(){
    return view('home', ['homeContent' => DB::table('home_content')->first()]);
});

Auth::routes();

Route::get('/home', function () {
    return redirect('/');
});
Route::get('/chat', 'ChatController@test');
Route::get('/systemMessage', 'ChatController@systemMessage');
/*For chat*/
Route::get('users', 'UserController@index')->name('user.list');

Route::get('private-chat/{chatroom}', 'PrivateChatController@index')->name('private.chat.index');
Route::post('private-chat/{chatroom}', 'PrivateChatController@store')->name('private.chat.store');
Route::get('private-chat/check/{chatroom}', 'PrivateChatController@check')->name('private.chat.check');
Route::get('fetch-private-chat/{chatroom}/', 'PrivateChatController@get')->name('fetch-private.chat');

Route::get('public-chat', 'PublicChatController@index')->name('public.chat.index');
Route::post('public-chat/{chatroom}', 'PublicChatController@store')->name('public.chat.store');
Route::get('fetch-public-chat/{chatroom}/', 'PublicChatController@get')->name('fetch-public.chat');
/*For chat*/


Route::post('/search-results/', 'SearchController@index');
Route::get('/search-results/', 'SearchController@getSearchResult');
Route::post('/search/advance/', 'SearchController@advance');

Route::get('/tags', 'TagController@index');
Route::post('/tag/add', 'TagController@add');
Route::post('/tag/remove', 'TagController@remove');

Route::get('/notification', 'AssetController@index');
Route::get('/allnotify', 'ChatController@test');

Route::get('/facebook', 'SocialAuthController@redirect');
Route::get('/callback', 'SocialAuthController@callback');
Route::post('/register', 'RegisterController@register')->name('register');
Route::get('/google', 'SocialAuthController@redirectGoogle');
Route::get('/callback/google', 'SocialAuthController@callbackGoogle');
Route::get('/verifyemail/{token}', 'RegisterController@verify')->name('verifyEmail');
Route::post('/is_username_available', 'UserController@isUserNameInUse')->name('isUserNameInUse');

Route::post('/user/followUnfollowUser', 'UserController@followUnfollowUser')->name('followUnfollowUser');
Route::post('/user/changeFriendStatus', 'UserController@changeFriendStatus')->name('changeFriendStatus');
Route::get('/all_users/', 'UserController@index')->name('all_users');
Route::post('/login', 'LoginController@authenticate')->name('login');

Route::get('/profile', 'UserController@authUserProfile')->name('authUserProfile');
Route::get('/{username}', 'UserController@singleUser')->name('singleUser')->where('username', '[a-zA-Z0-9.]+');
Route::get('/{username}/timeline', 'UserController@singleUserTimeline')->name('singleUserTimeline')->where('username', '[a-zA-Z0-9.]+');
Route::get('/{username}/edit', 'UserController@editUserForm')->name('editUser')->where('username', '[a-zA-Z0-9.]+');
Route::get('/{username}/friends', 'UserController@getUserFriends')->name('getUserFriends')->where('username', '[a-zA-Z0-9.]+');
Route::get('/{username}/followers', 'UserController@getUserFollowers')->name('getUserFollowers')->where('username', '[a-zA-Z0-9.]+');
Route::get('/{username}/followings', 'UserController@getUserFollowing')->name('getUserFollowing')->where('username', '[a-zA-Z0-9.]+');

Route::get('/{username}/assets/{id}', 'AssetController@showAsset')->name('showAsset')->where(['username' => '[a-zA-Z0-9.]+', 'id' => '[0-9]+' ]);
Route::get('/{username}/assets/add', 'AssetController@addAssetsForm')->name('addAssetsForm')->where('username', '[a-zA-Z0-9.]+');
Route::post('/{username}/assets/add', 'AssetController@add')->name('addAssets')->where('username', '[a-zA-Z0-9.]+');

Route::post('/asset/{id}/likeUnlike', 'AssetController@likeUnlike')->name('likeUnlikeAssets')->where('id', '[0-9]+');
Route::post('/asset/{id}/getLikes', 'AssetController@getLikes')->name('getLikesDetailsAssets')->where('id', '[0-9]+');
Route::post('/asset/comment/add', 'AssetController@addAssetComment')->name('addAssetComment');
Route::post('/asset/comments', 'AssetController@getComments')->name('getComments');
Route::post('/asset/incrView', 'AssetController@incrView')->name('incrAssetsView');

Route::post('/{username}/assets/unlink', 'AssetController@unlink')->name('unlinkAssets')->where('username', '[a-zA-Z0-9.]+');
Route::get('/{username}/notifications', 'NotificationController@getAll')->name('getAllNotification')->where('username', '[a-zA-Z0-9.]+');
Route::get('/{username}/notification', 'NotificationController@getJsonNotifications')->where('username', '[a-zA-Z0-9.]+');
Route::post('/notification/clear', 'NotificationController@clear');


Route::get('/posts', 'PostController@index')->name('posts');
Route::get('/post/add', 'PostController@addPostForm')->name('createPost');
Route::post('/post/add', 'PostController@add');

Route::get('/post/{id}', 'PostController@singlePost')->name('singlePost');

Route::get('/post/edit/{id}', 'PostController@editPostForm')->name('editPost');
Route::post('/post/edit/{id}', 'PostController@update')->name('updatePost');
Route::post('/post/delete/{id}', 'PostController@remove')->name('deletePost');

Route::post('/change-password', 'UserController@changePassword')->name('changePassword');

Route::post('/profile/upload', 'UserController@uploadProfileImage')->name('uploadProfileImage');
Route::post('/profile/update', 'UserController@updateBasicInfo')->name('updateBasicInfo');
Route::post('/profile/updatelinks', 'UserController@updateWebLinks')->name('updateWebLinks');
Route::post('/account/deactivate', 'UserController@deactiveAccount')->name('deactive-account');
Route::post('/account/activate', 'UserController@activeAccount')->name('active-account');
Route::post('/user/edit/{id}', 'UserController@update')->name('updateUser');
/*Routing for admin section*/
// Route::namespace('Admin')->group(function () {
	Route::prefix('admin')->namespace('Admin')->group(function () {
		// Route::get('/login',function(){ return view('admin.dashboard');});
		Route::post('login', 'LoginController@authenticate')->name('AdminLogin');
		// Route::get('admin/login','UserController@index')->name('allUsersForAdmin');
		Route::get('login','LoginController@loginForm')->name('loginFormAdmin');
		Route::get('dashboard','DashboardController@index')->name('AdminDashboard');
		Route::get('users','DashboardController@users')->name('allUsersForAdmin');
		Route::get('home/content','DashboardController@changeHomeContent')->name('changeHomeContent');
		Route::post('home/content','DashboardController@updateHomeContent')->name('updateHomeContent');
		Route::post('user/delete','UserController@delete')->name('deleteUser');
	});
