<?php $__env->startSection('content'); ?>
<div class="container">

  <div class="row">
  <?php if(Auth::id()): ?>
  <a href="<?php echo e(route('createPost')); ?>">
  <button type="button" class="btn btn-primary pull-right">
  Add Post
  </button>
  </a>
<?php endif; ?>

    <?php $__currentLoopData = $postsData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="col-sm-4">
      <a href="<?php echo e(route('singlePost', $post->getRouteKey())); ?>"><h3><?php echo e($post->title); ?> </h3></a>
        <p><?php echo e($post->content); ?></p>
        <?php if(Auth::id() == $post->author->id): ?>
        <span class="pull-left">    <a href="<?php echo e(route('editPost', $post->getRouteKey())); ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
</span>
        <span class="pull-right"><?php echo e($post->author->name); ?> @ <?php echo e(Carbon\Carbon::parse($post->created_at)->format('F j, Y')); ?></span>
        <?php else: ?>
        <span class="pull-right">
        <b title="Contact Person" style="cursor: pointer;" class="small-box-footer" data-post_id = "<?php echo e($post->id); ?>" data-toggle="modal" data-target="#Portfolio" id="openContactForm"><?php echo e($post->author->name); ?> </b>
         @ <?php echo e(Carbon\Carbon::parse($post->created_at)->format('F j, Y')); ?></span>
        <?php endif; ?>
        
      </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  </div>
</div>  
<?php $__env->startComponent('element.contact-person'); ?>

<?php echo $__env->renderComponent(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>