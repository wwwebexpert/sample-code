<?php $__env->startSection('content'); ?>
<div class="container">
  <div class="row">
    <?php $__currentLoopData = $usersData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="col-sm-4">
      <a href="<?php echo e(route('singleUser', $user->getRouteKey())); ?>"><h3><?php echo e($user->name); ?> </h3></a>
        <p><?php echo e($user->email); ?></p>        
      </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  </div>
</div>  

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>