<div class="modal fade" id="Portfolio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        <div class="alert alert-success" id="success-alert" style="display: none;"></div>
        <div class="alert alert-danger" id="danger-alert" style="display: none;"></div>
        <h3 class="modal-title" id="contact-person">Contact Person</h3>
      </div>
        <?php echo Form::open(['url' => ['/post/contact-person'], 'id'=>"contactPersonForm" ,'name'=>"contactPerson"]); ?>


      <div class="modal-body">

        <div class="alert alert-info info" style="display: none;">
            <ul></ul>
        </div>

        <div class="row" style="margin-bottom: 20px;">
            <div class="col-md-3 col-lg-3">
                <?php echo Form::label('message', 'Message', ['class' => 'label_message']); ?>

            </div>
            <div class="col-md-9 col-lg-9">
                <?php echo Form::textarea('message', '', ['class' => 'form-control input-md' , 'placeholder' => 'Please enter your message here', 'required' => 'required']); ?>

            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <?php echo Form::submit('Send', ['class' => 'btn btn-primary']); ?>

      </div>
    <?php echo Form::close(); ?>

    </div>
  </div>
</div>

<script>
$(document).ready(function() {
  var contactPostId;
  $('b#openContactForm').click(function() {
    <?php  if(!Auth::id()){   ?>
    window.location = "/login";
    <?php  }  ?>
    var post_id = $(this).data('post_id');
    contactPostId = post_id;
  });

  $('#contactPersonForm').submit(function(e){
    e.preventDefault();
    var message = $('#message').val();

    $.ajax({
      url: '/post/contact-person',
      type: 'POST',
      dataType: 'json',
      data:{message: message, contactPostId:contactPostId, "_token": "<?php echo e(csrf_token()); ?>"},
      success: function(msg) {
      if(msg){
      $("div#success-alert").html("<strong>Success!</strong> Email has been sent successfully").css('display','block').fadeTo(2000, 500).slideUp(500, function(){
        $("#success-alert").slideUp(500);
        $('textarea#message').val('');
        $('#Portfolio').modal('toggle');
      });

        // alert('msg sent');
      }else{
        $("#danger-alert").html("<strong>Danger!</strong> There is problem while sending email").css('display','block').fadeTo(2000, 500).slideUp(500, function(){
        $("#danger-alert").slideUp(500);
        $('textarea#message').val('');
        $('#Portfolio').modal('toggle');
      });

      }
      },
      error:function(data){
      $(".alert-info").append('titulo');
      }
    });
  });
});
</script>