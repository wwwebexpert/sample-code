<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">User Details</div>
                <div class="panel-body">                

                        <div class="form-group<?php echo e($errors->has('title') ? ' has-error' : ''); ?>">
                            <label for="username" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                               <label for="username" class="col-md-4 control-label"><?php echo e($userData->name); ?></label>
                            </div>
                        </div>

                        <div class="form-group<?php echo e($errors->has('content') ? ' has-error' : ''); ?>">
                            <label for="content" class="col-md-4 control-label">Email</label>

                            <div class="col-md-6">
                                <label for="content" class="col-md-4 control-label"><?php echo e($userData->email); ?></label>
                                
                            </div>
                        </div>
                    <?php if(Auth::id() == $userData->id): ?>
                        <a href="<?php echo e(route('editUser', $userData->getRouteKey())); ?>">Edit details</a>
                    <?php endif; ?>

                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">User's Posts</div>
                <div class="panel-body">                

                <?php $__currentLoopData = $userData->posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-sm-4">
                <a href="<?php echo e(route('singlePost', $post->getRouteKey())); ?>"><h3><?php echo e($post->title); ?> </h3></a>
                <p><?php echo e($post->content); ?></p>
                <?php if(Auth::id() == $post->author->id): ?>
                <span class="pull-left"><a href="<?php echo e(route('editPost', $post->getRouteKey())); ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a><a href="<?php echo e(route('deletePost', $post->getRouteKey())); ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a></span>
                <span class="pull-right"><?php echo e($post->author->name); ?> @ <?php echo e(Carbon\Carbon::parse($post->created_at)->format('F j, Y')); ?></span>
                <?php else: ?>
                <span class="pull-right">
                <b title="Contact Person" style="cursor: pointer;" class="small-box-footer" data-post_id = "<?php echo e($post->id); ?>" data-toggle="modal" data-target="#Portfolio" id="openContactForm"><?php echo e($post->author->name); ?> </b>
                @ <?php echo e(Carbon\Carbon::parse($post->created_at)->format('F j, Y')); ?></span>
                <?php endif; ?>

                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->startComponent('element.contact-person'); ?>
<?php echo $__env->renderComponent(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>