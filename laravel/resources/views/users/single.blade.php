@extends('layouts.app')

@section('content')
<!---Profile Section -->

@php $appreciations  = 0 @endphp
@foreach($userData->videos as $userVideos => $userVideo)
@php $appreciations += count($userVideo->likes) @endphp
@endforeach



<div class="container" id="myprofile" tabindex="-1">
  <div class="row">
    <div class="col-lg-4">
      <div class="row">
        <div class="right-sidebar">
          <div class="profile-information">
            <h4 class="username-heading"> <strong>{{$userData->profile->first_name ? $userData->profile->first_name.' '.$userData->profile->last_name : $userData->name }}</strong></h4> 
             <div class="overlay"></div>
             @if(Auth::id() == $userData->id)
                <div class="button editprofile"><a href="{{route('editUser', $userData->name)}}"> <i class="fa fa-pencil" aria-hidden="true"></i>
               Edit </a></div>
            @endif
            <div class="col-sm-6 col-md-5">
				<div class="img-circle">
					<img src="{{ $userData->profile->profile_img ? asset('storage/avatars/'.$userData->profile->profile_img) : asset('uploads/Dummy_User.png') }}" alt="" class="profile-pic" />
				</div>
			</div>
            <div class="col-sm-6 col-md-7 user-details">  <p> <i class="fa fa-map-marker"></i>City, {{$userData->profile->city}} </p>
              <p> <i class="fa fa-user"></i> <a href="{{ route('singleUser' , $userData->name)  }}">{{$userData->name}}</a> <br />
                </p>
                <p><i class="fa fa-gift"></i> {{ $userData->profile->dob ? Carbon\Carbon::parse($userData->profile->dob)->format('F j, Y') : 'N/A' }} </p>
              <!-- Split button --> 
            
            </div>
            @php $friendshipStatus = $userData->friendshipStatus(Auth::id()) @endphp
            <div class="col-sm-12 col-md-12">
              <div class="profile-userbuttons">

            @if(Auth::id() != $userData->id && Auth::check())

              @if(@$friendshipStatus->status != '3')

                @if($userData->isFollowedByMe(Auth::id())) <button type="button" data-user_id="{{Crypt::encryptString($userData->id)}}" class="btn btn-follow btn-md" id="unfollowUser">Unfollow</button> @else 
                  <button type="button" data-user_id="{{Crypt::encryptString($userData->id)}}" class="btn btn-follow btn-md" id="followUser">Follow</button>
                @endif
                
                @if(@$friendshipStatus->status == '1') 
                <button type="button" data-user_id="{{Crypt::encryptString($userData->id)}}" class="btn btn-follow btn-md" id="removeFriend">Unfriend</button> 
                @elseif(@$friendshipStatus->status == '0' && $friendshipStatus->first_user_id == Auth::id()) 
                <button type="button" data-user_id="{{Crypt::encryptString($userData->id)}}" class="btn btn-follow btn-md" id="cancelRequest">Cancel Request</button>
                @elseif(@$friendshipStatus->status == '0' && $friendshipStatus->second_user_id == Auth::id()) 
                  <button type="button" data-user_id="{{Crypt::encryptString($userData->id)}}" class="btn btn-follow btn-md" id="acceptRequest">Accept</button>
                  <button type="button" data-user_id="{{Crypt::encryptString($userData->id)}}" class="btn btn-follow btn-md" id="declineRequest">Decline</button>
                @else
                <!-- <button type="button" data-user_id="{{Crypt::encryptString($userData->id)}}" class="btn btn-follow btn-md" id="addUser">Add Friend</button>  -->
                @endif
             
                <!-- <button type="button" data-user_id="{{Crypt::encryptString($userData->id)}}" class="btn btn-follow btn-md" id="addUser">Add Friend</button> -->
                <!-- <button type="button" data-user_id="{{Crypt::encryptString($userData->id)}}" class="btn btn-follow btn-md" id="blockUser">Block</button> -->
                @if( ($userData->isFollowedByMe(Auth::id())) && (Auth::user()->isFollowedByMe($userData->id)) ) <button type="button" class="btn btn-messaage btn-md" id="messageUser"> <i class="fa fa-envelope"></i><a href="{{ route('private.chat.index', Crypt::encryptString($userData->id)) }}"> Message </a></button>
                @endif
                <button type="button" class="btn btn-messaage btn-md" style="display: none;" id="messageUser"> <i class="fa fa-envelope"></i><a href="{{ route('private.chat.index', Crypt::encryptString($userData->id)) }}"> Message </a></button>

              @else
              <button type="button" data-user_id="{{Crypt::encryptString($userData->id)}}" class="btn btn-follow btn-md" id="followUser" style="display: none;">Follow</button>
              <button type="button" data-user_id="{{Crypt::encryptString($userData->id)}}" class="btn btn-follow btn-md" id="addUser" style="display: none;">Add Friend</button> 
              <button type="button" data-user_id="{{Crypt::encryptString($userData->id)}}" class="btn btn-follow btn-md" id="unblockUser">Unblock</button>
              <button type="button" class="btn btn-messaage btn-md" id="messageUser" style="display: none;"> <i class="fa fa-envelope"></i>Message</button> 
              @endif

            @endif
              </div>
            </div>
          </div>

          <div class="profile-statics">
            <ul class="stats-info">
              <li class=""><span><i class="fa fa-eye" aria-hidden="true"></i> </span> Project Views <span class="pull-right stats-value"> {{$userData->projectView()}} </span> </li>
              <li class=""><span> <i class="fa fa-heart" aria-hidden="true"></i></span> Appreciations<span class="pull-right stats-value">{{$appreciations}} </span> </li>
              <li class=""> <span class="stats-icon-followers"> </span> Followers<span class="pull-right stats-value"> {{count($userData->followers)}} </span> </li>
              <li class=""> <span class="stats-icon-following"> </span> Following <span class="pull-right stats-value"> {{count($userData->following)}}</span></li>
            </ul>
          </div>
          <div class="talent-field profile-sidebar-label">
            <h3> Interests </h3>
            <div class="fields-list">{{$userData->profile->interest ? $userData->profile->interest : 'N/A'}} </div>
          </div>
          <hr>
          <div class="talent-on-web profile-sidebar-label">
            <h3> On the Web </h3>
            <div class="web-list">
              <li> <a href="https://www.facebook.com/{{$userData->profile->facebook_link_url}}"> <i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
              <li> <a href="https://www.instagram.com/{{$userData->profile->instagram_link_url}}"> <i class="fa fa-instagram" aria-hidden="true"></i></a> </li>
              <li> <a href="https://twitter.com/{{$userData->profile->twitter_link_url}}"> <i class="fa fa-twitter" aria-hidden="true"></i> </a> </li>
              <li> <a href="https://www.youtube.com/{{$userData->profile->youtube_link_url}}"> <i class="fa fa-youtube" aria-hidden="true"></i> </a> </li>
            </div>
          </div>
          <hr>
          <div class="about-user profile-sidebar-label">
            <h3> About </h3>
            <div class="about-description">
              <p>{{$userData->profile->about_user ? $userData->profile->about_user : 'N/A'}}</strong></a></p>
            </div>
          </div>
          @if(Auth::check())
          <hr>
          <div class="account-detail-user profile-sidebar-label">
            <div class="account-created-date">
              @if(Auth::id() != $userData->id)
                <p> MEMBER SINCE: {{ Carbon\Carbon::parse($userData->created_at)->format('F, Y') }} </p>
              @else  
              <p> MEMBER SINCE: {{ Carbon\Carbon::parse($userData->created_at)->format('F j, Y') }} </p>
              @endif
            </div>
          </div>
          @endif

        </div>
      </div>
      <div class="left-bar"> </div>
    </div>
    <script src='https://content.jwplatform.com/libraries/5udyR0Be.js'></script>

    <div class="col-lg-8">
      <div class="row">
        <div class="right-section">
          <div class="tabbable-panel">
            <div class="tabbable-line">
              <div class="col-lg-12">
                
                <ul class="nav nav-tabs pull-left">
                  <li class="active"> <a href="#tab_default_1" data-toggle="tab">{{ Auth::id() == $userData->id ? 'My' : 'User' }} Videos</a> </li>
                </ul>
                @if(Auth::check() && Auth::id() == $userData->id)
                <ul class="nav nav-tabs add-video-btn pull-right">                
                  <li> <i class="fa fa-plus" aria-hidden="true"></i>
                  <a href="{{ route('addAssetsForm', Auth::user()->name) }}">
                  <i class="fa fa-video-camera" aria-hidden="true"></i>
                  Videos</a></li>
                </ul>@endif
			       </div>
              <div class="tab-content">
                <div class="tab-pane active margin-t30" id="tab_default_1">
                @php $i = 0 @endphp
                  @foreach($userData->videos as $userVideos => $userVideo)
                  @if($userVideo->assets_visibility != "0")
                    @if(Auth::check())
                    @if(Auth::id() != $userData->id)
                      @if($userData->isFollowedByMe(Auth::id()))
                      @php $i = 1 @endphp
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 show-box">
                          <div class="card cart-block">
                            <div class="card-img link" id="link" data-title="{{$userVideo->title}}" data-desc="{{$userVideo->about}}" data-tags="{{$userVideo->tags}}" data-id="{{Crypt::encryptString($userVideo->id)}}">
                              @php $thumbnail_url = ''; @endphp
                              @if($userVideo->upload_type && (strpos($userVideo->path, 'youtube')))                             
                              @php preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $userVideo->path, $matches);
                              $thumbnail_url = 'https://img.youtube.com/vi/'.$matches[1].'/sddefault.jpg'; 
                              @endphp
                              @endif

                              @if($userVideo->upload_type && (strpos($userVideo->path, 'vimeo')))
                              @php
                              $thumbnail_url = json_decode(file_get_contents('https://vimeo.com/api/oembed.json?url='.$userVideo->path))->thumbnail_url;                              
                              @endphp

                              <video width="100%" height="auto" poster="{{$thumbnail_url}}">
                                <source src="{{$userVideo->path}}" type="video/mp4">
                              </video>
                              <!-- <iframe src="https://player.vimeo.com/video/12345?autoplay=0&loop=1&autopause=0" width="560" height="240" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> -->

                              @else 
                              <video width="100%" height="auto" poster="{{$thumbnail_url}}">
                              @if($userVideo->upload_type)
                              <source src="{{$userVideo->path}}" type="video/mp4">
                              @else
                              <source src="{{asset('storage/'.$userVideo->path) }}" type="video/mp4">
                              @endif
                                
                                <span> Your browser does not support the <video> tag</span>
                              </video>
                              @endif
                            <!--   <img src="img/live03.jpg" class="card-img-top img-responsive img-rounded"> -->
                            </div>

                            <div class="card-block">
                              <h4 class="card-title">{{$userVideo->title}}</h4>
                              <ul class="card-subtitle-cont">
                                <li class="card-subtitle ">{{$userVideo->created_at}}</li>
                                <li class="card-subtitle "><i class="fa fa-eye" aria-hidden="true"></i> {{$userVideo->views}} Views </li>

                                <li class="card-subtitle ">
                                @if($userVideo->isLikedByMe(Auth::id())) 
                                <span class="asset_like_unlike" data-id="{{$userVideo->id}}" data-action="unlike"><i class="fa fa-heart" aria-hidden="true" style="color: red;"></i></span>
                                <span class="asset_like_count" data-total-likes="{{count($userVideo->likes)}}" data-id="{{$userVideo->id}}" data-toggle="modal" data-target="#likesModal" > {{count($userVideo->likes)}} Likes </span>
                                @else
                                <span class="asset_like_unlike" data-id="{{$userVideo->id}}" data-action="like"><i class="fa fa-heart" aria-hidden="true"></i></span> <span class="asset_like_count" data-total-likes="{{count($userVideo->likes)}}" data-id="{{$userVideo->id}}" data-toggle="modal" data-target="#likesModal"> {{count($userVideo->likes)}} Likes </span>
                                @endif

                                </li>
                                <li class="card-subtitle showVideo" ><i class="fa fa-comment-o" aria-hidden="true"></i>{{count($userVideo->comments)}} Comment</li>

                              </ul>
                            </div>
                          </div>
                        </div>                        
                      @endif
                      @else
                      @php $i = 1 @endphp
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 show-box">
                          <div class="card cart-block">
                            <div class="card-img link" id="link" data-title="{{$userVideo->title}}" data-desc="{{$userVideo->about}}" data-tags="{{$userVideo->tags}}" data-id="{{Crypt::encryptString($userVideo->id)}}">
                            @php $thumbnail_url = ''; @endphp
                              @if($userVideo->upload_type && (strpos($userVideo->path, 'youtube')))                             
                              @php preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $userVideo->path, $matches);
                              $thumbnail_url = 'https://img.youtube.com/vi/'.$matches[1].'/sddefault.jpg'; 
                              @endphp
                              @endif

                              @if($userVideo->upload_type && (strpos($userVideo->path, 'vimeo')))
                              @php
                              $thumbnail_url = json_decode(file_get_contents('https://vimeo.com/api/oembed.json?url='.$userVideo->path))->thumbnail_url;                              
                              @endphp

                              <video width="100%" height="auto" poster="{{$thumbnail_url}}">
                                <source src="{{$userVideo->path}}" type="video/mp4">
                              </video>
                              <!-- <iframe src="https://player.vimeo.com/video/12345?autoplay=0&loop=1&autopause=0" width="560" height="240" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> -->

                              @else 
                              <video width="100%" height="auto" poster="{{$thumbnail_url}}">
                              @if($userVideo->upload_type)
                              <source src="{{$userVideo->path}}" type="video/mp4">
                              @else
                              <source src="{{asset('storage/'.$userVideo->path) }}" type="video/mp4">
                              @endif
                                
                                <span> Your browser does not support the <video> tag</span>
                              </video>
                              @endif
                            <!--   <img src="img/live03.jpg" class="card-img-top img-responsive img-rounded"> -->
                            </div>
                            <div class="card-block">
                              <h4 class="card-title">{{$userVideo->title}}</h4>
                              <ul class="card-subtitle-cont">
                                <li class="card-subtitle ">{{$userVideo->created_at}}</li>
                                <li class="card-subtitle "><i class="fa fa-eye" aria-hidden="true"></i> {{$userVideo->views}} Views </li>
                                <li class="card-subtitle ">
                                @if($userVideo->isLikedByMe(Auth::id())) 
                                <span class="asset_like_unlike" data-id="{{$userVideo->id}}" data-action="unlike"><i class="fa fa-heart" aria-hidden="true" style="color: red;"></i></span>
                                <span class="asset_like_count" data-total-likes="{{count($userVideo->likes)}}" data-id="{{$userVideo->id}}" data-toggle="modal" data-target="#likesModal"> {{count($userVideo->likes)}} Likes </span>
                                @else
                                <span class="asset_like_unlike" data-id="{{$userVideo->id}}" data-action="like"><i class="fa fa-heart" aria-hidden="true"></i></span> <span class="asset_like_count" data-total-likes="{{count($userVideo->likes)}}" data-id="{{$userVideo->id}}" data-toggle="modal" data-target="#likesModal"> {{count($userVideo->likes)}} Likes </span>
                                @endif

                                </li>
                                <li class="card-subtitle showVideo"><i class="fa fa-comment-o" aria-hidden="true"></i>{{count($userVideo->comments)}} Comment</li>
                              </ul>
                            </div>
                          </div>
                        </div>
                    @endif
                    @endif
                    @else
                    @php $i = 1 @endphp
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 show-box">
                          <div class="card cart-block">
                            <div class="card-img link" id="link" data-title="{{$userVideo->title}}" data-desc="{{$userVideo->about}}" data-tags="{{$userVideo->tags}}" data-id="{{Crypt::encryptString($userVideo->id)}}">
                            @php $thumbnail_url = ''; @endphp
                              @if($userVideo->upload_type && (strpos($userVideo->path, 'youtube')))                             
                              @php preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $userVideo->path, $matches);
                              $thumbnail_url = 'https://img.youtube.com/vi/'.$matches[1].'/sddefault.jpg'; 
                              @endphp
                              @endif

                              @if($userVideo->upload_type && (strpos($userVideo->path, 'vimeo')))
                              @php
                              $thumbnail_url = json_decode(file_get_contents('https://vimeo.com/api/oembed.json?url='.$userVideo->path))->thumbnail_url;                              
                              @endphp

                              <video width="100%" height="auto" poster="{{$thumbnail_url}}">
                                <source src="{{$userVideo->path}}" type="video/mp4">
                              </video>
                              <!-- <iframe src="https://player.vimeo.com/video/12345?autoplay=0&loop=1&autopause=0" width="560" height="240" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> -->

                              @else 
                              <video width="100%" height="auto" poster="{{$thumbnail_url}}">
                              @if($userVideo->upload_type)
                              <source src="{{$userVideo->path}}" type="video/mp4">
                              @else
                              <source src="{{asset('storage/'.$userVideo->path) }}" type="video/mp4">
                              @endif
                                
                                <span> Your browser does not support the <video> tag</span>
                              </video>
                              @endif
                            <!--   <img src="img/live03.jpg" class="card-img-top img-responsive img-rounded"> -->
                            </div>
                            <div class="card-block">
                              <h4 class="card-title">{{$userVideo->title}}</h4>
                              <ul class="card-subtitle-cont">
                                <li class="card-subtitle ">{{$userVideo->created_at}}</li>
                                <li class="card-subtitle "><i class="fa fa-eye" aria-hidden="true"></i> {{$userVideo->views}} Views </li>
                                @if(Auth::check())
                                <li class="card-subtitle ">
                                @if($userVideo->isLikedByMe(Auth::id())) 
                                <span class="asset_like_unlike" data-id="{{$userVideo->id}}" data-action="unlike"><i class="fa fa-heart" aria-hidden="true" style="color: red;"></i></span>
                                <span class="asset_like_count" data-total-likes="{{count($userVideo->likes)}}" data-id="{{$userVideo->id}}" data-toggle="modal" data-target="#likesModal"> {{count($userVideo->likes)}} Likes </span>
                                @else
                                <span class="asset_like_unlike" data-id="{{$userVideo->id}}" data-action="like"><i class="fa fa-heart" aria-hidden="true"></i></span> <span class="asset_like_count" data-total-likes="{{count($userVideo->likes)}}" data-id="{{$userVideo->id}}" data-toggle="modal" data-target="#likesModal"> {{count($userVideo->likes)}} Likes </span>
                                @endif
                                </li>
                                <li class="card-subtitle showVideo" ><i class="fa fa-comment-o" aria-hidden="true"></i>{{count($userVideo->comments)}} Comment</li>
                                @endif

                              </ul>
                            </div>
                          </div>
                      </div>
                  @endif                
                @endforeach
                @if($i==0)
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 show-box"><b> No Videos </b></div>
                @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="myModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 id="video_title"></h4>                
            </div>
            <div class="modal-body scrollbar-des">
                <div id="player">Loading the player...</div>
                <p id="video_desc"></p>
                <ul id="video_tags" class="list-inline"> </ul>
                  <div class="pop-comment-cls">
                    @if(Auth::check())
                    <div class="input-comm-cls"><figure><img src="{{ Auth::user()->profile->profile_img ? asset('storage/avatars/'.Auth::user()->profile->profile_img) : asset('uploads/Dummy_User.png') }}"></figure>
                    <form action="{{ route('addAssetComment') }}" id="addCommentForm" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" id="asset_id" name="id" value="">
                      <textarea class="form-control" id="addComment" data-id="" name="comment_value" placeholder="Enter Your Comment" rows="2"></textarea>
                      
                    </form>
                    
                    </div>
                    <hr/>
                    @endif
                    <div class="comment-update-cls scrollbar-des">
                   

                    </div>

                  </div>                 
            </div>
        </div>
    </div>
</div>


<!---Profile Section --> 


@component('element.footer')
@endcomponent

@endsection

@section('specificjs')
                    


<script type="text/javascript">
  $(function() {
        $("body").on("keypress", "#addComment", function (e) {
          var commentBox = $(this);
        if(e.which == 13) {
          e.preventDefault();
          var id = $(this).attr("data-id");
          $.ajax({
            url: APP_URL+"/asset/comment/add", 
            data: {"asset_id" : id, "_token": "{{ csrf_token() }}", "comment_value": $(this).val()},
            type: "post",
            cache:false,
            dataType : "json",
            success: function(result){
            if(result.success){
              commentBox.val('');
              $('#myModal .comment-update-cls').prepend('<div class="comment-up-pro"><figure><img src="'+result.profile_image_url+'"></figure><div class="update-comment-desc"><a href="'+result.user_url+'"><h2>'+result.name+'</h2></a> <span>'+result.comment_at+'</span><p>'+result.comment+'</p></div></div>');
              }else{
              location.reload();
              }
          }});
        }
        });
      });
    $("body").on("click", '.showVideo',function(){ 
      $(this).parent().parent().parent().find('.link').trigger("click"); 
    });
    $("body").on("click",'.link', function () {
      // var videoUrl = $("#link").next("video").next("source").attr("src");
      var videoUrl = $(this).find("video > source:first").attr("src");
      var title = $(this).data("title");
      var description = $(this).data("desc");
      var video_tags = $(this).data("tags");
      var arrayOfTags = video_tags.split(",");
      var video_tags_li = '';
      var commentators_html= '';
      if(arrayOfTags.length != 0){
        $.each(arrayOfTags, function(index, value){
          video_tags_li += '<li>'+value+'</li>';
        });
      }
      var video_id = $(this).data("id");
      $('#myModal #video_title').html(title);
      $('#myModal #video_tags').html(video_tags_li);
      $('#myModal #video_desc').html(description);
      // $('#myModal input#asset_id').val(video_id);
      $('#myModal textarea#addComment').attr("data-id", video_id);
      $.ajax({
        url: APP_URL+"/asset/comments", 
        data: {"asset_id" : video_id, "_token": "{{ csrf_token() }}"},
        type: "post",
        cache:false,
        dataType : "json",
        success: function(result){
          var commentatorsInfoArray = result.commentatorsInfo;
          if(result.success){

          if(commentatorsInfoArray.length == 0){
            commentators_html = '';
          }else{
          $.each(commentatorsInfoArray, function(index, value) {
            commentators_html += '<div class="comment-up-pro"><figure><img src="'+value.profile_image_url+'"></figure><div class="update-comment-desc"><a href="'+value.user_url+'"><h2>'+value.name+'</h2></a> <span>'+value.comment_at+'</span><p>'+value.comment+'</p></div></div>';
          });
          }
          $('#myModal .comment-update-cls').html(commentators_html);
          }else{
            location.reload();
          }     
          
        }
      });
      $.ajax({
        url: APP_URL+"/asset/incrView", 
        data: {"asset_id" : video_id, "_token": "{{ csrf_token() }}"},
        type: "post",
        cache:false,
        dataType : "json",
        success: function(result){
          if(result.success){
            $(this).attr("data-action",result.action)
            .html('<i class="fa fa-heart" aria-hidden="true" style="color:'+result.color+';"></i>'+result.count +' Likes');
          }else{
          // alert("fail");
            location.reload();
          }
      }});
        $('#myModal').modal('show');
        // alert(videoUrl);
        if(videoUrl.indexOf('vimeo') !== -1){
          $('div#player').html('<iframe src="'+videoUrl+'?autoplay=1&loop=1&autopause=0" width="560" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');

        }else{

          const player = jwplayer('player').setup({
              // file: '//www.youtube.com/watch?v=VoMUf6euJcM',
              // file: '//player.vimeo.com/video/223742779',
              file: videoUrl,
              title: title,
              /*vimeo: {
                  //not required
                  enableApiData: true,
                  showControls: false

              }*/
          });

          // Listen to an event
          player.on('pause', (event) => {
              // alert('Why did my user pause their video instead of watching it?');
          });
          player.on('play', (event) => {
              // alert('Play');
          });

          // Call the API
          const bumpIt = () => {
              const vol = player.getVolume();
              player.setVolume(vol + 10);
          }
          bumpIt();
        }
    });
    /*$('#myModal').on('hidden.bs.modal', function (e) {
      player.stop();
      });*/
    $('#myModal button').click(function () {
        // $('#myModal iframe').removeAttr('src');
    });
    $("body").on("click","span.asset_like_count", function(e){
      var id = $(this).data("id");
      var html = '';
      var total_likes = $(this).attr("data-total-likes");
      if(total_likes == 0){
        e.stopPropagation();
      }
      $('#likesModal #total_likes').html('Total Likes: '+total_likes);
      $.ajax({
        url: APP_URL+"/asset/"+id+"/getLikes", 
        data: {"_token": "{{ csrf_token() }}"},
        type: "post",
        cache:false,
        dataType : "json",
        success: function(result){
          var usersInfoArray = result.usersInfo;
          if(result.success){

          if(usersInfoArray.length == 0){
            return false; 
          }else{
          $.each(usersInfoArray, function(index, value) {
            html += '<li> <figure><img src="'+value.profile_image_url+'"></figure> <a href="'+value.user_url+'"><h3>'+value.name+'</h3></a> </li>';
            html += '</li></ul></div></div></div>';
          });
          }
          $('#likesModal .user_like_list').html(html);
          }else{
            location.reload();
          }
      }});
    });

    $("body").on("click","span.asset_like_unlike", function(e){
    var btn = $(this);
    var action = $(this).attr("data-action");
    var id = $(this).data("id");
    var user_id = "{{ Auth::id() }}";
    $.ajax({
      url: APP_URL+"/asset/"+id+"/likeUnlike", 
      data: {"user_id" : user_id, "action" : action, "_token": "{{ csrf_token() }}"},
      type: "post",
      cache:false,
      dataType : "json",
      success: function(result){
        if(result.success){
          btn.attr("data-action",result.action)
          .html('<i class="fa fa-heart" aria-hidden="true" style="color:'+result.color+';"></i>')
          btn.parent().find('span.asset_like_count').html(result.count +' Likes').attr("data-total-likes",result.count);
        }else{
          // alert("fail");
          location.reload();
        }
    }});

  });
  $('button#followUser,button#unfollowUser').click(function(){
    var btn = $(this);
    var action = $(this).attr("id");
    var user_id = $(this).data('user_id');
    $.ajax({
      url: "/user/followUnfollowUser", 
      data: {"user_id" : user_id, "action" : action, "_token": "{{ csrf_token() }}"},
      type: "post",
      dataType : "json",
      success: function(result){
        if(result.success){
          btn.attr("id",result.id).html(result.btnText).css("background",result.bgcolor);
          if(action == 'followUser' && result.showMsg){
          $('button#messageUser').first().show();            
          }else{
          $('button#messageUser').hide();
          }
        }else{
          location.reload();
        }
    }});
  });

  $('button#addUser,button#removeFriend,button#cancelRequest,button#declineRequest,button#acceptRequest,button#blockUser,button#unblockUser').click(function(){
    var btn = $(this);
    var action = $(this).attr("id");
    var user_id = $(this).data('user_id');
    $.ajax({
      url: "/user/changeFriendStatus", 
      data: {"user_id" : user_id, "action" : action, "_token": "{{ csrf_token() }}"},
      type: "post",
      dataType : "json",
      success: function(result){
        if(result.success){
          if(btn.attr("id") == "declineRequest"){
            $("button#acceptRequest").hide();
          }
          if(btn.attr("id") == "acceptRequest"){
            $("button#declineRequest").hide();
          }
          if(btn.attr("id") == "blockUser"){
            $("button#unfollowUser").attr("id","followUser").html("Follow").css("background","green");
            $("button#cancelRequest").attr("id","addUser").html("Add Friend").css("background","green");
            $("button#declineRequest").attr("id","addUser").html("Add Friend").css("background","green");
            $("button#addUser,button#removeFriend,button#cancelRequest,button#declineRequest,button#acceptRequest,button#followUser,button#messageUser,button#unfollowUser").hide();
          }
          if(btn.attr("id") == "unblockUser"){
            // console.log("here");
            $("button#addUser,button#followUser,button#messageUser").show();
          }
          btn.attr("id",result.id).html(result.btnText).css("background",result.bgcolor);
        }else{
          location.reload();
        }
    }});
  });
  var div = $('source[src="{{$path}}"]').parent().parent();
$(div).trigger("click");
</script>
@endsection

<style type="text/css">
    .modal.fade .modal-dialog {
  -webkit-transition: -webkit-transform 0.3s ease-out;
     -moz-transition: -moz-transform 0.3s ease-out;
       -o-transition: -o-transform 0.3s ease-out;
          transition: transform 0.3s ease-out;
}

.modal.in .modal-dialog {

}
video::-internal-media-controls-download-button {
    display:none;
}

video::-webkit-media-controls-enclosure {
    overflow:hidden;
}

video::-webkit-media-controls-panel {
    width: calc(100% + 30px); /* Adjust as needed */
}
video::-webkit-media-controls-overlay-play-button {
  display: none;
}

</style>

