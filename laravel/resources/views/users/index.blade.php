@extends('layouts.app')
@section('content')
<div class="container" id="login-page" tabindex="-1">
  <div class="row">
    <div class="col-md-12">
<div class="panel panel-default">
<div class="panel-heading">{{$title}}</div>
<div class="panel-body">
    @forelse ($usersData as $user)
    <div class="col-sm-4">
      <a href="{{ route('singleUser', $user->name) }}"><h5>{{ $user->name }} </h5></a>      
      </div>
	@empty
	      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<p>No User Found.</p>
  </div>
	@endforelse
</div>
</div>
</div>
</div>
</div>
@endsection