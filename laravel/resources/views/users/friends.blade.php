@extends('layouts.app')
@section('content')
{{$usersData}}
<div class="container" id="login-page" tabindex="-1">
  <div class="row">
  	@forelse ($usersData as $user)
		<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 show-box">
		<div class="user-cls-div">
		<figure><img src="{{ $user->details->profile->profile_img ? asset('storage/avatars/'.$user->details->profile->profile_img) : asset('uploads/Dummy_User.png') }}" alt=""></figure>
		<a href="{{ route('singleUser', $user->details->name) }}"><h2>{{ $user->details->name }} </h2></a>

                @if($user->details->isFollowedByMe(Auth::id())) 
                <button type="button" data-user_id="{{Crypt::encryptString($user->details->id)}}" class="btn btn-follow btn-md" id="unfollowUser">Unfollow</button> 
                @else 
                  <button type="button" data-user_id="{{Crypt::encryptString($user->details->id)}}" class="btn btn-follow btn-md" id="followUser">Follow</button>
                @endif
		</div>
		</div>
	@empty
		  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <p>No User Found.</p>
  </div>
	@endforelse

  
</div>
</div>

@endsection

@section('specificjs')
<script type="text/javascript">
  $('button#followUser,button#unfollowUser').click(function(){
    var btn = $(this);
    var action = $(this).attr("id");
    var user_id = $(this).data('user_id');
    $.ajax({
      url: "/user/followUnfollowUser", 
      data: {"user_id" : user_id, "action" : action, "_token": "{{ csrf_token() }}"},
      type: "post",
      dataType : "json",
      success: function(result){
        if(result.success){
          btn.attr("id",result.id).html(result.btnText).css("background",result.bgcolor);
        }else{
          location.reload();
        }
    }});
  });
</script>
@endsection