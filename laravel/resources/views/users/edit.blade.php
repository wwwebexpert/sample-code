@extends('layouts.app')

@section('content')
<!---Profile Section -->
<div class="container-fluid" id="profile-edit" tabindex="-1">

@component('element.alert',[
  'class'=>'danger','sessionVar' => 'passChangeError'
  ])
@endcomponent

@component('element.alert',[
  'class'=>'success','sessionVar' => 'accDeactivatesuccess'
  ])
@endcomponent

@component('element.alert',[
  'class'=>'success','sessionVar' => 'updateInfoSuccess'
  ])
@endcomponent

@component('element.alert',[
  'class'=>'success','sessionVar' => 'accAactivatesuccess'
  ])
@endcomponent

  <div class="row">
    <div class="inner-header">
      <div class="container">
        <div class="col-md-6">
          <h2>My Profile</h2>
        </div>
        <div class="col-md-6 pull-right"> <div class="row"> <a class="back-profile" href="{{ route('singleUser', $userData->name)  }}"> Back to Profile</a> </div></div>
      </div>
    </div>
  </div>
  <div class="edit-profile"> 
    <!--****************tab left***********************-->
    <div class="container">
      <div class="row">
        <div class="edit-left-section">
          <div class="col-md-3">
            <ul class="nav nav-profile admin-menu" >
              <li class="active"><a href="" data-target-id="profile"><i class="fa fa-user"></i> Basic Information</a></li>
              <li><a href="" data-target-id="change-password"><i class="fa fa-lock"></i> Account Settings</a></li>
              <li><a href="" data-target-id="ontheweb"><i class="fa fa-globe" aria-hidden="true"></i>  On The Web</a></li>
              <li><a href="" data-target-id="logout"><i class="fa fa-sign-out"></i> Logout</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-9  admin-content" id="profile">
          <div class="row">
            <h2> Basic Information</h2>
            <!-- left column -->
            <div class="form-inner">
            <!-- <form class="form-horizontal" id="upload" enctype="multipart/form-data" method="post" name="basicInfoForm" action="{!! route('uploadProfileImage') !!}"  autocomplete="off">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input type="file" accept="image/*" name="avatar" id="image"/>
            <input type="submit" name="upload">
            </form> -->
            <form class="form-horizontal" id="upload" enctype="multipart/form-data" method="post" name="basicInfoForm" action="{!! route('updateBasicInfo') !!}"  autocomplete="off">
              <div class="col-md-4 col-sm-6 col-xs-12 left-profile-image">
                <div class="text-center">                
                  <div class="camera-image">                  
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
					<img class="profile-pic" src="{{ $userData->profile->profile_img ? asset('storage/avatars/'.$userData->profile->profile_img) : asset('uploads/Dummy_User.png') }}" id="user-profile" />
                    <div class="upload-button"> <i class="fa fa-camera" aria-hidden="true"></i> </div>
                  </div>
                  <input class="file-upload" type="file" accept="image/*" name="image" id="image"/>
                  
                  <h6>Upload a different photo...</h6>
					<small>Image Should be at least 300px x 300px</small> 
				 </div>                  
				 <div id="validation-errors">				  
				</div>
              </div>
              <!-- edit form column -->




              <div class="right-editinfo">
                <div class="col-md-8 col-sm-6 col-xs-12 personal-info">
                  <form class="form-horizontal" role="form" method="POST" id="basic-info-form" name="basicInfoForm" action="{!! route('updateBasicInfo') !!}">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="form-group">
                      <div class="col-lg-6">
                        <label for="name"> First Name </label>
                        <input class="form-control" placeholder="First Name" name="first_name" type="text" value="{{$userData->profile->first_name}}">
                      </div>
                      <div class="col-lg-6">
                        <label>Last name</label>
                        <input class="form-control" placeholder="Last Name" type="text" name="last_name" value="{{$userData->profile->last_name}}">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-lg-12">
                        <label>Interests</label>
                        <input type="text" value="{{$userData->profile->interest}}" name="interest" id="tags-input" data-role="tagsinput" style="display: none;" />
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-lg-12">
                        <label>About Me</label>
                        <textarea rows="4" cols="50" name="about_user" class="form-control" placeholder="Write Something about Urself" type="text">{{$userData->profile->about_user}}</textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-12">
                        <label>Date Of Birth</label>
                        <div id="datepicker" class="input-group date" data-date-format="yyyy-mm-dd">
                            <input class="form-control" type="text" readonly name="dob" id="dob" type="text" value="{{$userData->profile->dob}}"/>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                     </div>
                    </div>                 

                    <div class="form-group">
                      <div class="col-md-12">
                        <label>Comapany</label>
                        <input class="form-control" placeholder="Company Name" name="company_name"  type="text" value="{{$userData->profile->company_name}}">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-6">
                        <label>Location</label>
                        <input class="form-control" placeholder="City" type="text" name="city" value="{{$userData->profile->city}}">
                      </div>
                      <div class="col-lg-6">
                        <label>Time Zone</label>
                        @php
                        $timezone = $userData->profile->timezone;
                        @endphp


                        <div class="ui-select">
                        {{ Form::select('timezone', ['Hawaii' => '(GMT-10:00) Hawaii', 'Alaska' => '(GMT-09:00) Alaska',
                        'Pacific Time (US & Canada)' => '(GMT-08:00) Pacific Time (US & Canada)',
                        'Arizona' => '(GMT-07:00) Arizona', 'Mountain Time (US & Canada)' => '(GMT-07:00) Mountain Time (US & Canada)', 'Central Time (US & Canada)' => '(GMT-06:00) Central Time (US & Canada)',
                        'Eastern Time (US & Canada)' => '(GMT-05:00) Eastern Time (US & Canada)',
                        'Indiana (East)' => '(GMT-05:00) Indiana (East)'
                        ], $timezone, $attributes = array('id' => 'user_time_zone', 'class'=>'form-control')) }}
                          <!-- <select id="user_time_zone" class="form-control" name="timezone" value="{{$userData->profile->timezone}}">
                            <option value="Hawaii">(GMT-10:00) Hawaii</option>
                            <option value="Alaska">(GMT-09:00) Alaska</option>
                            <option value="Pacific Time (US & Canada)">(GMT-08:00) Pacific Time (US & Canada)</option>
                            <option value="Arizona">(GMT-07:00) Arizona</option>
                            <option value="Mountain Time (US & Canada)">(GMT-07:00) Mountain Time (US & Canada)</option>
                            <option value="Central Time (US & Canada)" selected="selected">(GMT-06:00) Central Time (US & Canada)</option>
                            <option value="Eastern Time (US & Canada)">(GMT-05:00) Eastern Time (US & Canada)</option>
                            <option value="Indiana (East)">(GMT-05:00) Indiana (East)</option>
                          </select> -->
                        </div>
                      </div>
                    </div>
                    <div class="form-group"> </div>
                    <div class="form-group">
                      <div class="col-md-12">
                        <label>Website Url</label>
                        <input class="form-control" placeholder="Website Url" name="website_url"  type="url" value="{{$userData->profile->website_url}}">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-12">
                        <input class="btn btn-primary" value="Save Changes" type="submit">
                        <span></span>
                        <input class="btn btn-default" value="Cancel" type="reset">
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-9  admin-content"  id="change-password">
          <div class="row">
            <h2> Account Setting</h2>
            <div class="col-md-12 form-inner">
              
                <form action="{{ route('updateBasicInfo') }}" method="post" class="form-horizontal" id="changePrivacyPolicyForm">
                {{ csrf_field() }}
                <div class="form-group">
                  <div class="col-md-12">
                    <label>Username</label>
                    <input class="form-control" value="{{$userData->name}}" type="text" disabled>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-12">
                    <label>Email</label>
                    <input class="form-control" value="{{$userData->email}}" type="email" disabled>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-12">
                    <label>Privacy</label>
                    {{ Form::select('profile_visibility', ['0' => 'Public', '1' => 'Only Friends', '2' => 'Private'
                        ], $userData->profile->profile_visibility, $attributes = array('class'=>'form-control')) }}
                  </div>
                </div>
                <!-- <input type="submit" name="submit"> -->
                </form>
                <div class="form-inner-title">
                  <h4> Change Password</h4>
                  <hr>
                </div>
                <form action="{{ route('changePassword') }}" method="post" class="form-horizontal" id="changePasswordForm" name="changePasswordForm">
              {{ csrf_field() }}
                <div class="form-group">
                  <div class="col-md-12">
                    <label>Current Password</label>
                    <input class="form-control" placeholder="Current Password" type="password" name="current_password">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-12">
                    <label>New Password</label>
                    <input class="form-control" placeholder="New Password" type="password" name="password" id="password">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-12">
                    <label>Confirm Password</label>
                    <input class="form-control" placeholder="Confirm Password" type="password" id="password-confirm" name="password_confirmation">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-12">
                    <input class="btn btn-primary" value="Save Changes" type="submit">
                    <span></span>
                    <input class="btn btn-default" value="Cancel" type="reset">
                  </div>
                </div>
              </form>
              <div class="deactive-account">
                
                @if($userData->is_activated)
                <h5> Deactive  Your Account</h5>
                <hr>
                <form id="deactiveAccount-form" action="{{ route('deactive-account') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>                
                <p> Deactivating your account will disable your profile and remove your name and photo from most things you've shared on TalentPool. Some information may still be visible to others, such as your name in their friends list and messages you sent. <a href="#">Learn more</a>. </p>
                <p> <a href="{{ route('deactive-account') }}" 
                                            onclick="event.preventDefault();
                                                     document.getElementById('deactiveAccount-form').submit();">Deactivate your account. </a></p>
                @else
                <h5> Active  Your Account</h5>
                <hr>
                <form id="activeAccount-form" action="{{ route('active-account') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form> <a href="{{ route('active-account') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('activeAccount-form').submit();">Activate your account. </a></p>
                @endif
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-9  admin-content "  id="ontheweb">
          <div class="row">
            <h2> On The Web</h2>
          </div>
          <div class="row">
            <div class="social-links-list">
              <ul>
              @foreach ($webLinks as $webLinkKey => $webLinkVal)
              @if($loop->iteration == 5)
              <span data-toggle="collapse" data-target="#remainWebLinks" class="view-more-btn"> View More <i class="fa fa-angle-down" aria-hidden="true"></i></span>
              <div id="remainWebLinks" class="collapse">
              @endif
              
                <li>
                  <div class="social-icon"> <i class="fa fa-{{$webLinkKey}}" aria-hidden="true"></i> {{ ucfirst($webLinkKey) }} </div>
                  <div class="right-social-url"> 
                  <div class="pull-right">                  
                  <div id="signup">
                    @if(!$userData->profile->$webLinkVal)
                    @php $class = 'beforeAdd'; @endphp   
                    @else
                    @php $class = 'afterAdd'; @endphp              
                    @endif

                    <div id="{{ $webLinkVal}}" class="{{$class}}">
                    <span id="{{ $webLinkVal}}">
                    &#64;{{$userData->profile->$webLinkVal}}

                    </span>
                    <a class="removeWeblinksForm" data-name="{{ ucfirst($webLinkKey) }}" id="{{ $webLinkVal}}" href="#">X</a>
                    </div>
                    @if(!$userData->profile->$webLinkVal)
                    <button class="addlink">Link</button>
                    @endif
                    <form action="{{route('updateWebLinks')}}" method="POST" id="{{ ucfirst($webLinkKey) }}" class="webLinkForm">
                    {{ csrf_field() }}
                    <div class="form-group">
                    <input type="hidden" name="service_provider" value="{{ $webLinkVal}}">
                    <input name="{{ $webLinkVal}}" type="text" class="form-control webLinkVal" placeholder="Username" required="required">
                    <a class="submitWeblinksForm" data-name="{{ ucfirst($webLinkKey) }}" href="#">Submit</a>
                    <input class="closebtn" type="reset" value="X" name="register">
                    </div>
                    </form>                             
                  </div>
                  </div>
                  </div>
                </li>
            @if($loop->iteration ==5)</div>@endif
              @endforeach
                
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-9  admin-content" id="logout">
          <div class="row">
            <div style="margin: 1em;">
           
                <h3 >Confirm Logout</h3>
            
              <div> Do you really want to logout ?
              
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>

                                           <a href="{{ route('logout') }}" class="label label-danger"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <span >   Yes   </span>
                                        </a>     <a href="{{ route('authUserProfile')  }}" class="label label-success"> <span >  No   </span></a> </div>
              
            </div>

            @forelse($userData->assets as $profileImages => $profileImage)
            <div class="col-md-3">
            
             <figure class="pic-gall-cls"> <img class="profile-pic " src="{{asset('storage/avatars/'.$profileImage->path) }}" id="user-profile"> </figure>
            </div>
            @empty
            <div class="col-md-12"> <h4> No profile pic </h4> </div>
            @endforelse            
          </div>
        </div>
      </div>
      
      <!--****************end tab left***********************--> 
      
    </div>
  </div>
</div>
<!---Profile Section --> 


@component('element.footer')
@endcomponent
@endsection