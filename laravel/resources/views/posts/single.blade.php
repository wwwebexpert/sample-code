@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Post <span class="pull-right">By <a href="{{route('singleUser',[$postData->author->id])}}">{{$postData->author->name}}</a> at {{ Carbon\Carbon::parse($postData->created_at)->format('F j, Y') }}</span></div>
                <div class="panel-body">                

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="col-md-4 control-label">Title</label>

                            <div class="col-md-6">
                               <label for="title" class="col-md-4 control-label">{{$postData->title}}</label>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                            <label for="content" class="col-md-4 control-label">Content</label>

                            <div class="col-md-6">
                                <label for="content" class="col-md-4 control-label">{{$postData->content}}</label>
                                @if(Auth::id() == $postData->author->id)
    <a href="{{ route('editPost', $postData->getRouteKey()) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
@endif
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection