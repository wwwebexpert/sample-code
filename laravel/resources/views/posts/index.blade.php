@extends('layouts.app')

@section('content')
<div class="container">

  <div class="row">
  @if(Auth::id())
  <a href="{{route('createPost')}}">
  <button type="button" class="btn btn-primary pull-right">
  Add Post
  </button>
  </a>
@endif

    @foreach ($postsData as $post)
    <div class="col-sm-4">
      <a href="{{ route('singlePost', $post->getRouteKey()) }}"><h3>{{ $post->title }} </h3></a>
        <p>{{ $post->content }}</p>
        @if(Auth::id() == $post->author->id)
        <span class="pull-left">    <a href="{{ route('editPost', $post->getRouteKey()) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
</span>
        <span class="pull-right">{{$post->author->name}} @ {{ Carbon\Carbon::parse($post->created_at)->format('F j, Y') }}</span>
        @else
        <span class="pull-right">
        <b title="Contact Person" style="cursor: pointer;" class="small-box-footer" data-post_id = "{{$post->id}}" data-toggle="modal" data-target="#Portfolio" id="openContactForm">{{$post->author->name}} </b>
         @ {{ Carbon\Carbon::parse($post->created_at)->format('F j, Y') }}</span>
        @endif
        
      </div>
    @endforeach
  </div>
</div>  
@component('element.contact-person')

@endcomponent
@endsection