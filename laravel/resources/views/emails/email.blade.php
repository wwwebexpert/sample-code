<h1>Click the Button/Link To Verify Your Email</h1>
<p>You are receiving this email because we received a user account registration request for this email address.</p></br>
<a href="{{url('/verifyemail/'.$email_token)}}" target="_blank" rel="noopener noreferrer" class="x_button x_button-blue" style="font-family:Avenir,Helvetica,sans-serif; border-radius:3px; color:#FFF; display:inline-block; text-decoration:none; background-color:#3097D1; border-top:10px solid #3097D1; border-right:18px solid #3097D1; border-bottom:10px solid #3097D1; border-left:18px solid #3097D1">Verify Account</a>
<p>
If you’re having trouble clicking the "Verify Account" button, copy and paste the URL below into your web browser: </br> {{url('/verifyemail/'.$email_token)}} </p>