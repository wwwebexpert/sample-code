@if (@Session::has("$sessionVar"))
<div class="alert alert-{{ $class }} alert-dismissable" id="{{ $sessionVar }}">
 <button type="button" class="close" data-dismiss = "alert" aria-hidden = "true">
    &times;
 </button>

 {!! session("$sessionVar") !!}
</div>
@endif