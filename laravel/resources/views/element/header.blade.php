 <!-- Header -->
    <header>
    <section class="main-head-banner">
        <div class="container" id="maincontent" tabindex="-1">
        <?php //print_r(session()); ?>
@if (Session::has('loginMsg'))
<div class = "alert alert-success alert-dismissable" id="loginMsg">
 <button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
    &times;
 </button>

 {!! session('loginMsg') !!}
</div>
@endif
@if (Session::has('ResetSuccessfully'))
<div class = "alert alert-success alert-dismissable" id="ResetSuccessfully">
 <button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
    &times;
 </button>

 {!! session('ResetSuccessfully') !!}
</div>
@endif
@if (Session::has('passChangeSuccess'))
<div class = "alert alert-success alert-dismissable" id="passChangeSuccess">
 <button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
    &times;
 </button>

 {!! session('passChangeSuccess') !!}
</div>
@endif
@if (Session::has('ResetError'))
<div class = "alert alert-danger alert-dismissable" id="ResetError">
 <button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
    &times;
 </button>

 {!! session('ResetError') !!}
</div>
@endif
            <div class="row">
                <div>
                    <div class="intro-text">
<h3 class="name">{{$homeContent->first_section_heading}}</h3>
    <div id="morphsearch" class="morphsearch">
      <form class="morphsearch-form" action="search-results" method="POST">
      <input type="hidden" name="_token" value="{{csrf_token()}}">
        <input name="search" class="morphsearch-input" type="search" placeholder="Search Talents..." required="required" />
        @if($errors->has('search'))
        @foreach ($errors->all() as $error)
        <div style="color: black;">{{ $error }}</div>
        @endforeach
        @endif

      </form>
    <span class="morphsearch-close"></span>
    </div><!-- /morphsearch -->
      <header class="codrops-header">
      </header>
      <div class="overlay"></div>
                       <span class="skills">Are you an Artist?<a href="#"> Discover how to join with us </a></span>
                    </div>
                </div>
            </div>
        </div>
        </section>
    </header>
