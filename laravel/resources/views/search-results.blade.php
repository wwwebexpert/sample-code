@extends('layouts.app')
@section('content')
<!---Profile Section ---->

<div class="top-bar">
  <div class="container-fluid">
    <div class="row">
      <div class="search-section sub-search-section">
        <div class="container"><input class="advance-search" type="search" placeholder="Search Talents..." data-role="tagsinput">
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container search-sub-cls" id="search-results" tabindex="-1">
  <div class="row">
    <div class="col-lg-12">
      
    <div class="tabbable-panel">
            <div class="tabbable-line">
              <ul class="nav nav-tabs ">
                <li class="active"> <a href="#tab_default_1" data-toggle="tab"> Videos  </a> </li>
                <li> <a href="#tab_default_2" data-toggle="tab"> Users </a> </li>
                
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="tab_default_1"> 
                  @php $i = 0 @endphp
                  @foreach($videos as $userVideos => $userVideo)
                  @if($userVideo->assets_visibility != "0")
                    @if(Auth::check())
                    @if(Auth::id() != $userVideo->user_id)
                      @if($userVideo->owner->isFollowedByMe(Auth::id()))
                      @php $i = 1 @endphp
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 show-box">
                          <div class="card cart-block">
                            <div class="card-img link" id="link" data-title="{{$userVideo->title}}" data-desc="{{$userVideo->about}}" data-tags="{{$userVideo->tags}}" data-id="{{Crypt::encryptString($userVideo->id)}}">
                            @php $thumbnail_url = ''; @endphp
                              @if($userVideo->upload_type && (strpos($userVideo->path, 'youtube')))                             
                              @php preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $userVideo->path, $matches);
                              $thumbnail_url = 'https://img.youtube.com/vi/'.$matches[1].'/sddefault.jpg'; 
                              @endphp
                              @endif

                              @if($userVideo->upload_type && (strpos($userVideo->path, 'vimeo')))
                              @php
                              $thumbnail_url = json_decode(file_get_contents('https://vimeo.com/api/oembed.json?url='.$userVideo->path))->thumbnail_url;                              
                              @endphp

                              <video width="100%" height="auto" poster="{{$thumbnail_url}}">
                                <source src="{{$userVideo->path}}" type="video/mp4">
                              </video>
                              <!-- <iframe src="https://player.vimeo.com/video/12345?autoplay=0&loop=1&autopause=0" width="560" height="240" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> -->

                              @else 
                              <video width="100%" height="auto" poster="{{$thumbnail_url}}">
                              @if($userVideo->upload_type)
                              <source src="{{$userVideo->path}}" type="video/mp4">
                              @else
                              <source src="{{asset('storage/'.$userVideo->path) }}" type="video/mp4">
                              @endif
                                
                                <span> Your browser does not support the <video> tag</span>
                              </video>
                              @endif
                            <!--   <img src="img/live03.jpg" class="card-img-top img-responsive img-rounded"> -->
                            </div>
                            <div class="card-block">
                              <h4 class="card-title">{{$userVideo->title}}</h4>
                              <ul class="card-subtitle-cont">
                                <li class="card-subtitle ">{{$userVideo->created_at}}</li>
                                <li class="card-subtitle "><i class="fa fa-eye" aria-hidden="true"></i> {{$userVideo->views}} Views </li>

                                <li class="card-subtitle ">
                                @if($userVideo->isLikedByMe(Auth::id())) 
                                <span class="asset_like_unlike" data-id="{{$userVideo->id}}" data-action="unlike"><i class="fa fa-heart" aria-hidden="true" style="color: red;"></i></span>
                                <span class="asset_like_count" data-total-likes="{{count($userVideo->likes)}}" data-id="{{$userVideo->id}}" data-toggle="modal" data-target="#likesModal" > {{count($userVideo->likes)}} Likes </span>
                                @else
                                <span class="asset_like_unlike" data-id="{{$userVideo->id}}" data-action="like"><i class="fa fa-heart" aria-hidden="true"></i></span> <span class="asset_like_count" data-total-likes="{{count($userVideo->likes)}}" data-id="{{$userVideo->id}}" data-toggle="modal" data-target="#likesModal"> {{count($userVideo->likes)}} Likes </span>
                                @endif

                                </li>
                                <li class="card-subtitle showVideo" ><i class="fa fa-comment-o" aria-hidden="true"></i>{{count($userVideo->comments)}} Comment</li>
                              </ul>
                            </div>
                          </div>
                        </div>                        
                      @endif
                      @else
                      @php $i = 1 @endphp
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 show-box">
                          <div class="card cart-block">
                            <div class="card-img link" id="link" data-title="{{$userVideo->title}}" data-desc="{{$userVideo->about}}" data-tags="{{$userVideo->tags}}" data-id="{{Crypt::encryptString($userVideo->id)}}">
                            @php $thumbnail_url = ''; @endphp
                              @if($userVideo->upload_type && (strpos($userVideo->path, 'youtube')))                             
                              @php preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $userVideo->path, $matches);
                              $thumbnail_url = 'https://img.youtube.com/vi/'.$matches[1].'/sddefault.jpg'; 
                              @endphp
                              @endif

                              @if($userVideo->upload_type && (strpos($userVideo->path, 'vimeo')))
                              @php
                              $thumbnail_url = json_decode(file_get_contents('https://vimeo.com/api/oembed.json?url='.$userVideo->path))->thumbnail_url;                              
                              @endphp

                              <video width="100%" height="auto" poster="{{$thumbnail_url}}">
                                <source src="{{$userVideo->path}}" type="video/mp4">
                              </video>
                              <!-- <iframe src="https://player.vimeo.com/video/12345?autoplay=0&loop=1&autopause=0" width="560" height="240" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> -->

                              @else 
                              <video width="100%" height="auto" poster="{{$thumbnail_url}}">
                              @if($userVideo->upload_type)
                              <source src="{{$userVideo->path}}" type="video/mp4">
                              @else
                              <source src="{{asset('storage/'.$userVideo->path) }}" type="video/mp4">
                              @endif
                                
                                <span> Your browser does not support the <video> tag</span>
                              </video>
                              @endif
                            <!--   <img src="img/live03.jpg" class="card-img-top img-responsive img-rounded"> -->
                            </div>
                            <div class="card-block">
                              <h4 class="card-title">{{$userVideo->title}}</h4>
                              <ul class="card-subtitle-cont">
                                <li class="card-subtitle ">{{$userVideo->created_at}}</li>
                                <li class="card-subtitle "><i class="fa fa-eye" aria-hidden="true"></i> {{$userVideo->views}} Views </li>
                               <li class="card-subtitle ">
                                @if($userVideo->isLikedByMe(Auth::id())) 
                                <span class="asset_like_unlike" data-id="{{$userVideo->id}}" data-action="unlike"><i class="fa fa-heart" aria-hidden="true" style="color: red;"></i></span>
                                <span class="asset_like_count" data-total-likes="{{count($userVideo->likes)}}" data-id="{{$userVideo->id}}" data-toggle="modal" data-target="#likesModal" > {{count($userVideo->likes)}} Likes </span>
                                @else
                                <span class="asset_like_unlike" data-id="{{$userVideo->id}}" data-action="like"><i class="fa fa-heart" aria-hidden="true"></i></span> <span class="asset_like_count" data-total-likes="{{count($userVideo->likes)}}" data-id="{{$userVideo->id}}" data-toggle="modal" data-target="#likesModal"> {{count($userVideo->likes)}} Likes </span>
                                @endif

                                </li>
                                <li class="card-subtitle showVideo" ><i class="fa fa-comment-o" aria-hidden="true"></i>{{count($userVideo->comments)}} Comment</li>
                              </ul>
                            </div>
                          </div>
                        </div>
                    @endif
                    @endif
                    @else
                    @php $i = 1 @endphp
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 show-box">
                          <div class="card cart-block">
                             <div class="card-img link" id="link" data-title="{{$userVideo->title}}" data-desc="{{$userVideo->about}}" data-tags="{{$userVideo->tags}}" data-id="{{Crypt::encryptString($userVideo->id)}}">
                            @php $thumbnail_url = ''; @endphp
                              @if($userVideo->upload_type && (strpos($userVideo->path, 'youtube')))                             
                              @php preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $userVideo->path, $matches);
                              $thumbnail_url = 'https://img.youtube.com/vi/'.$matches[1].'/sddefault.jpg'; 
                              @endphp
                              @endif

                              @if($userVideo->upload_type && (strpos($userVideo->path, 'vimeo')))
                              @php
                              $thumbnail_url = json_decode(file_get_contents('https://vimeo.com/api/oembed.json?url='.$userVideo->path))->thumbnail_url;                              
                              @endphp

                              <video width="100%" height="auto" poster="{{$thumbnail_url}}">
                                <source src="{{$userVideo->path}}" type="video/mp4">
                              </video>
                              <!-- <iframe src="https://player.vimeo.com/video/12345?autoplay=0&loop=1&autopause=0" width="560" height="240" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> -->

                              @else 
                              <video width="100%" height="auto" poster="{{$thumbnail_url}}">
                              @if($userVideo->upload_type)
                              <source src="{{$userVideo->path}}" type="video/mp4">
                              @else
                              <source src="{{asset('storage/'.$userVideo->path) }}" type="video/mp4">
                              @endif
                                
                                <span> Your browser does not support the <video> tag</span>
                              </video>
                              @endif
                            <!--   <img src="img/live03.jpg" class="card-img-top img-responsive img-rounded"> -->
                            </div>
                            <div class="card-block">
                              <h4 class="card-title">{{$userVideo->title}}</h4>
                              <ul class="card-subtitle-cont">
                                <li class="card-subtitle ">{{$userVideo->created_at}}</li>
                                <li class="card-subtitle "><i class="fa fa-eye" aria-hidden="true"></i> {{$userVideo->views}} Views </li>
                                @if(Auth::check())
                                <li class="card-subtitle ">
                                @if($userVideo->isLikedByMe(Auth::id())) 
                                <span class="asset_like_unlike" data-id="{{$userVideo->id}}" data-action="unlike"><i class="fa fa-heart" aria-hidden="true" style="color: red;"></i></span>
                                <span class="asset_like_count" data-total-likes="{{count($userVideo->likes)}}" data-id="{{$userVideo->id}}" data-toggle="modal" data-target="#likesModal" > {{count($userVideo->likes)}} Likes </span>
                                @else
                                <span class="asset_like_unlike" data-id="{{$userVideo->id}}" data-action="like"><i class="fa fa-heart" aria-hidden="true"></i></span> <span class="asset_like_count" data-total-likes="{{count($userVideo->likes)}}" data-id="{{$userVideo->id}}" data-toggle="modal" data-target="#likesModal"> {{count($userVideo->likes)}} Likes </span>
                                @endif

                                </li>
                                <li class="card-subtitle showVideo" ><i class="fa fa-comment-o" aria-hidden="true"></i>{{count($userVideo->comments)}} Comment</li>
                                @endif
                              </ul>
                            </div>
                          </div>
                      </div>
                  @endif                
                @endforeach
                @if($i==0)
                <p>No videos</p>
                @endif
                </div>
                <div class="tab-pane" id="tab_default_2">
                @forelse($users as $key => $singleUser)
                
                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 show-box">
                    <div class="user-cls-div">
                    <figure><img src="{{ $singleUser->profile->profile_img ? asset('storage/avatars/'.$singleUser->profile->profile_img) : asset('uploads/Dummy_User.png') }}" alt=""></figure>
                    <a href="{{ route('singleUser', $singleUser->name) }}"><h2>{{ $singleUser->name }} </h2></a>

                    @if(Auth::check()) 
                    @if(Auth::id() != $singleUser->id) 
                    @if($singleUser->isFollowedByMe(Auth::id())) 
                    <button type="button" data-user_id="{{Crypt::encryptString($singleUser->id)}}" class="btn btn-follow btn-md" id="unfollowUser">Unfollow</button> 
                    @else 
                    <button type="button" data-user_id="{{Crypt::encryptString($singleUser->id)}}" class="btn btn-follow btn-md" id="followUser">Follow</button>
                    @endif
                    @endif
                    @endif
                    </div>
                    </div>

                @empty
                <p>No users</p>
                @endforelse
                </div>              
              </div>
            </div>
          </div>
    
    </div>

                  
  </div>
</div>

<div id="myModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 id="video_title"></h4>
                
            </div>
            <div class="modal-body scrollbar-des">
                <div id="player">Loading the player...</div>
                <p id="video_desc"></p>
                <ul id="video_tags" class="list-inline"> </ul>
                  <div class="pop-comment-cls">
                    @if(Auth::check())
                    <div class="input-comm-cls"><figure><img src="{{ Auth::user()->profile->profile_img ? asset('storage/avatars/'.Auth::user()->profile->profile_img) : asset('uploads/Dummy_User.png') }}"></figure>
                    <form action="{{ route('addAssetComment') }}" id="addCommentForm" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" id="asset_id" name="id" value="">
                      <textarea class="form-control" id="addComment" data-id="" name="comment_value" placeholder="Enter Your Comment" rows="2"></textarea>
                      
                    </form>
                    
                    </div>
                    <hr/>
                    @endif
                    <div class="comment-update-cls scrollbar-des">
                   

                    </div>

                  </div>                 
            </div>
        </div>
    </div>
</div>

<!---Profile Section --> 
@component('element.footer')
@endcomponent

@endsection

@section('specificjs')

<script type="text/javascript">

$('input.advance-search').tagsinput({
  maxTags: 8,
  maxChars: 8,
  trimValue: true,
  onTagExists: function(item, $tag) {
    $tag.hide().fadeIn();
  }
});
var tagToAdd = "<?php echo isset($_POST['search']) ? $_POST['search'] : ''; ?>";
if(tagToAdd.length){
  $('input.advance-search').tagsinput('add', tagToAdd);
}
$('input.advance-search').on('itemAdded itemRemoved', function(event) {
    if(!$("input.advance-search").tagsinput('items').length){
          $("div#tab_default_1").html('<p>Please add tag for searching.</p>');
          $("div#tab_default_2").html('<p>Please add tag for searching.</p>');
          return false;
    }
    var tag = event.item;
    var html = "", srcForVideo;
    var htmlForUsers = "";
    var ajaxData = {
    type : "POST", 
    dataType: 'json',
    data:{"title":$("input.advance-search").tagsinput('items'), "_token":'{{csrf_token()}}'},
    success: function(response){
        if(response.success){
          var videosArray = response.videos;
          var usersArray = response.users;
          if(videosArray.length == 0){
           html = '<p>No videos</p>'; 
          }else{
          $.each(videosArray, function(index, value) {
            if(value.upload_type){
              srcForVideo = value.path;
            }else{
              srcForVideo = APP_URL+'/storage/'+value.path;
            }
            html += '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 show-box"><div class="card cart-block"><div class="card-img link" id="link" data-title="'+value.title +'" data-desc="'+value.about +'" data-tags="'+value.tags +'" data-id="'+value.encryptId +'"><video width="100%" height="auto" poster="'+value.thumbnail_url +'"><source src="'+srcForVideo+'" type="video/mp4"><span> Your browser does not support the <video> tag</span></video></div><div class="card-block"><h4 class="card-title">'+value.title +'</h4><ul class="card-subtitle-cont"><li class="card-subtitle ">'+value.created_at +'</li><li class="card-subtitle "><i class="fa fa-eye" aria-hidden="true"></i>'+value.views +' Views </li><li class="card-subtitle">';
            if(value.likedByMe != 'notShown'){
            if(value.likedByMe == true){
              html += ' <span class="asset_like_unlike" data-id="'+value.id +'" data-action="unlike"><i class="fa fa-heart" aria-hidden="true" style="color: red;"></i></span><span class="asset_like_count" data-total-likes="'+value.total_likes+'" data-id="'+value.encryptId +'" data-toggle="modal" data-target="#likesModal" > '+value.total_likes+' Likes</span></li></li><li class="card-subtitle showVideo" ><i class="fa fa-comment-o" aria-hidden="true"></i>'+value.total_comments +' Comment</li>';
            }else{
              html += '<span class="asset_like_unlike" data-id="'+value.id +'" data-action="like"><i class="fa fa-heart" aria-hidden="true"></i></span><span class="asset_like_count" data-total-likes="'+value.total_likes+'" data-id="'+value.encryptId +'" data-toggle="modal" data-target="#likesModal" > '+value.total_likes +' Likes</span></li><li class="card-subtitle showVideo" ><i class="fa fa-comment-o" aria-hidden="true"></i>'+value.total_comments +' Comment</li>';
            }
          }
          html += '</ul></div></div></div>';

          });
          }
          if(usersArray.length == 0){
           htmlForUsers = '<p>No users</p>'; 
          }else{
          $.each(usersArray, function(index, value) {
            htmlForUsers += '<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 show-box"><div class="user-cls-div"><figure><img src="'+value.profile_pic_url+'" alt=""></figure><a href="'+APP_URL+'/'+value.name+'"><h2>'+value.name+' </h2></a>';

            if(value.followedByMe != 'notShown'){
            if(value.followedByMe == true){
              htmlForUsers += '<button type="button" data-user_id="'+value.encryptId+'" class="btn btn-follow btn-md" id="unfollowUser">Unfollow</button>';
            }else{
              htmlForUsers += '<button type="button" data-user_id="'+value.encryptId+'" class="btn btn-follow btn-md" id="followUser">Follow</button>';
            }
          }
          htmlForUsers += '</div></div>';

          });
        }
          
           $("div#tab_default_1").html(html);
           $("div#tab_default_2").html(htmlForUsers);
        }else{
          location.reload();
        }
      } 
    };
    $.ajax(APP_URL+'/search/advance', ajaxData, function(response) {

   alert(response);
    });
});

  $(function() {
        $("body").on("keypress", "#addComment", function (e) {
          var commentBox = $(this);
        if(e.which == 13) {
          e.preventDefault();
          var id = $(this).attr("data-id");
          $.ajax({
            url: APP_URL+"/asset/comment/add", 
            data: {"asset_id" : id, "_token": "{{ csrf_token() }}", "comment_value": $(this).val()},
            type: "post",
            cache:false,
            dataType : "json",
            success: function(result){
            if(result.success){
              commentBox.val('');
              $('#myModal .comment-update-cls').prepend('<div class="comment-up-pro"><figure><img src="'+result.profile_image_url+'"></figure><div class="update-comment-desc"><a href="'+result.user_url+'"><h2>'+result.name+'</h2></a> <span>'+result.comment_at+'</span><p>'+result.comment+'</p></div></div>');
              }else{
              location.reload();
              }
          }});
        }
        });
      });
    $("body").on("click", '.showVideo',function(){ 
      $(this).parent().parent().parent().find('.link').trigger("click"); 
    });
    $("body").on("click",'.link', function () {
      // var videoUrl = $("#link").next("video").next("source").attr("src");
      var videoUrl = $(this).find("video > source:first").attr("src");
      var title = $(this).data("title");
      var description = $(this).data("desc");
      var video_tags = $(this).data("tags");
      var arrayOfTags = video_tags.split(",");
      var video_tags_li = '';
      var commentators_html= '';
      if(arrayOfTags.length != 0){
        $.each(arrayOfTags, function(index, value){
          video_tags_li += '<li>'+value+'</li>';
        });
      }
      var video_id = $(this).data("id");
      $('#myModal #video_title').html(title);
      $('#myModal #video_tags').html(video_tags_li);
      $('#myModal #video_desc').html(description);

      $('#myModal textarea#addComment').attr("data-id", video_id);
      $.ajax({
        url: APP_URL+"/asset/comments", 
        data: {"asset_id" : video_id, "_token": "{{ csrf_token() }}"},
        type: "post",
        cache:false,
        dataType : "json",
        success: function(result){
          var commentatorsInfoArray = result.commentatorsInfo;
          if(result.success){

          if(commentatorsInfoArray.length == 0){
            commentators_html = '';
          }else{
          $.each(commentatorsInfoArray, function(index, value) {
            commentators_html += '<div class="comment-up-pro"><figure><img src="'+value.profile_image_url+'"></figure><div class="update-comment-desc"><a href="'+value.user_url+'"><h2>'+value.name+'</h2></a> <span>'+value.comment_at+'</span><p>'+value.comment+'</p></div></div>';
          });
          }
          $('#myModal .comment-update-cls').html(commentators_html);
          }else{
            location.reload();
          }     
          
        }
      });
      

      $.ajax({
        url: APP_URL+"/asset/incrView", 
        data: {"asset_id" : video_id, "_token": "{{ csrf_token() }}"},
        type: "post",
        cache:false,
        dataType : "json",
        success: function(result){
          if(result.success){
            $(this).attr("data-action",result.action)
            .html('<i class="fa fa-heart" aria-hidden="true" style="color:'+result.color+';"></i>'+result.count +' Likes');
          }else{
          // alert("fail");
            location.reload();
          }
      }});
        $('#myModal').modal('show');
        if(videoUrl.indexOf('vimeo') !== -1){
          $('div#player').html('<iframe src="'+videoUrl+'?autoplay=1&loop=1&autopause=0" width="560" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');

        }else{

          const player = jwplayer('player').setup({
              // file: '//www.youtube.com/watch?v=VoMUf6euJcM',
              // file: '//player.vimeo.com/video/223742779',
              file: videoUrl,
              title: title,
              /*vimeo: {
                  //not required
                  enableApiData: true,
                  showControls: false

              }*/
          });

          // Listen to an event
          player.on('pause', (event) => {
              // alert('Why did my user pause their video instead of watching it?');
          });
          player.on('play', (event) => {
              // alert('Play');
          });

          // Call the API
          const bumpIt = () => {
              const vol = player.getVolume();
              player.setVolume(vol + 10);
          }
          bumpIt();
        }
    });

    $('#myModal button').click(function () {
        // $('#myModal iframe').removeAttr('src');
    });
    $("body").on("click","span.asset_like_count", function(e){
      var id = $(this).data("id");
      var html = '';
      var total_likes = $(this).attr("data-total-likes");
      if(total_likes == 0){
        e.stopPropagation();
      }
      $('#likesModal #total_likes').html('Total Likes: '+total_likes);
      $.ajax({
        url: APP_URL+"/asset/"+id+"/getLikes", 
        data: {"_token": "{{ csrf_token() }}"},
        type: "post",
        cache:false,
        dataType : "json",
        success: function(result){
          var usersInfoArray = result.usersInfo;
          if(result.success){

          if(usersInfoArray.length == 0){
            return false; 
          }else{
          $.each(usersInfoArray, function(index, value) {
            html += '<li> <figure><img src="'+value.profile_image_url+'"></figure> <a href="'+value.user_url+'"><h3>'+value.name+'</h3></a> </li>';
            html += '</li></ul></div></div></div>';
          });
          }
          $('#likesModal .user_like_list').html(html);
          }else{
            location.reload();
          }
      }});
      });
    $("body").on("click","span.asset_like_unlike", function(e){
    var btn = $(this);
    var action = $(this).attr("data-action");
    var id = $(this).data("id");
    var user_id = "{{ Auth::id() }}";
    $.ajax({
      url: APP_URL+"/asset/"+id+"/likeUnlike", 
      data: {"user_id" : user_id, "action" : action, "_token": "{{ csrf_token() }}"},
      type: "post",
      cache:false,
      dataType : "json",
      success: function(result){
        if(result.success){
          btn.attr("data-action",result.action)
          .html('<i class="fa fa-heart" aria-hidden="true" style="color:'+result.color+';"></i>')
          btn.parent().find('span.asset_like_count').html(result.count +' Likes').attr("data-total-likes",result.count);
        }else{
          // alert("fail");
          location.reload();
        }
    }});

  });
    $("body").on("click",'button#followUser,button#unfollowUser',function(){
    var btn = $(this);
    var action = $(this).attr("id");
    var user_id = $(this).data('user_id');
    $.ajax({
      url: "/user/followUnfollowUser", 
      data: {"user_id" : user_id, "action" : action, "_token": "{{ csrf_token() }}"},
      type: "post",
      dataType : "json",
      success: function(result){
        if(result.success){
          btn.attr("id",result.id).html(result.btnText).css("background",result.bgcolor);
        }else{
          location.reload();
        }
    }});
  });

  </script>
@endsection


<style type="text/css">
    .modal.fade .modal-dialog {
  -webkit-transition: -webkit-transform 0.3s ease-out;
     -moz-transition: -moz-transform 0.3s ease-out;
       -o-transition: -o-transform 0.3s ease-out;
          transition: transform 0.3s ease-out;
}

.modal.in .modal-dialog {

}
video::-internal-media-controls-download-button {
    display:none;
}

video::-webkit-media-controls-enclosure {
    overflow:hidden;
}

video::-webkit-media-controls-panel {
    width: calc(100% + 30px); /* Adjust as needed */
}
video::-webkit-media-controls-overlay-play-button {
  display: none;
}

</style>