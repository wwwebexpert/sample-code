<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Talent Pool') }} - Explore the Talent </title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/talentstyle.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">    

    <!-- Custom Fonts -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" rel="stylesheet" type="text/css">
    <!-- For Notification -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/css/bootstrap-notify.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" />
    <link href="http://www.jqueryscript.net/demo/Elegant-Customizable-jQuery-PHP-File-Uploader-Fileuploader/css/jquery.fileuploader-theme-dragdrop.css" media="all" rel="stylesheet">
    <link href="{{ asset('css/jquery.steps.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link href="http://www.jqueryscript.net/demo/Elegant-Customizable-jQuery-PHP-File-Uploader-Fileuploader/jquery.fileuploader.css" media="all" rel="stylesheet">

    <script src="https://use.fontawesome.com/ddf0bb085a.js"></script>
    <script src="//{{ Request::getHost() }}:6001/socket.io/socket.io.js"></script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->  
    <script type="text/javascript">
      var APP_URL = {!! json_encode(url('/')) !!}
      window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
            'user' =>  auth()->user()
        ]) !!};
      var fetchChatURL = null;
    </script>
</head>

<body id="page-top" class="index">
  <div id="skipnav"><a href="#maincontent">Skip to main content</a></div>
    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}"><h3>{{ config('app.name', 'Laravel') }}</h3>
                  <small>Explore the Talent </small>
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                  <li class="hidden">
                      <a href="#page-top"></a>
                  </li>
                  <li class="page-scroll">
                      <a href="#portfolio">BROWSE TALENT</a>
                  </li>
                  <li class="page-scroll">
                      <a href="#about">BLOG</a>
                  </li>
                  <li class="page-scroll">
                      <a href="#contact">Contact</a>
                  </li>
                  
                  <li class="dropdown page-scroll"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Users <span class="caret"></span></a>
                    <ul class="dropdown-menu" style="background-color: #0d164d;">
                    @if (Auth::guest())
                      <li class="page-scroll">
                        <a href="{{ route('all_users') }}">All Users</a>
                      </li>
                      @else
                      <!-- <li class="page-scroll">
                        <a href="{{ route('getUserFriends', Auth::user()->name) }}">Friends</a>
                      </li> -->
                      <li class="page-scroll">
                        <a href="{{ route('getUserFollowers', Auth::user()->name) }}">Followers</a>
                      </li>
                      <li class="page-scroll">
                        <a href="{{ route('getUserFollowing', Auth::user()->name) }}">Following</a>
                      </li>
                      @endif
                    </ul>
                  </li>
                </ul>
                
                <ul class="nav navbar-nav navbar-right">
          @if (Auth::check())
            <li class="hidden-xs notifications-cont"> <a href="#" class="dropdown-toggle dk readNotification" data-toggle="dropdown"> <i class="fa fa-bell"></i>  </a>
            <section class="dropdown-menu aside-xl">
            <section class="panel bg-white">
            <header class="panel-heading b-light bg-light"> 
              
              <span class="clearNotification" style="cursor: pointer;" title="clear notifications"><i class="fa fa-trash-o" aria-hidden="true"></i></span>

              </header>
              <div class="list-group list-group-alt animated fadeInRight"> 
              @php $notifications = [];
                foreach(Auth::user()->notifications as $notification):
                    if($notification->type == 'App\Notifications\UserConnection'){
                    $user = App\User::where('id',$notification->data['broad_user_id'])->first();
                    $notifications[] = array(
                      'created_at' => $notification->created_at->diffForHumans(),
                      'broad_user_name' => trim($user->profile->first_name ?  $user->profile->first_name.' '. $user->profile->last_name : $user->name),
                      'broad_user_image' => $user->profile->profile_img ? asset('storage/avatars/'.$user->profile->profile_img) : asset('uploads/Dummy_User.png'),
                      'broad_user_url' => route('singleUser' , $user->name)
                    );
                    }
              
              endforeach; 
              @endphp
               
              @forelse($notifications as $notification)

                <a href="{{$notification['broad_user_url']}}" target="_blank" class="media list-group-item"> 
                <span class="pull-left thumb-sm"> <i class="fa fa-user-circle-o" aria-hidden="true"></i> </span> 
                <span class="media-body block m-b-none"> {{$notification['broad_user_name']}} starting following you!<br><small class="text-muted">{{$notification['created_at']}}</small></span>
                </a> 

              @empty
                No Notification. 
              @endforelse             
              </div>

              <footer class="panel-footer text-sm">  <a href="{{ route('getAllNotification' , Auth::user()->name)  }}" data-toggle="class:show animated fadeInRight">See all the notifications</a> </footer>
             
              </section>
              </section>
            </li>
            @endif

            <li class="page-scroll right-sign-menu">
            @if (Auth::guest())
            <a href="{{ route('login') }}"><i class="fa fa-user-circle-o" aria-hidden="true"></i>Sign In</a>                        
            @else
            <span><a href="{{ route('singleUser' , Auth::user()->name)  }}" ><i class="fa fa-user-circle-o" aria-hidden="true"></i>
            {{ Auth::user()->profile->first_name ?  Auth::user()->profile->first_name : Auth::user()->name}} </a></span>
            <span><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out" aria-hidden="true"></i></a></span>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
            </form>
            @endif
            </li>                    
            </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
  @yield('content')
  <!-- Scripts -->
  
  <script src="{{ asset('js/app.js') }}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
  <script src='https://content.jwplatform.com/libraries/5udyR0Be.js'></script>

      <!-- For Notification -->
  <script src="{{ asset('js/bootstrap-notify.min.js') }}"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
  <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
  <script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
  <!-- Plugin JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
  <!-- Bootstrap Core JavaScript -->
  <!-- Add this script before </body> -->
  <script src="{{ asset('js/freelancer.min.js') }}"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js"></script> 
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script> 
  <script type="text/javascript" src="http://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
  <script type="text/javascript">
  $('.statistic-counter_two, .statistic-counter, .count-number').counterUp({
    delay: 10,
    time: 2000
  });
  // var dateToday = new Date();
  var d = new Date();
  d.setFullYear(d.getFullYear() - 17);

  var currentMonth = d.getMonth();
  var currentDate = d.getDate();
  var currentYear = d.getFullYear();

  $(function () {
    $("#datepicker").datepicker({ 
          maxDate: d,
          autoclose: true, 
          todayHighlight: true
    })
    // .datepicker('setDate', d)
    .datepicker( "option", "maxDate", new Date(currentYear, currentMonth, currentDate));
  });         
</script> 
<script>
(function() {
  var morphSearch = document.getElementById( 'morphsearch' );
  // alert($('input.morphsearch-input').val());
  if($('input.morphsearch-input').val() != null){
   var input = morphSearch.querySelector( 'input.morphsearch-input' );
    var ctrlClose = morphSearch.querySelector( 'span.morphsearch-close' ),
    isOpen = isAnimating = false,
    // show/hide search area
    toggleSearch = function(evt) {
      // alert(input.value);
      // return if open and the input gets focused
      if( evt.type.toLowerCase() === 'focus' && isOpen ) return false;

      var offsets = morphsearch.getBoundingClientRect();
      if( isOpen ) {
        classie.remove( morphSearch, 'open' );

        // trick to hide input text once the search overlay closes 
        // todo: hardcoded times, should be done after transition ends
        if( input.value !== '' ) {
          alert(input.value);
          setTimeout(function() {
            classie.add( morphSearch, 'hideInput' );
            setTimeout(function() {
              classie.remove( morphSearch, 'hideInput' );
              input.value = '';
            }, 300 );
          }, 500);
        }
        
        input.blur();
      }
      else {
        classie.add( morphSearch, 'open' );
      }
      isOpen = !isOpen;
    };

  // events
  input.addEventListener( 'focus', toggleSearch );
  ctrlClose.addEventListener( 'click', toggleSearch );
  // esc key closes search overlay
  // keyboard navigation events
  document.addEventListener( 'keydown', function( ev ) {
    var keyCode = ev.keyCode || ev.which;
    if( keyCode === 27 && isOpen ) {
      toggleSearch(ev);
    }
  } );


  /***** for demo purposes only: don't allow to submit the form *****/
  // morphSearch.querySelector( 'button[type="submit"]' ).addEventListener( 'click', function(ev) { ev.preventDefault(); } );
}
})();
</script>
<script> 
( function( window ) 
{
  'use strict';
  // class helper functions from bonzo https://github.com/ded/bonzo

  function classReg( className ) {
    return new RegExp("(^|\\s+)" + className + "(\\s+|$)");
  }

  // classList support for class management
  // altho to be fair, the api sucks because it won't accept multiple classes at once
  var hasClass, addClass, removeClass;

  if ( 'classList' in document.documentElement ) {
    hasClass = function( elem, c ) {
      return elem.classList.contains( c );
    };
    addClass = function( elem, c ) {
      elem.classList.add( c );
    };
    removeClass = function( elem, c ) {
      elem.classList.remove( c );
    };
  }
  else {
    hasClass = function( elem, c ) {
      return classReg( c ).test( elem.className );
    };
    addClass = function( elem, c ) {
      if ( !hasClass( elem, c ) ) {
        elem.className = elem.className + ' ' + c;
      }
    };
    removeClass = function( elem, c ) {
      elem.className = elem.className.replace( classReg( c ), ' ' );
    };
  }

  function toggleClass( elem, c ) {
    var fn = hasClass( elem, c ) ? removeClass : addClass;
    fn( elem, c );
  }

  var classie = {
    // full names
    hasClass: hasClass,
    addClass: addClass,
    removeClass: removeClass,
    toggleClass: toggleClass,
    // short names
    has: hasClass,
    add: addClass,
    remove: removeClass,
    toggle: toggleClass
  };

  // transport
  if ( typeof define === 'function' && define.amd ) {
    // AMD
    define( classie );
  } else {
    // browser global
    window.classie = classie;
  }

  })( window );
</script>
<script> 
  function showPassword() {
      
      var key_attr = $('#key').attr('type');
      
      if(key_attr != 'text') {
          
          $('.checkbox').addClass('show');
          $('#key').attr('type', 'text');
          
      } else {
          
          $('.checkbox').removeClass('show');
          $('#key').attr('type', 'password');
          
      }
      
  }
$(document).ready(function()
{
  var navItems = $('.admin-menu li > a');
  var navListItems = $('.admin-menu li');
  var allWells = $('.admin-content');
  var allWellsExceptFirst = $('.admin-content:not(:first)');
  allWellsExceptFirst.hide();
  navItems.click(function(e)
  {
      e.preventDefault();
      navListItems.removeClass('active');
      $(this).closest('li').addClass('active');
      allWells.hide();
      var target = $(this).attr('data-target-id');
      $('#' + target).show();
  });
});

var tags = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  prefetch: {
    url: '/tags',
    cache:false,
    filter: function(list) {
      return $.map(list, function(tagname) {
        return { name: tagname }; });
    }
  }
});
tags.initialize();

$('input#tags-input').tagsinput({
  maxTags: 8,
  maxChars: 8,
  trimValue: true,
  onTagExists: function(item, $tag) {
    $tag.hide().fadeIn();
  },
  typeaheadjs: {
    name: 'tags',
    displayKey: 'name',
    valueKey: 'name',
    source: tags.ttAdapter()
  }
});

$('input#tags-input').on('itemAdded', function(event) {
    var tag = event.item;
    var ajaxData = {
    type : "POST", 
    dataType: 'json',
    data:{"title":tag, "_token":'{{csrf_token()}}',"interest":$("input#tags-input").val()} 
    };

    $.ajax(APP_URL+'/tag/add', ajaxData, function(response) {
      return;
    });
});
$('input#tags-input').on('itemRemoved', function(event) {
  var tag = event.item;
   var ajaxData = {
    type : "POST", 
    dataType: 'json',
    data:{"title":tag, "_token":'{{csrf_token()}}',"interest":$("input#tags-input").val()} 
    };
    $.ajax(APP_URL+'/tag/remove', ajaxData, function(response) {
        return;
    });
});
</script>
<script type="text/javascript">
    @yield('routes')
    </script>

  <script src="{{ asset('js/script.js') }}"></script>  

<!-- <script src="{{ asset('js/notify.min.js') }}"></script>
 -->

  @yield('specificjs')
</body>
</html>

<!--   <script src="{{ asset('js/chat.js') }}"></script>
  <script src="{{ asset('js/notify.min.js') }}"></script> -->
@if(Auth::check())
 <script type="text/javascript">
      window.Echo.private('App.User.{{Auth::user()->id}}')
        .listen('.Illuminate\\Notifications\\Events\\BroadcastNotificationCreated', (e) => {

          toastr.options = {
          "closeButton": true,
          "debug": false,
          "newestOnTop": false,
          "progressBar": false,
          "positionClass": "toast-top-right",
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": "300",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
          }
          toastr.info(e.notification_line, '',{onclick: function(){
            window.open(
            e.notification_url,
            '_blank' // <- This is what makes it open in a new window.
            )
          }});
          /*toastr.warning('My name is Inigo Montoya. You killed my father, prepare to die!')

          // Display a success toast, with a title
          toastr.success('Have fun storming the castle!', 'Miracle Max Says')

          // Display an error toast, with a title
          toastr.error('I do not think that word means what you think it means.', 'Inconceivable!')

          toastr.success('We do have the Kapua suite available.', 'Turtle Bay Resort', {onclick: function(){window.location.href = "http://www.google.com"}})*/


            $('.dropdown-toggle.dk').css("color","red");
            /*var notify = $.notify({
              message: e.notification_line,
              url: e.notification_url
            });
            setTimeout(function() {
            notify.close();
            }, 3000); */
        });
  $('a.readNotification').click(function(){
    var name = {!! json_encode(Auth::user()->name) !!}
    var html = '';
        $.ajax({
        url: APP_URL+'/'+name+'/notification', 
        data: {"_token": "{{ csrf_token() }}"},
        type: "GET",
        cache:false,
        dataType : "json",
        success: function(result){
          var notificationArray = result.notificationData;
          if(result.success){
            if(notificationArray.length == 0){
            html += 'No Notifications.'; 
            }else{
            $.each(notificationArray, function(index, value) {
              html += '<a href="'+value.notification_url+'" target="_blank" class="media list-group-item"><span class="pull-left thumb-sm"> <i class="fa fa-user-circle-o" aria-hidden="true"></i> </span><span class="media-body block m-b-none"> '+value.notification_line+' <br><small class="text-muted">'+ value.created_at +'</small></span></a>';
            });
            }
            $('.list-group').html(html);
          }else{
            location.reload();
          }
        }
      });
    $('.dropdown-toggle.dk').css("color","white");
  });


  $('.clearNotification').on('click', function () {
      $.confirm({
          title: 'Clear Notifications',
          content: 'Are you sure want to clear notifications?',
          buttons: {
              confirm: function () {
                html = '';
                  $.ajax({
                  url: APP_URL+'/notification/clear', 
                  data: {"_token": "{{ csrf_token() }}"},
                  type: "POST",
                  cache:false,
                  dataType : "json",
                  success: function(result){
                  var notificationArray = result.notificationData;
                  if(result.success){
                    html += 'No Notifications.'; 
                  }else{
                    location.reload();
                  }
                  }
                });
              },
              cancel: function () {
                  return;
              }
          }
      });
  });
</script> 
@endif
