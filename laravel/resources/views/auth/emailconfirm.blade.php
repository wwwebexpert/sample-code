@extends('layouts.app')
@section('content')
<div class="container" id="login-page" tabindex="-1">
  <div class="row">
    <div class="col-md-12">
<div class="panel panel-default">
<div class="panel-heading">Registration Confirmed</div>
<div class="panel-body">
Your Email is successfully verified. Click here to <a href="{{url('/login')}}">login</a>
</div>
</div>
</div>
</div>
</div>
@php
header( "refresh:5;url={{ url('/login') }}" );
@endphp
@endsection