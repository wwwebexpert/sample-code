@extends('layouts.app')

@section('content')
dfb
  @if (Session::has('message'))
    dfsjdn
        <li>{!! session('message') !!}</li>
   @endif

<!---Profile Section -->
<div class="container" id="signup-page" tabindex="-1">
  <div class="row">
    <div class="col-md-12">
    <div class="talent-login"> 
    <div class="register-heading"><h2>Sign up to Talent Pool </h2>
    <p> By Creating your Talent Pool account you agree to our  Terms of Use and Privacy Policy</p></div>
    <div class="left-social-section col-md-6">
        <div class="omb_login">
            
           <div class="row omb_row-sm-offset-3 omb_socialButtons">
            <div class="col-xs-12 col-sm-12">
                <a href="facebook" class="btn btn-lg btn-block omb_btn-facebook">
                    <i class="fa fa-facebook "></i>
                    <span>Login With <strong>Facebook</strong></span>
                </a>
            </div>
            
                <div class="col-xs-12 col-sm-12">
                <a href="google" class="btn btn-lg btn-block omb_btn-google">
                    <i class="fa fa-google-plus "></i>
                    <span>Login With<strong> Google+</strong></span>
                </a>
            </div>
            </div>
    </div>
    <span class="already-account">Already have an account? <a href="{{ route('login') }}"><strong>LogIn</strong></a> </span>
     </div>
    
     <div class="right-input-section col-md-6"> 
            <div class="row">
            <div class="col-xs-12">
                <div class="form-wrap">
                <h3>SIGN UP WITH EMAIL</h3>
               
                {!! Form::open(['route' => ['register'], 'id' => "signup-form", 'autocomplete'=> "off", "novalidate" => "novalidate"]) !!}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="sr-only">Email</label>
                            <input type="email" name="email" id="email" class="form-control" placeholder="somebody@example.com" value="{{ old('email') }}" required autofocus>
                            @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="sr-only">Username</label>
                            <input type="text" name="name" id="username" class="form-control" placeholder="Username" value="{{ old('name') }}" required>
                            @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                        </div>
                         <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="sr-only">Password</label>
                            <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
                             @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" id="password-confirm" class="sr-only">Confirm Password</label>
                            <input type="password" name="password_confirmation" id="password-confirm" class="form-control" placeholder="Confirm Password" required>
                             @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                        
                      
                        <input type="submit" id="btn-login" class="btn btn-custom btn-md btn-block submit-button" value="Create Account">
                {{ Form::close() }}
                   
                  
                </div>
            </div> <!-- /.col-xs-12 -->
        </div> <!-- /.row -->
     </div>
    <div class="modal fade forget-modal" tabindex="-1" role="dialog" aria-labelledby="myForgetModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Recovery password</h4>
            </div>
            <div class="modal-body">
                <p>Type your email account</p>
                <input type="email" name="recovery-email" id="recovery-email" class="form-control" autocomplete="off">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-custom">Recovery</button>
            </div>
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal -->
    </div>
    
     </div>
    
  </div>
</div>

<!---Profile Section --> 



@component('element.footer')
@endcomponent
@endsection
