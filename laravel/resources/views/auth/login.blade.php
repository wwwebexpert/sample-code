@extends('layouts.app')

@section('content')

<!---Profile Section -->
<div class="container" id="login-page" tabindex="-1">

@if (Session::has('status'))
<div class = "alert alert-success alert-dismissable" id="resetEmailSent">
 <button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
    &times;
 </button>
 {!! session('status') !!}
</div>
@endif

@if ($errors->first('email'))
<div class = "alert alert-danger alert-dismissable" id="resetEmailSent">
 <button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
    &times;
 </button>
{{ $errors->first('email') }}
</div>
@endif


  <div class="row">
    <div class="col-md-12">
    <div class="talent-login"> 
    <div class="register-heading"><h2>Login to Talent Pool </h2>
    <p> By Creating your Talent Pool account you agree to our  Terms of Use and Privacy Policy</p></div>
    <div class="left-social-section col-md-6">
        <div class="omb_login">
            
           <div class="row omb_row-sm-offset-3 omb_socialButtons">
            <div class="col-xs-12 col-sm-12">
                <a href="facebook" class="btn btn-lg btn-block omb_btn-facebook">
                    <i class="fa fa-facebook "></i>
                    <span>Login With <strong>Facebook</strong></span>
                </a>
            </div>
            
                <div class="col-xs-12 col-sm-12">
                <a href="google" class="btn btn-lg btn-block omb_btn-google">
                    <i class="fa fa-google-plus "></i>
                    <span>Login With<strong> Google+</strong></span>
                </a>
            </div>
            </div>
    </div>
    <span class="already-account">Already have an account? <a href="{{ route('register') }}"><strong>Sign Up</strong></a> </span>
     </div>
    
     <div class="right-input-section col-md-6"> 
            <div class="row">
            <div class="col-xs-12">
                <div class="form-wrap">
                <h3>Log in with your email account</h3>
                    <form method="POST" id="login-form" autocomplete="off" action="{{ route('login') }}"  novalidate>
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="username" class="sr-only">Username/E-Mail Address</label>
                            <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus placeholder="somebody@example.com">

                                @if ($errors->has('userNotExists'))
                                    <span class="help-block error">
                                        <strong>{{ $errors->first('userNotExists') }}</strong>
                                    </span>
                                @endif
                        </div>
                        <div class="form-group">
                            <label for="password" class="sr-only">Password</label>
                            <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block error">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                @if ($errors->has('userNotVerified'))
                                    <span class="help-block error">
                                        <strong>{{ $errors->first('userNotVerified') }}</strong>
                                    </span>
                                @endif
                        </div>
                        <!-- <div class="checkbox">
                            <span class="character-checkbox" onclick="showPassword()"></span>
                            <span class="label">Show password</span>
                        </div> -->
                        
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                            </label>
                        </div>
                        <input type="submit" id="btn-login" class="btn btn-custom btn-md btn-block submit-button" value="Login">
                    </form>
                    <a href="#" class="forget" data-toggle="modal" data-target=".forget-modal">Forgot your password?</a>
                    
                  
                </div>
            </div> <!-- /.col-xs-12 -->

           



        </div> <!-- /.row -->
     </div>
    <div class="modal fade forget-modal" tabindex="-1" role="dialog" aria-labelledby="myForgetModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
        <form class="form-horizontal" id="forgetPassword-form" method="POST" action="{{ route('password.email') }}">
        {{ csrf_field() }}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Recovery password</h4>
            </div>
            <div class="modal-body">
                <p>Type your email account</p>
                <!-- <input type="email" name="recovery-email" id="recovery-email" class="form-control" autocomplete="off"> -->
                <!-- <input id="email" type="email" class="form-control" name="recovery-email" id="recovery-email" value="{{ old('email') }}" required autocomplete="off"> -->
                 <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-custom">Recovery</button>
            </div>
            </form>
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal -->
    </div>
    
     </div>
    
  </div>
</div>

<!---Profile Section --> 

@component('element.footer')
@endcomponent
@endsection

@section('specificjs')
<script type="text/javascript">
        $('span.help-block').fadeOut(5000);
</script>
@endsection