@extends('layouts.app')

@section('content')
<div class="container" id="login-page" tabindex="-1">
  <div class="row">
    <div class="col-sm-12">
    <h4>Notifications</h4>
 <ul class="list-group">
    @foreach ($notificationData as $notification)
  <li class="list-group-item"><a target="_blank" href="{{$notification['notification_url']}}">{{$notification['notification_line']}}</a></li>
    @endforeach
</ul>

        
      </div>
  </div>
</div>  
@endsection