 @forelse($notifications as $notification)
  
  <a href="{{$notification['broad_user_url']}}" class="media list-group-item"> 
                  <span class="pull-left thumb-sm"> <i class="fa fa-user-circle-o" aria-hidden="true"></i> </span> 
                  <span class="media-body block m-b-none"> {{$notification['broad_user_name']}} Starting follows you!<br><small class="text-muted">{{$notification['created_at']}}</small></span>
                </a> 

@empty
  No Notification. 
@endforelse 