@extends('layouts.app')

@section('content')
		<div class="container" id="videoupload-page" tabindex="-1">
			<div class="row">
				<div class="col-md-12" id="myprofile">

					<div class="col-md-12 pull-right"> 
						<div class="row">
							<div class="alert alert-danger alert-dismissable" style="display: none;">
							<span id="errorWhileUploading"></span>    
							</div>
							<div id="successUploading" class="alert alert-success alert-dismissable" style="display: none;">
							<button type="button" class="close" data-dismiss = "alert" aria-hidden = "true">
							&times;
							</button>
							<span id="successUploading"></span>    
							</div>
							<div id="errorWhileUploadingServer">			
							</div>		
						</div>
					</div>

					<form class="form-horizontal" id="myAssetsUpload" name="myAssetsUploadForm" method="POST" autocomplete="off" enctype="multipart/form-data">
					
					  <div class="video-drag col-md-6">
						<input type="hidden" name="_token" id="uploadAssetsToken" value="{{ csrf_token() }}" />
						<input type="file" name="files">
					  </div>
					  <div class="video-upload-info col-md-6">
					  <!-- Form Here -->
					  <div class="personal-info">
					  
					    <div class="row form-group">
							<div class="col-xs-12">
								<ul class="nav nav-pills nav-justified thumbnail setup-panel">
									<li class="active"><a href="#step-1">
										<h4 class="list-group-item-heading">Option</h4>
										<!--<p class="list-group-item-text">First step description</p>-->
									</a></li>
									<li class=""><a href="#step-2">
										<h4 class="list-group-item-heading">Description</h4>										
									</a></li>
									<!-- <li class=""><a href="#step-3">
										<h4 class="list-group-item-heading">Advanced</h4>										
									</a></li> -->
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<div class="col-md-12 well setup-content" id="step-1">
									<div class="form-group">
									  <div class="col-lg-12">
										<label>Video Title</label>
										<input class="form-control" type="text" value="" name="title" id="title">
									  </div>
									</div>
									<div class="form-group">
									  <div class="col-lg-12">
										<label>Tags</label>
										<input type="text" name="tags" id="video-tags-input" data-role="tagsinput" style="display: none;">
									  </div>
									</div>
									<div class="form-group">
									  <div class="col-lg-6">
										<label for="name"> Category </label>
										<div class="ui-select"> {{ Form::select('category', ['musician'=> 'Musician', 'designers' => 'Designers', 'writers' => 'Writers','dancers' => 'Dancers'], 'musician', $attributes = array('id' => 'video_category', 'class'=>'form-control')) }} </div>
									  </div>
									  <div class="col-lg-6">
										<label for="name"> Privacy </label>
										<div class="ui-select"> {{ Form::select('visibility', ['0' => 'Public', '1' => 'Friends Only', '2' => 'Private'], '0', $attributes = array('id' => 'video_privacy', 'class'=>'form-control')) }} </div>
									  </div>
									</div>																		
									<div class="form-group">
										<div class="col-lg-12">
											<a id="activate-step-2" href="javascript:void(0)" class="btn btn-primary btn-lg">Next</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<div class="col-md-12 well setup-content" id="step-2">
									<div class="form-group">
									  <div class="col-lg-12">
										<label>About Video</label>
										<textarea rows="4" cols="50" name="about_video" class="form-control" placeholder="Video Description Here"></textarea>
									  </div>
									</div>
									<div class="form-group">
                                        <div class="col-lg-12">
                                            <span><button class="btn btn-primary btn-lg" id="addProject" href="javascript:void(0)">Save</button></span>
                                            <span><button class="btn btn-primary btn-lg" href="{{ route('singleUser' , Auth::user()->name)  }}"> Close </a></span>
                                        </div>
                                    </div>
								</div>
							</div>
						</div>
					  
						
					  </div>
					  <!-- Form Here -->
					  <input type="submit" name="uploadImage" style="display: none;" />
					  </div>
					  
					</form>
			  
				</div>    
			</div>
		</div>

    
  </div>
</div>


@component('element.footer')
@endcomponent

@endsection

@section('specificjs')

   <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
   
<script type="text/javascript">
var USER_URL = {!! json_encode(Auth::user()->name) !!}
    $(function() {
    $('#myAssetsUpload').areYouSure(
      {
        message: 'It looks like you have been editing something. '
             + 'If you leave before saving, your changes will be lost.'
      }
    );
  });
</script>
<script>
	$(document).ready(function() {
    
    var navListItems = $('ul.setup-panel li a'),
        allWells = $('.setup-content');

    allWells.hide();

    navListItems.click(function(e)
    {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this).closest('li');
        
        if (!$item.hasClass('disabled')) {
            navListItems.closest('li').removeClass('active');
            $item.addClass('active');
            allWells.hide();
            $target.show();
        }
    });
    
    $('ul.setup-panel li.active a').trigger('click');
    
    // DEMO ONLY //
    $('#activate-step-2').on('click', function(e) {
        $('ul.setup-panel li:eq(1)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-2"]').trigger('click');
        $(this).show();
    })
	$('#activate-step-3').on('click', function(e) {
        $('ul.setup-panel li:eq(2)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-3"]').trigger('click');
        $(this).show();
    })	
});

var videoTags = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  prefetch: {
    url: '/tags',
    cache:false,
    filter: function(list) {
      return $.map(list, function(tagname) {
        return { name: tagname }; });
    }
  }
});
videoTags.initialize();

$('input#video-tags-input').tagsinput({
    maxTags: 8,
    maxChars: 8,
    trimValue: true,
    onTagExists: function(item, $tag) {
        $tag.hide().fadeIn();
    },
    typeaheadjs: {
    name: 'tags',
    displayKey: 'name',
    valueKey: 'name',
    source: tags.ttAdapter()
  }
});

$('input#video-tags-input').on('itemAdded', function(event) {
    var tag = event.item;
    var ajaxData = {
    type : "POST", 
    dataType: 'json',
    data:{"title":tag, "_token":'{{csrf_token()}}',"tag" : "video"} 
    };

    $.ajax(APP_URL+'/tag/add', ajaxData, function(response) {
      return;
    });
});
$('input#video-tags-input').on('itemRemoved', function(event) {
  var tag = event.item;
   var ajaxData = {
    type : "POST", 
    dataType: 'json',
    data:{"title":tag, "_token":'{{csrf_token()}}',"tag" : "video"} 
    };
    $.ajax(APP_URL+'/tag/remove', ajaxData, function(response) {
        return;
    });
});

</script>
  <script type="text/javascript" src="https://www.jqueryscript.net/demo/Elegant-Customizable-jQuery-PHP-File-Uploader-Fileuploader/jquery.fileuploader.min.js"> </script>
  <script src="{{ asset('js/customupload.js') }}"></script>
  <script src="{{ asset('js/are-you-sure.js') }}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.min.js"> </script>

@endsection