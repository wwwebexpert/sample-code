@extends('layouts.app')

@section('content')
@component('element.header', ['homeContent' => $homeContent])
@endcomponent

    <!-- Portfolio Grid Section -->


    <section id="deliver-talent">

        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h2> <span> {{$homeContent->second_section_title}} </span></h2>
                 <p>{{$homeContent->second_section_content}}</p>
                </div>
            
            <div class="col-lg-6">
            <img src="{{ asset('uploads/' . $homeContent->second_section_image) }}" class="img-responsive">
            </div>
            </div>
        </div>
    </section>

    <!-- Video Section -->
    <section class="success" id="featurevideo">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>featured video of the day </h2>
                </div>
            </div>
            <div class="row">
                
              <div class="text-center main-video" > <iframe src="{{$homeContent->video_section_url}}" width="100%" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> </div>


                
            </div>
        </div>
    </section>
    
       <!-- Video Section -->
   
           <!-- Counter Section -->
    <section id="counter" class="counter">
            <div class="main_counter_area">
                <div class="p-y-3">
                    <div class="container">
                        <div class="row">
                            <div class="main_counter_content text-center white-text wow fadeInUp">
                                <div class="col-md-3">
                                    <div class="single_counter p-y-2 m-t-1">
                                     <img src="img/music-sign.png">
                                        <h2 class="statistic-counter">100</h2>
                                        <p>Designers</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="single_counter p-y-2 m-t-1">
                                        <img src="img/design-sign.png">
                                        <h2 class="statistic-counter">400</h2>
                                        <p>Designers</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="single_counter p-y-2 m-t-1">
                                        <img src="img/writer.png">
                                        <h2 class="statistic-counter">312</h2>
                                        <p>Writers</p>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="single_counter p-y-2 m-t-1">
                                    <img src="img/dance.png">
                                       
                                        <h2 class="statistic-counter">480</h2>
                                        <p>Dancers</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<!---Discover talent--->


    <section id="discovertalent" class="talentdiscover">
      
              
                    <div class="container">
                        <div class="row">
                         <div class="col-lg-12 text-center"> <h2>Find Talent You Already Have Match People Across Jobs 24/7 </h2></div>
                         <div class="col-lg-8 col-lg-offset-2 text-center">
                    <a href="#" class="btn btn-lg btn-red">
                        Dicover Talent
                    </a>
                </div>
                        </div>
                       </div>
                      
                  
                       </section>

    <!-- Contact Section -->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2> Subscribe for <strong>Newsletter</strong> and future <strong>Updates</strong></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19. -->
                    <!-- The form should work on most web servers, but if the form is not working you may need to configure your web server differently. -->
                    <form name="newsletter" id="newsletterform">
                        <div class="row control-group">
                            <div class="form-group col-md-8 col-xs-12 floating-label-form-group controls">
                                <input type="text" class="form-control" placeholder="Name" id="name" required data-validation-required-message="Please enter your name.">
                                
                            </div>
                             <div class="form-group col-md-4 col-xs-12">
                                <button type="submit" class="btn btn-success btn-lg">Subscribe Now! <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
</button>
                            </div>
                        </div>
                        
                        
                                                                       
                       
                       
                    </form>
                </div>
            </div>
        </div>
    </section>
    @component('element.footer')
@endcomponent
@endsection

