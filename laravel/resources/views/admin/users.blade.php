@extends('layouts.admin')
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Users
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Users</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">All Users</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="usersDataTable" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Sr. No.</th>
                  <th>Username</th>
                  <th>Email address</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($usersData as $user => $singleUser)
                <tr>
                  <td>{{ $loop->iteration }}</td>
                  <td>{{$singleUser->profile->first_name ? $singleUser->profile->first_name.' '.$singleUser->profile->last_name : $singleUser->name }}
                  </td>
                  <td>{{$singleUser->email}}</td>
                  <td>@if($singleUser->is_activated) <span class="label label-success">Activated</span> @else <span class="label label-danger">Deactivated</span> @endif
                  </td>
                  <td><a href="javascript:void(0)" class="deleteUser" data-id="{{Crypt::encryptString($singleUser->id)}}"><i class="fa fa-trash"></i></a></td>
                </tr>
                @endforeach

                
                 </tbody>
                <tfoot>
                <tr>
                  <th>Sr. No.</th>
                  <th>FullName/Username</th>
                  <th>Email address</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          </div>
            <!-- /.col -->
          </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection