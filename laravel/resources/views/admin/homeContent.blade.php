@extends('layouts.admin')
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Home
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Contents</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">General Elements</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" method="POST" action="{{route('updateHomeContent')}}" enctype= "multipart/form-data">
              {{csrf_field()}}
                <!-- text input -->
                <div class="form-group">
                  <label>First Section Heading</label>
                  <input type="text" class="form-control" placeholder="Enter Heading" name="first_section_heading" value="{{ $homeContent->first_section_heading }}">
                </div>
                <div class="form-group">
                  <label>Second Section Title</label>
                  <input type="text" class="form-control" placeholder="Enter Title" name="second_section_title" value="{{ $homeContent->second_section_title }}">
                </div>
                 <div class="form-group">
                  <label>Second Section Content</label>
                  <textarea class="form-control" rows="3" placeholder="Enter Content" name="second_section_content"> {{ $homeContent->second_section_content }} </textarea>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Second Section Image</label>
                  <input type="file" id="second_section_image" name="second_section_image">
                  <p class="help-block">Uplaod a file for second section</p>
                </div>
                <div class="form-group ">
                  <label for="exampleInputFile">Image Preview</label>
                  <img class="img-responsive" src="{{ asset('uploads/' . $homeContent->second_section_image) }}">
                </div>                
                <div class="form-group">
                  <label>Video Section Url</label>
                  <input type="text" class="form-control" placeholder="Enter Url" name="video_section_url" value="{{ $homeContent->video_section_url }}">
                </div>
                
               <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      
        <!-- /.col -->


        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>


  <!-- /.content-wrapper -->
@endsection