$(document).ready(function() {

	$('input[name="files"]').fileuploader({
        limit: 1,
        maxSize: 25,
        extensions: ['video/mp4'],
        changeInput: '<div class="fileuploader-input">' +
					      '<div class="fileuploader-input-inner">' +
						      '<img src="/img/fileuploader-dragdrop-icon.png">' +
							  '<h3 class="fileuploader-input-caption"><span>Drag and drop files here</span></h3>' +
							    '<h5 class="fileuploader-input-caption"><span>or select an option below</span></h5>' +
                              '<div class="fileuploader-input-button"><span>Add Video</span></div>' +
                              '<p>Maximum size 25 MB.</p><p>OR</p><p><input type="text" id="fileUrl" name="fileUrl" placeholder="Enter video Url."></p>' +
						  '</div>' +
					  '</div>',
        theme: 'dragdrop',
		onRemove: function(item) {},
        dialogs: {
            alert: function(text) {
            // return console.log(text);
            $("span#errorWhileUploading").html(text);
            $("span#errorWhileUploading").parent().show().fadeOut( 10000 );
            },
        },
		captions: {
            feedback: 'Drag and drop files here',
            feedback2: 'Drag and drop files here',
            drop: 'Drag and drop files here'
        },
	});
	
    $("input#fileUrl").click(function(event){  event.stopPropagation();});
    var options = { 
        complete: function(response) 
        {
        if(response.responseJSON.success){  
            $("span#successUploading").html("Video uploaded Successfully");
            $("div#successUploading").show().fadeOut( 7000 );
            setTimeout(function(){window.location.href=APP_URL+'/'+USER_URL+'/'},3000);
        }
        /*
            if($.isEmptyObject(response.responseJSON.success)){
                $("input[name='title']").val('');
                alert('Image Upload Successfully.');
            }else{
                printErrorMsg(response.responseJSON.error);
            }
        */},
        error:function(response) {
            // console.log(response.responseJSON.errors);
            var obj =  response.responseJSON.errors ;
            printErrorMsg(obj[Object.keys(obj)[0]][0]);
            // console.log(obj[Object.keys(obj)[0]][0]);
        }
    };
    $("body").on("click","button#addProject", function(e){
        e.preventDefault();
        $("input[name='uploadImage']").eq(0).trigger("click");
    });
    $("body").on("click","input[name='uploadImage']",function(e){
        $(this).parents("form").ajaxForm(options);
    });
    $("body").on("mouseover","div.alert_server",function(e){
        $(this).hide();
    });

    function printErrorMsg(msg) {
        // $("span#errorWhileUploadingServer").html(msg);
        $("div#errorWhileUploadingServer")
        .html('<div class="alert alert_server alert-danger alert-dismissable"><button type="button" class="close" data-dismiss = "alert" aria-hidden = "true">&times;</button><span>'+msg+'</span></div>')
    }
    $.validator.addMethod('filesize', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, 'File size must be less than 25 MB');


      $.validator.addMethod("urlCheck", function (value, element) {
            var regex = /http:\/\/(?:www.)?(?:(vimeo).com\/(.*)|(youtube).com\/watch\?v=(.*?)&)/;
             return regex.test($('#fileUrl').val());
      }, 'Please provide correct youtube or vimeo url.');

    $("#myAssetsUpload").validate({
        rules:{
            fileUrl:{
                required: {depends: function (element) {return !$("input[name='fileuploader-list-files']").is(":filled");}},
                // urlCheck : true
            },
            files:{
                required: {depends: function (element) {return !$("#fileUrl").is(":filled");}}
                ,accept: "video/mp4",filesize:25000000,extension: "mp4"},
            title:{required: true, minlength:3, maxlength:255},
            about_video:{required: true, minlength:8,maxlength:350},
            tags:{required: true}
        },
        messages:{
            fileUrl:{required: "Please select Video file or Url."},
            files:{
            required: "Please select Video file or Url.",
            accept: "Only video/mp4 files are allowed to be uploaded."},
            about_video:{required: "Please enter video description.", minlength:"Minimum 8 characters required.",
            maxlength : "Only 350 characters allow for video description."},
            title:{required: "Please enter video title.", minlength:"Minimum 3 characters required.",
            maxlength:"Only 255 characters allow."},
            tags:{required: "Please select tag for video."}
        }
    });

});