
function showRequest(formData, jqForm, options) {
  $("#validation-errors").hide().empty();
  return true; 
}

function showResponse(response, statusText, xhr, $form)  { 
 if(response.success == false)
 {
  var arr = response.errors;
  $.each(arr, function(index, value)
  {
  if (value.length != 0)
  {
    $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div></br>');
  }
  });
  $("#validation-errors").show();
 }
}

function showWebLinkResponse(response, statusText, xhr, $form)  { 
 if(response.success && response.service_provider_link_val)
 {
  $('div#'+response.service_provider_link).html('<span id="'+response.service_provider_link+'">@'+response.service_provider_link_val+'</span><a href="#" data-name="'+response.service_provider+'" class="removeWeblinksForm" id='+response.service_provider_link+'>X</a>');
    // $('input[name="'+service_provider_link+'"]').val();
    $('form#'+response.service_provider).trigger("reset").hide();
    $("div#"+response.service_provider_link+".beforeAdd").show();
    // $('form#Facebook').
 }
}
function removeWebLinkResponse(response, statusText, xhr, $form)  { 
 if(response.success)
 {
  $('a#'+response.service_provider_link).hide();
  $('span#'+response.service_provider_link).hide();
  $('div#'+response.service_provider_link+'> span#'+response.service_provider_link).hide();
    $('form#'+response.service_provider).trigger("reset").show();
    if($('form#'+response.service_provider).prev( "button.addlink" ).length == 0){
    $( "<button class='addlink' style='display:none;'>Link</button>" ).insertBefore('form#'+response.service_provider);
    }
 }
}
$(document).ready(function () {
  $(document).on("keydown",".webLinkVal",function(e) {
      if (e.keyCode == 13) {
        $(this).next("a.submitWeblinksForm").trigger("click");
         return false;
      }
   });
  


      $(document).on("click","button.addlink",function() {
      $(this).next("form.webLinkForm").toggle( 'slow' );
       $( this ).toggle( 'hide' );
      });
      $(document).on("click",".closebtn",function() {

          $(this).parent().parent("form.webLinkForm").toggle( 'hide' );
           $(this).parent().parent().parent().find(".addlink").toggle( 'show' );
      });

    var fileTypes = ['jpg', 'jpeg', 'png'];
    function readURL(input) {
      if (input.files && input.files[0]) {
          var extension = input.files[0].name.split('.').pop().toLowerCase(),  //file extension from input file
              isSuccess = fileTypes.indexOf(extension) > -1;  //is extension in acceptable types

          if (isSuccess) { //yes
            $("#validation-errors").hide();
              var reader = new FileReader();
              reader.onload = function (e) {
                var image = new Image();
                image.src = e.target.result;
                image.onload = function() {
                 if(this.width >= 300 && this.height >= 300){
                  $("#validation-errors").hide();
                  $('.profile-pic').attr('src', this.src);
                  }else{
                    $("#validation-errors").html('').append('<div class="alert alert-error"><strong>Your file should have atlease 300 X 300</strong><div></br>').show();
                  }
                };
              }

              reader.readAsDataURL(input.files[0]);
          }
          else { 
            $("#validation-errors").html('').append('<div class="alert alert-error"><strong>Your file should be an image and it have these extenstions jpg, jpeg, png</strong><div></br>').show();
          }
      }
    }
    

    $(".file-upload").on('change', function(){
        readURL(this);
    });

    $("select[name='profile_visibility']").on('change', function(){
        // readURL(this);
        $("form#changePrivacyPolicyForm").submit();
        // alert(this.value);
        // changePrivacyPolicyForm
    });
    
    $(".upload-button").on('click', function() {
       $("input[name='image']").click();
    });


    var options = { 
    // beforeSubmit:  showRequest,
    success: showWebLinkResponse,
    dataType: 'json' 
    };
    var removeOptions = { 
    // beforeSubmit:  showRequest,
    success: removeWebLinkResponse,
    dataType: 'json' 
    }; 
    // $('.webLinkForm').ajaxForm(options).submit();

    $('a.submitWeblinksForm').click(function(event) {
      event.preventDefault();
      $('#'+$(this).data('name')).ajaxForm(options).submit();
    });
     $(document).on("click","a.removeWeblinksForm",function(event) {
      event.preventDefault();
      $('#'+$(this).data('name')).ajaxForm(removeOptions).submit();
      // $('#'+$(this).data('name')).ajaxForm(options).submit();
    });


      /*close alert boxes or Flash message*/
      $("div.alert").fadeOut( 7000 );

      /*$("#loginMsg,#resetEmailSent,#passChangeSuccess,#passChangeError,#accAactivatesuccess,#accDeactivatesuccess,#ResetError,#ResetSuccessfully").fadeOut( 5000 );*/
      // setInterval(function(){ $("#loginMsg").fadeOut( options ).alert("close"); }, 3000);
      $.validator.addMethod("usernameCheck", function (value, element) {
            var regex = /^[a-zA-Z0-9.]+$/;
             return regex.test($('#username').val());
      }, 'only alphanumeric and dot symbol allowed');


      $("#login-form").validate({
        rules: {
        username: {
          required: true,
          minlength:6
        },
        password: {
          required: true,
          minlength:6
        }
      },
      messages: {
        username: {
          required: "Please enter a username/email.",
          minlength: "Your username must consist of at least 6 characters."
        },
        password: {
          required: "Please provide a password.",
          minlength: "Your password must be at least 6 characters long."
        },
      }
      });
      $("#signup-form,#changePasswordForm").validate({
      rules: {
        name: {
          required: true,
          usernameCheck: true,
          minlength: 6,
          remote:{
            url: '/is_username_available',
            type: "post",
            data:{name: function()
              {
                  return $('#signup-form :input[name="name"]').val();
              }, "_token": $('#signup-form :input[name="_token"]').val()}
          }
        },
        current_password: {
          required: true,
          minlength: 6
        },
        password: {
          required: true,
          minlength: 6
        },
        password_confirmation: {
          required: true,
          minlength: 6,
          equalTo: "#password"
        },
        email: {
          required: true,
          email: true
        }
      },
      messages: {
        name: {
          required: "Please enter a username.",
          minlength: "Your username must consist of at least 6 characters.",
          remote: "The username has already been taken."
        },
        current_password: {
          required: "Please provide a password.",
          minlength: "Your password must be at least 6 characters long."
        },password: {
          required: "Please provide a password.",
          minlength: "Your password must be at least 6 characters long."
        },
        password_confirmation: {
          required: "Please provide a confirm password.",
          minlength: "Your password must be at least 6 characters long.",
          equalTo: "Please enter the same password as above."
        },
        email: "Please enter a valid email address."
      }
    });
      

    $( '#comment' ).on( 'submit', function(e) {
        e.preventDefault();

        var name = $(this).find('input[name=name]').val();

        $.ajax({
            type: "POST",
            url: host+'/comment/add',
        }).done(function( msg ) {
            alert( msg );
        });

    });
});