$(function(){
$.validator.addMethod("pwcheck", function(value) {
   return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
       && /[A-Z]/.test(value) // has a lowercase letter
       && /\d/.test(value) // has a digit
},'Password must contain a uppercase letter, has a digit');

jQuery.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-z]+$/i.test(value);
}, "Letters only please");
jQuery.validator.addMethod(
    "money",
    function(value, element) {
        var isValidMoney = /^\d{0,4}(\.\d{0,2})?$/.test(value);
        return this.optional(element) || isValidMoney;
    },
    "Invalid currency format!"
); 
jQuery.validator.addMethod("phone", function(phone_number, element) {
    phone_number = phone_number.replace(/\s+/g, "");
    return this.optional(element) || phone_number.length > 9 && 
    phone_number.match(/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
}, "Please specify a valid phone number");
	
	jQuery("body").on("click",function(){
			$('#error_msg').fadeOut(700, function() {
				$('#error_msg').html('');
			});		   
	});
$.validator.addMethod('CCExp', function(value, element, params) {	
	  var minMonth = new Date().getMonth() + 1;
      var minYear = new Date().getFullYear();
	if($(params.month).val() && $(params.year).val()){
	  var month = parseInt($(params.month).val(), 10);
      var year = parseInt($(params.year).val(), 10);
      return (year > minYear || (year === minYear && month >= minMonth));
	}else{
		return true;
	}
    }, 'Your Credit Card Expiration date is invalid.');
  /* Get invoice customer Data*/
	$("#chooseCustomer").on("change", function(){
		var html = '<h4>Bill To</h4><br />';
		var customer = $(this).val();
		var postUrl = baseUrl+'customeraddr';
			$('.ajax-loader').css("visibility", "visible");
			$.post(postUrl, { customer: customer }, function (response) {
				$('.ajax-loader').css("visibility", "hidden");
			 var result = $.parseJSON(response);
			  if(result.status == "ok"){
				 $('.ajax-loader').css("visibility", "hidden");
				 if(result.data.firstName)
				 html += '<p><strong>'+result.data.firstName+' '+result.data.lastName+'</strong></p>';
                 if(result.data.sAddress){
				  html += '<p><strong>'+result.data.sAddress+'</strong></p>';
				 }else{
					  html += '<p><strong>'+result.data.address+'</strong></p>';
				 } 
				 html += '<p><input type="hidden" name="cust" value="'+result.data.cusomer+'"><input type="hidden" name="currency" value="'+result.data.currency+'"><button class="btn" id="changeCustomer">Choose A Different Customer</button></p>';
				// $("#customerSelection").css("display", "none");
				 $("#chooseCustomer").selectpicker('hide');
				 $("#chooseCustomer").selectpicker('val', '');

				 $("#custmerShipAddr").css("display", "block").html(html);
				 return false;
				
			  }else{
				   $('.ajax-loader').css("visibility", "hidden");
					swal(
						'Error!',
						result.error,
						'error'
					  );
			  }			 
					 
			});
	});
	$("#addProducts").on("change", function(){
		var getTable = document.getElementById("selectedProducts");
		var getRowsCount = 1;
		if($( "#selectedProducts tr:last" ).attr("id") != undefined){
			var lastRow = $( "#selectedProducts tr:last" ).attr("id");
			var getRowsCount = parseInt(lastRow.replace('product', '')) + 1;
		}
		var html = '<tr id="product'+getRowsCount+'">';
		var prod = $(this).val();
		var postUrl = baseUrl+'getproducttax';
			$('.ajax-loader').css("visibility", "visible");
			$.post(postUrl, { prod: prod }, function (response) {
				$('.ajax-loader').css("visibility", "hidden");
			 var result = $.parseJSON(response);
			  if(result.status == "ok"){
				  var taxselection = '<select name="product['+getRowsCount+'][taxes][]" id="taxesDropDown'+getRowsCount+'" class="form-control chosen-select" multiple>';
				if(result.taxes != ''){
				 $.each(result.taxes, function(key, val){
					taxselection += '<option data-percentage = "'+val.taxRate+'" value="'+val.id+'">'+val.taxName+' ('+val.taxRate+'%)</option>';
				 });	
				}
				taxselection += '</select>';
				 $('.ajax-loader').css("visibility", "hidden");
				 html += '<td>'+result.data.productName+'<input type="hidden" name="product['+getRowsCount+'][product_id]" value="'+result.data.id+'"></td>';
				 html += '<td><img src="/uploads/images/products/'+result.data.productImg+'" title="'+result.data.productImg+'" width="60px"></td>';
				 html += '<td><input class="form-control" type="text" name="product['+getRowsCount+'][productDes]" value="'+result.data.productDesc+'"></td>';
				 html += '<td><input class="form-control productQuenties" type="text" name="product['+getRowsCount+'][productQuantity]" value="1"><br/><div class="text-right"> Tax</div></td>';
			     html += '<td><input class="form-control productUnitPrice" type="text" name="product['+getRowsCount+'][productPrice]" value="'+result.data.productPrice+'" readonly><br/>'+taxselection+'<script>$(".chosen-select").select2();</script></td>';				 				
				 html += '<td>$<span class = "finalAmtTxt">'+result.data.productPrice+'</span><input class="totalFormProduct" type="hidden" name="product['+getRowsCount+'][totalProductPrice]" value="'+result.data.productPrice+'"> <br/><a href="javascript:void(0)" id="deletePro'+getRowsCount+'" class="deleteProFromList btn btn-default btn-danger"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></i></a><span class="taxchilds">$<span class="tatalTaxes">0.00</span><input type="hidden" name="product['+getRowsCount+'][totalfromTaxes]" value="0.00" class="totalfromTaxes"><br/><a href="javascript:void(0)" class="addTax btn btn-default btn-success" id="taxIcon'+getRowsCount+'"><i class="fa fa-plus-circle fa-lg" aria-hidden="true"></i></a></span></td>';
				 html += '</tr>';
				$(".chosen-select").trigger("chosen:updated");
                 $("#prodListing").append(html);
				 var totalSubtotal = 0.00;
				  $(".totalFormProduct").each(function() {
					 totalSubtotal += parseFloat($(this).val());
				  });
				  $("#subTotal").text(parseFloat(totalSubtotal).toFixed(2));
				  $("#IsubTotal").val(parseFloat(totalSubtotal).toFixed(2));
				  var totaltaxes = 0.00;
				  $(".totalfromTaxes").each(function() {
					 totaltaxes += parseFloat($(this).val());
				  });
				  $("#taxesVal").text(parseFloat(totaltaxes).toFixed(2));
				  $("#ItaxesVal").val(parseFloat(totaltaxes).toFixed(2));
				  var grandTotal =parseFloat(totalSubtotal) + parseFloat(totaltaxes); 
				  $("#grandTotal").text(parseFloat(grandTotal).toFixed(2));
				  $("#IgrandTotal").val(parseFloat(grandTotal).toFixed(2));
				 
    			 return false;
				
			  }else{
				   $('.ajax-loader').css("visibility", "hidden");
					swal(
						'Error!',
						result.error,
						'error'
					  );
			  }			 
					 
			});
	});
	
	$("body").on("click", "#changeCustomer", function(){
		$("#custmerShipAddr").css("display", "none").html("");
		$("#chooseCustomer").selectpicker('show');
		return false;
	});
	
	/* TAX popup */
	$("body").on("click", ".addTax", function(){
		var proId = $(this).attr("id");
		$("#productRowId").val(proId);
		$("#add_tax_model").modal("toggle");
	});
	
	/* submit a Tax */
jQuery("#add_new_tax_estimation").validate({
	  errorClass: 'validateError',
	  rules: {
		cusmter: {
		  required: true
		},
		 tax_name: {
		  required: true
		},
		
		 tax_rate: {
		  required: true,
		  money: true
		}
	},
	submitHandler: function (form) {	
			var postUrl = baseUrl+'createTax';		
			var prodData = $(form).serialize();
			var getSelectID = $("#productRowId").val();
			var selectID = getSelectID.replace("taxIcon", "taxesDropDown");	
			$('.ajax-loader').css("visibility", "visible");
			$.post(postUrl, { prodData: prodData }, function (response) {
			var result = $.parseJSON(response);
			  if(result.status == "ok"){
				 $('.ajax-loader').css("visibility", "hidden");
				 $.notify(result.message, {color: "#fff", background: "#7FC75F"});
    			 $('.chosen-select').append($("<option></option>").prop("selected", true).attr("value",result.data.id).text(result.data.value+' ('+result.data.price+'%)').attr("data-percentage", result.data.price)); 
			/*tax calculation*/	
			     var changeSelect = $("#"+selectID);
                 var getUnitPrice = changeSelect.siblings(".productUnitPrice").val();
				 var productQuenties = changeSelect.parent("td").siblings().find(".productQuenties").val();
				 var totalProductAmount =  parseFloat(getUnitPrice) * parseFloat(productQuenties);
			
				 var getAllTaxes = [];	
					 $("#"+selectID).find('option:selected').each(function(){
					 var value =$(this).data('percentage');
					   getAllTaxes.push(value);
					  }); 
				  var taxTotalPercentage = parseFloat(0);
					if(getAllTaxes.length > 0){
						$.each(getAllTaxes, function(index, value){
							taxTotalPercentage += parseFloat((parseFloat(totalProductAmount) * parseFloat(value))/ parseFloat(100));
						});
					}
				changeSelect.parent("td").siblings().find(".tatalTaxes").text(parseFloat(taxTotalPercentage).toFixed(2));
				changeSelect.parent("td").siblings().find(".totalfromTaxes").val(parseFloat(taxTotalPercentage).toFixed(2));
				
				/*get final input and update price*/
				changeSelect.parent("td").siblings().find(".finalAmtTxt").text(parseFloat(totalProductAmount).toFixed(2));
				changeSelect.parent("td").siblings().find(".totalFormProduct").val(parseFloat(totalProductAmount).toFixed(2));
				var totalSubtotal = 0.00;
				  $(".totalFormProduct").each(function() {
					 totalSubtotal += parseFloat($(this).val());
				  });
				  $("#subTotal").text(parseFloat(totalSubtotal).toFixed(2));
				  $("#IsubTotal").val(parseFloat(totalSubtotal).toFixed(2));
				  var totaltaxes = 0.00;
				  $(".totalfromTaxes").each(function() {
					 totaltaxes += parseFloat($(this).val());
				  });
				  $("#taxesVal").text(parseFloat(totaltaxes).toFixed(2));
				  $("#ItaxesVal").val(parseFloat(totaltaxes).toFixed(2));
				  var grandTotal =parseFloat(totalSubtotal) + parseFloat(totaltaxes); 
				  $("#grandTotal").text(parseFloat(grandTotal).toFixed(2));
				  $("#IgrandTotal").val(parseFloat(grandTotal).toFixed(2));
				  $("#add_tax_model").modal("toggle"); 
				  $(form)[0].reset();
			  }else{
				   $('.ajax-loader').css("visibility", "hidden");
				   $.notify(result.error, {color: "#fff", background: "#ff0000"});
			  }			 
						 
			 });				
	  }
  });
 /*on unit price chagne*/
 /*$("body").on("focusout", ".productUnitPrice", function(){
	 var unitPriceChange = $(this);
	// var getRowId = unitPriceChange
	 var quentity = unitPriceChange.parent("td").siblings().find(".productQuenties").val();
	 var getUnitPrice = unitPriceChange.val();
	 var tatalProductAmt = parseFloat(getUnitPrice) * parseInt(quentity);

	var getAllTaxes = [];
	unitPriceChange.siblings().find("select").each(function(){
    var value =$(this).val();
    getAllTaxes.push(value);
  });
  console.log(getAllTaxes); return false;
  var taxPercentage = parseFloat(0);
	if(getAllTaxes.length > 0){
		$.each(getAllTaxes, function(index, value){
			taxPercentage += parseFloat((parseFloat(tatalProductAmt) * parseFloat(value))/ parseFloat(100));

		});
	}
	alert(taxPercentage);
 });*/
 /*Price calculation when change in product quanties*/
 $("body").on("focusout", ".productQuenties", function(){
	var focusedInput = $(this);
	var quentity = focusedInput.val();
	var getUnitPrice = focusedInput.parent("td").siblings().find(".productUnitPrice").val();
	var tatalProductAmt = parseFloat(getUnitPrice) * parseInt(quentity);
	/*get tax variables*/
	var getAllTaxes = [];	
	focusedInput.parent("td").siblings().find(".chosen-select option:selected").each(function(){
    var value =$(this).data('percentage');
    getAllTaxes.push(value);
  });
  var taxPercentage = parseFloat(0);
	if(getAllTaxes.length > 0){
		$.each(getAllTaxes, function(index, value){
			taxPercentage += parseFloat((parseFloat(tatalProductAmt) * parseFloat(value))/ parseFloat(100));

		});
	}
	focusedInput.parent("td").siblings().find(".tatalTaxes").text(parseFloat(taxPercentage).toFixed(2));
	focusedInput.parent("td").siblings().find(".totalfromTaxes").val(parseFloat(taxPercentage).toFixed(2));
	
	/*get final input and update price*/
	focusedInput.parent("td").siblings().find(".finalAmtTxt").text(parseFloat(tatalProductAmt).toFixed(2));
	focusedInput.parent("td").siblings().find(".totalFormProduct").val(parseFloat(tatalProductAmt).toFixed(2));
    var totalSubtotal = 0.00;
				  $(".totalFormProduct").each(function() {
					 totalSubtotal += parseFloat($(this).val());
				  });
				  $("#subTotal").text(parseFloat(totalSubtotal).toFixed(2));
				  $("#IsubTotal").val(parseFloat(totalSubtotal).toFixed(2));
				  var totaltaxes = 0.00;
				  $(".totalfromTaxes").each(function() {
					 totaltaxes += parseFloat($(this).val());
				  });
				  $("#taxesVal").text(parseFloat(totaltaxes).toFixed(2));
				  $("#ItaxesVal").val(parseFloat(totaltaxes).toFixed(2));
				  var grandTotal =parseFloat(totalSubtotal) + parseFloat(totaltaxes); 
				  $("#grandTotal").text(parseFloat(grandTotal).toFixed(2));
				  $("#IgrandTotal").val(parseFloat(grandTotal).toFixed(2));
 }); 
  /*on select a tax*/
  $("body").on("change", ".chosen-select", function(){
	 var changeSelect = $(this);
	 var getUnitPrice = changeSelect.siblings(".productUnitPrice").val();
	 var productQuenties = changeSelect.parent("td").siblings().find(".productQuenties").val();
	 var totalProductAmount =  parseFloat(getUnitPrice) * parseFloat(productQuenties);
     var getAllTaxes = [];	
	 changeSelect.find('option:selected').each(function(){
     var value =$(this).data('percentage');
       getAllTaxes.push(value);
	  });
	  var taxTotalPercentage = parseFloat(0);
		if(getAllTaxes.length > 0){
			$.each(getAllTaxes, function(index, value){
				taxTotalPercentage += parseFloat((parseFloat(totalProductAmount) * parseFloat(value))/ parseFloat(100));
			});
		}
	changeSelect.parent("td").siblings().find(".tatalTaxes").text(parseFloat(taxTotalPercentage).toFixed(2));
	changeSelect.parent("td").siblings().find(".totalfromTaxes").val(parseFloat(taxTotalPercentage).toFixed(2));
	
	/*get final input and update price*/
	changeSelect.parent("td").siblings().find(".finalAmtTxt").text(parseFloat(totalProductAmount).toFixed(2));
	changeSelect.parent("td").siblings().find(".totalFormProduct").val(parseFloat(totalProductAmount).toFixed(2));
	             var totalSubtotal = 0.00;
				  $(".totalFormProduct").each(function() {
					 totalSubtotal += parseFloat($(this).val());
				  });
				  $("#subTotal").text(parseFloat(totalSubtotal).toFixed(2));
				  $("#IsubTotal").val(parseFloat(totalSubtotal).toFixed(2));
				  var totaltaxes = 0.00;
				  $(".totalfromTaxes").each(function() {
					 totaltaxes += parseFloat($(this).val());
				  });
				  $("#taxesVal").text(parseFloat(totaltaxes).toFixed(2));
				  $("#ItaxesVal").val(parseFloat(totaltaxes).toFixed(2));
				  var grandTotal =parseFloat(totalSubtotal) + parseFloat(totaltaxes); 
				  $("#grandTotal").text(parseFloat(grandTotal).toFixed(2));
				  $("#IgrandTotal").val(parseFloat(grandTotal).toFixed(2));
  });
  
  
  /*delete product row*/
  $("body").on("click", ".deleteProFromList", function(){
	  var deleteBtn = $(this);
	  var deleteBtnID = deleteBtn.attr("id");
	  var trID = deleteBtnID.replace('deletePro', 'product');
	  swal({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then(function () {	
           $("#"+trID).remove();
		          var totalSubtotal = 0.00;
				  $(".totalFormProduct").each(function() {
					 totalSubtotal += parseFloat($(this).val());
				  });
				  $("#subTotal").text(parseFloat(totalSubtotal).toFixed(2));
				  $("#IsubTotal").val(parseFloat(totalSubtotal).toFixed(2));
				  var totaltaxes = 0.00;
				  $(".totalfromTaxes").each(function() {
					 totaltaxes += parseFloat($(this).val());
				  });
				  $("#taxesVal").text(parseFloat(totaltaxes).toFixed(2));
				  $("#ItaxesVal").val(parseFloat(totaltaxes).toFixed(2));
				  var grandTotal =parseFloat(totalSubtotal) + parseFloat(totaltaxes); 
				  $("#grandTotal").text(parseFloat(grandTotal).toFixed(2));
				  $("#IgrandTotal").val(parseFloat(grandTotal).toFixed(2));
		});
  });
  /**/
jQuery("#createInvoiceForm").validate({
	  errorClass: 'validateError',
	  rules: {
		tax_abbreviation: {
		  required: true
		},
		 tax_name: {
		  required: true
		},
		
		 tax_rate: {
		  required: true,
		  money: true
		}
	},
	submitHandler: function (form) {	
		var postUrl = baseUrl+'invoicecreate';		
		var prodData = $(form).serialize();	
		$('.ajax-loader').css("visibility", "visible");
			$.post(postUrl, { invoiceData: prodData }, function (result) {
				var response = $.parseJSON(result);
				if(response.status == "ok"){						   
				   $.notify(response.message, {color: "#fff", background: "#7FC75F"});
				  setTimeout(function(){ window.location = baseUrl+'invoice'; }, 1000);
			   }else{
					$('.ajax-loader').css("visibility", "hidden");
					$.notify(response.error, {color: "#fff", background: "#ff0000"}); 
					return false;
			   }
			});	
	  }
  });
 /*Delete an invoice*/
$(".deleteAnInvice").click(function(){
	var postUrl = baseUrl+'deleteinvoice';	
	var invoice = $(this).data("invoice");
	$('.ajax-loader').css("visibility", "visible");
			$.post(postUrl, { invoice: invoice }, function (result) {
				var response = $.parseJSON(result);
				if(response.status == "ok"){						   
				   $.notify(response.message, {color: "#fff", background: "#7FC75F"});
				  setTimeout(function(){ window.location = baseUrl+'invoice'; }, 1000);
			   }else{
					$('.ajax-loader').css("visibility", "hidden");
					$.notify(response.error, {color: "#fff", background: "#ff0000"}); 
					return false;
			   }
			});	
});

/*Skipt invoice to send*/
$("#skipinvoieemailsend").click(function(){
	var postUrl = baseUrl+'skipinvoicesend';	
	var invoice = $(this).data("invoice");
	$('.ajax-loader').css("visibility", "visible");
			$.post(postUrl, { invoice: invoice }, function (result) {
				var response = $.parseJSON(result);
				if(response.status == "ok"){						   
				   $.notify(response.message, {color: "#fff", background: "#7FC75F"});
				  setTimeout(function(){ window.location = baseUrl+'viewinvoice/'+invoice; }, 1000);
			   }else{
					$('.ajax-loader').css("visibility", "hidden");
					$.notify(response.error, {color: "#fff", background: "#ff0000"}); 
					return false;
			   }
			});	
});

/*Mark invoice as sent*/
$("#marksent").click(function(){
	var postUrl = baseUrl+'marksentinvoice';	
	var invoice = $(this).data("invoice");
	$('.ajax-loader').css("visibility", "visible");
			$.post(postUrl, { invoice: invoice }, function (result) {
				var response = $.parseJSON(result);
				if(response.status == "ok"){						   
				   $.notify(response.message, {color: "#fff", background: "#7FC75F"});
				   setTimeout(function(){ window.location = baseUrl+'viewinvoice/'+invoice; }, 1000);
			   }else{
					$('.ajax-loader').css("visibility", "hidden");
					$.notify(response.error, {color: "#fff", background: "#ff0000"}); 
					return false;
			   }
			});	
});


   /*send invoice popup*/
   $(".sendInvoice").click(function(){
	   $("#Send_Invoice").modal("toggle");
	   $("#invoiceNumber").val($(this).data("invoice"));
	   $("#invoiceSubject").val("Invoice #"+$(this).data("invoice")+" from "+$(this).data("sender"));
   });
	/*Add new to mail fields*/
	$("#add_more_field_to").click(function(){
		var Html = '<div id="" class="sendinpit_fields"><input name="sendemail[]" class="usr1" type="text" required="required"><i aria-hidden="true" class="fa fa-times delete_text_box"></i></div>';
		$("#tocontact").append(Html);
	});
	 /*remove multiple added fields*/
	 $("body").on("click", ".delete_text_box", function(){
		 $(this).parent().remove();
	 });
	 /*send invoice email*/
	jQuery("#sendInvoiceEmail").validate({
	  errorClass: 'validateError',
	  rules: {
		 'sendemail[]': {
		  required: true,
		  email:true
		},
		
		 subjectIncoice: {
		  required: true
		},
		messageIncoice: {
			required: true
		}
	},
	submitHandler: function (form) {	
		var postUrl = baseUrl+'sendinvoiceemail';		
		var sendEmailData = $(form).serialize();	
		$('.ajax-loader').css("visibility", "visible");
			$.post(postUrl, { sendEmailData: sendEmailData }, function (result) {
				var response = $.parseJSON(result);
				if(response.status == "ok"){						   
					$("#Send_Invoice").modal("toggle");
    			   $.notify(response.message, {color: "#fff", background: "#7FC75F"});
    			   $('.ajax-loader').css("visibility", "hidden");
				  setTimeout(function(){ window.location = baseUrl+'invoice'; }, 1000);
			   }else{
					$('.ajax-loader').css("visibility", "hidden");
					$.notify(response.error, {color: "#fff", background: "#ff0000"}); 
					return false;
			   }
			});	
	  }
  });

/*Record payments*/
$(".payProcess").click(function(){
	$("#paymnet_record").modal('toggle');
	var invoiceTotal = $(this).data("invoicetotal");
	var invoice = $(this).data("invoice");
	$("#paidAmount").val(invoiceTotal);
	$("#invoiceID").val(invoice);

 });	

	 /*Record payment for cash, cheque or bank transfer*/
	jQuery("#paymentRecords").validate({
	  errorClass: 'validateError',
	  rules: {
		 payment_method: {
		  required: true
		},
		
		 payment_account: {
		  required: true
		},
		payment_memo: {
			required: true
		}
	},
	submitHandler: function (form) {	
		var postUrl = baseUrl+'paymentrecord';		
		var paymentrecords = $(form).serialize();	
		$('.ajax-loader').css("visibility", "visible");
			$.post(postUrl, { paymentRecords: paymentrecords }, function (result) {
				var response = $.parseJSON(result);
				if(response.status == "ok"){
				    $(form)[0].reset();						   
					$("#paymnet_record").modal("toggle");
    			   $.notify(response.message, {color: "#fff", background: "#7FC75F"});    			   
				  setTimeout(function(){ window.location = baseUrl+'invoice'; }, 1000);
			   }else{
					$('.ajax-loader').css("visibility", "hidden");
					$.notify(response.error, {color: "#fff", background: "#ff0000"}); 
					return false;
			   }
			});	
	  }
  });
 
});
