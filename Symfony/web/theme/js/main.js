$(function(){
$.validator.addMethod("pwcheck", function(value) {
   return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
       && /[A-Z]/.test(value) // has a lowercase letter
       && /\d/.test(value) // has a digit
},'Password must contain a uppercase letter, has a digit');

jQuery.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-z]+$/i.test(value);
}, "Letters only please");
jQuery.validator.addMethod(
    "money",
    function(value, element) {
        var isValidMoney = /^\d{0,4}(\.\d{0,2})?$/.test(value);
        return this.optional(element) || isValidMoney;
    },
    "Invalid currency format!"
); 
jQuery.validator.addMethod("phone", function(phone_number, element) {
    phone_number = phone_number.replace(/\s+/g, "");
    return this.optional(element) || phone_number.length > 9 && 
    phone_number.match(/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
}, "Please specify a valid phone number");
	
	jQuery("body").on("click",function(){
			$('#error_msg').fadeOut(700, function() {
				$('#error_msg').html('');
			});		   
	});
$.validator.addMethod('CCExp', function(value, element, params) {	
	  var minMonth = new Date().getMonth() + 1;
      var minYear = new Date().getFullYear();
	if($(params.month).val() && $(params.year).val()){
	  var month = parseInt($(params.month).val(), 10);
      var year = parseInt($(params.year).val(), 10);
      return (year > minYear || (year === minYear && month >= minMonth));
	}else{
		return true;
	}
    }, 'Your Credit Card Expiration date is invalid.');

	
	jQuery( "#userlogin" ).validate({
	  errorClass: 'validateError',
	  rules: {
		 username: {
		  required: true
		},
		password:{
		  required: true
		}
	},
	submitHandler: function (form) {
     var postUrl = baseUrl+'auth'		
	 var userData = jQuery(form).serialize();
	 $('.ajax-loader').css("visibility", "visible");
     $.post(postUrl, { userData: userData }, function (response) { 
      
	  var parseResponse = $.parseJSON(response);
	  if(parseResponse.status == "ok"){
		  window.location.href = baseUrl+'dashboard';
	  }else{
		  $('.ajax-loader').css("visibility", "hidden");
		  $("#userlogin")[0].reset();
		  $("#error_msg").css("display", "block");
		  $("#error_msg").html(parseResponse.error);
	  }
	 });
		return false;
	 }
	});
	/* forget password */
	jQuery( "#forgetPass" ).validate({
	  errorClass: 'validateError',
	  rules: {
		email: {
		  required: true,
		  email: true,
          remote: baseUrl+'checkUserEmail'
		}
	  },
		messages: {
			email: {
				remote : "Email is not exist in our database!"
			}
	  },
	submitHandler: function (form) {
     var postUrl = baseUrl+'forget'	
	 var userData = jQuery(form).serialize();
	 $('.ajax-loader').css("visibility", "visible");
     $.post(postUrl, { userData: userData }, function (response) {       
	 var parseResponse = $.parseJSON(response);
	 $('.ajax-loader').css("visibility", "hidden");
	  if(parseResponse.status == "ok"){
		  $(form)[0].reset();
		   swal('Password Reset Email!', parseResponse.message,'success');
	  }else{
		  $(form)[0].reset();
		   swal('Password Reset Email!', parseResponse.error,'success');	  }
	 });
		return false;
	 }
	});
	
	/*distributor*/
	jQuery( "#addDistributor, #editDistributor" ).validate({
	  errorClass: 'validateError',
	  rules: {
		   username: {
		   required: true,
		   minlength:6,
		   remote: baseUrl+'uniqueUser'
		},
		password: {
		  required: true,
		  minlength:6,
		  pwcheck: true
		},
		confirmpassword: {
		  required: true,
		  equalTo:"#password"
		},
		firstname: {
		  required: true,
		  lettersonly: true
		},
		  lastname: {
		  required: true,
		  lettersonly: true
		},
		email:{
		  required: true,
		  email: true,
          remote: baseUrl+'uniqueEmail'
		},
		editemail:{
		  required: true,
		  email: true
		},
		 birthday: {
		  required: true
		}
		,
		 address: {
		  required: true
		},
		'files[]':{
		required: true,
        extension: "gif|png|jpeg|jpg|bmp"	
		},
	},
	messages: {
		    username: {
				remote : "Username is not available!"
			},
			email: {
				remote : "Email Address is not available!"
			}
	  },
	submitHandler: function (form) {
	if($(form).attr('id') == 'addDistributor'){

        var postUrl = baseUrl+'insertdistributor';
			$('.ajax-loader').css("visibility", "visible");
			$.ajax({
					  type: 'POST',
					  url: postUrl,
					  data: new FormData($(form)[0]),
					  dataType: 'json',
					  processData: false,
					  contentType: false,
					  cache: false,
					  success: function(response) {
						  if(response.status == "ok"){
							  $.notify(response.message, {color: "#fff", background: "#7FC75F"});
						      setTimeout(function(){ window.location = baseUrl+'distributors'; }, 1000);
						  }else{
							  $('.ajax-loader').css("visibility", "hidden");
							  $.notify(response.error, {color: "#fff", background: "#ff0000"}); 
				              return false;
						  }
						   
						   
					  },
			});



/*
	 var postUrl = baseUrl+'insertdistributor';		
	 var disbributorData = jQuery(form).serialize();
	 $('.ajax-loader').css("visibility", "visible");
     $.post(postUrl, { userData: disbributorData }, function (response) {	 
     var distributoradd = $.parseJSON(response);
     if(distributoradd.status == 'ok'){
		 $.notify(distributoradd.message, {color: "#fff", background: "#7FC75F"});
         setTimeout(function(){ window.location = baseUrl+'distributors'; }, 1000);		 
	 }else{
		 $('.ajax-loader').css("visibility", "hidden");
		$.notify(distributoradd.error, {color: "#fff", background: "#ff0000"}); 
		return false;
	  }
	 });*/
		
	}else if($(form).attr('id') == 'editDistributor'){
				var postUrl = baseUrl+'updatedistributor';		
				var disbributorData = jQuery(form).serialize();
				$('.ajax-loader').css("visibility", "visible");
				$.post(postUrl, { userData: disbributorData }, function (response) {	
				 var distributoredit = $.parseJSON(response);
				 if(distributoredit.status == 'ok'){
					 $.notify(distributoredit.message, {color: "#fff", background: "#7FC75F"});
					 setTimeout(function(){ window.location = baseUrl+'distributors'; }, 1000);		 
				 }else{
					 $('.ajax-loader').css("visibility", "hidden");
					$.notify(distributoredit.error, {color: "#fff", background: "#ff0000"}); 
					return false;
				  }
			});
	  }
	 }
	});
//distributor birthday calander
var start = moment().subtract(15, 'years');
var end = moment();
 $('#birthday, .editbirthday').daterangepicker({
	  startDate: start,
       endDate: end,
	    locale: {
            format: 'YYYY-MM-DD'
          },
		  dateLimit: {
            years: '10'
          },
        singleDatePicker: true,
        showDropdowns: true
});
/*edit distributor*/
$(".editDistributor").click(function(){
	var getDistri = $(this).attr("id");
	var getDistritor = getDistri.replace("editDistributor", '');
	var postUrl = baseUrl+'getdistributor';
	$("#editDistributor")[0].reset();
	$('.ajax-loader').css("visibility", "visible");
     $.post(postUrl, { distributor: getDistritor }, function (response) {
	 $('.ajax-loader').css("visibility", "hidden");
	 var result = $.parseJSON(response);
	   $("#editDistributor #distributor").val(result.account.user.id);
	   $("#editDistributor #email").val(result.account.user.email);	   
	   $("#editDistributor #first-name").val(result.account.firstName);
	   var $radios = $('input:radio[name=gender]');
	   if(result.account.gender == 'Male'){
		   
			$radios.filter('[value=M]').prop('checked', true);
	   }else{
		   $radios.filter('[value=F]').prop('checked', true);
	   }
	   $("#editDistributor #last-name").val(result.account.lastName);
	   $("#editDistributor #address").val(result.account.address);
	   $("#editDistributor #locality").val(result.account.city);
	   $("#editDistributor #administrative_area_level_1").val(result.account.state);
	   $("#editDistributor #country").val(result.account.country);
	   $("#editDistributor #postal_code").val(result.account.zipcode);
	   var birthdate = result.account.dob.date;
	   $("#editDistributor #birthday").val(birthdate.replace(' 00:00:00.000000', ''));
	 });
 	$("#editpopup").modal("toggle");
});
/*Delete distributor*/
$(".deleteDistribuotr").on("click", function(){
	var Distributor = ($(this).attr("id")).replace('deleteDistribuotr', '');
	var postUrl = baseUrl+'deletedistributor';
	swal({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then(function () {			
	         $('.ajax-loader').css("visibility", "visible");
			 $.post(postUrl, { distributor: Distributor }, function (response) {
			 var result = $.parseJSON(response);
              if(result.status == "ok"){
				 $.notify(result.message, {color: "#fff", background: "#7FC75F"});
				 setTimeout(function(){ window.location = baseUrl+'distributors'; }, 1000); 
			  }else{
				   $('.ajax-loader').css("visibility", "hidden");
                    swal(
						'Error!',
						result.error,
						'error'
					  );
			  }			 
						 
			 });
		});
	
});

/*ACCOUNT*/
jQuery( "#addAccount, #editAccount" ).validate({
	  errorClass: 'validateError',
	  rules: {
		 username: {
		   required: true,
		   minlength:6,
		   remote: baseUrl+'uniqueUser'
		},
		password: {
		  required: true,
		  minlength:6,
		  pwcheck: true
		},
		confirmpassword: {
		  required: true,
		  equalTo:"#password"
		},
		firstname: {
		  required: true,
		  lettersonly: true
		},
		 lastname: {
		  required: true,
		  lettersonly: true
		},
		email:{
		  required: true,
		  email: true,
		  remote: baseUrl+'checkUserEmail'
		},
		editemail:{
		  required: true,
		  email: true,
		},
		 birthday: {
		  required: true
		}
		,
		 address: {
		  required: true
		},
		'files[]':{
		required: true,
        extension: "gif|png|jpeg|jpg|bmp"	
		},
	},
	messages: {
		    username: {
				remote : "Username is not available!"
			},
			email: {
				remote : "Email Address is not available!"
			}
	  },
	submitHandler: function (form) {
	if($(form).attr('id') == 'addAccount'){

       var postUrl = baseUrl+'insertaccount';
			$('.ajax-loader').css("visibility", "visible");
			$.ajax({
					  type: 'POST',
					  url: postUrl,
					  data: new FormData($(form)[0]),
					  dataType: 'json',
					  processData: false,
					  contentType: false,
					  cache: false,
					  success: function(response) {
						  if(response.status == "ok"){
							  $.notify(response.message, {color: "#fff", background: "#7FC75F"});
						      setTimeout(function(){ window.location = baseUrl+'accounts'; }, 1000);
						  }else{
							  $('.ajax-loader').css("visibility", "hidden");
							  $.notify(response.error, {color: "#fff", background: "#ff0000"}); 
				              return false;
						  }
						   
						   
					  },
			});
	 /*var postUrl = baseUrl+'insertaccount';		
	 var accountData = jQuery(form).serialize();
	 $('.ajax-loader').css("visibility", "visible");
     $.post(postUrl, { userData: accountData }, function (response) {	 
     var distributoradd = $.parseJSON(response);
     if(distributoradd.status == 'ok'){
		 $.notify(distributoradd.message, {color: "#fff", background: "#7FC75F"});
         setTimeout(function(){ window.location = baseUrl+'accounts'; }, 1000);		 
	 }else{
		 $('.ajax-loader').css("visibility", "hidden");
		$.notify(distributoradd.error, {color: "#fff", background: "#ff0000"}); 
		return false;
	  }
	 });*/
		
	}else if($(form).attr('id') == 'editAccount'){
				var postUrl = baseUrl+'updateaccount';		
				var accountData = jQuery(form).serialize();
				$('.ajax-loader').css("visibility", "visible");
				$.post(postUrl, { userData: accountData }, function (response) {	
				 var distributoredit = $.parseJSON(response);
				 if(distributoredit.status == 'ok'){
					 $.notify(distributoredit.message, {color: "#fff", background: "#7FC75F"});
					 setTimeout(function(){ window.location = baseUrl+'accounts'; }, 1000);		 
				 }else{
					 $('.ajax-loader').css("visibility", "hidden");
					$.notify(distributoredit.error, {color: "#fff", background: "#ff0000"}); 
					return false;
				  }
			});
	  }
	 }
	});
/*edit Account*/
$(".editAccount").click(function(){
	var getACCiD = $(this).attr("id");
	var getAccount = getACCiD.replace("editAccount", '');
	var postUrl = baseUrl+'getaccount';
	$("#editAccount")[0].reset();
	$('.ajax-loader').css("visibility", "visible");
     $.post(postUrl, { accID: getAccount }, function (response) {
	 $('.ajax-loader').css("visibility", "hidden");
	 var result = $.parseJSON(response);
	   $("#editAccount #account").val(result.account.user.id);
	   $("#editAccount #email").val(result.account.user.email);	   
	   $("#editAccount #first-name").val(result.account.firstName);
	   var $radios = $('input:radio[name=gender]');
	   if(result.account.gender == 'Male'){		   
			$radios.filter('[value=M]').prop('checked', true);
	   }else{
		   $radios.filter('[value=F]').prop('checked', true);
	   }
	   $("#editAccount #last-name").val(result.account.lastName);
	   $("#editAccount #address").val(result.account.address);
	   $("#editAccount #locality").val(result.account.city);
	   $("#editAccount #administrative_area_level_1").val(result.account.state);
	   $("#editAccount #country").val(result.account.country);
	   $("#editAccount #postal_code").val(result.account.zipcode);
	   var birthdate = result.account.dob.date;
	   $("#editAccount #birthday").val(birthdate.replace(' 00:00:00.000000', ''));
	 });
 	$("#editpopup").modal("toggle");
});

/*Delete distributor*/
$(".deleteAccount").on("click", function(){
	var account = ($(this).attr("id")).replace('deleteAccount', '');
	var postUrl = baseUrl+'deleteAccount';
	swal({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then(function () {			
	         $('.ajax-loader').css("visibility", "visible");
			 $.post(postUrl, { account: account }, function (response) {
			 var result = $.parseJSON(response);
              if(result.status == "ok"){
				 $.notify(result.message, {color: "#fff", background: "#7FC75F"});
				 setTimeout(function(){ window.location = baseUrl+'accounts'; }, 1000); 
			  }else{
				   $('.ajax-loader').css("visibility", "hidden");
                    swal(
						'Error!',
						result.error,
						'error'
					  );
			  }			 
						 
			 });
		});
   });

/*Organization's  form submision*/
jQuery( "#addOrganiztion, #editOrganization" ).validate({
	  errorClass: 'validateError',
	  rules: {
		 username: {
		   required: true,
		   minlength:6,
		   remote: baseUrl+'uniqueUser'
		},
		password: {
		  required: true,
		  minlength:6,
		  pwcheck: true
		},
		confirmpassword: {
		  required: true,
		  equalTo:"#password"
		},
		firstname: {
		  required: true,
		  lettersonly: true
		},
		 lastname: {
		  required: true,
		  lettersonly: true
		},
		email:{
		  required: true,
		  email: true,
		  remote: baseUrl+'checkUserEmail'
		},
		editemail:{
		  required: true,
		  email: true
		},
		 birthday: {
		  required: true
		},
		 address: {
		  required: true
		},
		'files[]':{
		required: true,
        extension: "gif|png|jpeg|jpg|bmp"	
		},
	},
	messages: {
		    username: {
				remote : "Username is not available!"
			},
			email: {
				remote : "Email Address is not available!"
			}
	  },
	submitHandler: function (form) {
	if($(form).attr('id') == 'addOrganiztion'){
       var postUrl = baseUrl+'insertorganization';
			$('.ajax-loader').css("visibility", "visible");
			$.ajax({
					  type: 'POST',
					  url: postUrl,
					  data: new FormData($(form)[0]),
					  dataType: 'json',
					  processData: false,
					  contentType: false,
					  cache: false,
					  success: function(response) {
						  if(response.status == "ok"){
							  $.notify(response.message, {color: "#fff", background: "#7FC75F"});
						      setTimeout(function(){ window.location = baseUrl+'organizations'; }, 1000);
						  }else{
							  $('.ajax-loader').css("visibility", "hidden");
							  $.notify(response.error, {color: "#fff", background: "#ff0000"}); 
				              return false;
						  }
						   
						   
					  },
			});
	 /*var postUrl = baseUrl+'insertorganization';		
	 var accountData = jQuery(form).serialize();
	 $('.ajax-loader').css("visibility", "visible");
     $.post(postUrl, { userData: accountData }, function (response) {	 
     var distributoradd = $.parseJSON(response);
     if(distributoradd.status == 'ok'){
		 $.notify(distributoradd.message, {color: "#fff", background: "#7FC75F"});
         setTimeout(function(){ window.location = baseUrl+'organizations'; }, 1000);		 
	 }else{
		 $('.ajax-loader').css("visibility", "hidden");
		$.notify(distributoradd.error, {color: "#fff", background: "#ff0000"}); 
		return false;
	  }
	 });*/
		
	}else if($(form).attr('id') == 'editOrganization'){
				var postUrl = baseUrl+'updateorganization';		
				var accountData = jQuery(form).serialize();
				$('.ajax-loader').css("visibility", "visible");
				$.post(postUrl, { userData: accountData }, function (response) {	
				 var distributoredit = $.parseJSON(response);
				 if(distributoredit.status == 'ok'){
					 $.notify(distributoredit.message, {color: "#fff", background: "#7FC75F"});
					 setTimeout(function(){ window.location = baseUrl+'organizations'; }, 1000);		 
				 }else{
					 $('.ajax-loader').css("visibility", "hidden");
					$.notify(distributoredit.error, {color: "#fff", background: "#ff0000"}); 
					return false;
				  }
			});
	  }
	 }
	});
/*edit organization*/
$(".editOrganization").click(function(){
	var getACCiD = $(this).attr("id");
	var getAccount = getACCiD.replace("editOrganization", '');
	var postUrl = baseUrl+'getorganization';
	$("#editOrganization")[0].reset();
	$('.ajax-loader').css("visibility", "visible");
     $.post(postUrl, { accID: getAccount }, function (response) {
	 $('.ajax-loader').css("visibility", "hidden");
	 var result = $.parseJSON(response);
	   $("#editOrganization #organization").val(result.account.user.id);
	   $("#editOrganization #email").val(result.account.user.email);	   
	   $("#editOrganization #first-name").val(result.account.firstName);
	   var $radios = $('input:radio[name=gender]');
	   if(result.account.gender == 'Male'){		   
			$radios.filter('[value=M]').prop('checked', true);
	   }else{
		   $radios.filter('[value=F]').prop('checked', true);
	   }
	   $("#editOrganization #last-name").val(result.account.lastName);
	   $("#editOrganization #address").val(result.account.address);
	   $("#editOrganization #locality").val(result.account.city);
	   $("#editOrganization #administrative_area_level_1").val(result.account.state);
	   $("#editOrganization #country").val(result.account.country);
	   $("#editOrganization #postal_code").val(result.account.zipcode);
	   var birthdate = result.account.dob.date;
	   $("#editOrganization #birthday").val(birthdate.replace(' 00:00:00.000000', ''));
	 });
 	$("#editpopup").modal("toggle");
});

/*Delete organization*/
$(".deleteOrganization").on("click", function(){
	var organization = ($(this).attr("id")).replace('deleteOrganization', '');
	var postUrl = baseUrl+'deleteOrganization';
	swal({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then(function () {			
	         $('.ajax-loader').css("visibility", "visible");
			 $.post(postUrl, { organization: organization }, function (response) {
			 var result = $.parseJSON(response);
              if(result.status == "ok"){
				 $.notify(result.message, {color: "#fff", background: "#7FC75F"});
				 setTimeout(function(){ window.location = baseUrl+'organizations'; }, 1000); 
			  }else{
				   $('.ajax-loader').css("visibility", "hidden");
                    swal(
						'Error!',
						result.error,
						'error'
					  );
			  }			 
						 
			 });
		});
   });	
	
/*Employee's  form submision*/
jQuery( "#addemployee, #editEmployee" ).validate({
	  errorClass: 'validateError',
	  rules: {
		 username: {
		  required: true,
		   minlength:6,
		   remote: baseUrl+'uniqueUser'
		},
		password: {
		  required: true
		},
		confirmpassword: {
		  required: true,
		  equalTo:"#password"
		},
		firstname: {
		  required: true
		},
		 lastname: {
		  required: true
		},
		email:{
		  required: true,
		  email: true,
		  remote: baseUrl+'checkUserEmail'
		},
		editemail:{
		  required: true,
		  email: true
		},
		 birthday: {
		  required: true
		},
		 address: {
		  required: true
		},
		'files[]':{
		required: true,
        extension: "gif|png|jpeg|jpg|bmp"	
		},
	},
	messages: {
			email: {
				remote : "Email Address is not available!"
			}
	  },
	submitHandler: function (form) {
	if($(form).attr('id') == 'addemployee'){
       var postUrl = baseUrl+'insertemployee';
			$('.ajax-loader').css("visibility", "visible");
			$.ajax({
					  type: 'POST',
					  url: postUrl,
					  data: new FormData($(form)[0]),
					  dataType: 'json',
					  processData: false,
					  contentType: false,
					  cache: false,
					  success: function(response) {
						  if(response.status == "ok"){
							  $.notify(response.message, {color: "#fff", background: "#7FC75F"});
						      setTimeout(function(){ window.location = baseUrl+'employees'; }, 1000);
						  }else{
							  $('.ajax-loader').css("visibility", "hidden");
							  $.notify(response.error, {color: "#fff", background: "#ff0000"}); 
				              return false;
						  }
						   
						   
					  },
			});
   

	/* var postUrl = baseUrl+'insertemployee';		
	 var accountData = jQuery(form).serialize();
	 $('.ajax-loader').css("visibility", "visible");
     $.post(postUrl, { userData: accountData }, function (response) {	 
     var distributoradd = $.parseJSON(response);
     if(distributoradd.status == 'ok'){
		 $.notify(distributoradd.message, {color: "#fff", background: "#7FC75F"});
         setTimeout(function(){ window.location = baseUrl+'employees'; }, 1000);		 
	 }else{
		 $('.ajax-loader').css("visibility", "hidden");
		$.notify(distributoradd.error, {color: "#fff", background: "#ff0000"}); 
		return false;
	  }
	 });*/
		
	}else if($(form).attr('id') == 'editEmployee'){
				var postUrl = baseUrl+'updateemployee';		
				var accountData = jQuery(form).serialize();
				$('.ajax-loader').css("visibility", "visible");
				$.post(postUrl, { userData: accountData }, function (response) {	
				 var distributoredit = $.parseJSON(response);
				 if(distributoredit.status == 'ok'){
					 $.notify(distributoredit.message, {color: "#fff", background: "#7FC75F"});
					 setTimeout(function(){ window.location = baseUrl+'employees'; }, 1000);		 
				 }else{
					 $('.ajax-loader').css("visibility", "hidden");
					$.notify(distributoredit.error, {color: "#fff", background: "#ff0000"}); 
					return false;
				  }
			});
	  }
	 }
	});
/*edit Employee*/
$(".editEmployee").click(function(){
	var getACCiD = $(this).attr("id");
	var getAccount = getACCiD.replace("editEmployee", '');
	var postUrl = baseUrl+'getorganization';
	$("#editEmployee")[0].reset();
	$('.ajax-loader').css("visibility", "visible");
     $.post(postUrl, { accID: getAccount }, function (response) {
	 $('.ajax-loader').css("visibility", "hidden");
	 var result = $.parseJSON(response);
	   $("#editEmployee #employee").val(result.account.user.id);
	   $("#editEmployee #email").val(result.account.user.email);	   
	   $("#editEmployee #first-name").val(result.account.firstName);
	   var $radios = $('input:radio[name=gender]');
	   if(result.account.gender == 'Male'){		   
			$radios.filter('[value=M]').prop('checked', true);
	   }else{
		   $radios.filter('[value=F]').prop('checked', true);
	   }
	   $("#editEmployee #last-name").val(result.account.lastName);
	   $("#editEmployee #address").val(result.account.address);
	   $("#editEmployee #locality").val(result.account.city);
	   $("#editEmployee #administrative_area_level_1").val(result.account.state);
	   $("#editEmployee #country").val(result.account.country);
	   $("#editEmployee #postal_code").val(result.account.zipcode);
	   var birthdate = result.account.dob.date;
	   $("#editEmployee #birthday").val(birthdate.replace(' 00:00:00.000000', ''));
	 });
 	$("#editpopup").modal("toggle");
});

/*Delete Employee*/
$(".deleteEmployee").on("click", function(){
	var employee = ($(this).attr("id")).replace('deleteEmployee', '');
	var postUrl = baseUrl+'deleteEmployee';
	swal({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then(function () {			
	         $('.ajax-loader').css("visibility", "visible");
			 $.post(postUrl, { employee: employee }, function (response) {
			 var result = $.parseJSON(response);
              if(result.status == "ok"){
				 $.notify(result.message, {color: "#fff", background: "#7FC75F"});
				 setTimeout(function(){ window.location = baseUrl+'employees'; }, 1000); 
			  }else{
				   $('.ajax-loader').css("visibility", "hidden");
                    swal(
						'Error!',
						result.error,
						'error'
					  );
			  }			 
						 
			 });
		});
   });
$("#resetPass").validate({
	  errorClass: 'validateError',
	  rules: {
		 newPassword: {
		  required: true,
		  minlength:6,
		  pwcheck: true,
		},
		confmPass:{
		  required: true,
		  equalTo: "#newPassword"
		}
	},
	submitHandler: function (form) {
     var postUrl = baseUrl+'resetPassword'		
	 var userData = jQuery(form).serialize();
	 $('.ajax-loader').css("visibility", "visible");
     $.post(postUrl, { userData: userData }, function (response) { 	 
	  var parseResponse = $.parseJSON(response);
	  if(parseResponse.status == "ok"){
		  $(form)[0].reset();
		  $.notify(parseResponse.message, {color: "#fff", background: "#7FC75F"});
		  setTimeout(function(){ window.location = baseUrl; }, 1000); 
	  }else{
		  $('.ajax-loader').css("visibility", "hidden");
		  $(form)[0].reset();
		  $('.ajax-loader').css("visibility", "hidden");
                    swal(
						'Error!',
						parseResponse.error,
						'error'
					  );
	  }
	 });
		return false;
	 }
	});
//user Profile

$("#ProfileEdit").on("click", function(){
	var postUrl = baseUrl+'getprofile';
	$("#editprofile")[0].reset();
	$('.ajax-loader').css("visibility", "visible");
     $.get(postUrl, function (response) {
	 $('.ajax-loader').css("visibility", "hidden");
	 var result = $.parseJSON(response);
	   $("#editprofile #profile").val(result.user.id);
	   $("#editprofile #email").val(result.user.email);	   
	   $("#editprofile #first-name").val(result.firstName);
	   var $radios = $('input:radio[name=gender]');
	   if(result.gender == 'Male'){		   
			$radios.filter('[value=M]').prop('checked', true);
	   }else{
		   $radios.filter('[value=F]').prop('checked', true);
	   }
	   $("#editprofile #last-name").val(result.lastName);
	   $("#editprofile #address").val(result.address);
	   $("#editprofile #locality").val(result.city);
	   $("#editprofile #administrative_area_level_1").val(result.state);
	   $("#editprofile #country").val(result.country);
	   $("#editprofile #postal_code").val(result.zipcode);
	   var birthdate = result.dob.date;
	   $("#editprofile #birthday").val(birthdate.replace(' 00:00:00.000000', ''));
	 });
 	$("#Profilepopup").modal("toggle");
});  

 
/*user's  Profile edit*/
jQuery( "#editprofile" ).validate({
	  errorClass: 'validateError',
	  rules: {
		 username: {
		   required: true,
		   minlength:6,
		   remote: baseUrl+'uniqueUser'
		},
		password: {
		  required: true,
		  minlength:6,
		  pwcheck: true
		},
		confirmpassword: {
		  required: true,
		  equalTo:"#password"
		},
		firstname: {
		  required: true,
		  lettersonly: true
		},
		 lastname: {
		  required: true,
		  lettersonly: true
		},
		email:{
		  required: true,
		  email: true,
		  remote: baseUrl+'checkUserEmail'		  
		}
		,
		 birthday: {
		  required: true
		}
		,
		 address: {
		  required: true
		}
	},
	messages: {
		    username: {
				remote : "Username is not available!"
			},
			email: {
				remote : "Email Address is not available!"
			}
	  },
	submitHandler: function (form) {
				var postUrl = baseUrl+'updateprofile';		
				var accountData = jQuery(form).serialize();
				$('.ajax-loader').css("visibility", "visible");
				$.post(postUrl, { userData: accountData }, function (response) {	
				 var distributoredit = $.parseJSON(response);
				 if(distributoredit.status == 'ok'){
					 $.notify(distributoredit.message, {color: "#fff", background: "#7FC75F"});
					 setTimeout(function(){ window.location = baseUrl+'profile'; }, 1000);		 
				 }else{
					 $('.ajax-loader').css("visibility", "hidden");
					$.notify(distributoredit.error, {color: "#fff", background: "#ff0000"}); 
					return false;
				  }
			});
	 }
	});
	
 /*Add customers*/
	
   //additional info of a customers*/
    $("#addInfoForm").click(function(){
		$("#additionalCustmInfo").slideToggle("slow");
	});
	$("#shippngBtn").change(function(){
		$(".shippingInfo").slideToggle("slow");
	});
/*customer add */
jQuery( "#addCustomerForm, #editCustomerForm" ).validate({
	  errorClass: 'validateError',
	  rules: {
		employee: {
		  required: true		  
		},
		fname: {
		  required: true,
		  lettersonly: true
		},
		 lname: {
		  required: true,
		  lettersonly: true
		},
		email:{
		  required: true,
		  email: true,
		  remote: baseUrl+'checkcustomeremail'
		},
		 phone: {
		  required: true,
		  phone:true
		},
		fax: {
		  phone:true
		},
		mobile: {
		  phone:true
		},
		 currency: {
		  required: true
		},
		 ccnumber: {
		  creditcard: true
		},
		 nameOnCard: {
		  minlength: 2
		},		
		 expYear: {
			  CCExp: {
				month: '#expMon',
				year: '#expYear'
			  }
		 }  
	},messages: {
		    fax: {
		  phone:"Please enter a valid Fax number."
		},
			email: {
				remote : "Email Address is not available!"
			}
	  },
	submitHandler: function (form) {
		if($(form).attr('id') == 'addCustomerForm'){
			   var postUrl = baseUrl+'insertcusomer';
				$('.ajax-loader').css("visibility", "visible");
				$.ajax({
						  type: 'POST',
						  url: postUrl,
						  data: new FormData($(form)[0]),
						  dataType: 'json',
						  processData: false,
						  contentType: false,
						  cache: false,
						  success: function(response) {
							  if(response.status == "ok"){
								  $.notify(response.message, {color: "#fff", background: "#7FC75F"});
							      setTimeout(function(){ window.location = baseUrl+'customers'; }, 1000);
							  }else{
								  $('.ajax-loader').css("visibility", "hidden");
								  $.notify(response.error, {color: "#fff", background: "#ff0000"}); 
					              return false;
							  }
							   
							   
						  },
				});
		}else{
			/*edit section*/
			    var postUrl = baseUrl+'updatecustomer';		
				var customerEditData = jQuery(form).serialize();
				$('.ajax-loader').css("visibility", "visible");
				$.ajax({
						  type: 'POST',
						  url: postUrl,
						  data: new FormData($(form)[0]),
						  dataType: 'json',
						  processData: false,
						  contentType: false,
						  cache: false,
						  success: function(response) {
							  if(response.status == "ok"){
								  $.notify(response.message, {color: "#fff", background: "#7FC75F"});
							      setTimeout(function(){ window.location = baseUrl+'customers'; }, 1000);
							  }else{
								  $('.ajax-loader').css("visibility", "hidden");
								  $.notify(response.error, {color: "#fff", background: "#ff0000"}); 
					              return false;
							  }
							   
							   
						  },
				});
				return false;
		}
				
	 }
	});
	
/*Delete Customer*/
$(".deletecustomer").on("click", function(){
	var customer = ($(this).attr("id")).replace('deletecustomer', '');
	var postUrl = baseUrl+'deletecustomer';
	swal({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then(function () {			
	         $('.ajax-loader').css("visibility", "visible");
			 $.post(postUrl, { customer: customer }, function (response) {
			 var result = $.parseJSON(response);
              if(result.status == "ok"){
				 $.notify(result.message, {color: "#fff", background: "#7FC75F"});
				 setTimeout(function(){ window.location = baseUrl+'customers'; }, 1000); 
			  }else{
				   $('.ajax-loader').css("visibility", "hidden");
                    swal(
						'Error!',
						result.error,
						'error'
					  );
			  }			 
						 
			 });
		});
   });	
	/*remove a thumb*/
	$(".thumbclose").click(function(){
		$(this).closest("li").remove();
	});		
	
});

/*Add Products and Services*/


	//Add product popup
	$("#addproduct").on("click", function(){
		$("#addproductpopup").modal("toggle");	
    });
/*Product management*/
$(function(){
 $("#prodImage, #EprodImage").on('change', function () {
     //Get count of selected files
     var countFiles = $(this)[0].files.length;
     var imgPath = $(this)[0].value;
     var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
	 if($(this).attr('id') == 'EprodImage'){
		 var image_holder = $("#Eimage-holder");
	 }else{
		 var image_holder = $("#image-holder");
	 }
     image_holder.empty();
     if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
         if (typeof (FileReader) != "undefined") {

             //loop for each file selected for uploaded.
             for (var i = 0; i < countFiles; i++) {

                 var reader = new FileReader();
                 reader.onload = function (e) {
                     $("<img />", {
                         "src": e.target.result,
                             "class": "thumb-image"
                     }).appendTo(image_holder);
                 }

                 image_holder.show();
                 reader.readAsDataURL($(this)[0].files[i]);
             }

         } else {
             alert("This browser does not support FileReader.");
         }
     } else {
         alert("Pls select only images");
     }
 });	
	
/*Add product*/
jQuery("#productAdd").validate({
	  errorClass: 'validateError',
	  rules: {
		prodName: {
		  required: true
		},
		 prodPrice: {
		  required: true,
		  money: true
		},
		quantity:{
		  required: true
		}
		,
		 prodDescription: {
		  required: true
		}
		,
		prodImage:{
		required: true,
        extension: "gif|png|jpeg|jpg|bmp"	
		},
		 address: {
		  required: true
		}
	},
	submitHandler: function (form) {
			var postUrl = baseUrl+'addProduct';		
				var prodData = new FormData(form);
				$('.ajax-loader').css("visibility", "visible");
				$.ajax({
				  url: postUrl,
				  data: new FormData(form),
				  processData: false,
				  contentType: false,
				  type: 'POST',
				  dataType: 'JSON',
				  success: function(response){
					   if(response.status == "ok"){
						   jQuery( "#productAdd" )[0].reset();
						   $("#addproductpopup").modal("toggle");						   
						  $.notify(response.message, {color: "#fff", background: "#7FC75F"});
					      setTimeout(function(){ window.location = baseUrl+'products'; }, 1000);
					   }else{
						    $('.ajax-loader').css("visibility", "hidden");
							$.notify(response.error, {color: "#fff", background: "#ff0000"}); 
							return false;
					   }
				  }
				});		
				return false;				
	 }
 });
 /*Add product*/
jQuery("#editProductDetail").validate({
	  errorClass: 'validateError',
	  rules: {
		prodName: {
		  required: true
		},
		 prodPrice: {
		  required: true,
		  money: true
		},
		
		 prodDescription: {
		  required: true
		},
		prodImage:{
        extension: "gif|png|jpeg|jpg|bmp"	
		},
		 address: {
		  required: true
		}
	},
	submitHandler: function (form) {		
			var postUrl = baseUrl+'editProduct';		
				var prodData = new FormData(form);
				$('.ajax-loader').css("visibility", "visible");
				$.ajax({
				  url: postUrl,
				  data: new FormData(form),
				  processData: false,
				  contentType: false,
				  type: 'POST',
				  dataType: 'JSON',
				  success: function(response){
					   if(response.status == "ok"){
						   jQuery( "#editProductDetail" )[0].reset();
						   $("#addproductpopup").modal("toggle");						   
						  $.notify(response.message, {color: "#fff", background: "#7FC75F"});
					      setTimeout(function(){ window.location = baseUrl+'products'; }, 1000);
					   }else{
						    $('.ajax-loader').css("visibility", "hidden");
							$.notify(response.error, {color: "#fff", background: "#ff0000"}); 
							return false;
					   }
				  }
				});
				return false;				
	 }
 });
 /*edit product*/
$(".editproduct").click(function(){
	var getACCiD = $(this).attr("id");
	var getprod = getACCiD.replace("editProduct", '');
	var postUrl = baseUrl+'getproduct';
	$("#editProductDetail")[0].reset();
	$('.ajax-loader').css("visibility", "visible");
     $.post(postUrl, { prod: getprod }, function (response) {
	 $('.ajax-loader').css("visibility", "hidden");
	 var result = $.parseJSON(response);
       $("#editProductDetail #Eproduct").val(result.data.id);
	   $("#editProductDetail #EprodName").val(result.data.productName);
	   $("#editProductDetail #EprodPrice").val(result.data.productPrice);	   
	   $("#editProductDetail #Equantity").val(result.data.productQuantity);
	   $("#editProductDetail #EprodDesc").val(result.data.productDesc);
	   $("#editProductDetail #Eimage-holder").html('<img src="/uploads/images/products/'+result.data.productImg+'" />');	   
	 });
 	$("#editProdPopUp").modal("toggle");
}); 
/*Delete product*/
$(".deleteProduct").on("click", function(){
	var product = ($(this).attr("id")).replace('deleteproduct', '');
	var postUrl = baseUrl+'deleteProduct';
	swal({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then(function () {			
	         $('.ajax-loader').css("visibility", "visible");
			 $.post(postUrl, { product: product }, function (response) {
			 var result = $.parseJSON(response);
              if(result.status == "ok"){
				 $.notify(result.message, {color: "#fff", background: "#7FC75F"});
				 setTimeout(function(){ window.location = baseUrl+'products'; }, 1000); 
			  }else{
				   $('.ajax-loader').css("visibility", "hidden");
                    swal(
						'Error!',
						result.error,
						'error'
					  );
			  }			 
						 
			 });
		});
   });
});

/*maintain user session for View As ...*/
$(document).ready(function(){
	$(".viewDistribuotr").on("click", function(){
	var user = ($(this).attr("id")).replace('viewDistribuotr', '');
	var postUrl = baseUrl+'usernavigate';
	swal({
		  title: 'Are you sure?',
		  text: "You will Only be able to view User Dashboard!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, go for it!'
		}).then(function () {			
	         $('.ajax-loader').css("visibility", "visible");
			 $.post(postUrl, { user: user }, function (response) {
			 var result = $.parseJSON(response);
              if(result.status == "ok"){
				 $.notify(result.message, {color: "#fff", background: "#7FC75F"});
				 setTimeout(function(){ window.location = baseUrl; }, 1000); 
			  }else{
				   $('.ajax-loader').css("visibility", "hidden");
                    swal(
						'Error!',
						result.error,
						'error'
					  );
			  }			 
						 
			 });
		});
   });
/*remove user session to come back own dashboard*/
$("#backtodashboard").click(function(){
	var user = ($(this).attr("id")).replace('viewDistribuotr', '');
	var postUrl = baseUrl+'backtodashboard';
	swal({
		  title: 'Are you sure?',
		  text: "You will be come back to your Dashboard!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, go for it!'
		}).then(function () {			
	         $('.ajax-loader').css("visibility", "visible");
			 $.get(postUrl, function (response) {
			 var result = $.parseJSON(response);
              if(result.status == "ok"){
				 $.notify(result.message, {color: "#fff", background: "#7FC75F"});
				 setTimeout(function(){ window.location = baseUrl; }, 1000); 
			  }else{
				   $('.ajax-loader').css("visibility", "hidden");
                    swal(
						'Error!',
						result.error,
						'error'
					  );
			  }			 
						 
			 });
		});
   });

  /*navegator authorization*/
   $(".navigatorLogin").click(function(){
	   $("#loginNavUser").modal("toggle");
   });
/*password matching*/
jQuery("#loginUserByNavigator").validate({
	  errorClass: 'validateError',
	  rules: {
		password: {
		  required: true
		}
	},
	submitHandler: function (form) {		
			var postUrl = baseUrl+'checkuserpass';		
				var prodData = new FormData(form);
				$('.ajax-loader').css("visibility", "visible");
				$.ajax({
				  url: postUrl,
				  data: new FormData(form),
				  processData: false,
				  contentType: false,
				  type: 'POST',
				  dataType: 'JSON',
				  success: function(response){
					   if(response.status == "ok"){
						   jQuery( "#loginUserByNavigator" )[0].reset();
						   $("#loginNavUser").modal("toggle");						   
						  $.notify(response.message, {color: "#fff", background: "#7FC75F"});
					      setTimeout(function(){ location.reload(); }, 1000);
					   }else{
						    $('.ajax-loader').css("visibility", "hidden");
							$.notify(response.error, {color: "#fff", background: "#ff0000"}); 
							return false;
					   }
				  }
				});
				return false;				
	 }
  });
$('#newcard').prop('checked', true);
$(".cardType").on('change', function(){
	var myRadio = $('input[name="cardtype"]');
	var checkedValue = myRadio.filter(':checked').val();
	if(checkedValue == 'new'){
        $("#ccnumber").show();
		$("#storedCard").hide();
		$("#storedCard, #ccnumber, #expMon, #expYear, #nameOnCard").val('');
		$("#nameOnCard").attr('placeholder', 'Name On Card');
	}else{
		$("#ccnumber").hide();
		$("#storedCard").show();

	}
  });
$("#storedCard").on('change', function(){
	var cardID = $(this).val();
	var ccname = $(this).find(':selected').data('ccname');
	var expdate = $(this).find(':selected').data('expdate');
	var res = expdate.split("/");
	$("#expMon").val(res[0]);
	$("#expYear").val(res[1]);
	var ccexpdate = $(this).data('expdate');
	$("#nameOnCard").val(ccname).attr('placeholder', (ccname == '') ? 'Name On Card' : ccname);
});
/*Pay process*/
  jQuery("#payprocess").validate({
	  errorClass: 'validateError',
	  rules: {
		 ccnumber: {
		 required:true,	
		  creditcard: true
		},
		 nameOnCard: {
		  required:true,
		  minlength: 2
		},	
		expMon:{
           required:true
		},	
		 expYear: {
		 	required:true,
			  CCExp: {
				month: '#expMon',
				year: '#expYear'
			  }
		 }  
	},
	submitHandler: function (form) {
	var postUrlnew = baseUrl+'addnewcard';		
	var formdData = jQuery(form).serialize();
	var myRadio = $('input[name="cardtype"]');
	var checkedValue = myRadio.filter(':checked').val();
    if(checkedValue  == 'new'){
       /*inset card info*/
        $('.ajax-loader').css("visibility", "visible");
			 $.post(postUrlnew, { formData: formdData }, function (response) {
			 var result = $.parseJSON(response);
              if(result.status == "ok"){
              	$('.ajax-loader').css("visibility", "hidden");
				swal(
						'Coming Soon!',
						"Card Added Successfully.Payment Process Functionality Coming Soon!",
						'error'
					  );  
			  }else{
				   $('.ajax-loader').css("visibility", "hidden");
                    swal(
						'Error!',
						result.error,
						'error'
					  );
			  }			 
						 
			 });
    }else{
       swal(
			'Coming Soon!',
			"Payment Process Functionality Coming Soon!",
			'error'
		  );     
    }
	return false;				
	 }
 });
/*save invoice reminder data*/
$(".reminder").on('click', function(){
	var postURL = baseUrl+'addreminder';
	var invoiceID = $(this).data('invoice-id');
	var reminder=[];
	/*get all checked reminder checkbox to update same in database*/
	$('body').find(".reminder").each(function(){
		var reminderID = $(this).attr("id");
		if($("#"+reminderID).is(':checked')){
			reminder.push(reminderID);
		}
	});
	$.post(postURL, {reminderData : reminder, invoiceID:invoiceID },function(response){
		 var result = $.parseJSON(response);
              if(result.status == "ok"){
              	$('.ajax-loader').css("visibility", "hidden");
				swal(
						'Message',
						result.message,
						'success'
					  );  
			  }else{
				   $('.ajax-loader').css("visibility", "hidden");
                    swal(
						'Error!',
						result.error,
						'error'
					  );
			  }	

	});
	
});


});

