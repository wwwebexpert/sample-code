 <?php 
 public function flightpaymentAction(Request $request){
		$session 	= $request->getSession();
        $id_account = $session->get('ID_ACCOUNT');  
        if($session==null ||  empty($id_account) ){ 
            //return $this->redirectToRoute('login');    
        }
		$postData = array();
        parse_str($request->request->get('flightData'), $postData);
		if($postData['terms'] == 'on'){
			$oneWayData = json_decode($postData['oneWayData']);
			$roundData = json_decode($postData['roundData']);
			$clientInfo = $postData['clientInfo'];
			//$cardInfo = $postData['cardData'];
			$creat_Pnr = $flight_info_dep = $flight_info_ret = $client_info = $agency_emails = $fop = $airline = $cardInform = $assignedInfants = array();
			foreach($clientInfo as $ckey => $cvalue){
				$client_info[$ckey+1]["email"] =$cvalue['email'];
				$client_info[$ckey+1]["first_name"] =$cvalue['first-name'];
				$client_info[$ckey+1]["surname"] =$cvalue['last-name'];
				$client_info[$ckey+1]["title"] = "";
				$client_info[$ckey+1]["passenger_type"] = $cvalue['passType'];				
				$client_document = array("type" => $cvalue['docType'], "number" => $cvalue['docNumber'], "country" => $cvalue['docCounty'], "exp_date" =>$cvalue['docExDate']);
				$client_info[$ckey+1]["client_document"] = array($client_document);
				$client_info[$ckey+1]["birth_date"] =$cvalue['dateofbirth'];
				$client_info[$ckey+1]["gender"] = ($cvalue['gender'] == "Male") ? "M" : "F";
				if($cvalue['passType'] == "INF"){
					$assignedInfants[$cvalue['docNumber']] = array("doc" => $clientInfo[0]['docNumber'], "type" => $cvalue['passType'], "birth_date" => $cvalue['dateofbirth']);
				}
			}
			/*flight one way info */
			$oneWayflightDetails = $oneWayData->details;
			foreach($oneWayflightDetails as $key => $value){
				$flight_info_dep[$key]['DepartureDateTime'] = 	$value->DepartureDateTime;
				$flight_info_dep[$key]['ArrivalDateTime']   = 	$value->ArrivalDateTime;
				$flight_info_dep[$key]['StopQuantity'] = 	$oneWayData->stops;
				$flight_info_dep[$key]['FlightNumber'] = 	$value->FlightNumber;
				$flight_info_dep[$key]['ResBookDesigCode'] = 	$value->ResBookDesigCode;
				$flight_info_dep[$key]['ElapsedTime'] = 	$value->ElapsedTime;
				
				$DepartureAirport = array("LocationCode" => $value->DepartureAirport, "content" => "");
				$flight_info_dep[$key]['DepartureAirport'] = 	$DepartureAirport;
				
				$ArrivalAirport = array("LocationCode" => $value->ArrivalAirport, "content" => "");
				$flight_info_dep[$key]['ArrivalAirport'] = 	$ArrivalAirport;
				
				$OperatingAirline = array("Code" => $value->OperatingAirline, "FlightNumber" => $value->FlightNumber, "content" => "");
				$flight_info_dep[$key]['OperatingAirline'] = 	$OperatingAirline;
							
				
				$DepartureTimeZone = array("GMTOffset" => $value->DepartureTimeZone);
				$flight_info_dep[$key]['DepartureTimeZone'] = 	$DepartureTimeZone;
				
				$ArrivalTimeZone = array("GMTOffset" => $value->ArrivalTimeZone);
				$flight_info_dep[$key]['ArrivalTimeZone'] = 	$ArrivalTimeZone;
				
				if(!empty($value->Equipment)){
					$Equipment = array();
					foreach($value->Equipment as $equip){
					$Equipment[$key] = array("AirEquipType" => $equip['AirEquipType'], "content" => "");	
					}					
				    $flight_info_dep[$key]['ArrivalAirport'] = 	$Equipment;
				}
				
				if(!empty($value->MarketingAirline)){
					$MarketingAirline = array("Code" => $value->MarketingAirline['Code'], "content" => "");
				    $flight_info_dep[$key]['MarketingAirline'] = 	$MarketingAirline;
				}
				
				if(!empty($value->MarriageGrp)){
					$flight_info_dep[$key]['MarriageGrp'] = 	$value->MarriageGrp;
				}
				
				if(!empty($value->TPA_Extensions)){
					$eTicket = array();
					foreach($value->TPA_Extensions['eTicket'] as $tk => $tval){
						$eTicket[$key] = array($tk => true);
					}
					$MarketingAirline['eTicket'] = $eTicket;
				    $flight_info_dep[$key]['MarketingAirline'] = 	$MarketingAirline;
				}
				
			}
			/*flight return info */
			$returnflightDetails = $roundData->details;
			if(!empty($returnflightDetails[0]->DepartureAirport)){
			 foreach($returnflightDetails as $key => $value){
				$flight_info_ret[$key]['DepartureDateTime'] = 	$value->DepartureDateTime;
				$flight_info_ret[$key]['ArrivalDateTime']   = 	$value->ArrivalDateTime;
				$flight_info_ret[$key]['StopQuantity'] = 	$oneWayData->stops;
				$flight_info_ret[$key]['FlightNumber'] = 	$value->FlightNumber;
				$flight_info_ret[$key]['ResBookDesigCode'] = 	$value->ResBookDesigCode;
				$flight_info_ret[$key]['ElapsedTime'] = 	$value->ElapsedTime;
				
				$DepartureAirport = array("LocationCode" => $value->DepartureAirport, "content" => "");
				$flight_info_ret[$key]['DepartureAirport'] = 	$DepartureAirport;
				
				$ArrivalAirport = array("LocationCode" => $value->ArrivalAirport, "content" => "");
				$flight_info_ret[$key]['ArrivalAirport'] = 	$ArrivalAirport;
				
				$OperatingAirline = array("Code" => $value->OperatingAirline, "FlightNumber" => $value->FlightNumber, "content" => "");
				$flight_info_ret[$key]['OperatingAirline'] = 	$OperatingAirline;
							
				
				$DepartureTimeZone = array("GMTOffset" => $value->DepartureTimeZone);
				$flight_info_ret[$key]['DepartureTimeZone'] = 	$DepartureTimeZone;
				
				$ArrivalTimeZone = array("GMTOffset" => $value->ArrivalTimeZone);
				$flight_info_ret[$key]['ArrivalTimeZone'] = 	$ArrivalTimeZone;
				
				if(!empty($value->Equipment)){
					$Equipment = array();
					foreach($value->Equipment as $equip){
					$Equipment[$key] = array("AirEquipType" => $equip['AirEquipType'], "content" => "");	
					}					
				    $flight_info_ret[$key]['ArrivalAirport'] = 	$Equipment;
				}
				
				if(!empty($value->MarketingAirline)){
					$MarketingAirline = array("Code" => $value->MarketingAirline['Code'], "content" => "");
				    $flight_info_ret[$key]['MarketingAirline'] = 	$MarketingAirline;
				}
				
				if(!empty($value->MarriageGrp)){
					$flight_info_ret[$key]['MarriageGrp'] = 	$value->MarriageGrp;
				}
				
				if(!empty($value->TPA_Extensions)){
					$eTicket = array();
					foreach($value->TPA_Extensions['eTicket'] as $tk => $tval){
						$eTicket[$key] = array($tk => true);
					}
					$MarketingAirline['eTicket'] = $eTicket;
				    $flight_info_ret[$key]['MarketingAirline'] = 	$MarketingAirline;
				}
				
			 }
			
			}
			$agent_name = array("agent_name" => "Minumerolocal");
			$agency_emails = array("1" => "travels@minumerolocal.com");
			
			/*$cardInform['number'] = $cardInfo['ccNumber'];
			$cardInform['expdate'] = $cardInfo['expiryYear'].'-'.$cardInfo['expiryMonth'];
			$cardInform['cvv'] = $cardInfo['cvv'];
			$cardInform['code'] = '';			
			$fop = array("type" =>"card", "card_info" => $cardInform);*/
			$fop = array("type" => "cash");
			$airlineName = $postData['airline'];
			
			
				/*get token*/
				$username = 'example_user';
				$password = '12121212';
				$tokenFields     = array('class' => 'UserInfo','function' => 'get_token','data' => json_encode(array('user_name'=> $username,'password'=>$password)));  
				$token =$this->getToken($tokenFields);
				
				$creat_Pnr = array(
				      "action" => "finish_operation",
				      "flight_info_dep" => json_encode($flight_info_dep),
					  "flight_info_ret" => json_encode($flight_info_ret),
					  "client_info" =>json_encode($client_info),					 
					  "agent_name" => '"Ashok"',
					  "emails" => json_encode($agency_emails),
					  "fop" =>  json_encode($fop),			
                      "airline" => $postData['airline'],
                      "token" =>  $token->data->token
				    );
				if(!empty($assignedInfants)){
				$creat_Pnr['assignedInfants'] = json_encode($assignedInfants);
			}
				$getResponse     = array('class' => 'RestApi','function' => 'create_pnr','data' => json_encode($creat_Pnr));  
				$flightAPIResponse =$this->flightAPIcurl($getResponse);
				return new JsonResponse($flightAPIResponse);  
	
		}else{
			$array = array('result'=>"error");
			return new JsonResponse(json_encode($array));  
		}		 
	}
   