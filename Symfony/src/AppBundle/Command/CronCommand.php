<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use AppBundle\Entity\TblInvoice;
use AppBundle\Entity\TblPaymentReminder;

class CronCommand extends ContainerAwareCommand
{
    protected $em;


    protected function configure()
    {
        $this->setName('cronjob:paymentreminder')
            ->setDescription('Invoice Payment Reminder')
            ->addArgument('args', InputArgument::OPTIONAL,'Who do you want to greet?')
            ->addOption('yell', null, InputOption::VALUE_NONE, 'If set, the task will yell in uppercase letters');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $query = $this->em->createQueryBuilder()
                 ->select('t', 'u')
                 ->from('AppBundle:TblPaymentReminder', 't')
                 ->leftjoin("t.invoice", 'u')
                 ->where('u.paidStatus = :paidval')
                 ->andWhere('t.status = :status')
                 ->setParameter('paidval', '0')
                 ->setParameter('status', '1')
                 ->getQuery()
                 ->getResult(Query::HYDRATE_ARRAY);  

       
            $message = \Swift_Message::newInstance()
            ->setSubject('Invoice Payment Reminder!')
            ->setFrom('ashok.sharmae@mobilyte.com')
            ->setTo('ashok.sharmae@mobilyte.com')
            ->setCharset('iso-8859-1')
            ->setContentType('text/html')
            ->setBody(
               $this->getContainer()->get('templating')->render(
                    'emails/profile.html.twig',
                    array('urlString' => 'test')
                )
                //$this->templating->render('emails/profile.html.twig', array('urlString' => 'test') )
            );
            $this->get('mailer')->send($message);
        $args = $input->getArgument('args');
        if ($args) {
            $text = 'Hello '.$args. json_encode($query);
        } else {
            $text = 'Hello';
        }
        $output->writeln($text);
    }
}

