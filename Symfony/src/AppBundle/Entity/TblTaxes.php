<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblTaxes
 *
 * @ORM\Table(name="tbl_taxes", indexes={@ORM\Index(name="org_id", columns={"org_id"})})
 * @ORM\Entity
 */
class TblTaxes
{
    /**
     * @var string
     *
     * @ORM\Column(name="tax_name", type="string", length=100, nullable=false)
     */
    private $taxName;

    /**
     * @var string
     *
     * @ORM\Column(name="tax_abbreviation", type="string", length=100, nullable=true)
     */
    private $taxAbbreviation;

    /**
     * @var string
     *
     * @ORM\Column(name="tax_desc", type="string", length=512, nullable=true)
     */
    private $taxDesc;

    /**
     * @var integer
     *
     * @ORM\Column(name="tax_number", type="bigint", nullable=false)
     */
    private $taxNumber;

    /**
     * @var float
     *
     * @ORM\Column(name="tax_rate", type="float", precision=10, scale=0, nullable=false)
     */
    private $taxRate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\TblUsers
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TblUsers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="org_id", referencedColumnName="id")
     * })
     */
    private $org;



    /**
     * Set taxName
     *
     * @param string $taxName
     *
     * @return TblTaxes
     */
    public function setTaxName($taxName)
    {
        $this->taxName = $taxName;

        return $this;
    }

    /**
     * Get taxName
     *
     * @return string
     */
    public function getTaxName()
    {
        return $this->taxName;
    }

    /**
     * Set taxAbbreviation
     *
     * @param string $taxAbbreviation
     *
     * @return TblTaxes
     */
    public function setTaxAbbreviation($taxAbbreviation)
    {
        $this->taxAbbreviation = $taxAbbreviation;

        return $this;
    }

    /**
     * Get taxAbbreviation
     *
     * @return string
     */
    public function getTaxAbbreviation()
    {
        return $this->taxAbbreviation;
    }

    /**
     * Set taxDesc
     *
     * @param string $taxDesc
     *
     * @return TblTaxes
     */
    public function setTaxDesc($taxDesc)
    {
        $this->taxDesc = $taxDesc;

        return $this;
    }

    /**
     * Get taxDesc
     *
     * @return string
     */
    public function getTaxDesc()
    {
        return $this->taxDesc;
    }

    /**
     * Set taxNumber
     *
     * @param integer $taxNumber
     *
     * @return TblTaxes
     */
    public function setTaxNumber($taxNumber)
    {
        $this->taxNumber = $taxNumber;

        return $this;
    }

    /**
     * Get taxNumber
     *
     * @return integer
     */
    public function getTaxNumber()
    {
        return $this->taxNumber;
    }

    /**
     * Set taxRate
     *
     * @param float $taxRate
     *
     * @return TblTaxes
     */
    public function setTaxRate($taxRate)
    {
        $this->taxRate = $taxRate;

        return $this;
    }

    /**
     * Get taxRate
     *
     * @return float
     */
    public function getTaxRate()
    {
        return $this->taxRate;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return TblTaxes
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return TblTaxes
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set org
     *
     * @param \AppBundle\Entity\TblUsers $org
     *
     * @return TblTaxes
     */
    public function setOrg(\AppBundle\Entity\TblUsers $org = null)
    {
        $this->org = $org;

        return $this;
    }

    /**
     * Get org
     *
     * @return \AppBundle\Entity\TblUsers
     */
    public function getOrg()
    {
        return $this->org;
    }
}
