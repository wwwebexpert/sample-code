<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblCcinfo
 *
 * @ORM\Table(name="tbl_ccinfo", indexes={@ORM\Index(name="customer_id", columns={"customer_id"})})
 * @ORM\Entity
 */
class TblCcinfo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="cc_number", type="bigint", nullable=false)
     */
    private $ccNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="cc_name", type="string", length=60, nullable=false)
     */
    private $ccName;

    /**
     * @var string
     *
     * @ORM\Column(name="cc_expdate", type="string", length=15, nullable=false)
     */
    private $ccExpdate;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\TblCustomers
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TblCustomers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     * })
     */
    private $customer;



    /**
     * Set ccNumber
     *
     * @param integer $ccNumber
     *
     * @return TblCcinfo
     */
    public function setCcNumber($ccNumber)
    {
        $this->ccNumber = $ccNumber;

        return $this;
    }

    /**
     * Get ccNumber
     *
     * @return integer
     */
    public function getCcNumber()
    {
        return $this->ccNumber;
    }

    /**
     * Set ccName
     *
     * @param string $ccName
     *
     * @return TblCcinfo
     */
    public function setCcName($ccName)
    {
        $this->ccName = $ccName;

        return $this;
    }

    /**
     * Get ccName
     *
     * @return string
     */
    public function getCcName()
    {
        return $this->ccName;
    }

    /**
     * Set ccExpdate
     *
     * @param string $ccExpdate
     *
     * @return TblCcinfo
     */
    public function setCcExpdate($ccExpdate)
    {
        $this->ccExpdate = $ccExpdate;

        return $this;
    }

    /**
     * Get ccExpdate
     *
     * @return string
     */
    public function getCcExpdate()
    {
        return $this->ccExpdate;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return TblCcinfo
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customer
     *
     * @param \AppBundle\Entity\TblCustomers $customer
     *
     * @return TblCcinfo
     */
    public function setCustomer(\AppBundle\Entity\TblCustomers $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \AppBundle\Entity\TblCustomers
     */
    public function getCustomer()
    {
        return $this->customer;
    }
}
