<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblInvoice
 *
 * @ORM\Table(name="tbl_invoice", indexes={@ORM\Index(name="currency", columns={"currency"}), @ORM\Index(name="user_id", columns={"user_id"}), @ORM\Index(name="customer_id", columns={"customer_id"})})
 * @ORM\Entity
 */
class TblInvoice
{
    /**
     * @var string
     *
     * @ORM\Column(name="poso_number", type="string", length=100, nullable=true)
     */
    private $posoNumber;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="invoice_start", type="datetime", nullable=false)
     */
    private $invoiceStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="payment_due", type="datetime", nullable=false)
     */
    private $paymentDue;

    /**
     * @var float
     *
     * @ORM\Column(name="total_amount", type="float", precision=10, scale=0, nullable=true)
     */
    private $totalAmount;

    /**
     * @var float
     *
     * @ORM\Column(name="total_tax", type="float", precision=10, scale=0, nullable=true)
     */
    private $totalTax;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="invoice_sent_date", type="datetime", nullable=true)
     */
    private $invoiceSentDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="invoice_skip_send", type="integer", nullable=true)
     */
    private $invoiceSkipSend;

    /**
     * @var string
     *
     * @ORM\Column(name="invoice_due", type="string", nullable=false)
     */
    private $invoiceDue = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="invoice_sent", type="string", nullable=false)
     */
    private $invoiceSent = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="invoice_data", type="text", length=65535, nullable=true)
     */
    private $invoiceData;

    /**
     * @var string
     *
     * @ORM\Column(name="paid_status", type="string", nullable=false)
     */
    private $paidStatus = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\TblCurrencies
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TblCurrencies")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="currency", referencedColumnName="id")
     * })
     */
    private $currency;

    /**
     * @var \AppBundle\Entity\TblCustomers
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TblCustomers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     * })
     */
    private $customer;

    /**
     * @var \AppBundle\Entity\TblUsers
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TblUsers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;



    /**
     * Set posoNumber
     *
     * @param string $posoNumber
     *
     * @return TblInvoice
     */
    public function setPosoNumber($posoNumber)
    {
        $this->posoNumber = $posoNumber;

        return $this;
    }

    /**
     * Get posoNumber
     *
     * @return string
     */
    public function getPosoNumber()
    {
        return $this->posoNumber;
    }

    /**
     * Set invoiceStart
     *
     * @param \DateTime $invoiceStart
     *
     * @return TblInvoice
     */
    public function setInvoiceStart($invoiceStart)
    {
        $this->invoiceStart = $invoiceStart;

        return $this;
    }

    /**
     * Get invoiceStart
     *
     * @return \DateTime
     */
    public function getInvoiceStart()
    {
        return $this->invoiceStart;
    }

    /**
     * Set paymentDue
     *
     * @param \DateTime $paymentDue
     *
     * @return TblInvoice
     */
    public function setPaymentDue($paymentDue)
    {
        $this->paymentDue = $paymentDue;

        return $this;
    }

    /**
     * Get paymentDue
     *
     * @return \DateTime
     */
    public function getPaymentDue()
    {
        return $this->paymentDue;
    }

    /**
     * Set totalAmount
     *
     * @param float $totalAmount
     *
     * @return TblInvoice
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = $totalAmount;

        return $this;
    }

    /**
     * Get totalAmount
     *
     * @return float
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     * Set totalTax
     *
     * @param float $totalTax
     *
     * @return TblInvoice
     */
    public function setTotalTax($totalTax)
    {
        $this->totalTax = $totalTax;

        return $this;
    }

    /**
     * Get totalTax
     *
     * @return float
     */
    public function getTotalTax()
    {
        return $this->totalTax;
    }

    /**
     * Set invoiceSentDate
     *
     * @param \DateTime $invoiceSentDate
     *
     * @return TblInvoice
     */
    public function setInvoiceSentDate($invoiceSentDate)
    {
        $this->invoiceSentDate = $invoiceSentDate;

        return $this;
    }

    /**
     * Get invoiceSentDate
     *
     * @return \DateTime
     */
    public function getInvoiceSentDate()
    {
        return $this->invoiceSentDate;
    }

    /**
     * Set invoiceSkipSend
     *
     * @param integer $invoiceSkipSend
     *
     * @return TblInvoice
     */
    public function setInvoiceSkipSend($invoiceSkipSend)
    {
        $this->invoiceSkipSend = $invoiceSkipSend;

        return $this;
    }

    /**
     * Get invoiceSkipSend
     *
     * @return integer
     */
    public function getInvoiceSkipSend()
    {
        return $this->invoiceSkipSend;
    }

    /**
     * Set invoiceDue
     *
     * @param string $invoiceDue
     *
     * @return TblInvoice
     */
    public function setInvoiceDue($invoiceDue)
    {
        $this->invoiceDue = $invoiceDue;

        return $this;
    }

    /**
     * Get invoiceDue
     *
     * @return string
     */
    public function getInvoiceDue()
    {
        return $this->invoiceDue;
    }

    /**
     * Set invoiceSent
     *
     * @param string $invoiceSent
     *
     * @return TblInvoice
     */
    public function setInvoiceSent($invoiceSent)
    {
        $this->invoiceSent = $invoiceSent;

        return $this;
    }

    /**
     * Get invoiceSent
     *
     * @return string
     */
    public function getInvoiceSent()
    {
        return $this->invoiceSent;
    }

    /**
     * Set invoiceData
     *
     * @param string $invoiceData
     *
     * @return TblInvoice
     */
    public function setInvoiceData($invoiceData)
    {
        $this->invoiceData = $invoiceData;

        return $this;
    }

    /**
     * Get invoiceData
     *
     * @return string
     */
    public function getInvoiceData()
    {
        return $this->invoiceData;
    }

    /**
     * Set paidStatus
     *
     * @param string $paidStatus
     *
     * @return TblInvoice
     */
    public function setPaidStatus($paidStatus)
    {
        $this->paidStatus = $paidStatus;

        return $this;
    }

    /**
     * Get paidStatus
     *
     * @return string
     */
    public function getPaidStatus()
    {
        return $this->paidStatus;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return TblInvoice
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return TblInvoice
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return TblInvoice
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set currency
     *
     * @param \AppBundle\Entity\TblCurrencies $currency
     *
     * @return TblInvoice
     */
    public function setCurrency(\AppBundle\Entity\TblCurrencies $currency = null)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return \AppBundle\Entity\TblCurrencies
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set customer
     *
     * @param \AppBundle\Entity\TblCustomers $customer
     *
     * @return TblInvoice
     */
    public function setCustomer(\AppBundle\Entity\TblCustomers $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \AppBundle\Entity\TblCustomers
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\TblUsers $user
     *
     * @return TblInvoice
     */
    public function setUser(\AppBundle\Entity\TblUsers $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\TblUsers
     */
    public function getUser()
    {
        return $this->user;
    }
}
