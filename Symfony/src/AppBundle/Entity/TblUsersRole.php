<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblUsersRole
 *
 * @ORM\Table(name="tbl_users_role")
 * @ORM\Entity
 */
class TblUsersRole
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=60, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=30, nullable=false)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status = 'enable';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
        public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }
	/**
     * Set password
     *
     * @param string $password
     * @return password
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }
	/**
     * Set email
     *
     * @param string $email
     * @return email
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }
	
	/**
     * Set userRole
     *
     * @param int $userRole
     * @return userRole
     */
    public function setUserRole($userRole)
    {
        $this->userRole = $userRole;

        return $this;
    }

    /**
     * Get userRole
     *
     * @return int 
     */
    public function getUserRole()
    {
        return $this->userRole;
    }
	 /**
     * Get id
     *
     * @return string 
     */
    public function getId()
    {
        return $this->id;
    }
	/**
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function setCreatedOn()
	{
		$this->createdOn = new \DateTime();
		return $this;
	}
	/**
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function setCreatedBy($createdBy)
	{
		$this->createdBy = $createdBy;
		return $this;
	}

}

