<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblPaymentRecords
 *
 * @ORM\Table(name="tbl_payment_records", indexes={@ORM\Index(name="user_id", columns={"user_id"}), @ORM\Index(name="invoice_id", columns={"invoice_id"})})
 * @ORM\Entity
 */
class TblPaymentRecords
{
    /**
     * @var integer
     *
     * @ORM\Column(name="payment_id", type="bigint", nullable=false)
     */
    private $paymentId;

    /**
     * @var float
     *
     * @ORM\Column(name="payment_amount", type="float", precision=10, scale=0, nullable=false)
     */
    private $paymentAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_method", type="string", length=100, nullable=false)
     */
    private $paymentMethod;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_account", type="string", length=100, nullable=false)
     */
    private $paymentAccount;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_memo", type="text", length=65535, nullable=false)
     */
    private $paymentMemo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=false)
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=true)
     */
    private $updatedDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\TblInvoice
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TblInvoice")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="invoice_id", referencedColumnName="id")
     * })
     */
    private $invoice;

    /**
     * @var \AppBundle\Entity\TblUsers
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TblUsers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;



    /**
     * Set paymentId
     *
     * @param integer $paymentId
     *
     * @return TblPaymentRecords
     */
    public function setPaymentId($paymentId)
    {
        $this->paymentId = $paymentId;

        return $this;
    }

    /**
     * Get paymentId
     *
     * @return integer
     */
    public function getPaymentId()
    {
        return $this->paymentId;
    }

    /**
     * Set paymentAmount
     *
     * @param float $paymentAmount
     *
     * @return TblPaymentRecords
     */
    public function setPaymentAmount($paymentAmount)
    {
        $this->paymentAmount = $paymentAmount;

        return $this;
    }

    /**
     * Get paymentAmount
     *
     * @return float
     */
    public function getPaymentAmount()
    {
        return $this->paymentAmount;
    }

    /**
     * Set paymentMethod
     *
     * @param string $paymentMethod
     *
     * @return TblPaymentRecords
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return string
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * Set paymentAccount
     *
     * @param string $paymentAccount
     *
     * @return TblPaymentRecords
     */
    public function setPaymentAccount($paymentAccount)
    {
        $this->paymentAccount = $paymentAccount;

        return $this;
    }

    /**
     * Get paymentAccount
     *
     * @return string
     */
    public function getPaymentAccount()
    {
        return $this->paymentAccount;
    }

    /**
     * Set paymentMemo
     *
     * @param string $paymentMemo
     *
     * @return TblPaymentRecords
     */
    public function setPaymentMemo($paymentMemo)
    {
        $this->paymentMemo = $paymentMemo;

        return $this;
    }

    /**
     * Get paymentMemo
     *
     * @return string
     */
    public function getPaymentMemo()
    {
        return $this->paymentMemo;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     *
     * @return TblPaymentRecords
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Set updatedDate
     *
     * @param \DateTime $updatedDate
     *
     * @return TblPaymentRecords
     */
    public function setUpdatedDate($updatedDate)
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    /**
     * Get updatedDate
     *
     * @return \DateTime
     */
    public function getUpdatedDate()
    {
        return $this->updatedDate;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set invoice
     *
     * @param \AppBundle\Entity\TblInvoice $invoice
     *
     * @return TblPaymentRecords
     */
    public function setInvoice(\AppBundle\Entity\TblInvoice $invoice = null)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get invoice
     *
     * @return \AppBundle\Entity\TblInvoice
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\TblUsers $user
     *
     * @return TblPaymentRecords
     */
    public function setUser(\AppBundle\Entity\TblUsers $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\TblUsers
     */
    public function getUser()
    {
        return $this->user;
    }
}
