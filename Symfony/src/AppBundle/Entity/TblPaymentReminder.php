<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblPaymentReminder
 *
 * @ORM\Table(name="tbl_payment_reminder", indexes={@ORM\Index(name="invoice_id", columns={"invoice_id"}), @ORM\Index(name="user_id", columns={"user_id"})})
 * @ORM\Entity
 */
class TblPaymentReminder
{
    /**
     * @var string
     *
     * @ORM\Column(name="reminder_data", type="text", length=65535, nullable=true)
     */
    private $reminderData;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\TblInvoice
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TblInvoice")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="invoice_id", referencedColumnName="id")
     * })
     */
    private $invoice;

    /**
     * @var \AppBundle\Entity\TblUsers
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TblUsers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;



    /**
     * Set reminderData
     *
     * @param string $reminderData
     *
     * @return TblPaymentReminder
     */
    public function setReminderData($reminderData)
    {
        $this->reminderData = $reminderData;

        return $this;
    }

    /**
     * Get reminderData
     *
     * @return string
     */
    public function getReminderData()
    {
        return $this->reminderData;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return TblPaymentReminder
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return TblPaymentReminder
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return TblPaymentReminder
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set invoice
     *
     * @param \AppBundle\Entity\TblInvoice $invoice
     *
     * @return TblPaymentReminder
     */
    public function setInvoice(\AppBundle\Entity\TblInvoice $invoice = null)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get invoice
     *
     * @return \AppBundle\Entity\TblInvoice
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\TblUsers $user
     *
     * @return TblPaymentReminder
     */
    public function setUser(\AppBundle\Entity\TblUsers $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\TblUsers
     */
    public function getUser()
    {
        return $this->user;
    }
}
