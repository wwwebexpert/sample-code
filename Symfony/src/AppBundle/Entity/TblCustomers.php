<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblCustomers
 *
 * @ORM\Table(name="tbl_customers", indexes={@ORM\Index(name="emp_id", columns={"emp_id"}), @ORM\Index(name="org_id", columns={"org_id"})})
 * @ORM\Entity
 */
class TblCustomers
{
    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=60, nullable=false)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=60, nullable=false)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=40, nullable=false)
     */
    private $email;

    /**
     * @var integer
     *
     * @ORM\Column(name="phone", type="bigint", nullable=false)
     */
    private $phone;

    /**
     * @var integer
     *
     * @ORM\Column(name="currency", type="integer", nullable=true)
     */
    private $currency;

    /**
     * @var integer
     *
     * @ORM\Column(name="fax", type="bigint", nullable=true)
     */
    private $fax;

    /**
     * @var integer
     *
     * @ORM\Column(name="mobile", type="bigint", nullable=true)
     */
    private $mobile;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text", length=65535, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=60, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=60, nullable=true)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=60, nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="zipcode", type="string", length=15, nullable=true)
     */
    private $zipcode;

    /**
     * @var string
     *
     * @ORM\Column(name="s_address", type="text", length=65535, nullable=true)
     */
    private $sAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="s_city", type="string", length=60, nullable=true)
     */
    private $sCity;

    /**
     * @var string
     *
     * @ORM\Column(name="s_state", type="string", length=60, nullable=true)
     */
    private $sState;

    /**
     * @var string
     *
     * @ORM\Column(name="s_country", type="string", length=60, nullable=true)
     */
    private $sCountry;

    /**
     * @var string
     *
     * @ORM\Column(name="s_zipcode", type="string", length=15, nullable=true)
     */
    private $sZipcode;

    /**
     * @var string
     *
     * @ORM\Column(name="doc_files", type="text", length=65535, nullable=true)
     */
    private $docFiles;

    /**
     * @var string
     *
     * @ORM\Column(name="user_ip", type="string", length=30, nullable=true)
     */
    private $userIp;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_by", type="bigint", nullable=false)
     */
    private $createdBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="is_deleted", type="string", nullable=false)
     */
    private $isDeleted = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\TblUsers
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TblUsers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="emp_id", referencedColumnName="id")
     * })
     */
    private $emp;

    /**
     * @var \AppBundle\Entity\TblUsers
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TblUsers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="org_id", referencedColumnName="id")
     * })
     */
    private $org;



    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return TblCustomers
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return TblCustomers
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return TblCustomers
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param integer $phone
     *
     * @return TblCustomers
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return integer
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set currency
     *
     * @param integer $currency
     *
     * @return TblCustomers
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return integer
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set fax
     *
     * @param integer $fax
     *
     * @return TblCustomers
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return integer
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set mobile
     *
     * @param integer $mobile
     *
     * @return TblCustomers
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return integer
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return TblCustomers
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return TblCustomers
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return TblCustomers
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return TblCustomers
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set zipcode
     *
     * @param string $zipcode
     *
     * @return TblCustomers
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * Get zipcode
     *
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * Set sAddress
     *
     * @param string $sAddress
     *
     * @return TblCustomers
     */
    public function setSAddress($sAddress)
    {
        $this->sAddress = $sAddress;

        return $this;
    }

    /**
     * Get sAddress
     *
     * @return string
     */
    public function getSAddress()
    {
        return $this->sAddress;
    }

    /**
     * Set sCity
     *
     * @param string $sCity
     *
     * @return TblCustomers
     */
    public function setSCity($sCity)
    {
        $this->sCity = $sCity;

        return $this;
    }

    /**
     * Get sCity
     *
     * @return string
     */
    public function getSCity()
    {
        return $this->sCity;
    }

    /**
     * Set sState
     *
     * @param string $sState
     *
     * @return TblCustomers
     */
    public function setSState($sState)
    {
        $this->sState = $sState;

        return $this;
    }

    /**
     * Get sState
     *
     * @return string
     */
    public function getSState()
    {
        return $this->sState;
    }

    /**
     * Set sCountry
     *
     * @param string $sCountry
     *
     * @return TblCustomers
     */
    public function setSCountry($sCountry)
    {
        $this->sCountry = $sCountry;

        return $this;
    }

    /**
     * Get sCountry
     *
     * @return string
     */
    public function getSCountry()
    {
        return $this->sCountry;
    }

    /**
     * Set sZipcode
     *
     * @param string $sZipcode
     *
     * @return TblCustomers
     */
    public function setSZipcode($sZipcode)
    {
        $this->sZipcode = $sZipcode;

        return $this;
    }

    /**
     * Get sZipcode
     *
     * @return string
     */
    public function getSZipcode()
    {
        return $this->sZipcode;
    }

    /**
     * Set docFiles
     *
     * @param string $docFiles
     *
     * @return TblCustomers
     */
    public function setDocFiles($docFiles)
    {
        $this->docFiles = $docFiles;

        return $this;
    }

    /**
     * Get docFiles
     *
     * @return string
     */
    public function getDocFiles()
    {
        return $this->docFiles;
    }

    /**
     * Set userIp
     *
     * @param string $userIp
     *
     * @return TblCustomers
     */
    public function setUserIp($userIp)
    {
        $this->userIp = $userIp;

        return $this;
    }

    /**
     * Get userIp
     *
     * @return string
     */
    public function getUserIp()
    {
        return $this->userIp;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return TblCustomers
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param integer $createdBy
     *
     * @return TblCustomers
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return integer
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return TblCustomers
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set isDeleted
     *
     * @param string $isDeleted
     *
     * @return TblCustomers
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return string
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return TblCustomers
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set emp
     *
     * @param \AppBundle\Entity\TblUsers $emp
     *
     * @return TblCustomers
     */
    public function setEmp(\AppBundle\Entity\TblUsers $emp = null)
    {
        $this->emp = $emp;

        return $this;
    }

    /**
     * Get emp
     *
     * @return \AppBundle\Entity\TblUsers
     */
    public function getEmp()
    {
        return $this->emp;
    }

    /**
     * Set org
     *
     * @param \AppBundle\Entity\TblUsers $org
     *
     * @return TblCustomers
     */
    public function setOrg(\AppBundle\Entity\TblUsers $org = null)
    {
        $this->org = $org;

        return $this;
    }

    /**
     * Get org
     *
     * @return \AppBundle\Entity\TblUsers
     */
    public function getOrg()
    {
        return $this->org;
    }
}
