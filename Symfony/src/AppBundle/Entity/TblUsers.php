<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblUsers
 * 
 * @ORM\Table(name="tbl_users", indexes={@ORM\Index(name="user_role", columns={"user_role"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TblUsersRepository")
 */
class TblUsers
{
    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=60, nullable=false)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=60, nullable=false)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=50, nullable=false)
     */
    private $email;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_by", type="integer", nullable=false)
     */
    private $createdBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime", nullable=false)
     */
    private $createdOn = 'CURRENT_TIMESTAMP';
	
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_on", type="datetime", nullable=false)
     */
    private $updatedOn = 'CURRENT_TIMESTAMP';
	
    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status = 'enable';
    
    /**
     * @var string
     *
     * @ORM\Column(name="forget_pass", type="string", length=100, nullable=true)
     */
    private $forgetPass;
	
	/**
     * @var string
     *
     * @ORM\Column(name="isDeleted", type="string", nullable=false)
     */
    private $isDeleted = '0';
	
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\TblUsersRole
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TblUsersRole")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_role", referencedColumnName="id")
     * })
     */
    private $userRole;
	/**
     * Set username
     *
     * @param string $username
     * @return Username
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }
	/**
     * Set password
     *
     * @param string $password
     * @return password
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }
	/**
     * Set email
     *
     * @param string $email
     * @return email
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }
	
	/**
     * Set userRole
     *
     * @param int $userRole
     * @return userRole
     */
    public function setUserRole($userRole)
    {
        $this->userRole = $userRole;

        return $this;
    }

    /**
     * Get userRole
     *
     * @return int 
     */
    public function getUserRole()
    {
        return $this->userRole;
    }
	 /**
     * Get id
     *
     * @return string 
     */
    public function getId()
    {
        return $this->id;
    }
	/**
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function setCreatedOn()
	{
		$this->createdOn = new \DateTime();
		return $this->createdOn;
	}
	public function setUpdatedOn()
	{
		$this->updatedOn = new \DateTime();
		return $this->updatedOn;
	}
	/**
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function setCreatedBy($createdBy)
	{
		$this->createdBy = $createdBy;
		return $this;
	}
	public function getCreatedBy()
	{
		return $this->createdBy;
	}
	/**
	* @ getter setter for forgetPass
	*/
	public function setForgetPass($forgetPass)
    {
        $this->forgetPass = $forgetPass;

        return $this;
    }
    public function getForgetPass()
    {
        return $this->forgetPass;
    }
}

