<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use AppBundle\Entity\TblUsersRole;
use AppBundle\Entity\TblUsers;
use AppBundle\Entity\TblAccount;
use AppBundle\Entity\TblProducts;

class ProductsController extends Controller
{
    /**
     * @Route("/products", name="products")
     */
    public function productsAction(Request $request)
    {
		$session 	= $request->getSession();
		if(empty($session->get('userInfo'))){
			return $this->redirectToRoute('login'); 
		}else{
			$userData = $session->get('userInfo');
			$getAllaccounts = $getUserProd = array();
			$getproduct = $this->getDoctrine()->getManager();
			/*get product listing*/
			   $userID = $userData['userID'];
				if($userData['userRoleSlug'] == 'organization'){
				$getUserProd = $getproduct->createQueryBuilder('t')
                ->select('t')
                ->from('AppBundle:TblProducts', 't')
				->where('t.user = :userId')
				->andWhere('t.status = :status')
                ->setParameter('userId', $userData['userID'])
                ->setParameter('status', '1')				
                ->orderBy('t.id', 'DESC')				
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
				}else if($userData['userRoleSlug'] == 'employee'){
				$getUserProd = $getproduct->createQueryBuilder('t')
                ->select('t')
                ->from('AppBundle:TblProducts', 't')
				->where('t.user = :userId')
				->andWhere('t.status = :status')
                ->setParameter('userId', $userData['creatdby'])
                ->setParameter('status', '1')				
                ->orderBy('t.id', 'DESC')				
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);	
				}
				
			$data = array();
			 return $this->render('dashboard/organization/product.html.twig', array(
				'title'			=>'Products',
				'data'	=> $data,
				'userData' => $userData,
				'accounts' => $getAllaccounts,
				'productData' => $getUserProd
			));
		} 
		 
    }
	/**
     * @Route("/addProduct", name="addProduct")
     */
	public function addProduct(Request $request){
		$session 	= $request->getSession();
		$sessData = $session->get('userInfo');
		if($sessData['userRoleName'] == 'Employee'){
			$userID = $sessData['creatdby'];
		}else{
			$userID = $sessData['userID'];
		}
		$web = $request->server->get('DOCUMENT_ROOT');
		if(!empty($request->request->get('prodName'))){
			$img = $request->files->get('prodImage');
			$fileName = uniqid() . '_' . $img->getClientOriginalName();
			$img->move($this->getParameter('product_img'), $fileName);			
			$em = $this->getDoctrine()->getManager();
			$product= new TblProducts();
			$product->setProductName($request->request->get('prodName'));
			$product->setProductDesc($request->request->get('prodDescription'));
			$product->setProductPrice($request->request->get('prodPrice'));
			$product->setProductQuantity($request->request->get('quantity'));
			$product->setProductImg($fileName);			
			$product->setCreatedAt();
			$product->setUpdatedAt();
			$product->setStatus('1');
			$userId = $em->getRepository('AppBundle:TblUsers')->find($userID);
			$product->setUser($userId);
            $em->persist($product);
			$em->flush();
			if(!empty($product->getId())){
				$response['status']= "ok";
				$response['data'] = "";
				$response['message'] = "Product has been added successfully!";
				echo json_encode($response); die(); 
			}else{
				$response['status']= "error";
				$response['data'] = "";
				$response['message'] = "Something went wrong!";
				echo json_encode($response); die(); 
			}			
		}else{
			    $response['status']= "error";
				$response['data'] = "";
				$response['error'] = "Not able to process this request!";
				echo json_encode($response); die();
		}
 
	}
	/**
     * @Route("/getproduct", name="getproduct")
     */
	public function getproduct(Request $request){
		$session 	= $request->getSession();
		$productID = $request->request->get('prod');
		$getproduct = $this->getDoctrine()->getManager();
		if(!empty($productID)){
			$getProdDetail = $getproduct->createQueryBuilder('t')
                ->select('t')
                ->from('AppBundle:TblProducts', 't')
				->where('t.id = :id')
				->andWhere('t.status = :status')
                ->setParameter('id', $productID)
                ->setParameter('status', '1')			
                ->getQuery()
                ->getoneorNullResult(Query::HYDRATE_ARRAY);
			if(!empty($getProdDetail)){
				$response['status']= "ok";
				$response['data'] = $getProdDetail;
				$response['message'] = "";
				echo json_encode($response); die();
			}else{
				$response['status']= "error";
				$response['data'] = "";
				$response['error'] = "Not able to process this request!";
				echo json_encode($response); die();
			}	
				
		}else{
			    $response['status']= "error";
				$response['data'] = "";
				$response['error'] = "Not able to process this request!";
				echo json_encode($response); die();
		}
	}
	/**
     * @Route("/editProduct", name="editProduct")
     */
	public function editProduct(Request $request)
	{		
        $session 	= $request->getSession();
		$productID = $request->request->get('prod');
		$em = $this->getDoctrine()->getManager();
		$web = $request->server->get('DOCUMENT_ROOT');
		if(!empty($request->request->get('product'))){	
			//get old image name to deleteProduct
			$getProdDetail = $em->createQueryBuilder('t')
                ->select('t.productImg')
                ->from('AppBundle:TblProducts', 't')
				->where('t.id = :id')
                ->setParameter('id', $request->request->get('product'))		
                ->getQuery()
                ->getoneorNullResult(Query::HYDRATE_ARRAY);
				/*if (@getimagesize($web.'\uploads\images\products\/'.$getProdDetail['productImg'])) {
					unlink($web.'\uploads\images\products\/'.$getProdDetail['productImg']);
				}*/  
				
			$img = $request->files->get('prodImage');
			if($img){
			  $fileName = uniqid() . '_' . $img->getClientOriginalName();
			  $img->move($this->getParameter('product_img'), $fileName);	
			}
			if(!empty($request->files->get('prodImage'))){
			$productUpdate = $em->createQueryBuilder();
			 $updateQuery = $productUpdate->update('AppBundle:TblProducts', 'u')
					->set('u.productName', '?1')
					->set('u.productDesc', '?2')
					->set('u.productPrice', '?3')
					->set('u.productQuantity', '?4')
					->set('u.productImg', '?5')
					->where('u.id = ?6')
					->setParameter(1, $request->request->get('prodName'))
					->setParameter(2, $request->request->get('prodDescription'))
					->setParameter(3, $request->request->get('prodPrice'))
					->setParameter(4, $request->request->get('quantity'))					
					->setParameter(5, $fileName)
					->setParameter(6, $request->request->get('product'))
					->getQuery();
                $updateResult = $updateQuery->execute();
				if($updateResult == true){
					$response['status']= "ok";
					$response['data'] = "";
					$response['message'] = "Product has been updated successfully!";
					echo json_encode($response); die();
				}else{
					$response['status']= "error";
					$response['data'] = "";
					$response['error'] = "Not able to process this request!";
					echo json_encode($response); die();
				}
			
		}else{
			$productUpdate = $em->createQueryBuilder();
			  $updateQuery = $productUpdate->update('AppBundle:TblProducts', 'u')
					->set('u.productName', '?1')
					->set('u.productDesc', '?2')
					->set('u.productPrice', '?3')
					->set('u.productQuantity', '?4')
					->where('u.id = ?6')
					->setParameter(1, $request->request->get('prodName'))
					->setParameter(2, $request->request->get('prodDescription'))
					->setParameter(3, $request->request->get('prodPrice'))
					->setParameter(4, $request->request->get('quantity'))					
					->setParameter(6, $request->request->get('product'))
					->getQuery();
                $updateResult = $updateQuery->execute();
				if($updateResult == true){
					$response['status']= "ok";
					$response['data'] = "";
					$response['message'] = "Product has been updated successfully!";
					echo json_encode($response); die();
				}else{
					$response['status']= "error";
					$response['data'] = "";
					$response['error'] = "Not able to process this request!";
					echo json_encode($response); die();
				}
		}
		}else{
			    $response['status']= "error";
				$response['data'] = "";
				$response['error'] = "Not able to process this request!";
				echo json_encode($response); die();
		}
     
	}
	/**
     * @Route("/deleteProduct", name="deleteProduct")
     */
	public function deleteProduct(Request $request){
		if(!empty($request->request->get('product'))){
		 $session 	= $request->getSession();
		 $prodId = $request->request->get('product');
		 $emu = $this->getDoctrine()->getManager();
		 $userUpdate = $emu->createQueryBuilder();
		 $updateQuery = $userUpdate->update('AppBundle:TblProducts', 'u')
					->set('u.status', '?1')					
					->where('u.id = ?2')
					->setParameter(1, '0')
					->setParameter(2, $prodId)
					->getQuery();
         $updateResult = $updateQuery->execute();
		 		if($updateResult == true){			
			        $response['status']= "ok";
					$response['data'] = "";
					$response['message'] = "Product has been deleted successfully!";
					echo json_encode($response); die();
		}else{
			$response['status']= "error";
			$response['data'] = "";
			$response['error'] = "Not able to process this request! Please try again later";
			echo json_encode($response); die();
		} 
		 
		}else{
			$response['status']= "error";
			$response['data'] = "";
			$response['error'] = "Not able to process this request!";
			echo json_encode($response); die();
		}
	}
	
}
