<?php
namespace AppBundle\Controller;

use AppBundle\Entity\TblUsersRole;
use AppBundle\Entity\TblUsers;
use AppBundle\Entity\TblAccount;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class SecurityController extends Controller
{
    /**
     * @Route("/", name="login")
     */
    public function indexAction(Request $request)
    {
    	$session 	= $request->getSession();
    	if(!empty($session->get('userInfo'))){
    		return $this->redirectToRoute('dashboard'); 
    	}
    	$query = $this->getDoctrine()->getRepository('AppBundle:TblUsersRole')->createQueryBuilder('c')->getQuery();
    	$userRole = $query->getResult(Query::HYDRATE_ARRAY);
    	$data = array();
    	return $this->render('security/index.html.twig', array(
    		'title'			=>'Login User',
    		'data'	=> $data,
    		'userRoles' => $userRole
    	));        
    }
	/**
     * @Route("/auth", name="auth")
     */
	public function auth(Request $request)
	{
		$session 	= $request->getSession();
		$getUser = $this->getDoctrine()->getManager();
		$response = array();
		
		if(!empty($request->request->get("userData"))){
			$postData = array();
			parse_str($request->request->get('userData'), $postData);
			if(!empty($postData)){
				$userName = $postData['username'];
				$password = $postData['password'];
				$getUserData = $getUser->createQueryBuilder('t')
				->select('t.id', '(t.userRole) as userRole', 't.email', 't.username', 't.createdBy', 'u.name', 'u.slug')
				->from('AppBundle:TblUsers', 't')
				->leftjoin('t.userRole', 'u')
				->where('t.username = :username')
				->andWhere('t.password = :password')
				->setParameter('username',$userName)                
				->setParameter('password',$password)
				->getQuery()
				->getOneOrNullResult(Query::HYDRATE_ARRAY);
				$getUser->flush();
				if(!empty($getUserData)){
					  $getAccountData = $getUser->createQueryBuilder('t')
										->select('t')
										->from('AppBundle:TblAccount', 't')
										->where('t.user = :user')
										->setParameter('user', $getUserData['id'])
										->getQuery()->getoneorNullResult(Query::HYDRATE_ARRAY);
										$getUser->flush();
					/***************count differnt users according to roles*********************/
	             	// count total users not super user
						$usersArray = array();				
						if($getUserData['slug'] == "super"){
							$totalUsers = $getUser->createQueryBuilder('t')
							->select('count(t.id)')
							->from('AppBundle:TblUsers', 't')
							->where('t.userRole != :userRoleid')
							->andWhere('t.isDeleted != 1')
							->setParameter('userRoleid', $getUser->getRepository('AppBundle:TblUsersRole')->find(1))
							->getQuery()->getSingleScalarResult();
							$getUser->flush();
							$usersArray['totalUsers'] = $totalUsers;	

					 // count total distributors users
							$totaldistributorsUsers = $getUser->createQueryBuilder('t')
							->select('count(t.id)')
							->from('AppBundle:TblUsers', 't')
							->where('t.userRole = :userid')
							->andWhere('t.isDeleted != 1')
							->setParameter('userid', $getUser->getRepository('AppBundle:TblUsersRole')->find(2))
							->getQuery()->getSingleScalarResult();
							$getUser->flush();
							$usersArray['totaldistributors'] = $totaldistributorsUsers;

					 // count total accounts users
							$totalaccountUsers = $getUser->createQueryBuilder('t')
							->select('count(t.id)')
							->from('AppBundle:TblUsers', 't')
							->where('t.userRole = :userrole')
							->andWhere('t.isDeleted != 1')
							->setParameter('userrole', $getUser->getRepository('AppBundle:TblUsersRole')->find(3))
							->getQuery()->getSingleScalarResult();
							$getUser->flush();
							$usersArray['totalaccounts'] = $totalaccountUsers;
					// count total organization users
							$totalorgUsers = $getUser->createQueryBuilder('t')
							->select('count(t.id)')
							->from('AppBundle:TblUsers', 't')
							->where('t.userRole = :userrole')
							->andWhere('t.isDeleted != 1')
							->setParameter('userrole', $getUser->getRepository('AppBundle:TblUsersRole')->find(5))
							->getQuery()->getSingleScalarResult();
							$getUser->flush();
							$usersArray['totalorg'] = $totalorgUsers;
					// count total empoyee users
							$totalempUsers = $getUser->createQueryBuilder('t')
							->select('count(t.id)')
							->from('AppBundle:TblUsers', 't')
							->where('t.userRole = :userrole')
							->andWhere('t.isDeleted != 1')
							->setParameter('userrole', $getUser->getRepository('AppBundle:TblUsersRole')->find(5))
							->getQuery()->getSingleScalarResult();
							$getUser->flush();
							$usersArray['totalemp'] = $totalempUsers;
						}

						/*********************************User counts when a distributor login ****************************/
						if($getUserData['slug'] == "distributor"){					
					     //total accounts under distributor
					 		$totalaccountUsers = $getUser->createQueryBuilder('t')
							->select('t.id')
							->from('AppBundle:TblUsers', 't')
							->where('t.userRole = :userrole')
							->andWhere('t.createdBy = :createdby')
							->andWhere('t.isDeleted != 1')
							->setParameter('userrole', $getUser->getRepository('AppBundle:TblUsersRole')->find(3))
							->setParameter('createdby', $getUserData['id'])
							->getQuery()->getResult(Query::HYDRATE_ARRAY);
							$getUser->flush();
						 //total Organizations
							$orgnizations = 0;
							$organizationsArray = array();
							foreach($totalaccountUsers as $val){
								$organizationUsers = $getUser->createQueryBuilder('t')
								->select('t.id')
								->from('AppBundle:TblUsers', 't')
								->where('t.userRole = :userrole')
								->andWhere('t.createdBy = :createdby')
								->andWhere('t.isDeleted != 1')
								->setParameter('userrole', $getUser->getRepository('AppBundle:TblUsersRole')->find(4))
								->setParameter('createdby', $val['id'])
								->getQuery()->getResult(Query::HYDRATE_ARRAY);
								$getUser->flush();
								if(!empty($organizationUsers)){
									for($i = 0; $i < count($organizationUsers); $i++){
									array_push($organizationsArray, $organizationUsers[$i]);
								   }
								}
							}

							$employeeUserArray = array();
							/*total Employees*/
                            if(count($organizationsArray) > 0){
                             foreach($organizationsArray as $val){
								$employeeUser = $getUser->createQueryBuilder('t')
								->select('t.id')
								->from('AppBundle:TblUsers', 't')
								->where('t.userRole = :userrole')
								->andWhere('t.createdBy = :createdby')
								->andWhere('t.isDeleted != 1')
								->setParameter('userrole', $getUser->getRepository('AppBundle:TblUsersRole')->find(5))
								->setParameter('createdby', $val['id'])
								->getQuery()->getResult(Query::HYDRATE_ARRAY);
								$getUser->flush();
								if(!empty($employeeUser)){
									for($i = 0; $i < count($employeeUser); $i++){
									 array_push($employeeUserArray, $employeeUser[$i]);	
									}
									
								}
							 }
                            } 
                            /*get all cusomers*/

							$usersArray['totalUsers'] = count($totalaccountUsers) + $orgnizations;
							$usersArray['totalaccounts'] = count($totalaccountUsers);	
							$usersArray['totalorganizations'] = count($organizationsArray);
							$usersArray['totalemployees'] = count($employeeUserArray);					 
						}
						/*********************************User counts when a accounts login ****************************/
						if($getUserData['slug'] == "account"){
						// total organization
							$totalaccountUsers = $getUser->createQueryBuilder('t')
							->select('t.id')
							->from('AppBundle:TblUsers', 't')
							->where('t.userRole = :userrole')
							->andWhere('t.createdBy = :createdby')
							->andWhere('t.isDeleted != 1')
							->setParameter('userrole', $getUser->getRepository('AppBundle:TblUsersRole')->find(4))
							->setParameter('createdby', $getUserData['id'])
							->getQuery()->getResult();
							$getUser->flush();
							$usersArray['totalorganizations'] = count($totalaccountUsers);	
						}				
					$setUserSession = array(
						'userID' => $getUserData['id'],
						'userEmail' => $getUserData['email'],
						'userRoleName'=> $getUserData['name'],
						'userRoleSlug'=> $getUserData['slug'],
						'userFullName' => ucfirst($getAccountData['firstName']).' '.ucfirst($getAccountData['lastName']),
						'username' =>$getUserData['username'],
						'creatdby' => $getUserData['createdBy'],
						'userRole' => $getUserData['userRole'],
						'accID' => $getAccountData['id'],
						'profileImg' => $getAccountData['profileimg'],
						'totalusers' => $usersArray,
						'is_navigate' => 0,
						'authorize' => 0
					);
					$userData = $session->set('userInfo', $setUserSession);
					$response['status']= "ok";
					$response['data'] = $setUserSession;
					$response['error'] = "";
					echo json_encode($response); die;
				}else{
					$response['status']= "error";
					$response['data'] = "";
					$response['error'] = "Incorrect Username or Password!";
					echo json_encode($response); die;
				}	
			}else{
				$response['status']= "error";
				$response['data'] = "";
				$response['error'] = "Not able to process this request!";
				echo json_encode($response); die;
			}
		}else{
			$response['status']= "error";
			$response['data'] = "";
			$response['error'] = "Not able to process this request!";
			echo json_encode($response); die;
		}		       

	}
	/**
     * @Route("/checkUserEmail", name="checkUserEmail")
     */
	public function checkUserEmail(Request $request){
		$emailID = $request->query->get('email');
		$em = $this->getDoctrine()->getManager();
		$chekuserEmail = $em->createQueryBuilder('t')
		->select('t.id')
		->from('AppBundle:TblUsers', 't')
		->where('t.email = :email')
		->setParameter('email', $emailID)
		->getQuery()
		->getResult(Query::HYDRATE_ARRAY);
		if(empty($chekuserEmail)){
			echo "true"; die;
		}else{
			echo "false"; die;
		}
	}
	/**
     * @Route("/forget", name="forget")
     */
	public function forget(Request $request){
		$host = 'http://'.$request->server->get('HTTP_HOST').'/';
		$resetPass = 'reset?e=';
		parse_str($request->request->get('userData'), $postData);
		if(!empty($postData)){
			$userMailID = $postData['email'];
			$length = rand(30, 40);
			$forgetString = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1, $length);
			$em = $this->getDoctrine()->getManager();
			$userUpdate = $em->createQueryBuilder();
			$updateQuery = $userUpdate->update('AppBundle:TblUsers', 'u')
			->set('u.forgetPass',':stringForPass')
			->where('u.email = :email')
			->setParameter('stringForPass', $forgetString)
			->setParameter('email', $userMailID)
			->getQuery();
			$updateResult = $updateQuery->execute();
			$resetUrl = $host.$resetPass.base64_encode($userMailID).'&key='.$forgetString;
			//$sentTo = "ashok.mobilyte@gmail.com";
			$sentTo = $userMailID;
			$message = \Swift_Message::newInstance()
			->setSubject('Reset Password!')
			->setFrom('ashok.mobilyte@gmail.com')
			->setTo($sentTo)
			->setCharset('iso-8859-1')
			->setContentType('text/html')
			->setBody(
				$this->renderView(
					'emails/profile.html.twig',
					array('urlString' => $resetUrl)
				)
			);
			$this->get('mailer')->send($message);
			$response['status']= "ok";
			$response['data'] = "";
			$response['message'] = "Please check your Email box to reset password!";
			echo json_encode($response); die;

		}else{
			$response['status']= "error";
			$response['data'] = "";
			$response['error'] = "Not able to process this request!";
			echo json_encode($response); die;
		}
		
	}
	/**
     * @Route("/reset", name="reset")
     */
	public function resetAction(Request $request){				
		$session 	= $request->getSession();
		if(!empty($session->get('userInfo'))){
			return $this->redirectToRoute('dashboard'); 
		}
		$getKey = $request->query->get('key');
		$email = base64_decode($request->query->get('e'));
		$em = $this->getDoctrine()->getManager();
		$chekuserEmail = $em->createQueryBuilder('t')
		->select('t')
		->from('AppBundle:TblUsers', 't')
		->where('t.email = :email')
		->andWhere('t.forgetPass = :getString')
		->setParameter('email', $email)
		->setParameter('getString', $getKey)
		->getQuery()
		->getOneOrNullResult(Query::HYDRATE_ARRAY);
		if(!empty($chekuserEmail)){
			$chekuserEmail['userkey'] = base64_encode(base64_encode($chekuserEmail['id']));
			return $this->render('security/reset.html.twig', array(
				'title'			=>'Reset Password',
				'data'	=> $chekuserEmail
			));   
		}else{
			$this->addFlash(
				'error',
				'Something went wrong! Please try again.'
			);
			return $this->redirectToRoute('login'); 
		} 		  
	}
	/**
     * @Route("/resetPassword", name="resetPassword")
     */
	public function resetPassword(Request $request){
		parse_str($request->request->get('userData'), $postData);
		if(!empty($postData)){
			$em = $this->getDoctrine()->getManager();
			$userid = base64_decode(base64_decode($postData['userKey']));
			if(is_numeric($userid) && $userid >= 0){				
				$userUpdate = $em->createQueryBuilder();
				$updateQuery = $userUpdate->update('AppBundle:TblUsers', 'u')
				->set('u.password',':password')
				->set('u.forgetPass', 'Null')
				->where('u.id = :id')
				->setParameter('password', $postData['newPassword'])
				->setParameter('id', $userid)
				->getQuery();
				$updateResult = $updateQuery->execute();
				if($updateResult > 0){
					$response['status']= "ok";
					$response['data'] = "";
					$response['message'] = "Password has been reset successfully!";
					echo json_encode($response); die;
				}
			}else{
				$response['status']= "error";
				$response['data'] = "";
				$response['error'] = "Not able to process this request!";
				echo json_encode($response); die;
			} 
		}else{
			$response['status']= "error";
			$response['data'] = "";
			$response['error'] = "Not able to process this request!";
			echo json_encode($response); die;
		}
	}
}
