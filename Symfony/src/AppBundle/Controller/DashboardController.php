<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use AppBundle\Entity\TblUsersRole;
use AppBundle\Entity\TblUsers;
use AppBundle\Entity\TblAccount;
use AppBundle\Entity\TblCustomers;
use AppBundle\Entity\TblCcinfo;


class DashboardController extends Controller
{
    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function indexAction(Request $request)
    {
		$session 	= $request->getSession();
		if(empty($session->get('userInfo'))){
			return $this->redirectToRoute('login'); 
		}else{
			$userData = $session->get('userInfo');
			$data = array();
			 return $this->render('dashboard/index.html.twig', array(
				'title'			=>'Dashboard',
				'data'	=> $data,
				'userData' => $userData
			));
		}
		 
    }
	/**
     * @Route("/profile", name="profile")
     */
	public function profileAction(Request $request){
		$session 	= $request->getSession();
		if(empty($session->get('userInfo'))){
			return $this->redirectToRoute('login'); 
		}else{
			$userData = $session->get('userInfo');
			$em = $this->getDoctrine()->getManager();
			$getProfile = $em->createQueryBuilder('t')
                ->select('t, a, r')
                ->from('AppBundle:TblAccount', 't')
				->leftjoin('t.user', 'a')
				->leftjoin('a.userRole', 'r')
				->where('a.id = :id')				
                ->andWhere('t.isDeleted = :isDeleted')
                ->setParameter('id', $userData['userID'])
                ->setParameter('isDeleted', '0')				
                ->orderBy('t.id', 'DESC')				
                ->getQuery()
                ->getOneOrNullResult(Query::HYDRATE_ARRAY);
			$data = array();
			 return $this->render('dashboard/profile.html.twig', array(
				'title'	=> 'User Profile',
				'data'	=> $getProfile,
				'userData' => $userData
			));
		}
		 
	}
	/**
     * @Route("/getprofile", name="getprofile")
     */
	public function getprofile(Request $request){
		$session 	= $request->getSession();
		if(empty($session->get('userInfo'))){
			return $this->redirectToRoute('login'); 
		}else{
			$userData = $session->get('userInfo');
			$em = $this->getDoctrine()->getManager();
			$getProfile = $em->createQueryBuilder('t')
                ->select('t, a, r')
                ->from('AppBundle:TblAccount', 't')
				->leftjoin('t.user', 'a')
				->leftjoin('a.userRole', 'r')
				->where('a.id = :id')				
                ->andWhere('t.isDeleted = :isDeleted')
                ->setParameter('id', $userData['userID'])
                ->setParameter('isDeleted', '0')				
                ->orderBy('t.id', 'DESC')				
                ->getQuery()
                ->getOneOrNullResult(Query::HYDRATE_ARRAY);
			echo json_encode($getProfile); die;
		}
		 
	}
	 /**
     * @Route("/updateprofile", name="updateprofile")
     */
	 public function updateprofile(Request $request){
		if(!empty($request->request->get('userData'))){
		  parse_str($request->request->get('userData'), $postData);
			 if(!empty($postData)){
				 $em = $this->getDoctrine()->getManager();
				 if($postData['gender'] == 'M'){
					 $gender = 'Male';
				 }else{
					 $gender = 'Female';
				 }
				 $userUpdate = $em->createQueryBuilder();
				 $updateQuery = $userUpdate->update('AppBundle:TblUsers', 'u')
					->set('u.email', '?3')
					->set('u.updatedOn', '?4')
					->where('u.id = ?5')
					->setParameter(3, $postData['email'])
					->setParameter(4, date('Y-m-d H:i:s'))
					->setParameter(5, $postData['profile'])
					->getQuery();
                $updateResult = $updateQuery->execute();
				$accoutUpdate= $em->createQueryBuilder();
				$updateAccQuery = $accoutUpdate->update('AppBundle:TblAccount', 'a')
					->set('a.firstName', '?1')
					->set('a.lastName', '?2')
					->set('a.dob', '?3')
					->set('a.gender', '?4')
					->set('a.address', '?5')
					->set('a.city', '?6')
					->set('a.state', '?7')
					->set('a.country', '?8')
					->set('a.zipcode', '?9')
					->where('a.user = ?10')
					->setParameter(1, $postData['firstname'])
					->setParameter(2, $postData['lastname'])
					->setParameter(3, $postData['birthday'])
					->setParameter(4, $gender)
					->setParameter(5, $postData['address'])
					->setParameter(6, $postData['city'])
					->setParameter(7, $postData['state'])
					->setParameter(8, $postData['country'])
					->setParameter(9, $postData['zipcode'])
					->setParameter(10, $postData['profile'])
					->getQuery();
                $updateAccResult = $updateAccQuery->execute();
				if($updateAccResult == true || $updateResult == true){
					$response['status']= "ok";
					$response['data'] = "";
					$response['message'] = "Data has been updated!";
					echo json_encode($response); die();
				}else{
					$response['status']= "error";
					$response['data'] = "";
					$response['error'] = "Not able to process this request! Please try again later";
					echo json_encode($response); die(); 
				}
				
				 
			 }else{
				$response['status']= "error";
				$response['data'] = "";
				$response['error'] = "Not able to process this request!";
				echo json_encode($response); die(); 
			 } 
		 }else{
			$response['status']= "error";
			$response['data'] = "";
			$response['error'] = "Not able to process this request!";
			echo json_encode($response); die(); 
		 } 
	 }

    /**
     * @Route("/uniqueUser", name="uniqueUser")
     */
	public function uniqueUser(Request $request){
		$uniqueUser = $request->query->get('username');
			$em = $this->getDoctrine()->getManager();
			$chekuserName = $em->createQueryBuilder('t')
                ->select('t.id')
                ->from('AppBundle:TblUsers', 't')
                ->where('t.username = :username')
                ->setParameter('username', $uniqueUser)
                ->getQuery()
                ->getOneOrNullResult(Query::HYDRATE_ARRAY);

		if(empty($chekuserName)){
			echo "true"; die;
		}else{
			echo "false"; die;
		}
	}
	
	/**
     * @Route("/uniqueEmail", name="uniqueEmail")
     */
	public function uniqueEmail(Request $request){
		$uniqueEmail = $request->query->get('email');
			$em = $this->getDoctrine()->getManager();
			$chekuserEmail = $em->createQueryBuilder('t')
                ->select('t.id')
                ->from('AppBundle:TblUsers', 't')
                ->where('t.email = :email')
                ->setParameter('email', $uniqueEmail)
                ->getQuery()
                ->getOneOrNullResult(Query::HYDRATE_ARRAY);
		if(empty($chekuserEmail)){
			echo "true"; die;
		}else{
			echo "false"; die;
		}
	}
	
	/**
     * @Route("/checkcustomeremail", name="checkcustomeremail")
     */
	public function checkcustomeremail(Request $request){
		$emailID = $request->query->get('email');
			$em = $this->getDoctrine()->getManager();
			$chekuserEmail = $em->createQueryBuilder('t')
                ->select('t.id')
                ->from('AppBundle:TblUsers', 't')
                ->where('t.email = :email')
                ->setParameter('email', $emailID)
                ->getQuery()
                ->getOneOrNullResult(Query::HYDRATE_ARRAY);
				$em->flush();
		if(count($chekuserEmail) > 0){
			$chekcustomerEmail = $em->createQueryBuilder('a')
                ->select('a.id')
                ->from('AppBundle:TblCustomers', 'a')
                ->where('a.email = :email')
                ->setParameter('email', $emailID)
                ->getQuery()
                ->getOneOrNullResult(Query::HYDRATE_ARRAY);
				if($chekcustomerEmail > 0){
					echo "true"; die;
				}else{
					echo "false"; die;
				}
			
		}else{
			echo "false"; die;
		}
	}

	/**
     * @Route("/logout", name="logout")
     */
	public function logoutAction(Request $request){
		$session 	= $request->getSession();
		$session->clear();
		return $this->redirectToRoute('login');
	}
	/**
     * @Route("/usernavigate", name="usernavigate")
     */
    public function usernavigateAction(Request $request)
    { 
	if($request->request->get("user")){
		$usersArray = array();
		$session 	= $request->getSession(); 
		$navigateUser = $request->request->get("user");
		$currentUser = $session->get('userInfo')['userID'];
		$em = $this->getDoctrine()->getManager();
		$getNavigateUser = $em->createQueryBuilder('t')
		                ->select('t.id', '(t.userRole) as userRole', 't.email', 't.username', 't.createdBy', 'u.name', 'u.slug', 'a.firstName', 'a.lastName', 'a.id as accountID', 'a.profileimg')
						->from('AppBundle:TblUsers', 't')
						->leftjoin('t.userRole', 'u')
						->leftjoin('AppBundle:TblAccount', 'a', 'WITH', 't.id = a.user')
						->where('t.id = :id')
						->setParameter('id', $navigateUser)
						->getQuery()
						->getOneOrNullResult(Query::HYDRATE_ARRAY);
		$currentUserSession =	$session->get('userInfo');	
        $navUser = ucfirst($getNavigateUser['firstName']).' '.ucfirst($getNavigateUser['lastName']); 
     /*********************************User counts when a distributor login ****************************/
				if($getNavigateUser['slug'] == "distributor"){					
				  //total accounts under distributor
						$totalaccountUsers = $em->createQueryBuilder('t')
						->select('t.id')
						->from('AppBundle:TblUsers', 't')
						->where('t.userRole = :userrole')
						->andWhere('t.createdBy = :createdby')
						->andWhere('t.isDeleted != 1')
						->setParameter('userrole', $em->getRepository('AppBundle:TblUsersRole')->find(3))
						->setParameter('createdby', $getNavigateUser['id'])
						->getQuery()->getResult();
						$em->flush();
					//total Organizations
					$orgnizations = 0;
                    foreach($totalaccountUsers as $val){
						$organizationUsers = $em->createQueryBuilder('t')
						->select('count(t.id)')
						->from('AppBundle:TblUsers', 't')
						->where('t.userRole = :userrole')
						->andWhere('t.createdBy = :createdby')
						->andWhere('t.isDeleted != 1')
						->setParameter('userrole', $em->getRepository('AppBundle:TblUsersRole')->find(4))
						->setParameter('createdby', $val['id'])
						->getQuery()->getSingleScalarResult();
						$em->flush();
						$orgnizations = $orgnizations + $organizationUsers; 
					   }	
                       
                     $usersArray['totalUsers'] = count($totalaccountUsers) + $orgnizations;
				     $usersArray['totalaccounts'] = count($totalaccountUsers);	
                     $usersArray['totalorganizations'] = $orgnizations;					 
				}
				
			/*********************************User counts when a accounts login ****************************/
				if($getNavigateUser['slug'] == "account"){
					// total organization
					  $totalaccountUsers = $em->createQueryBuilder('t')
						->select('t.id')
						->from('AppBundle:TblUsers', 't')
						->where('t.userRole = :userrole')
						->andWhere('t.createdBy = :createdby')
						->andWhere('t.isDeleted != 1')
						->setParameter('userrole', $em->getRepository('AppBundle:TblUsersRole')->find(4))
						->setParameter('createdby', $getNavigateUser['id'])
						->getQuery()->getResult();
						$em->flush();
					 $usersArray['totalorganizations'] = count($totalaccountUsers);	
					}			

				
		$UserSessionVar = array(
					'userID' => $getNavigateUser['id'],
					'userEmail' => $getNavigateUser['email'],
					'userRoleName'=> $getNavigateUser['name'],
					'userRoleSlug'=> $getNavigateUser['slug'],
					'userFullName' => $navUser,
					'username' =>$getNavigateUser['username'],
					'creatdby' => $getNavigateUser['createdBy'],
					'userRole' => $getNavigateUser['userRole'],
					'accID' => $getNavigateUser['accountID'],
					'profileImg' => $getNavigateUser['profileimg'],
					'totalusers' => $usersArray,
					'currentSession' => $currentUserSession,
					'is_navigate' => 1,
					'authorize' => 0
				);
  		$userData = $session->set('userInfo', $UserSessionVar);		
                $response['status']= "ok";
				$response['data'] = "";
				$response['message'] = "Successfully Navigated to User ".$navUser;
				echo json_encode($response); die();
		
	}else{
		        $response['status']= "error";
				$response['data'] = "";
				$response['error'] = "Not able to process this request!";
				echo json_encode($response); die();
	    }
    }
	/**
     * @Route("/backtodashboard", name="backtodashboard")
     */
    public function backtodashboardAction(Request $request)
    { 
	$session 	= $request->getSession();
	if($session->get('userInfo')['currentSession']){
		$currentUser = $session->get('userInfo')['currentSession'];
		$session->clear();
  		$userData = $session->set('userInfo', $currentUser);		
                $response['status']= "ok";
				$response['data'] = "";
				$response['message'] = "Successfully Back To Dashboard";
				echo json_encode($response); die();
		
	}else{
		        $response['status']= "error";
				$response['data'] = "";
				$response['error'] = "Not able to process this request!";
				echo json_encode($response); die();
	    }
    }
	/**
     * @Route("/checkuserpass", name="checkuserpass")
     */
    public function checkuserpass(Request $request)
    { 
	$session 	= $request->getSession();
	$getUserSession = $session->get("userInfo");
	if($getUserSession['is_navigate'] == 1 && $getUserSession['authorize'] == 0){
	     if(!empty($request->request->get("password"))){
		      $userPassword = $request->request->get("password");
			  $userID = $session->get("userInfo")['userID'];
			  $em = $this->getDoctrine()->getManager();
			  $queryUser = $em->createQueryBuilder('t')
		                ->select('t.id')
						->from('AppBundle:TblUsers', 't')
						->where('t.id = :id')
						->andwhere('t.password = :password')
						->setParameter('id', $userID)
						->setParameter('password', $userPassword)
						->getQuery();
				$affectedRecords = $queryUser->getResult();
				if(count($affectedRecords) == 1){
				       $userSession = $session->get('userInfo');
					   $userSession['authorize']=1;
					   $session->set('userInfo', $userSession);
						$response['status']= "ok";
						$response['data'] = "";
						$response['message'] = "Successfully Logged In!";
						echo json_encode($response); die();
				}else{
						$response['status']= "error";
						$response['data'] = "";
						$response['error'] = "Incorrect Password!";
						echo json_encode($response); die();
				}
		 }else{ 
		        $response['status']= "error";
				$response['data'] = "";
				$response['error'] = "Not able to process this request!";
				echo json_encode($response); die();
		 }
	}else if($getUserSession['is_navigate'] == 1 && $getUserSession['authorize'] == 1){
	            $response['status']= "ok";
				$response['data'] = "";
				$response['message'] = "Already Logged In!";
				echo json_encode($response); die();
	}else{
	            $response['status']= "error";
				$response['data'] = "";
				$response['error'] = "Not able to process this request!";
				echo json_encode($response); die();
	}
  }
	
	
	
}
