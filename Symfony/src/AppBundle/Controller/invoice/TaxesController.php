<?php

namespace AppBundle\Controller\invoice;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use AppBundle\Entity\TblUsersRole;
use AppBundle\Entity\TblUsers;
use AppBundle\Entity\TblAccount;
use AppBundle\Entity\TblProducts;
use AppBundle\Entity\TblCustomers;
use AppBundle\Entity\TblCcinfo;
use AppBundle\Entity\TblTaxes;

class TaxesController extends Controller
{
    /**
     * @Route("/createTax", name="createTax")
     */
    public function createTaxAction(Request $request)
    {
		$session 	= $request->getSession();
		if(empty($session->get('userInfo'))){
			return $this->redirectToRoute('login'); 
		}else{
			if(!empty($request->request->get("prodData"))){
				$userData = $session->get('userInfo');
				 parse_str($request->request->get('prodData'), $postData);
				 $em = $this->getDoctrine()->getManager();
				 $org = $userData['creatdby'];
				 $taxData = new TblTaxes();
				 $taxData->setOrg($em->getRepository('AppBundle:TblUsers')->find($org));
				 $taxData->setTaxName($postData['tax_name']);
				 $taxData->setTaxAbbreviation($postData['tax_name']);
				 $taxData->setTaxNumber($postData['tax_number']);
				 $taxData->setTaxRate($postData['tax_rate']);
				 $taxData->setTaxDesc($postData['tax_description']);
				 $taxData->setCreatedAt(new \DateTime("now"));
				 $em->persist($taxData);
				 $em->flush();
				 $taxID = $taxData->getId();
				 if(!empty($taxID)){
        			$response['status']= "ok";
					$response['data'] = array('id'=> $taxID, 'value' => $postData['tax_name'], 'price' => $postData['tax_rate']);
					$response['message'] = "Tax is added successfully.";
					$response['error'] = "";
					echo json_encode($response); die;
				 }else{
					$response['status']= "error";
					$response['data'] = "";
					$response['error'] = "Not able to process this request!";
					echo json_encode($response); die();
				 }
			}else{
				$response['status']= "error";
				$response['data'] = "";
				$response['error'] = "Not able to process this request!";
				echo json_encode($response); die();
			}
			
		} 
    }
	
}
