<?php

namespace AppBundle\Controller\invoice;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use AppBundle\Entity\TblUsersRole;
use AppBundle\Entity\TblUsers;
use AppBundle\Entity\TblAccount;
use AppBundle\Entity\TblProducts;
use AppBundle\Entity\TblCustomers;
use AppBundle\Entity\TblCcinfo;
use AppBundle\Entity\TblTaxes;
use AppBundle\Entity\TblInvoice;
use AppBundle\Entity\TblPaymentRecords;
use AppBundle\Entity\TblPaymentReminder;

class PayremindersController extends Controller
{
    /**
     * @Route("/addreminder", name="addreminder")
     */
    public function addreminderAction(Request $request)
    {
		$session 	= $request->getSession();
		if(empty($session->get('userInfo'))){
			return $this->redirectToRoute('login'); 
		}else{
			if(!empty($request->request->get("invoiceID"))){
				$invoiceID = base64_decode(base64_decode(base64_decode($request->request->get("invoiceID"))));
				$reminderData = (!empty($request->request->get("reminderData"))) ? json_encode($request->request->get("reminderData")) : '';
				$userData = $session->get('userInfo');
				$userID = $userData['userID'];
				$em = $this->getDoctrine()->getManager();
				$Query = $em->createQueryBuilder()
				         ->select('t')
				         ->from('AppBundle:TblPaymentReminder', 't')
				         ->where('t.invoice= :invoiceid')
			        	 ->setParameter("invoiceid", $em->getRepository('AppBundle:TblInvoice')->find($invoiceID))
			        	 ->getQuery()
					     ->getResult(Query::HYDRATE_ARRAY);
						 $em->flush();
				if(!empty($Query)){
                         $invoiceReminder = $em->createQueryBuilder();
		                 $invoiceReminderQuery = $invoiceReminder->update('AppBundle:TblPaymentReminder', 'u')
							->set('u.reminderData', '?1')
							->set('u.updatedAt', '?2')
							->where('u.invoice = ?3')
							->setParameter(1, $reminderData)
                            ->setParameter(2, new \DateTime("now"))
							->setParameter(3, $em->getRepository('AppBundle:TblInvoice')->find($invoiceID))
							->getQuery();
		                 $invoiceReminderResult = $invoiceReminderQuery->execute();
		                 if(!empty($invoiceReminderResult)){
		        			$response['status']= "ok";
							$response['data'] = $invoiceReminderResult;
							$response['message'] = "Reminder Action Updated Successfully.";
							$response['error'] = "";
							echo json_encode($response); die;
						 }else{
							$response['status']= "error";
							$response['data'] = "";
							$response['error'] = "Not able to process this request!";
							echo json_encode($response); die();
						 }
				}else{
					 $insertRemider = new TblPaymentReminder();
					 $insertRemider->setInvoice($em->getRepository('AppBundle:TblInvoice')->find($invoiceID));
					 $insertRemider->setUser($em->getRepository('AppBundle:TblUsers')->find($userID));
					 $insertRemider->setReminderData($reminderData);
					 $insertRemider->setCreatedAt(new \DateTime("now"));
					 $em->persist($insertRemider);
					 $em->flush();
					 $insertRemidersID = $insertRemider->getId();
					 if(!empty($insertRemidersID)){
	        			$response['status']= "ok";
						$response['data'] = $insertRemidersID;
						$response['message'] = "Reminder Action Updated Successfully.";
						$response['error'] = "";
						echo json_encode($response); die();
					 }else{
						$response['status']= "error";
						$response['data'] = "";
						$response['error'] = "Not able to process this request!";
						echo json_encode($response); die();
					 }
				}
			} else{
				$response['status']= "error";
				$response['data'] = "";
				$response['error'] = "Not able to process this request!";
				echo json_encode($response); die();
			}
		} 
    }

 }
