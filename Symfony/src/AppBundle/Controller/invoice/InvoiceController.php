<?php
namespace AppBundle\Controller\invoice;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use AppBundle\Entity\TblUsersRole;
use AppBundle\Entity\TblUsers;
use AppBundle\Entity\TblAccount;
use AppBundle\Entity\TblProducts;
use AppBundle\Entity\TblCustomers;
use AppBundle\Entity\TblCcinfo;
use AppBundle\Entity\TblTaxes;
use AppBundle\Entity\TblInvoice;
use AppBundle\Entity\TblPaymentReminder;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Symfony\Component\HttpFoundation\RequestStack;
class InvoiceController extends Controller
{
    /**
     * @Route("/invoice", name="invoice")
     */
    public function invoiceAction(Request $request)
    {
		$session 	= $request->getSession();
		if(empty($session->get('userInfo'))){
			return $this->redirectToRoute('login'); 
		}else{
			$userData = $session->get('userInfo');
			$allEmployees = $invoices = array();
			$em = $this->getDoctrine()->getManager();
			if($userData['userRoleSlug'] == 'organization'){
				$allEmployees = $em->createQueryBuilder('t')
				->select('t.id', 'c.firstName', 'c.lastName')
				->from('AppBundle:TblUsers', 't')
				->leftjoin('AppBundle:TblAccount', 'c', 'WITH', 't.id = c.user')
				->where('t.createdBy = :createdby')
				->setParameter("createdby", $userData['userID'])
				->orderBy('c.firstName', 'ASC')
				->getQuery()
				->getResult(Query::HYDRATE_ARRAY);
				$em->flush();
			/* Invoice Listing*/				
			}else if($userData['userRoleSlug'] == 'employee'){							
			/* Invoice Listing*/
			   $invoices = $em->createQueryBuilder()
                            ->select('t.posoNumber, t.invoiceStart, t.paymentDue, t.paidStatus, t.totalAmount, t.totalTax, t.invoiceData, t.createdAt, t.id, t.invoiceSent, t.invoiceSkipSend, c.id as acc_id, c.firstName, c.lastName')
                            ->from('AppBundle:TblInvoice', 't')
							->leftjoin('AppBundle:TblAccount', 'c', 'WITH', 't.user = c.user')
                            ->where('t.user = :userID')
							->andWhere('t.isDeleted = :deleteInfo')
							->andWhere('t.status = :status')
                            ->setParameter('userID', $userData['userID'])
							->setParameter('deleteInfo', '0')
							->setParameter('status', '1')							
                            ->orderBy('t.id', 'DESC')
                            ->getQuery()
                            ->getResult(QUERY::HYDRATE_ARRAY);
                            $em->flush(); 
                 if(!empty($invoices)){
				   foreach($invoices as $key => $val){
					   $invoiceID = base64_encode(base64_encode(base64_encode($val['id'])));
					   $invoices[$key]['encInvoice'] = $invoiceID;
					   }
				   }
			}
			$getCurrencies =  $em->createQueryBuilder('t')
                ->select('t')
                ->from('AppBundle:TblCurrencies', 't')		
                ->orderBy('t.country', 'ASC')				
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
                $em->flush();				
			$data = $getUserData =array();
			 return $this->render('dashboard/invoice/invoice.html.twig', array(
				'title'			=>'Add Customer',
				'data'	=> $data,
				'userDatauplevel' => $getUserData,
				'userData' => $userData,
				'employees' => $allEmployees,
				'currencies' => $getCurrencies,
				'invoices' => $invoices
			));
		} 
    }
	/**
     * @Route("/createinvoice", name="createinvoice")
     */
    public function createinvoiceAction(Request $request)
    {
		$session 	= $request->getSession();
		if(empty($session->get('userInfo'))){
			return $this->redirectToRoute('login'); 
		}else{
			$data = $getUserData =array();
			$userData = $session->get('userInfo');
			/*get employee's organization info*/
			$em = $this->getDoctrine()->getManager();
			$getOrgData = $em->createQueryBuilder()
			                        ->select('t')
                                    ->from('AppBundle:TblAccount', 't')
                                    ->where('t.user = :create')
                                    ->setParameter('create', $userData['creatdby'])
                                    ->getQuery()	
                                    ->getOneOrNullResult(QUERY::HYDRATE_ARRAY);  
									$em->flush();
            
			$getCustomers = $em->createQueryBuilder()
                            ->select("t.id, t.firstName, t.lastName")
                            ->from("AppBundle:TblCustomers", 't')
                            ->where("t.emp = :emp")
  					        ->setParameter("emp", $userData['userID'])
                            ->getQuery()
                            ->getResult(QUERY::HYDRATE_ARRAY);
							$em->flush();				
			/*encrypt customer ID*/
			foreach($getCustomers as $key => $val){
				$val['cust'] = base64_encode(base64_encode(base64_encode($val['id'])));
				$getCustomers[$key] = $val;
			}
			/*Get Product*/
			$getProducts = $em->createQueryBuilder()
			               ->select("t.id, t.productName")
						   ->from("AppBundle:TblProducts", 't')
						   ->where('t.user = :userId')
						   ->andWhere('t.status = :status')
						   ->setParameter('userId', $userData['creatdby'])
						   ->setParameter('status', '1')
						   ->getQuery()
						   ->getResult(QUERY::HYDRATE_ARRAY);	
                           $em->flush();
            $getTaxes = $em->createQueryBuilder()
			            ->select("t")
                        ->from("AppBundle:TblTaxes", "t")
                        ->where("t.org = :orgID")
                        ->setParameter("orgID", $userData['creatdby'])
                        ->getQuery()
                        ->getResult(QUERY::HYDRATE_ARRAY);
						$em->flush();	  						   
			 return $this->render('dashboard/invoice/add_invoice.html.twig', array(
				'title'	=>'Add Customer',
				'data' => $data,
				'orgData' => $getOrgData,
				'custsData' => $getCustomers,
				'productsData' => $getProducts,
				'getTaxes' => $getTaxes,
				'userDatauplevel' => $getUserData,
				'userData' => $userData
			));
		}
	}
	/**
     * @Route("/getproducttax", name="getproducttax")
     */
	public function getproducttax(Request $request){
		$session 	= $request->getSession();
		$productID = $request->request->get('prod');
		$userData = $session->get('userInfo');
		$em = $this->getDoctrine()->getManager();
		if(!empty($productID)){
			$getProdDetail = $em->createQueryBuilder('t')
                ->select('t')
                ->from('AppBundle:TblProducts', 't')
				->where('t.id = :id')
				->andWhere('t.status = :status')
                ->setParameter('id', $productID)
                ->setParameter('status', '1')			
                ->getQuery()
                ->getoneorNullResult(Query::HYDRATE_ARRAY);
				$em->flush();
           $getTaxes = $em->createQueryBuilder()
			            ->select("t")
                        ->from("AppBundle:TblTaxes", "t")
                        ->where("t.org = :orgID")
                        ->setParameter("orgID", $userData['creatdby'])
                        ->getQuery()
                        ->getResult(QUERY::HYDRATE_ARRAY);
						$em->flush();  						
			if(!empty($getProdDetail)){
				$response['status']= "ok";
				$response['data'] = $getProdDetail;
				$response['taxes'] = $getTaxes;	
				$response['message'] = "";
				echo json_encode($response); die();
			}else{
				$response['status']= "error";
				$response['data'] = "";
				$response['error'] = "Not able to process this request!";
				echo json_encode($response); die();
			}	
				
		}else{
			    $response['status']= "error";
				$response['data'] = "
";
				$response['error'] = "Not able to process this request!";
				echo json_encode($response); die();
		}
	}
	/**
     * @Route("/invoicecreate", name="invoicecreate")
     */
	public function invoicecreate(Request $request){
		$session 	= $request->getSession();
		if(empty($session->get('userInfo'))){
			return $this->redirectToRoute('login'); 
		}else{
			$data = $getUserData =array();
			$userData = $session->get('userInfo');
			parse_str($request->request->get('invoiceData'), $postData);
			if(!empty($postData)){
			$userID = $userData['userID'];
			$customerID = $postData['cust'];
			//124 for usa currency
			$currency = (!empty($postData['currency']) ? $postData['currency'] : 124);
			$posoNumber = $postData['posoNumber'];
			$invoicedate = $postData['invoicedate'];
			$paymentDue = $postData['paymentDue'];
			$subTotal = $postData['subTotal'];
			$taxesVal = $postData['taxesVal'];
			$productData = json_encode($postData['product']);			
			$em = $this->getDoctrine()->getManager();
			$invoiceData = new TblInvoice();
			$invoiceData->setUser($em->getRepository('AppBundle:TblUsers')->find($userID));
			$invoiceData->setCustomer($em->getRepository('AppBundle:TblCustomers')->find($customerID));
			$invoiceData->setCurrency($em->getRepository('AppBundle:TblCurrencies')->find($currency));
			$invoiceData->setPosoNumber($posoNumber);
			$invoiceData->setInvoiceStart(new \DateTime($invoicedate));
			$invoiceData->setPaymentDue(new \DateTime($paymentDue));
			$invoiceData->setTotalAmount($subTotal);
			$invoiceData->setTotalTax($taxesVal);
			$invoiceData->setInvoiceData($productData);
			$invoiceData->setCreatedAt(new \DateTime("now"));
			$em->persist($invoiceData);
			$em->flush();
            $invoiceID = $invoiceData->getId();
			if(!empty($invoiceID)){
        			$response['status']= "ok";
					$response['data'] = '';
					$response['message'] = "Invoice is added successfully.";
					$response['error'] = "";
					echo json_encode($response); die;
				 }else{
					$response['status']= "error";
					$response['data'] = "";
					$response['error'] = "Not able to process this request!";
					echo json_encode($response); die();
				 }
			}else{
				    $response['status']= "error";
					$response['data'] = "";
					$response['error'] = "Not able to process this request!";
					echo json_encode($response); die();
			}	 
		}
	}
	
	/**
	 * @Route("/viewinvoice/{id}", name="viewinvoice")
	 */
	public function viewinvoice(Request $request){
		$invoiceID = $request->attributes->get('id');
		$session 	= $request->getSession();
		if(empty($session->get('userInfo'))){
			return $this->redirectToRoute('login'); 
		}else{
			if(!empty($invoiceID)){
			$data = $getUserData =array();
			$userData = $session->get('userInfo');
			$em = $this->getDoctrine()->getManager();
			$getAllInvoices = $em->createQueryBuilder()
			                  ->select("t", 'u')
                              ->from("AppBundle:TblInvoice", "t")
							  ->leftjoin("t.customer", 'u')
                              ->where("t.id = :userid")
							  ->andWhere('t.isDeleted = :deleteInfo')
                              ->setParameter("userid", base64_decode(base64_decode(base64_decode($invoiceID))))
							  ->setParameter('deleteInfo', '0')
                              ->getQuery()
                              ->getOneOrNullResult(QUERY::HYDRATE_ARRAY); 
                              $em->flush();      
					    if(!empty($getAllInvoices)){
							$taxesDetails = array();
									if(!empty($getAllInvoices['invoiceData'])){
											foreach(json_decode($getAllInvoices['invoiceData'], true) as $k => $v){
												/*get product information*/
												 $getProductInfo = $em->createQueryBuilder()
												                   ->select("t")
																   ->from("AppBundle:TblProducts", "t")
                                                                   ->where("t.id = :prodID")
                                                                   ->setParameter("prodID", $v['product_id'])
                                                                   ->getQuery()
                                                                   ->getOneOrNullResult(QUERY::HYDRATE_ARRAY);
                                                                $em->flush();    																   
												 $taxesDetails[$k]['prodID'] = $v['product_id'];
												 $taxesDetails[$k]['prodName'] = $getProductInfo['productName'];
												 $taxesDetails[$k]['prodimg'] = $getProductInfo['productImg'];
												 $taxesDetails[$k]['prodDesc'] = $v['productDes'];
											     $taxesDetails[$k]['prodQty'] = $productQty = $v['productQuantity'];
                                                 $taxesDetails[$k]['prodprice'] = $productprice = $v['productPrice'];												 
												  if(!empty($v['taxes'])){
													  foreach($v['taxes'] as $tax){
														$taxesDetails[$k]['taxDetails'][] = $this->getTaxByID($tax, $productQty, $productprice);
													  }
												  }
											}
										unset($getAllInvoices['invoiceData']);
										$getAllInvoices['invoiceData']	= $taxesDetails;
									}	
								$getCustomers = $em->createQueryBuilder()
														->select("t.id, t.firstName, t.lastName")
														->from("AppBundle:TblCustomers", 't')
														->where("t.emp = :emp")
														->setParameter("emp", $userData['userID'])
														->getQuery()
														->getResult(QUERY::HYDRATE_ARRAY);
														$em->flush();				
										/*encrypt customer ID*/
										foreach($getCustomers as $key => $val){
											$val['cust'] = base64_encode(base64_encode(base64_encode($val['id'])));
											$getCustomers[$key] = $val;
										}
										$getOrgData = $em->createQueryBuilder()
																->select('t')
																->from('AppBundle:TblAccount', 't')
																->where('t.user = :create')
																->setParameter('create', $userData['creatdby'])
																->getQuery()	
																->getOneOrNullResult(QUERY::HYDRATE_ARRAY);  
																$em->flush();
										$getProducts = $em->createQueryBuilder()
													   ->select("t.id, t.productName")
													   ->from("AppBundle:TblProducts", 't')
													   ->where('t.user = :userId')
													   ->andWhere('t.status = :status')
													   ->setParameter('userId', $userData['creatdby'])
													   ->setParameter('status', '1')
													   ->getQuery()
													   ->getResult(QUERY::HYDRATE_ARRAY);	
													   $em->flush();
										$getTaxes = $em->createQueryBuilder()
													->select("t")
													->from("AppBundle:TblTaxes", "t")
													->where("t.org = :orgID")
													->setParameter("orgID", $userData['creatdby'])
													->getQuery()
													->getResult(QUERY::HYDRATE_ARRAY);
													$em->flush();
										/*get reminders if any */
										$getReminder = $em->createQueryBuilder()
							                           ->select('t.reminderData')
							                           ->from('AppBundle:TblPaymentReminder', 't')
							                           ->where('t.invoice = :invoiceid')
							                           ->setParameter('invoiceid', $em->getRepository('AppBundle:TblInvoice')->find(base64_decode(base64_decode(base64_decode($invoiceID)))))
							                           ->getQuery()	
													   ->getOneOrNullResult(QUERY::HYDRATE_ARRAY);  
													$em->flush();	

										$reminderData = !(empty($getReminder)) ? json_decode($getReminder['reminderData']) : "";
														
										return $this->render('dashboard/invoice/view_invoice.html.twig', array(
											'title'	=>'Invoice View',
											'data' => $data,
											'orgData' => $getOrgData,
											'userData' => $userData,
											'custsData' => $getCustomers,
											'productsData' => $getProducts,
											'getTaxes' => $getTaxes,
											'productDetails' => $getAllInvoices,
											'invID' => $invoiceID,
											'reminderData' => $reminderData
										));
							}else{
												$response['status']= "error";
												$response['data'] = "";
												$response['error'] = "Not able to process this request!";
												echo json_encode($response); die();
							}							  
			        
		}else{
			        $response['status']= "error";
					$response['data'] = "";
					$response['error'] = "Not able to process this request!";
					echo json_encode($response); die();
		}	
	}
		
		
	} 
	
	public function getTaxByID($id, $proQty, $proPri){
		$em = $this->getDoctrine()->getManager();
		$taxArray = array();
	    if(!empty($id)){
			$getTaxDetatil = $em->createQueryBuilder()
		                 ->select("t")
						 ->from("AppBundle:TblTaxes", "t")
                         ->where("t.id = :taxID")
						 ->setParameter("taxID", $id)
						 ->getQuery()
						 ->getOneOrNullResult(QUERY::HYDRATE_ARRAY);
			$taxArray['name'] = ucfirst($getTaxDetatil['taxName']);
			$taxArray['percentage'] = $getTaxDetatil['taxRate'];
			$taxArray['amount'] = ($proQty * $proPri) * ($getTaxDetatil['taxRate']/100);
 			
		}
        return $taxArray; 		
	}
     /**
	 * @Route("/pdf/{id}", name="pdf")
	 */
		public function pdfAction(Request $request)
         {
 
     		$getBaseUrl = $request->server->get('DOCUMENT_ROOT');
			$invoiceID = $request->attributes->get('id');
		    $session 	= $request->getSession();
            $data = $getUserData =array();
			$userData = $session->get('userInfo');			
		   if(!empty($invoiceID)){
			$data = $getUserData =array();
			$userData = $session->get('userInfo');
			$em = $this->getDoctrine()->getManager();
			$getAllInvoices = $em->createQueryBuilder()
			                  ->select("t", 'u')
                              ->from("AppBundle:TblInvoice", "t")
							  ->leftjoin("t.customer", 'u')
                              ->where("t.id = :userid")
							  ->andWhere('t.isDeleted = :deleteInfo')
                              ->setParameter("userid", base64_decode(base64_decode(base64_decode($invoiceID))))
							  ->setParameter('deleteInfo', "0")
                              ->getQuery()
                              ->getOneOrNullResult(QUERY::HYDRATE_ARRAY); 
                              $em->flush();
					    if(!empty($getAllInvoices)){
							$taxesDetails = array();
									if(!empty($getAllInvoices['invoiceData'])){
											foreach(json_decode($getAllInvoices['invoiceData'], true) as $k => $v){
												/*get product information*/
												 $getProductInfo = $em->createQueryBuilder()
												                   ->select("t")
																   ->from("AppBundle:TblProducts", "t")
                                                                   ->where("t.id = :prodID")
                                                                   ->setParameter("prodID", $v['product_id'])
                                                                   ->getQuery()
                                                                   ->getOneOrNullResult(QUERY::HYDRATE_ARRAY);
                                                                $em->flush();    																   
												 $taxesDetails[$k]['prodID'] = $v['product_id'];
												 $taxesDetails[$k]['prodName'] = $getProductInfo['productName'];
												 $taxesDetails[$k]['prodimg'] = $getProductInfo['productImg'];
												 $taxesDetails[$k]['prodDesc'] = $v['productDes'];
											     $taxesDetails[$k]['prodQty'] = $productQty = $v['productQuantity'];
                                                 $taxesDetails[$k]['prodprice'] = $productprice = $v['productPrice'];												 
												  if(!empty($v['taxes'])){
													  foreach($v['taxes'] as $tax){
														$taxesDetails[$k]['taxDetails'][] = $this->getTaxByID($tax, $productQty, $productprice);
													  }
												  }
											}
										unset($getAllInvoices['invoiceData']);
										$getAllInvoices['invoiceData']	= $taxesDetails;
									}	
								$getCustomers = $em->createQueryBuilder()
														->select("t.id, t.firstName, t.lastName")
														->from("AppBundle:TblCustomers", 't')
														->where("t.emp = :emp")
														->setParameter("emp", $userData['userID'])
														->getQuery()
														->getResult(QUERY::HYDRATE_ARRAY);
														$em->flush();				
										/*encrypt customer ID*/
										foreach($getCustomers as $key => $val){
											$val['cust'] = base64_encode(base64_encode(base64_encode($val['id'])));
											$getCustomers[$key] = $val;
										}
										$getOrgData = $em->createQueryBuilder()
																->select('t')
																->from('AppBundle:TblAccount', 't')
																->where('t.user = :create')
																->setParameter('create', $userData['creatdby'])
																->getQuery()	
																->getOneOrNullResult(QUERY::HYDRATE_ARRAY);  
																$em->flush();
										$getProducts = $em->createQueryBuilder()
													   ->select("t.id, t.productName")
													   ->from("AppBundle:TblProducts", 't')
													   ->where('t.user = :userId')
													   ->andWhere('t.status = :status')
													   ->setParameter('userId', $userData['creatdby'])
													   ->setParameter('status', '1')
													   ->getQuery()
													   ->getResult(QUERY::HYDRATE_ARRAY);	
													   $em->flush();
										$getTaxes = $em->createQueryBuilder()
													->select("t")
													->from("AppBundle:TblTaxes", "t")
													->where("t.org = :orgID")
													->setParameter("orgID", $userData['creatdby'])
													->getQuery()
													->getResult(QUERY::HYDRATE_ARRAY);
													$em->flush();
										$pdfFile = 'invoice_'.base64_decode(base64_decode(base64_decode($invoiceID)));
													$protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
										$hostName = $request->server->get('HTTP_HOST');
										$baseDirectory = "";
										if(!empty($this->container->get('router')->getContext()->getBaseUrl())){
										    $baseDirectory = str_replace('app_dev.php', '', $this->container->get('router')->getContext()->getBaseUrl());
										}
										$rootUrl = $protocol.$hostName.$baseDirectory; 

										$html = $this->renderView('dashboard/invoice/pdf_invoice.html.twig', array(
											'title'	=>'Invoice View',
											'data' => $data,
											'orgData' => $getOrgData,
											'userData' => $userData,
											'custsData' => $getCustomers,
											'productsData' => $getProducts,
											'getTaxes' => $getTaxes,
											'productDetails' => $getAllInvoices,
											"siteURL" => $getBaseUrl,
											"rootURL" => $rootUrl
											
										));	
										return new Response(
											$this->get('knp_snappy.pdf')->getOutputFromHtml($html),
											200,
											array(
												'Content-Type'          => 'application/pdf',
												'Content-Disposition'   => 'attachment; filename="'.$pdfFile.'_'.time().'.pdf"'
											)
										);
								
							}else{
												$response['status']= "error";
												$response['data'] = "";
												$response['error'] = "Not able to process this request!";
												echo json_encode($response); die();
							}							  
			        
		}
    }
	
	/**
	 * @Route("/userpreview/{id}", name="userpreview")
	 */
		public function userpreviewAction(Request $request)
         {
		   $getBaseUrl = $request->server->get('DOCUMENT_ROOT');
			$invoiceID = $request->attributes->get('id');
		    $session 	= $request->getSession();
            $data = $getUserData =array();
			$userData = $session->get('userInfo');			
		   if(!empty($invoiceID)){
			$data = $getUserData =array();
			$userData = $session->get('userInfo');
			$em = $this->getDoctrine()->getManager();
			$getAllInvoices = $em->createQueryBuilder()
			                  ->select("t", 'u')
                              ->from("AppBundle:TblInvoice", "t")
							  ->leftjoin("t.customer", 'u')
                              ->where("t.id = :userid")
							  ->andWhere('t.isDeleted = :deleteInfo')
                              ->setParameter("userid", base64_decode(base64_decode(base64_decode($invoiceID))))
							  ->setParameter('deleteInfo', '0')
                              ->getQuery()
                              ->getOneOrNullResult(QUERY::HYDRATE_ARRAY); 
                              $em->flush();
					    if(!empty($getAllInvoices)){
							$taxesDetails = array();
									if(!empty($getAllInvoices['invoiceData'])){
											foreach(json_decode($getAllInvoices['invoiceData'], true) as $k => $v){
												/*get product information*/
												 $getProductInfo = $em->createQueryBuilder()
												                   ->select("t")
																   ->from("AppBundle:TblProducts", "t")
                                                                   ->where("t.id = :prodID")
                                                                   ->setParameter("prodID", $v['product_id'])
                                                                   ->getQuery()
                                                                   ->getOneOrNullResult(QUERY::HYDRATE_ARRAY);
                                                                $em->flush();    																   
												 $taxesDetails[$k]['prodID'] = $v['product_id'];
												 $taxesDetails[$k]['prodName'] = $getProductInfo['productName'];
												 $taxesDetails[$k]['prodimg'] = $getProductInfo['productImg'];
												 $taxesDetails[$k]['prodDesc'] = $v['productDes'];
											     $taxesDetails[$k]['prodQty'] = $productQty = $v['productQuantity'];
                                                 $taxesDetails[$k]['prodprice'] = $productprice = $v['productPrice'];												 
												  if(!empty($v['taxes'])){
													  foreach($v['taxes'] as $tax){
														$taxesDetails[$k]['taxDetails'][] = $this->getTaxByID($tax, $productQty, $productprice);
													  }
												  }
											}
										unset($getAllInvoices['invoiceData']);
										$getAllInvoices['invoiceData']	= $taxesDetails;
									}	
								$getCustomers = $em->createQueryBuilder()
														->select("t.id, t.firstName, t.lastName")
														->from("AppBundle:TblCustomers", 't')
														->where("t.emp = :emp")
														->setParameter("emp", $userData['userID'])
														->getQuery()
														->getResult(QUERY::HYDRATE_ARRAY);
														$em->flush();				
										/*encrypt customer ID*/
										foreach($getCustomers as $key => $val){
											$val['cust'] = base64_encode(base64_encode(base64_encode($val['id'])));
											$getCustomers[$key] = $val;
										}
										$getOrgData = $em->createQueryBuilder()
																->select('t')
																->from('AppBundle:TblAccount', 't')
																->where('t.user = :create')
																->setParameter('create', $userData['creatdby'])
																->getQuery()	
																->getOneOrNullResult(QUERY::HYDRATE_ARRAY);  
																$em->flush();
										$getProducts = $em->createQueryBuilder()
													   ->select("t.id, t.productName")
													   ->from("AppBundle:TblProducts", 't')
													   ->where('t.user = :userId')
													   ->andWhere('t.status = :status')
													   ->setParameter('userId', $userData['creatdby'])
													   ->setParameter('status', '1')
													   ->getQuery()
													   ->getResult(QUERY::HYDRATE_ARRAY);	
													   $em->flush();
										$getTaxes = $em->createQueryBuilder()
													->select("t")
													->from("AppBundle:TblTaxes", "t")
													->where("t.org = :orgID")
													->setParameter("orgID", $userData['creatdby'])
													->getQuery()
													->getResult(QUERY::HYDRATE_ARRAY);
													$em->flush();
										$pdfFile = 'invoice_'.base64_decode(base64_decode(base64_decode($invoiceID)));
										return $this->render('dashboard/invoice/preview_invoice.html.twig', array(
											'title'	=>'Invoice View',
											'data' => $data,
											'orgData' => $getOrgData,
											'userData' => $userData,
											'custsData' => $getCustomers,
											'productsData' => $getProducts,
											'getTaxes' => $getTaxes,
											'productDetails' => $getAllInvoices,
											"siteURL" => $getBaseUrl
											
										));	
							}else{
												$response['status']= "error";
												$response['data'] = "";
												$response['error'] = "Not able to process this request!";
												echo json_encode($response); die();
							}							  
			        
		}
							
       
        
    }
	/**
     * @Route("/deleteinvoice", name="deleteinvoice")
     */
	public function deleteinvoice(Request $request){

		$session 	= $request->getSession();
		if(empty($session->get('userInfo'))){
			return $this->redirectToRoute('login'); 
		}else{
			$data = $getUserData =array();
			$userData = $session->get('userInfo');
			if(!empty($request->request->get('invoice'))){
				$invoice = $request->request->get('invoice');
				$em = $this->getDoctrine()->getManager();
				$deleteInvoice = $em->createQueryBuilder()
				                 ->update("AppBundle:TblInvoice", "t")
								 ->set('t.isDeleted', '?1')
								 ->where('t.id = :invoiceID')
								 ->setParameter(1, '1')
								 ->setParameter('invoiceID', base64_decode(base64_decode(base64_decode($invoice))))
								 ->getQuery();
				$updateRecord =	$deleteInvoice->execute();	
                if($updateRecord == true){
								$response['status']= "ok";
								$response['data'] = "";
								$response['message'] = "Invoice deleted successfully!";
								echo json_encode($response); die();
					}else{
						$response['status']= "error";
						$response['data'] = "";
						$response['error'] = "Not able to process this request! Please try again later";
						echo json_encode($response); die();
					} 				
			}else{
				$response['status']= "error";
				$response['data'] = "";
				$response['error'] = "Not able to process this request!";
				echo json_encode($response); die();
			}
			
		}
	}
  	
	/**
     * @Route("/sendinvoiceemail", name="sendinvoiceemail")
     */		
	public function sendinvoiceemail(Request $request){
		$session 	= $request->getSession();
		$getBaseUrl = $request->server->get('DOCUMENT_ROOT');
		if(empty($session->get('userInfo'))){
			return $this->redirectToRoute('login'); 
		}else{
			$userData = $session->get('userInfo');
			parse_str($request->request->get('sendEmailData'), $postData);
	      if(!empty($postData)){
			$invoiceID = $postData['invoiceId'];
			$subjectLine = $postData['subjectIncoice'];
			$invoiceMessage = $postData['messageIncoice'];
			$sendInvoiceToMe = isset($postData['send_invoive_to_me']) ? $postData['send_invoive_to_me'] : "";
			$pdfAttachment = isset($postData['attach_invoice_as_pdf']) ? $postData['attach_invoice_as_pdf'] : "";

			$em = $this->getDoctrine()->getManager();
			$getAllInvoices = $em->createQueryBuilder()
				  ->select("t", 'u')
				  ->from("AppBundle:TblInvoice", "t")
				  ->leftjoin("t.customer", 'u')
				  ->where("t.id = :userid")
				  ->andWhere('t.isDeleted = :deleteInfo')
				  ->setParameter("userid", $invoiceID)
				  ->setParameter('deleteInfo', '0')
				  ->getQuery()
				  ->getOneOrNullResult(QUERY::HYDRATE_ARRAY); 
				  $em->flush();
			$taxesDetails = array();
			if(!empty($getAllInvoices['invoiceData'])){
					foreach(json_decode($getAllInvoices['invoiceData'], true) as $k => $v){
						/*get product information*/
						 $getProductInfo = $em->createQueryBuilder()
										   ->select("t")
										   ->from("AppBundle:TblProducts", "t")
										   ->where("t.id = :prodID")
										   ->setParameter("prodID", $v['product_id'])
										   ->getQuery()
										   ->getOneOrNullResult(QUERY::HYDRATE_ARRAY);
										$em->flush();    																   
						 $taxesDetails[$k]['prodID'] = $v['product_id'];
						 $taxesDetails[$k]['prodName'] = $getProductInfo['productName'];
						 $taxesDetails[$k]['prodimg'] = $getProductInfo['productImg'];
						 $taxesDetails[$k]['prodDesc'] = $v['productDes'];
						 $taxesDetails[$k]['prodQty'] = $productQty = $v['productQuantity'];
						 $taxesDetails[$k]['prodprice'] = $productprice = $v['productPrice'];												 
						  if(!empty($v['taxes'])){
							  foreach($v['taxes'] as $tax){
								$taxesDetails[$k]['taxDetails'][] = $this->getTaxByID($tax, $productQty, $productprice);
							  }
						  }
					}
				unset($getAllInvoices['invoiceData']);
				$getAllInvoices['invoiceData']	= $taxesDetails;
			}	
			$getCustomers = $em->createQueryBuilder()
							->select("t.id, t.firstName, t.lastName")
							->from("AppBundle:TblCustomers", 't')
							->where("t.emp = :emp")
							->setParameter("emp", $userData['userID'])
							->getQuery()
							->getResult(QUERY::HYDRATE_ARRAY);
							$em->flush();				
			/*encrypt customer ID*/
			foreach($getCustomers as $key => $val){
				$val['cust'] = base64_encode(base64_encode(base64_encode($val['id'])));
				$getCustomers[$key] = $val;
			}
			$getOrgData = $em->createQueryBuilder()
									->select('t')
									->from('AppBundle:TblAccount', 't')
									->where('t.user = :create')
									->setParameter('create', $userData['creatdby'])
									->getQuery()	
									->getOneOrNullResult(QUERY::HYDRATE_ARRAY);  
									$em->flush();
			$getProducts = $em->createQueryBuilder()
						   ->select("t.id, t.productName")
						   ->from("AppBundle:TblProducts", 't')
						   ->where('t.user = :userId')
						   ->andWhere('t.status = :status')
						   ->setParameter('userId', $userData['creatdby'])
						   ->setParameter('status', '1')
						   ->getQuery()
						   ->getResult(QUERY::HYDRATE_ARRAY);	
						   $em->flush();
			$getTaxes = $em->createQueryBuilder()
						->select("t")
						->from("AppBundle:TblTaxes", "t")
						->where("t.org = :orgID")
						->setParameter("orgID", $userData['creatdby'])
						->getQuery()
						->getResult(QUERY::HYDRATE_ARRAY);
						$em->flush();
		   
							$protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';

							$hostName = $request->server->get('HTTP_HOST');
							$baseDirectory = "";
							if(!empty($this->container->get('router')->getContext()->getBaseUrl())){
							    $baseDirectory = str_replace('app_dev.php', '', $this->container->get('router')->getContext()->getBaseUrl());
							}
							$protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
							$hostName = $request->server->get('HTTP_HOST');
							$baseDirectory = "";
							if(!empty($this->container->get('router')->getContext()->getBaseUrl())){
							    $baseDirectory = str_replace('app_dev.php', '', $this->container->get('router')->getContext()->getBaseUrl());
							}
							$rootUrl = $protocol.$hostName.$baseDirectory; 


						$data = array('title'=>"Invoice",
											'orgData' => $getOrgData,
											'userData' => $userData,
											'custsData' => $getCustomers,
											'productsData' => $getProducts,
											'getTaxes' => $getTaxes,
											'productDetails' => $getAllInvoices,
											"siteURL" => $getBaseUrl,
											"rootURL" => $rootUrl
								);		
						$html = $this->renderView('dashboard/invoice/pdf_invoice.html.twig', $data);

						if(!empty($sendInvoiceToMe)){
							$sendEmailTo = array_push($postData['sendemail'], $fromInvoice);
						}else{
							$sendEmailTo = $postData['sendemail'];
						}		
						if(!empty($pdfAttachment)){
							$pdffileName = $invoiceID.'_'.time();
						$this->get('knp_snappy.pdf')->generateFromHtml(
							$this->renderView(
								'dashboard/invoice/pdf_invoice.html.twig',
								$data
							),
							$getBaseUrl.'/uploads/invoices/pdf_'.$pdffileName.'.pdf'
						);
						$message;

						$message = \Swift_Message::newInstance()
							->setSubject($subjectLine)
							->setFrom($fromInvoice)
							->setTo($sendEmailTo)
							->setCharset('iso-8859-1')
							->setContentType('text/html')
							->attach(\Swift_Attachment::fromPath($getBaseUrl.'/uploads/invoices/pdf_'.$pdffileName.'.pdf'))
							->setBody(
								$this->renderView(
									'emails/preview_invoice.html.twig',$data)
							);
							$this->get('mailer')->send($message);
						}else{
							$message;

						$message = \Swift_Message::newInstance()
							->setSubject($subjectLine)
							->setFrom($fromInvoice)
							->setTo($sendEmailTo)
							->setCharset('iso-8859-1')
							->setContentType('text/html')
							->setBody(
								$this->renderView(
									'emails/preview_invoice.html.twig',$data)
							);
							$this->get('mailer')->send($message);
						}
                     /*database update of email sent*/
						 $userUpdate = $em->createQueryBuilder();
						 $updateQuery = $userUpdate->update('AppBundle:TblInvoice', 'I')
									->set('I.invoiceSentDate', '?1')
									->set('I.invoiceSent', '?2')					
									->where('I.id = ?3')
									->setParameter(1, date('Y-m-d H:i:s'))
                                    ->setParameter(2, '1')
									->setParameter(3, $postData['invoiceId'])
									->getQuery();
				         $updateResult = $updateQuery->execute();


                        $response['status']= "ok";
						$response['data'] = "";
						$response['message'] = "Invoice sent successfully.";
						echo json_encode($response); die();  

		       }else{
                        $response['status']= "error";
						$response['data'] = "";
						$response['error'] = "Not able to process this request!";
						echo json_encode($response); die();
		       } 
			
		
			
					
		}

	}
    /**
     * @Route("/skipinvoicesend", name="skipinvoicesend")
     */	
	public function skipinvoicesend(Request $request){
       $session 	= $request->getSession();
		if(empty($session->get('userInfo'))){
			return $this->redirectToRoute('login'); 
		}else{
			$data = $getUserData =array();
			$userData = $session->get('userInfo');
			if(!empty($request->request->get('invoice'))){
				$invoice = $request->request->get('invoice');
				$em = $this->getDoctrine()->getManager();
				$marksentInvoice = $em->createQueryBuilder()
				                 ->update("AppBundle:TblInvoice", "t")
								 ->set('t.invoiceSkipSend', '?1')
								 ->where('t.id = :invoiceID')
								 ->setParameter(1, '1')
								 ->setParameter('invoiceID', base64_decode(base64_decode(base64_decode($invoice))))
								 ->getQuery();
				$updateRecord =	$marksentInvoice->execute();	
                if($updateRecord == true){
								$response['status']= "ok";
								$response['data'] = "";
								$response['message'] = "Invoice Mark sent successfully!";
								echo json_encode($response); die();
					}else{
						$response['status']= "error";
						$response['data'] = "";
						$response['error'] = "Not able to process this request! Please try again later";
						echo json_encode($response); die();
					}
			}else{
				$response['status']= "error";
				$response['data'] = "";
				$response['error'] = "Not able to process this request!";
				echo json_encode($response); die();
			}
		}	
	}

	  /**
     * @Route("/marksentinvoice", name="marksentinvoice")
     */
	public function marksentinvoice(Request $request){

		$session 	= $request->getSession();
		if(empty($session->get('userInfo'))){
			return $this->redirectToRoute('login'); 
		}else{
			$data = $getUserData =array();
			$userData = $session->get('userInfo');
			if(!empty($request->request->get('invoice'))){
				$invoice = $request->request->get('invoice');
				$em = $this->getDoctrine()->getManager();
				$marksentInvoice = $em->createQueryBuilder()
				                 ->update("AppBundle:TblInvoice", "t")
								 ->set('t.invoiceSent', '?1')
								 ->set('t.invoiceSentDate', '?2')
								 ->where('t.id = :invoiceID')
								 ->setParameter(1, '1')
								 ->setParameter(2, date('Y-m-d H:i:s'))
								 ->setParameter('invoiceID', base64_decode(base64_decode(base64_decode($invoice))))
								 ->getQuery();
				$updateRecord =	$marksentInvoice->execute();	
                if($updateRecord == true){
								$response['status']= "ok";
								$response['data'] = "";
								$response['message'] = "Invoice Mark as sent successfully!";
								echo json_encode($response); die();
					}else{
						$response['status']= "error";
						$response['data'] = "";
						$response['error'] = "Not able to process this request! Please try again later";
						echo json_encode($response); die();
					} 				
			}else{
				$response['status']= "error";
				$response['data'] = "";
				$response['error'] = "Not able to process this request!";
				echo json_encode($response); die();
			}
			
		}
	}
	
	 
}
