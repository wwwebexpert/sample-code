<?php

namespace AppBundle\Controller\invoice;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use AppBundle\Entity\TblUsersRole;
use AppBundle\Entity\TblUsers;
use AppBundle\Entity\TblAccount;
use AppBundle\Entity\TblProducts;
use AppBundle\Entity\TblCustomers;
use AppBundle\Entity\TblCcinfo;
use AppBundle\Entity\TblTaxes;
use AppBundle\Entity\TblPaymentRecords;

class PaymentController extends Controller
{
    /**
     * @Route("/paymentrecord", name="paymentrecord")
     */
    public function paymentrecordAction(Request $request)
    {
		$session 	= $request->getSession();
		if(empty($session->get('userInfo'))){
			return $this->redirectToRoute('login'); 
		}else{
			if(!empty($request->request->get("paymentRecords"))){
				$userData = $session->get('userInfo');
				 parse_str($request->request->get('paymentRecords'), $postData);
				 $paymentID = $userData['userID'].time();
				 $invoiceID = $postData['invoice'];
				 $userID = $userData['userID'];
				 $paymentAmount = $postData['payment_amount'];
				 $paymentMethod = $postData['payment_method'];
				 $paymentAccount = $postData['payment_account'];
				 $paymentMemo = $postData['payment_memo'];
				 $paymentDate = $postData['payment_date'];
                 $em = $this->getDoctrine()->getManager();   
    			 $paymentRecord = new TblPaymentRecords();
				 $paymentRecord->setUser($em->getRepository('AppBundle:TblUsers')->find($userID));
				 $paymentRecord->setInvoice($em->getRepository('AppBundle:TblInvoice')->find($invoiceID));
				 $paymentRecord->setPaymentId($paymentID);
				 $paymentRecord->setPaymentAmount($paymentAmount);
				 $paymentRecord->setPaymentMethod($paymentMethod);
				 $paymentRecord->setPaymentAccount($paymentAccount);
				 $paymentRecord->setPaymentMemo($paymentMemo);
				 $paymentRecord->setCreatedDate(new \DateTime($paymentDate));
				 $em->persist($paymentRecord);
				 $em->flush();
				 $paymentRecordsID = $paymentRecord->getId();
                 /*update invoice table*/
                 $invoiceUpdate = $em->createQueryBuilder();
                 $invoiceQuery = $invoiceUpdate->update('AppBundle:TblInvoice', 'u')
					->set('u.paidStatus', '?1')
					->set('u.invoiceSent', '?2')
					->where('u.id = ?3')
					->setParameter(1, '1')
					->setParameter(2, '1')
					->setParameter(3, $invoiceID)
					->getQuery();
                 $invoiceResult = $invoiceQuery->execute();


				 if(!empty($paymentRecordsID)){
        			$response['status']= "ok";
					$response['data'] = array('id'=> $paymentRecordsID);
					$response['message'] = "Payment has been successfully recorded.";
					$response['error'] = "";
					echo json_encode($response); die;
				 }else{
					$response['status']= "error";
					$response['data'] = "";
					$response['error'] = "Not able to process this request!";
					echo json_encode($response); die();
				 }
			}else{
				$response['status']= "error";
				$response['data'] = "";
				$response['error'] = "Not able to process this request!";
				echo json_encode($response); die();
			}
			
		} 
    }

    /**
     * @Route("/paylist", name="paylist")
     */
    public function paylistAction(Request $request)
    {
		$session 	= $request->getSession();
		if(empty($session->get('userInfo'))){
			return $this->redirectToRoute('login'); 
		}else{			
			$userData = $session->get('userInfo');
			$allEmployees = $invoices = array();
			$em = $this->getDoctrine()->getManager();
			if($userData['userRoleSlug'] == 'organization'){
				/*get invoice which are already sent by not paid yet*/
				$allEmployees = $em->createQueryBuilder('t')
				->select('t.id', 'c.firstName', 'c.lastName')
				->from('AppBundle:TblUsers', 't')
				->leftjoin('AppBundle:TblAccount', 'c', 'WITH', 't.id = c.user')
				->where('t.createdBy = :createdby')
				->setParameter("createdby", $userData['userID'])
				->orderBy('c.firstName', 'ASC')
				->getQuery()
				->getResult(Query::HYDRATE_ARRAY);
				$em->flush();
			/* Invoice Listing*/			
			}else if($userData['userRoleSlug'] == 'employee'){							
			/*get invoice which are already sent by not paid yet*/
			   $invoices = $em->createQueryBuilder()
                            ->select('t.posoNumber, t.invoiceStart, t.paymentDue, t.paidStatus, t.totalAmount, t.totalTax, t.invoiceData, t.createdAt, t.id, t.invoiceSent, t.invoiceSentDate, t.invoiceSkipSend, c.id as acc_id, c.firstName, c.lastName', 'u.id as customer')
                            ->from('AppBundle:TblInvoice', 't')
                            ->leftjoin('t.customer', 'u')
							->leftjoin('AppBundle:TblAccount', 'c', 'WITH', 't.user = c.user')
                            ->where('t.user = :userID')
                            ->andWhere('t.invoiceSent = :invoicesent')
                            ->andWhere('t.paidStatus = :paidstatus')
							->andWhere('t.isDeleted = :deleteInfo')
							->andWhere('t.status = :status')
                            ->setParameter('userID', $userData['userID'])
                            ->setParameter('invoicesent', '1')
                            ->setParameter('paidstatus', '0')
							->setParameter('deleteInfo', '0')
							->setParameter('status', '1')							
                            ->orderBy('t.id', 'DESC')
                            ->getQuery()
                            ->getResult(QUERY::HYDRATE_ARRAY);
                            $em->flush();
                 if(!empty($invoices)){
				   foreach($invoices as $key => $val){
					   $invoiceID = base64_encode(base64_encode(base64_encode($val['customer'])));
					   $invoices[$key]['customer'] = $invoiceID;
					   }
				   }
			}
			$getCurrencies =  $em->createQueryBuilder('t')
                ->select('t')
                ->from('AppBundle:TblCurrencies', 't')		
                ->orderBy('t.country', 'ASC')				
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
                $em->flush();				
			$data = $getUserData =array();
			 return $this->render('dashboard/invoice/paylist.html.twig', array(
				'title'	=>'Payment List',
				'data'	=> $data,
				'userDatauplevel' => $getUserData,
				'userData' => $userData,
				'employees' => $allEmployees,
				'currencies' => $getCurrencies,
				'invoices' => $invoices
			));
		}  
    }

     /**
     * @Route("/ccinfo/{id}", name="ccinfo")
     */
	 public function ccinfoAction(Request $request){
	    $session 	= $request->getSession();
		if(empty($session->get('userInfo'))){
			return $this->redirectToRoute('login'); 
		}else{
			if(!empty($request->attributes->get('id'))){
				$data = array();
				$userData = $session->get('userInfo');
				$cusermer = base64_decode(base64_decode(base64_decode($request->attributes->get('id'))));
				/*get customers stored cards information*/
				$cardArray = array();
				$em = $this->getDoctrine()->getManager();
				$getCustomerInfo = $em->createQueryBuilder()
				                   ->select('t.ccNumber, t.ccName, t.ccExpdate, t.id')
				                   ->from('AppBundle:TblCcinfo', 't')
				                   ->where('t.customer = :custmoer')
				                   ->setParameter("custmoer", $cusermer)
				                   ->getQuery()
				                   ->getResult(Query::HYDRATE_ARRAY);
				                   $em->flush();
				if(!empty($getCustomerInfo)){
					foreach($getCustomerInfo as $key => $val){
						$ccid = base64_encode(base64_encode($val['id']));
						$lastFourDigit = substr(base64_decode(base64_decode($val['ccNumber'])), -4);
						$val['ccNumber'] = 'XXXX-XXXX-XXXX-'.$lastFourDigit;
						$val['ccid']= $ccid;
                        $cardArray[$key]= $val; 
					}
				}
				/*get customer info*/
				$getCustomer = $em->createQueryBuilder()
				               ->select("c.id, c.firstName, c.lastName, c.email, c.phone")
				               ->from("AppBundle:TblCustomers", 'c')
				               ->where('c.id = :customer')
				               ->setParameter('customer', $cusermer)
				               ->getQuery()
				               ->getoneorNullResult(Query::HYDRATE_ARRAY);  
				$getCustomer['id'] = base64_encode(base64_encode($getCustomer['id']));
                return $this->render('dashboard/invoice/cardinfo.html.twig', array(
				'title'			=>'Credit Card Info',
				'data'	=> $data,
				'userData' => $userData,
				'countOldCard' => count($cardArray),
				'storedCards' => $cardArray,
				'customerInfo' => $getCustomer
			)); 
			}else{
				$response['status']= "error";
				$response['data'] = "";
				$response['error'] = "Not able to process this request!";
				echo json_encode($response); die();
			}
		}
	 	
	 }
	 /**
     * @Route("/getcardinfo", name="getcardinfo")
     */
	 public function getcardinfoAction(Request $request){
	    $session 	= $request->getSession();
		if(empty($session->get('userInfo'))){
			return $this->redirectToRoute('login'); 
		}else{
			if(!empty($request->request->get('id'))){
				$data = array();
				$cusermer = base64_decode(base64_decode($request->request->get('id')));
				/*get customers stored cards information*/
				$cardArray = array();
				$em = $this->getDoctrine()->getManager();
				$getCustomerInfo = $em->createQueryBuilder()
				                   ->select('t.ccNumber, t.ccName, t.ccExpdate, t.id')
				                   ->from('AppBundle:TblCcinfo', 't')
				                   ->where('t.id = :id')
				                   ->setParameter("id", $cusermer)
				                   ->getQuery()
				                   ->getResult(Query::HYDRATE_ARRAY);
				                   $em->flush();
				if(!empty($getCustomerInfo)){
					foreach($getCustomerInfo as $key => $val){
						$lastFourDigit = substr(base64_decode(base64_decode($val['ccNumber'])), -4);
						$val['ccNumber'] = 'XXXX-XXXX-XXXX-'.$lastFourDigit;
                        $cardArray[$key]= $val; 
					}
				}
				$response['status']= "ok";
				$response['data'] = "";
				$response['message'] = "";
				echo json_encode($response); die();
			}else{
				$response['status']= "error";
				$response['data'] = "";
				$response['error'] = "Not able to process this request!";
				echo json_encode($response); die();
			}
		}
	 	
	 }
	  /**
     * @Route("/addnewcard", name="addnewcard")
     */
	 public function addnewcardAction(Request $request){
	    $session 	= $request->getSession();
		if(empty($session->get('userInfo'))){
			return $this->redirectToRoute('login'); 
		}else{
			if(!empty($request->request->get('formData'))){
			 parse_str($request->request->get('formData'), $postData);
			 $em =$this->getDoctrine()->getManager();
			            $cinfo= new TblCcinfo();
						$cinfo->setCcNumber(base64_encode(base64_encode($postData['ccnumber'])));
						$cinfo->setCcName($postData['nameOnCard']);
						$cinfo->setCcExpdate($postData['expMon']."/".$postData['expYear']);
						$cinfo->setCustomer($em->getRepository('AppBundle:TblCustomers')->find(base64_decode(base64_decode($postData['customer']))));
						$em->persist($cinfo);
						$em->flush();
				$ccID = $cinfo->getId();
				if(!empty($ccID)){
					$response['status']= "ok";
					$response['data'] = $ccID;
					$response['message'] = "";
					echo json_encode($response); die();

				}else{
                    $response['status']= "error";
					$response['data'] = "";
					$response['error'] = "Not able to process this request!";
					echo json_encode($response); die();
				}
				
			}else{
				$response['status']= "error";
				$response['data'] = "";
				$response['error'] = "Not able to process this request!";
				echo json_encode($response); die();
			
			}
			
		}
	 	
	 }
	
}
