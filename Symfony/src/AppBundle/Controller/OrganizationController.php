<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use AppBundle\Entity\TblUsersRole;
use AppBundle\Entity\TblUsers;
use AppBundle\Entity\TblAccount;

class OrganizationController extends Controller
{
    /**
     * @Route("/add-organization", name="add_organization")
     */
    public function add_organizationAction(Request $request)
    {
		$session 	= $request->getSession();
		$em = $this->getDoctrine()->getManager();
		$userAccess =array('super', 'distributor', 'account');
		if(empty($session->get('userInfo'))  || !in_array($session->get('userInfo')['userRoleSlug'], $userAccess)){
			return $this->redirectToRoute('login'); 
		}else{
			$userData = $session->get('userInfo');
			if($userData['is_navigate'] == 1 && $userData['authorize'] == 0 ){
					$response['status']= "error";
					$response['data'] = "";
					$response['error'] = "You can't access directly to this page!";
					echo json_encode($response); die(); 
				}
            $getUserData = $em->createQueryBuilder('t')
                ->select('t.id', 't.username')
                ->from('AppBundle:TblUsers', 't')
				->where('t.userRole = :userRole')
                ->andWhere('t.isDeleted != 1')
				->andWhere('t.createdBy = :createdby')
                ->setParameter('userRole', 3)
                ->setParameter('createdby', $userData['userID'])
                ->orderBy('t.id', 'DESC')				
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
			$data = array();
			 return $this->render('dashboard/addUsers/add_organization.html.twig', array(
				'title'			=>'Add Account',
				'data'	=> $getUserData,
				'userData' => $userData
			));
		}
		 
    }
	/**
     * @Route("/insertorganization", name="insertorganization")
     */
    public function insertorganizationAction(Request $request)
    {
		$session 	= $request->getSession();
		$docmentPath = $this->getParameter('profileImg');
		$response = array();
		$userAccess =array('super', 'distributor', 'account');
		if(empty($session->get('userInfo'))  || !in_array($session->get('userInfo')['userRoleSlug'], $userAccess)){
			return $this->redirectToRoute('login'); 
		}else{
			$postData = array();
			$sessData = $session->get('userInfo');
			if($sessData['is_navigate'] == 1 && $sessData['authorize'] == 0 ){
					$response['status']= "error";
					$response['data'] = "";
					$response['error'] = "You can't access directly to this page!";
					echo json_encode($response); die(); 
				}
			$currentUserRole =  $sessData['userRoleSlug'];
            $postData = $request->request->all(); 
			 $createdByVal = ($currentUserRole == 'distributor') ? $postData['account'] : $sessData['userID']; 
			 if(!empty($postData)){
				 if($postData['gender'] == 'M'){
					 $gender = 'Male';
				 }else{
					 $gender = 'Female';
				 }
				 /*upload documnets of user */  
					$uploadeing_img = $request->files->get('files');
					$fileName = "";
					$imgArrName = array();
						if(!empty($uploadeing_img)){
							if($uploadeing_img[0]->guessExtension() == 'jpeg'){
								$ext = '.jpg';
							}else{
								$ext = '.'.$uploadeing_img[0]->guessExtension();
							}
							$fileName = $postData['username'].'_'.time().$ext;
							$uploadeing_img[0]->move($docmentPath."/", $fileName);
						}
				    $em = $this->getDoctrine()->getManager();
				    $userinfo= new TblUsers();
					$userinfo->setUsername($postData['username']);
					$userinfo->setPassword($postData['password']);
					$userinfo->setEmail($postData['email']);
					$userinfo->setCreatedBy($createdByVal);
					$userinfo->setCreatedOn();
					$userinfo->setUpdatedOn();
					//2 for distributor type of usersetUpdatedOn
					$getUserRole = $em->getRepository('AppBundle:TblUsersRole')->find(4);
					$userinfo->setUserRole($getUserRole);
					$em->persist($userinfo);
					$em->flush();
					$organizationID = $userinfo->getId();
					
					$accountinfo= new TblAccount();
					$user_id = $em->getRepository('AppBundle:TblUsers')->find($organizationID);
					$accountinfo->setUser($user_id);
					$accountinfo->setFirstName($postData['firstname']);
					$accountinfo->setLastName($postData['lastname']);
					$accountinfo->setDob( new \DateTime($postData['birthday']));
					$accountinfo->setGender($gender);
					$accountinfo->setProfileimg($fileName);
					$accountinfo->setAddress($postData['address']);
					$accountinfo->setCity($postData['city']);
					if(!empty($postData['state'])){
					$accountinfo->setState($postData['state']);
					}
					if(!empty($postData['country'])){
					$accountinfo->setCountry($postData['country']);
					}
					if(!empty($postData['zipcode'])){
						$accountinfo->setZipcode($postData['zipcode']);
					}
					$accountinfo->setGender($gender);
					$accountinfo->setUserRole(4);
					$em->persist($accountinfo);
					$em->flush();
					$organisationACID = $accountinfo->getId();
					if(!empty($organizationID) && !empty($organisationACID)){
						/*send email to user with credencials*/
						$sentTo = $postData['email'];
						//$sentTo = 'ashok.mobilyte@gmail.com';
						$message = \Swift_Message::newInstance()
							->setSubject('Welcome at Merchant Services')
							->setFrom('ashok.mobilyte@gmail.com')
							->setTo($sentTo)
							->setCharset('iso-8859-1')
							->setContentType('text/html')
							->setBody(
								$this->renderView(
									'emails/adduser.html.twig',
									array('username' => $postData['username'], 'password' => $postData['password'], 'role' => "Distributor")
								)
							);
							$this->get('mailer')->send($message);
						$sessArray = $session->get('userInfo');
						$sessArray['totalusers']['totalUsers'] = !empty($sessArray['totalusers']['totalUsers']) ? $sessArray['totalusers']['totalUsers'] + 1 : 1;
						$sessArray['totalusers']['totalorganizations'] = !empty($sessArray['totalusers']['totalorganizations']) ? $sessArray['totalusers']['totalorganizations'] + 1 : 1;
						$userData = $session->set('userInfo', $sessArray);
						$response['status']= "ok";
						$response['data'] = "";
						$response['message'] = "Orgnaization has been created successfully!";
						echo json_encode($response); die(); 
					}else{
						$response['status']= "error";
						$response['data'] = "";
						$response['error'] = "Something was wrong!";
						echo json_encode($response); die(); 
					}
			 }else{
				$response['status']= "error";
				$response['data'] = "";
				$response['error'] = "Not able to process this request!";
				echo json_encode($response); die(); 
			 }
		}
		 
    }
    /**
     * @Route("/organizations", name="organizations")
     */	
	 public function organizations(Request $request){
		$session 	= $request->getSession();
		$userAccess =array('super', 'distributor', 'account');
		if(empty($session->get('userInfo'))  || !in_array($session->get('userInfo')['userRoleSlug'], $userAccess)){
			return $this->redirectToRoute('login'); 
		}else{
			$userData = $session->get('userInfo');
			$getdistributors = $this->getDoctrine()->getManager();
			if($userData['userRoleSlug'] == 'distributor'){
				$allOrganizations = array();
				$getAllaccounts = $getdistributors->createQueryBuilder('t')
                ->select('a.id')
                ->from('AppBundle:TblAccount', 't')
				->leftjoin('t.user', 'a')
				->where('t.userRole = :userRole')				
                ->andWhere('t.isDeleted != 1')
				->andWhere('a.createdBy = :createdby')
                ->setParameter('userRole', 3)
                ->setParameter('createdby', $userData['userID'])				
                ->orderBy('t.id', 'DESC')				
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
				$getdistributors->flush();
				$arrayAllAccount = array();
				if(!empty($getAllaccounts)){
					foreach($getAllaccounts as $key => $val){
						$getAllaccounts1 = $getdistributors->createQueryBuilder('t')
						->select('t, a')
						->from('AppBundle:TblAccount', 't')
						->leftjoin('t.user', 'a')
						->where('t.userRole = :userRole')				
						->andWhere('t.isDeleted != 1')
						->andWhere('a.createdBy = :id')
						->setParameter('userRole', 4)
						->setParameter('id', $val['id'])				
						->orderBy('t.id', 'DESC')				
						->getQuery()
						->getResult(Query::HYDRATE_ARRAY);
						if(!empty($getAllaccounts1)){
							array_push($arrayAllAccount, $getAllaccounts1);
						}
						$getAllaccounts1 = array();
					}
				}
				
				foreach($arrayAllAccount as $val){
					if(!empty($val)){
						foreach($val as $v){
						  $allOrganizations[] = $v;		
						}					 
					}					
				}
			
			}else if($userData['userRoleSlug'] == 'account'){
				$allOrganizations = $getdistributors->createQueryBuilder('t')
                ->select('t, a')
                ->from('AppBundle:TblAccount', 't')
				->leftjoin('t.user', 'a')
				->where('t.userRole = :userRole')				
                ->andWhere('t.isDeleted != 1')
				->andWhere('a.createdBy = :createdby')
                ->setParameter('userRole', 4)
                ->setParameter('createdby', $userData['userID'])				
                ->orderBy('t.id', 'DESC')				
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
                $getdistributors->flush();				
			}
			$data = $getUserData =array();
			 return $this->render('dashboard/addUsers/list_organization.html.twig', array(
				'title'			=>'Organizations',
				'data'	=> $data,
				'userDatauplevel' => $getUserData,
				'userData' => $userData,
				'organizations' => $allOrganizations
			));
		} 
	 }
	 /**
     * @Route("/getorganization", name="getorganization")
     */	
	 public function getorganization(Request $request){
		 if(!empty($request->request->get('accID'))){
		 $session 	= $request->getSession();
		 $em = $this->getDoctrine()->getManager();
		$getuserAccountData = $em->createQueryBuilder('t')
				->select('t,a')
				->from('AppBundle:TblAccount', 't')
				->leftjoin('t.user', 'a')
				->where('a.id = :accID')
				->setParameter('accID', $request->request->get('accID'))
				->getQuery()->getoneorNullResult(Query::HYDRATE_ARRAY);
		    $response['status']= "ok";
			$response['data'] = "";
			$response['account'] = $getuserAccountData;
			echo json_encode($response); die();	
      
		 }else{
			$response['status']= "error";
			$response['data'] = "";
			$response['error'] = "Not able to process this request!";
			echo json_encode($response); die(); 
		 }
	 }
	 /**
     * @Route("/updateorganization", name="updateorganization")
     */
	 public function updateorganization(Request $request){
		 $session 	= $request->getSession();
		if(!empty($request->request->get('userData'))){
			$sessData = $session->get('userInfo');
			if($sessData['is_navigate'] == 1 && $sessData['authorize'] == 0 ){
					$response['status']= "error";
					$response['data'] = "";
					$response['error'] = "You can't access directly to this page!";
					echo json_encode($response); die(); 
				}
		  parse_str($request->request->get('userData'), $postData);
			 if(!empty($postData)){
				 $em = $this->getDoctrine()->getManager();
				 if($postData['gender'] == 'M'){
					 $gender = 'Male';
				 }else{
					 $gender = 'Female';
				 }
				 $userUpdate = $em->createQueryBuilder();
				 $updateQuery = $userUpdate->update('AppBundle:TblUsers', 'u')
					//->set('u.email', '?3')
					->set('u.updatedOn', '?4')
					->where('u.id = ?5')
					//->setParameter(3, $postData['editemail'])
					->setParameter(4, date('Y-m-d H:i:s'))
					->setParameter(5, $postData['organization'])
					->getQuery();
                $updateResult = $updateQuery->execute();
				$accoutUpdate= $em->createQueryBuilder();
				$updateAccQuery = $accoutUpdate->update('AppBundle:TblAccount', 'a')
					->set('a.firstName', '?1')
					->set('a.lastName', '?2')
					->set('a.dob', '?3')
					->set('a.gender', '?4')
					->set('a.address', '?5')
					->set('a.city', '?6')
					->set('a.state', '?7')
					->set('a.country', '?8')
					->set('a.zipcode', '?9')
					->where('a.user = ?10')
					->setParameter(1, $postData['firstname'])
					->setParameter(2, $postData['lastname'])
					->setParameter(3, $postData['birthday'])
					->setParameter(4, $gender)
					->setParameter(5, $postData['address'])
					->setParameter(6, $postData['city'])
					->setParameter(7, $postData['state'])
					->setParameter(8, $postData['country'])
					->setParameter(9, $postData['zipcode'])
					->setParameter(10, $postData['organization'])
					->getQuery();
                $updateAccResult = $updateAccQuery->execute();
				if($updateAccResult == true || $updateResult == true){
					$response['status']= "ok";
					$response['data'] = "";
					$response['message'] = "Data has been updated!";
					echo json_encode($response); die();
				}else{
					$response['status']= "error";
					$response['data'] = "";
					$response['error'] = "Not able to process this request! Please try again later";
					echo json_encode($response); die(); 
				}
				
				 
			 }else{
				$response['status']= "error";
				$response['data'] = "";
				$response['error'] = "Not able to process this request!";
				echo json_encode($response); die(); 
			 } 
		 }else{
			$response['status']= "error";
			$response['data'] = "";
			$response['error'] = "Not able to process this request!";
			echo json_encode($response); die(); 
		 } 
	 }
	 /**
     * @Route("/deleteOrganization", name="deleteOrganization")
     */
	 public function deleteOrganization(Request $request){
		 $session 	= $request->getSession();
			$sessData = $session->get('userInfo');
			if($sessData['is_navigate'] == 1 && $sessData['authorize'] == 0 ){
					$response['status']= "error";
					$response['data'] = "";
					$response['error'] = "You can't access directly to this page!";
					echo json_encode($response); die(); 
				}
		if(!empty($request->request->get('organization'))){
		 $account = $request->request->get('organization');
		 $emu = $this->getDoctrine()->getManager();
		 $userUpdate = $emu->createQueryBuilder();
		 $updateQuery = $userUpdate->update('AppBundle:TblUsers', 'u')
					->set('u.isDeleted', '?1')					
					->where('u.id = ?2')
					->setParameter(1, '1')
					->setParameter(2, $account)
					->getQuery();
         $updateResult = $updateQuery->execute();
		 $ema = $this->getDoctrine()->getManager();
		 $accoutUpdate= $ema->createQueryBuilder();
		 $updateAccQuery = $accoutUpdate->update('AppBundle:TblAccount', 'a')
		                   ->set('a.isDeleted', '?1')
						   ->where('a.user = ?2')
					       ->setParameter(1, '1')
						   ->setParameter(2, $account)
						   ->getQuery();
        $updateAccResult = $updateAccQuery->execute();
		if($updateResult == true && $updateAccResult == true){
			$sessArray = $session->get('userInfo');
			$sessArray['totalusers']['totalUsers'] = !empty($sessArray['totalusers']['totalUsers']) ? $sessArray['totalusers']['totalUsers'] - 1 : 0;
			$sessArray['totalusers']['totalorganizations'] = !empty($sessArray['totalusers']['totalorganizations']) ? $sessArray['totalusers']['totalorganizations'] - 1 : 0;
			$userData = $session->set('userInfo', $sessArray);
			        $response['status']= "ok";
					$response['data'] = "";
					$response['message'] = "Orgnaization has been deleted successfully!";
					echo json_encode($response); die();
		}else{
			$response['status']= "error";
			$response['data'] = "";
			$response['error'] = "Not able to process this request! Please try again later";
			echo json_encode($response); die();
		} 
		 
		}else{
			$response['status']= "error";
			$response['data'] = "";
			$response['error'] = "Not able to process this request!";
			echo json_encode($response); die();
		}		  
	 }
	 /**
	* @Route("/getorgs", name="getorgs")
	*/
	 public function getorgs(Request $request){
		$session 	= $request->getSession();
		$userAccess =array('super', 'distributor', 'account');
		if(empty($session->get('userInfo'))  || !in_array($session->get('userInfo')['userRoleSlug'], $userAccess)){
			return $this->redirectToRoute('login'); 
		}else{
			$userData = $session->get('userInfo');
			$em = $this->getDoctrine()->getManager();
			$getProfile = $em->createQueryBuilder('t')
                ->select('t, a, r')
                ->from('AppBundle:TblAccount', 't')
				->leftjoin('t.user', 'a')
				->leftjoin('a.userRole', 'r')
				->where('t.userRole = :userRole')				
                ->andWhere('t.isDeleted = :isDeleted')
                ->setParameter('userRole', '4')
                ->setParameter('isDeleted', '0')				
                ->orderBy('t.id', 'DESC')				
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
			$data = array();
			 return $this->render('dashboard/addUsers/get_organization.html.twig', array(
				'title'	=> 'Organizations',
				'organizations'	=> $getProfile,
				'userData' => $userData
			));	
			} 
	 }
}
