<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use AppBundle\Entity\TblUsersRole;
use AppBundle\Entity\TblUsers;
use AppBundle\Entity\TblAccount;
use AppBundle\Entity\TblCustomers;
use AppBundle\Entity\TblCcinfo;

class CustomerController extends Controller
{
    /**
     * @Route("/add-customer", name="add_customer")
     */
    public function add_customerAction(Request $request)
    {
		$session 	= $request->getSession();
		if(empty($session->get('userInfo'))){
			return $this->redirectToRoute('login'); 
		}else{
			$userData = $session->get('userInfo');
			if($userData['is_navigate'] == 1 && $userData['authorize'] == 0 ){
					$response['status']= "error";
					$response['data'] = "";
					$response['error'] = "You can't access directly to this page!";
					echo json_encode($response); die(); 
				}
			$allEmployees = array();
			$em = $this->getDoctrine()->getManager();
			if($userData['userRoleSlug'] == 'organization'){
				$allEmployees = $em->createQueryBuilder('t')
				->select('t.id', 'c.firstName', 'c.lastName')
				->from('AppBundle:TblUsers', 't')
				->leftjoin('AppBundle:TblAccount', 'c', 'WITH', 't.id = c.user')
				->where('t.createdBy = :createdby')
				->setParameter("createdby", $userData['userID'])
				->orderBy('c.firstName', 'ASC')
				->getQuery()
				->getResult(Query::HYDRATE_ARRAY);
				$em->flush();
			}else if($userData['userRoleSlug'] == 'employee'){
							
			}
			
			$getCurrencies =  $em->createQueryBuilder('t')
                ->select('t')
                ->from('AppBundle:TblCurrencies', 't')		
                ->orderBy('t.country', 'ASC')				
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
                $em->flush();
				
			$data = $getUserData =array();
			 return $this->render('dashboard/addUsers/add_customer.html.twig', array(
				'title'			=>'Add Customer',
				'data'	=> $data,
				'userDatauplevel' => $getUserData,
				'userData' => $userData,
				'employees' => $allEmployees,
				'currencies' => $getCurrencies
			));
		} 
    }
	/**
     * @Route("/edit-customer/{customer}", name="edit_customer")
     */
    public function edit_customerAction(Request $request, $customer)
    {
		$session 	= $request->getSession();
		if(empty($session->get('userInfo'))){
			return $this->redirectToRoute('login'); 
		}else{
			$userData = $session->get('userInfo');
			$customerID = base64_decode(base64_decode(base64_decode($customer)));
			$allEmployees = array();
			$em = $this->getDoctrine()->getManager();
			if($userData['userRoleSlug'] == 'organization'){
				$allEmployees = $em->createQueryBuilder('t')
				->select('t.id', 'c.firstName', 'c.lastName')
				->from('AppBundle:TblUsers', 't')
				->leftjoin('AppBundle:TblAccount', 'c', 'WITH', 't.id = c.user')
				->where('t.createdBy = :createdby')
				->setParameter("createdby", $userData['userID'])
				->orderBy('c.firstName', 'ASC')
				->getQuery()
				->getResult(Query::HYDRATE_ARRAY);
				$em->flush();
			}else if($userData['userRoleSlug'] == 'employee'){
							
			}
			if(!empty($customerID)){
					$getCustomer =  $em->createQueryBuilder('t')
						->select('t', 'a')
						->from('AppBundle:TblCustomers', 't')
						->leftjoin('t.emp', 'a')
						->where('t.id = :id')
						->setParameter('id', $customerID)				
						->getQuery()
						->getoneorNullResult(Query::HYDRATE_ARRAY);
						$em->flush();
						
					$getCcvalues =  $em->createQueryBuilder('t')
						->select('t')
						->from('AppBundle:TblCcinfo', 't')
						->where('t.customer = :customerID')
						->setParameter('customerID', $customerID)	
						->getQuery()
						->getoneorNullResult(Query::HYDRATE_ARRAY);
						$em->flush();
					if(!empty($getCcvalues)){
						$getCcvalues['ccNumber'] = base64_decode(base64_decode($getCcvalues['ccNumber']));
					}	
						
						$value[ 'ccard' ] = $getCcvalues;	
					$data = $getUserData =array();
					if(!empty($getCustomer)){
							$getCurrencies =  $em->createQueryBuilder('t')
							->select('t')
							->from('AppBundle:TblCurrencies', 't')		
							->orderBy('t.country', 'ASC')				
							->getQuery()
							->getResult(Query::HYDRATE_ARRAY);
							$em->flush();
							 return $this->render('dashboard/addUsers/edit_customer.html.twig', array(
							'title'			=>'Edit Customer',
							'data'	=> $data,
							'userData' => $userData,
							'userDatauplevel' => $getUserData,
							'customer' => $customer,
							'customerDATA' => $getCustomer,
							'currencies' => $getCurrencies,
							'cardDetail' => $getCcvalues,
							'employees' => $allEmployees,
						 ));
					}else{
						$response['status']= "error";
						$response['data'] = "";
						$response['error'] = "Not able to process this request!";
						echo json_encode($response); die(); 
					}
					
			}else{
				$response['status']= "error";
				$response['data'] = "";
				$response['error'] = "Not able to process this request!";
				echo json_encode($response); die(); 
			}
		} 
    }
	/**
     * @Route("/insertcusomer", name="insertcusomer")
     */
    public function insertcusomerAction(Request $request)
    {
		$docmentPath = $this->getParameter('customerDoc');
     	$session 	= $request->getSession();
		$userSessData = $session->get("userInfo");
		$response = array();
		if(empty($session->get('userInfo'))){
			return $this->redirectToRoute('login'); 
		}else{
			$sessData = $session->get('userInfo');
			$postData = array();

            $postData = $request->request->all();  
			$em = $this->getDoctrine()->getManager();
			  if(!empty($postData)){
				    /*get employee Id*/
					$employeeID = ($userSessData['userRoleSlug'] == 'organization') ? $postData['employee']: $userSessData['userID'];
					/*get organization */
					if($userSessData['userRoleSlug'] == 'organization'){
					$orgID = $userSessData['userID'];
					}else{
						$getOrganization = $em->createQueryBuilder('t')
						->select('t.createdBy')
						->from('AppBundle:TblUsers', 't')
						->where('t.id = :userID')				
						->andWhere('t.isDeleted != 1')
						->setParameter('userID', $employeeID)			
						->getQuery()
						->getOneOrNullResult(Query::HYDRATE_ARRAY);
						$em->flush();
					$orgID = $getOrganization['createdBy'];
					}
					
				    $customerinfo= new TblCustomers();
					$customerinfo->setEmp($em->getRepository('AppBundle:TblUsers')->find($employeeID));
					$customerinfo->setOrg($em->getRepository('AppBundle:TblUsers')->find($orgID));
					$customerinfo->setFirstName($postData['fname']);
					$customerinfo->setLastName($postData['lname']);
					$customerinfo->setEmail($postData['email']);
					$customerinfo->setPhone(str_replace('+', '', $postData['phone']));
					$customerinfo->setCurrency($postData['currency']);
					$customerinfo->setFax(str_replace('+', '', $postData['fax']));
					$customerinfo->setMobile(str_replace('+', '', $postData['mobile']));
					$customerinfo->setAddress($postData['address']);
					$customerinfo->setCity($postData['city']);
					$customerinfo->setState($postData['state']);
					$customerinfo->setCountry($postData['country']);
					$customerinfo->setZipcode($postData['zipcode']);
					$customerinfo->setSAddress($postData['sAddress']);
					$customerinfo->setSCity($postData['sCity']);
					$customerinfo->setSState($postData['sState']);
					$customerinfo->setSCountry($postData['sCountry']);
					$customerinfo->setSZipcode($postData['sZipcode']);
					$customerinfo->setUserIp($request->server->get("REMOTE_ADDR"));
					$customerinfo->setCreatedBy($userSessData['userID']);
					$customerinfo->setCreatedAt(new \DateTime("now"));
					$em->persist($customerinfo);
					$em->flush();
					$customerID = $customerinfo->getId();

                    /*upload documnets of user */  
					$uploadeing_img = $request->files->get('files');
					$old = umask(0);
					mkdir($docmentPath."/doc_".$customerID, 0777);
					umask($old);
					$imgArrName = array();
					foreach($uploadeing_img as $img){
						if(!empty($img)){
							$fileName = $img->getClientOriginalName();
							$fileName = str_replace("|", "", $fileName);
							$img->move($docmentPath."/doc_".$customerID, $fileName);
							$imgArrName[] = $fileName;
						}
					} 
				    
					/*update docs colums*/
					if(!empty($imgArrName)){
						$docsString = implode("|", $imgArrName);
						$addDocs = $em->createQueryBuilder();
						$updateQuery = $addDocs->update('AppBundle:TblCustomers', 'u')
						->set('u.docFiles', '?1')
						->where('u.id = ?2')
						->setParameter(1, $docsString)
						->setParameter(2, $customerID)
						->getQuery();
					   $updateResult = $updateQuery->execute();
					}
					/*card detail*/
					if($postData['ccnumber']){
						$cinfo= new TblCcinfo();
						$cinfo->setCcNumber(base64_encode(base64_encode($postData['ccnumber'])));
						$cinfo->setCcName($postData['nameOnCard']);
						$cinfo->setCcExpdate($postData['expMon']."/".$postData['expYear']);
						$cinfo->setCustomer($em->getRepository('AppBundle:TblCustomers')->find($customerID));
						$em->persist($cinfo);
						$em->flush();
					}
					if(!empty($customerID)){
					/*	$sentTo = $postData['email'];
						$message = \Swift_Message::newInstance()
							->setSubject('Welcome at Merchant Services')
							->setFrom('ashok.mobilyte@gmail.com')
							->setTo($sentTo)
							->setCharset('iso-8859-1')
							->setContentType('text/html')
							->setBody(
								$this->renderView(
									'emails/adduser.html.twig',
									array('username' => $postData['username'], 'password' => $postData['password'], 'role' => "Distributor")
								)
							);
							$this->get('mailer')->send($message);
					*/
					    $response['status']= "ok";
						$response['data'] = "";
						$response['message'] = "Customer has been created successfully!";
						echo json_encode($response); die(); 
					}else{
						$response['status']= "error";
						$response['data'] = "";
						$response['error'] = "Something was wrong!";
						echo json_encode($response); die(); 
					}
			 }else{
				$response['status']= "error";
				$response['data'] = "";
				$response['error'] = "Not able to process this request!";
				echo json_encode($response); die(); 
			 }
		}
		 
    }
    /**
     * @Route("/customers", name="customers")
     */	
	 public function customers(Request $request){
		$session 	= $request->getSession();
		if(empty($session->get('userInfo'))){
			return $this->redirectToRoute('login'); 
		}else{
			$userData = $session->get('userInfo');
			$em = $this->getDoctrine()->getManager();
			if($userData['userRoleSlug'] == 'organization'){
				 $getCustomers =  $em->createQueryBuilder('t')
                ->select('t.firstName','t.lastName','t.email','t.phone','t.currency','t.fax','t.mobile','t.address','t.sAddress','t.docFiles','t.id','c.firstName as a_firstName', 'c.lastName as a_lastName')
                ->from('AppBundle:TblCustomers', 't')
				->leftjoin('AppBundle:TblAccount', 'c', 'WITH', 't.emp = c.user')
                ->where('t.org = :userRole')
				->andWhere('t.isDeleted = :deleteid')
                ->setParameter('userRole', $userData['userID'])	
				->setParameter('deleteid', '0')					
                ->orderBy('t.id', 'DESC')				
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
                $em->flush();
			}else if($userData['userRoleSlug'] == 'employee'){
				 $getCustomers =  $em->createQueryBuilder('t')
                ->select('t')
                ->from('AppBundle:TblCustomers', 't')
                ->where('t.emp = :userRole')
				->andWhere('t.isDeleted = :deleteid')
                ->setParameter('userRole', $userData['userID'])	
				->setParameter('deleteid', '0')					
                ->orderBy('t.id', 'DESC')				
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
                $em->flush();				
			}
			
			$getCurrencies =  $em->createQueryBuilder('t')
                ->select('t')
                ->from('AppBundle:TblCurrencies', 't')		
                ->orderBy('t.country', 'ASC')				
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
                $em->flush();
			/*get all customer for listing*/
            	
             
			$data = $getUserData =array();
			foreach($getCustomers as $key => $value){
				$value[ 'encid' ] = base64_encode(base64_encode(base64_encode($value['id'])));
				$getCustomers[$key] = $value;
			}
			 return $this->render('dashboard/addUsers/list_customer.html.twig', array(
				'title'			=>'Customers',
				'data'	=> $data,
				'userDatauplevel' => $getUserData,
				'userData' => $userData,
				'customers' => $getCustomers,
				'currencies' => $getCurrencies
			));
		} 
	 }

	 /**
     * @Route("/updatecustomer", name="updatecustomer")
     */
	 public function updatecustomer(Request $request){
		 $docmentPath = $this->getParameter('customerDoc');
		    $postData = array();
            $postData = $request->request->all();
			if(!empty($postData)){
				/*upload documnets of user */  
				$session 	= $request->getSession();
	        	$userSessData = $session->get("userInfo");
				$customerID = base64_decode(base64_decode(base64_decode($postData['customer'])));
				$employeeID = ($userSessData['userRoleSlug'] == 'organization') ? $postData['employee']: $userSessData['userID'];
				$uploadeing_img = $request->files->get('files');
				$allDocsFiles = array();
				$imgArrName = array();
				if(!empty($uploadeing_img)){
						if(!file_exists($docmentPath."/doc_".$customerID)){
						$old = umask(0);
						mkdir($docmentPath."/doc_".$customerID, 0777);
						umask($old);
					}
					foreach($uploadeing_img as $img){
						if(!empty($img)){
							$fileName = $img->getClientOriginalName();
							$fileName = str_replace("|", "", $fileName);
							$img->move($docmentPath."/doc_".$customerID, $fileName);
							$imgArrName[] = $fileName;
						}
					} 
				}
				if(!empty($postData['olddocs'])){
					$allDocsFiles = array_unique(array_merge($imgArrName, $postData['olddocs']));
				}else{
					$allDocsFiles = $imgArrName;
				}
				$em = $this->getDoctrine()->getManager();
				$accoutUpdate= $em->createQueryBuilder();
				$updateCustQuery = $accoutUpdate->update('AppBundle:TblCustomers', 'a')
				    ->set('a.emp', '?21')
					->set('a.firstName', '?1')
					->set('a.lastName', '?2')
					//->set('a.email', '?3')
					->set('a.phone', '?4')
					->set('a.currency', '?5')
					->set('a.fax', '?6')
					->set('a.mobile', '?7')
					->set('a.address', '?8')
					->set('a.city', '?9')
					->set('a.state', '?10')
					->set('a.country', '?11')
					->set('a.zipcode', '?12')
					->set('a.sAddress', '?13')
					->set('a.sCity', '?14')
					->set('a.sState', '?15')
					->set('a.sCountry', '?16')
					->set('a.sZipcode', '?17')
					->set('a.updatedAt', '?18')
					->set('a.docFiles', '?19')
					->where('a.id = ?20')
					->setParameter(21, $em->getRepository('AppBundle:TblUsers')->find($employeeID))
					->setParameter(1, $postData['fname'])
					->setParameter(2, $postData['lname'])
					//->setParameter(3, $postData['email'])
					->setParameter(4, str_replace('+', '', $postData['phone']))
					->setParameter(5, $postData['currency'])
					->setParameter(6, str_replace('+', '', $postData['fax']))
					->setParameter(7, str_replace('+', '', $postData['mobile']))
					->setParameter(8, $postData['address'])
					->setParameter(9, $postData['city'])
					->setParameter(10, $postData['state'])
					->setParameter(11, $postData['country'])
					->setParameter(12, $postData['zipcode'])
					->setParameter(13, $postData['sAddress'])
					->setParameter(14, $postData['sCity'])
					->setParameter(15, $postData['sState'])
					->setParameter(16, $postData['sCountry'])
					->setParameter(17, $postData['sZipcode'])
					->setParameter(18, new \DateTime("now"))
					->setParameter(19, implode("|", $allDocsFiles))
					->setParameter(20, $customerID)
					->getQuery();
                $updateCustResult = $updateCustQuery->execute();
				if(!empty($postData['ccnumber'])){
				 $userUpdate = $em->createQueryBuilder();
				 $updateQuery = $userUpdate->update('AppBundle:TblCcinfo', 'u')
					->set('u.ccNumber', '?1')
					->set('u.ccName', '?2')
					->set('u.ccExpdate', '?3')					
					->where('u.customer = ?4')
					->setParameter(1, base64_encode(base64_encode($postData['ccnumber'])))
					->setParameter(2, $postData['nameOnCard'])
					->setParameter(3, $postData['expMon'].'/'.$postData['expYear'])
					->setParameter(4, $customerID)
					->getQuery();
                $updateResult = $updateQuery->execute();
				}
				if($updateCustResult == true){
					$response['status']= "ok";
					$response['data'] = "";
					$response['message'] = "Customer Profile has been updated!";
					echo json_encode($response); die();
				}else{
					$response['status']= "error";
					$response['data'] = "";
					$response['error'] = "Not able to process this request! Please try again later";
					echo json_encode($response); die(); 
				}
				
				 
			 }else{
				$response['status']= "error";
				$response['data'] = "";
				$response['error'] = "Not able to process this request!";
				echo json_encode($response); die(); 
			 } 
		 
	 }
	 /**
     * @Route("/deletecustomer", name="deletecustomer")
     */
	 public function deletecustomer(Request $request){
		if(!empty($request->request->get('customer'))){
		 $session 	= $request->getSession();
		 $customer = base64_decode(base64_decode(base64_decode($request->request->get('customer'))));
		 $emu = $this->getDoctrine()->getManager();
		 $userUpdate = $emu->createQueryBuilder();
		 $updateQuery = $userUpdate->update('AppBundle:TblCustomers', 'u')
					->set('u.isDeleted', '?1')					
					->where('u.id = ?2')
					->setParameter(1, '1')
					->setParameter(2, $customer)
					->getQuery();
         $updateResult = $updateQuery->execute();		 
		if($updateResult == true){
			        $response['status']= "ok";
					$response['data'] = "";
					$response['message'] = "Customer has been deleted successfully!";
					echo json_encode($response); die();
		}else{
			$response['status']= "error";
			$response['data'] = "";
			$response['error'] = "Not able to process this request! Please try again later";
			echo json_encode($response); die();
		} 
		 
		}else{
			$response['status']= "error";
			$response['data'] = "";
			$response['error'] = "Not able to process this request!";
			echo json_encode($response); die();
		}		  
	 }
	 
	 /**
	 * @Route("/customeraddr", name="customeraddr")
	 */
	 public function customeraddr(Request $request){
		if($request->request->get('customer')){
			$customer = base64_decode(base64_decode(base64_decode($request->request->get('customer'))));
			$em= $this->getDoctrine()->getManager();
			$getCustomer = $em->createQueryBuilder()
			              ->select('t.id as cusomer, t.firstName, t.lastName, t.address , t.sAddress, t.currency')
						  ->from('AppBundle:TblCustomers', 't')
                          ->where('t.id= :id')
	                      ->setParameter("id", $customer)
                          ->getQuery()
                          ->getOneOrNullResult(); 
                       $em->flush();
        if(!empty($getCustomer)){
			        $response['status']= "ok";
					$response['data'] = $getCustomer;
					$response['message'] = "Customer has been deleted successfully!";
					echo json_encode($response); die();
		}else{
			$response['status']= "error";
			$response['data'] = "";
			$response['error'] = "Not able to process this request!";
			echo json_encode($response); die();
		}					   
          				  
		}else{
			$response['status']= "error";
			$response['data'] = "";
			$response['error'] = "Not able to process this request!";
			echo json_encode($response); die();
		} 
	 }
	 
	 /**
     * @Route("/removedoc", name="removedoc")
     */
	 public function removedoc(Request $request){
		    echo json_encode(array('isSuccess'=> true, 'files' => array('test.png')));
			die;
	 }	
	 /**
	* @Route("/getcusts", name="getcusts")
	*/
	 public function getcusts(Request $request){
		$session 	= $request->getSession();
		if(empty($session->get('userInfo'))){
			return $this->redirectToRoute('login'); 
		}else{
			$userData = $session->get('userInfo');
			$em = $this->getDoctrine()->getManager();
			$getCustomers =  $em->createQueryBuilder('t')
                ->select('t.firstName','t.lastName','t.email','t.phone','t.currency','t.fax','t.mobile','t.address','t.sAddress','t.docFiles','t.id','c.firstName as a_firstName', 'c.lastName as a_lastName', 'd.firstName as orgFirstName', 'd.lastName as orgLastName' )
                ->from('AppBundle:TblCustomers', 't')
				->leftjoin('AppBundle:TblAccount', 'c', 'WITH', 't.emp = c.user')
				->leftjoin('AppBundle:TblAccount', 'd', 'WITH', 't.org = d.user')
				->andWhere('t.isDeleted = :deleteid')
				->setParameter('deleteid', '0')					
                ->orderBy('t.id', 'DESC')				
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
                $em->flush();
			$data = array();
			 return $this->render('dashboard/addUsers/get_customer.html.twig', array(
				'title'	=> 'Customers',
				'customers'	=> $getCustomers,
				'userData' => $userData
			));	
		} 
	 }	
}
