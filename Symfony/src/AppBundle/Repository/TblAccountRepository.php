<?php 
namespace AppBundle\Repository;

use App\Entity\TblAccount;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class TblAccountRepository extends EntityRepository
{
    public function findUser($userId)
 {

    $query = $this->getEntityManager()
        ->createQuery(
        'SELECT u, a FROM AppBundle:TblUsers u
        JOIN u.TblAccount a
        WHERE u.id = :id'
    )->setParameter('id', $userId);

    try {
        return $query->getSingleResult();
    } catch (\Doctrine\ORM\NoResultException $e) {
        return null;
    }
 }
}
