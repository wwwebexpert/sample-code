<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class TblUsersRepository extends EntityRepository
{
    public function findByid($userid)
    {
        $query = $this->getEntityManager()->createQuery(
            'SELECT p FROM AppBundle:TblUsers p 
				WHERE p.id = :id'
        )->setParameter('id', $userid);
        return $query->getOneOrNullResult(Querry::HYDRATE_ARRAY);
    }
	
}
