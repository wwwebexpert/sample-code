<aside id="sticky-social">
		<button data-toggle="modal" data-target="#exampleModal" style="text-transform: uppercase;">Free &nbsp; Quote </button>
	</aside>
	<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       
        <button type="button" class="closebtn close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="col-md-12 contactform1">
	<h2><?php the_field('form_title','option')?></h2>
	<img class="text-formed" src="<?php the_field('image_under_title','option')?>" alt="text-bootom">
	<?php the_field('form','option')?>
	</div>
      </div>
     
    </div>
  </div>
</div>
	<footer>
<div class="container">
    <div class="row">
        <div class="col-md-5">
            <div id="footer-left"> 
                <ul>
                <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer 1') ) : ?>
                    <li> <?php endif; ?>
                </ul>
            </div> 
            <h2><?php the_field('left_widget_header','option');?></h2>
            <ul>
                <?php if( have_rows('left_widget_social_icon','option') ): ?>
                    <?php while( have_rows('left_widget_social_icon','option') ): the_row(); ?> 
                        <li>
                            <a href="<?php the_sub_field('social_links','option');?>">
                            <?php the_sub_field('image','option');?>                 
                            </a>
                        </li>
                    <?php endwhile;
                endif;?>
            </ul>
        </div>
        <div class="col-md-7">
            <div id="footer-right" class="contdtlst"> 
                <?php if ( !function_exists('dynamic_sidebar') ||!dynamic_sidebar('footer 2') ) : ?> 
                   <?php endif; ?>
                
            </div>
        </div>
    </div>
</div>
<p><?php the_field('copyright','option');?></p>
</footer>
<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left">
    <span class="glyphicon glyphicon-chevron-up"></span>
</a> 
    <!-- JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>



<script>
    jQuery(".tnp-email").attr("placeholder","Join for lifetime access");
</script>

<script type="text/javascript">
        // Setup form validation on the #register-form element
        //jQuery(".wpcf7-form").validate({
            // Specify the validation rules
           /* rules: {
                your_name: 
                {
                    required: true
                },            
           		your_massage: 
                {
                    required: true
                },
                your_email: {
                    required: true,
                    email: true
                }
            }
        });
    jQuery(document).ready(function(){
      jQuery("#submit").click(function(event){
        event.preventDefault();
        if(jQuery(".wpcf7-form").valid()){
            jQuery(".wpcf7-form").submit();
        }
      });
    });*/
    </script>
<?php wp_footer(); ?>
</body>  
</html>