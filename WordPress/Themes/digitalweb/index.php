<?php
get_header();

?>
  <!----slider---->
<section class="sliderpart">
<div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="first-slide" src="<?php echo get_template_directory_uri();?>/assets/images/banner1.png" alt="First slide">
            <div class="container">
              <div class="carousel-text carousel-caption ">
                <h1>Traffic</h1>
                <h2>Increase your websit<br>
e traffic with inflow of relevant users.</h2>
<h3>Our potential customers.</h3>
                <p><a class="btn btn-lg buttonmore" href="#" role="button">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a></p>
              </div>
            </div>
          </div>
          <div class="carousel-item">
             <img class="first-slide" src="<?php echo get_template_directory_uri();?>/assets/images/banner1.png" alt="second slide">
            <div class="container">
              <div class="carousel-text carousel-caption ">
                <h1>Traffic</h1>
                <h2>Increase your websit<br>
e traffic with inflow of relevant users.</h2>
<h3>Our potential customers.</h3>
                <p><a class="btn btn-lg buttonmore" href="#" role="button">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a></p>
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <img class="first-slide" src="<?php echo get_template_directory_uri();?>/assets/images/banner1.png" alt="Third slide">
            <div class="container">
             <div class="carousel-text carousel-caption ">
                <h1>Traffic</h1>
                <h2>Increase your websit<br>
e traffic with inflow of relevant users.</h2>
<h3>Our potential customers.</h3>
                <p><a class="btn btn-lg buttonmore" href="#" role="button">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a></p>
              </div>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
</section>
<section class="ourservice">
<div class="container">
<div class="row ">
<div class="col-md-12">
<h2>SERVICES WE PROVIDE</h2>
<img class="text-bootom" src="<?php echo get_template_directory_uri();?>/assets/images/text-bootom.png" alt="text-bootom">
 </div>
<div class="col-sm-6 col-md-6 col-lg-4 ">
<div class="row">
<div class="col-sm-4">
<img class="" src="<?php echo get_template_directory_uri();?>/assets/images/service1.png" alt="service1">
 </div>
 <div class="col-sm-8">
 <h3>Affiliate Services</h3>
<p>We provide various marketing models such as CPI, CPL, CPS, CPM, CPC, E-mail Marketing, Social 
Media etc.</p>
 </div>
  </div>
 </div>
<div class="col-sm-6 col-md-6 col-lg-4 ">
<div class="row">
<div class="col-sm-4">
<img class="" src="<?php echo get_template_directory_uri();?>/assets/images/service2.png" alt="service2">
 </div>
 <div class="col-sm-8">
 <h3>Performance Marketing</h3>
<p>Performance is the key, pay only when your ads perform and gets you results.</p>
 </div>
  </div>
 </div>
 
<div class="col-sm-6 col-md-6 col-lg-4 ">
<div class="row">
<div class="col-sm-4">
<img class="" src="<?php echo get_template_directory_uri();?>/assets/images/service3.png" alt="service3">
 </div>
 <div class="col-sm-8">
 <h3>Brand Building</h3>
<p>We provide specialized services for brand building for new start - ups.</p>
 </div>
  </div>
 </div>
 
<div class="col-sm-6 col-md-6 col-lg-4 ">
<div class="row">
<div class="col-sm-4">
<img class="" src="<?php echo get_template_directory_uri();?>/assets/images/service4.png" alt="service4">
 </div>
 <div class="col-sm-8">
 <h3>Wide Reach</h3>
<p>With one of the largest network of affiliates, we provide a wide reach to appropriate set of customers.</p>
 </div>
  </div>
 </div>
<div class="col-sm-6 col-md-6 col-lg-4 ">
<div class="row">
<div class="col-sm-4">
<img class="" src="<?php echo get_template_directory_uri();?>/assets/images/service5.png" alt="service5">
 </div>
 <div class="col-sm-8">
 <h3>Robust Platform</h3>
<p>We provide a robust system for all advertisers and publishers to easily manage all campaigns and performance.</p>
 </div>
  </div>
 </div>
 
<div class="col-sm-6 col-md-6 col-lg-4 ">
<div class="row">
<div class="col-sm-4">
<img class="" src="<?php echo get_template_directory_uri();?>/assets/images/service6.png" alt="service6">
 </div>
 <div class="col-sm-8">
 <h3>Real Time Tracking </h3>
<p>Our cutting edge technology is best for keeping real time tracking and report generation.</p>
 </div>
  </div>
 </div>
 </div>
 </div>
</section>

<section class="listprod">
<div class="container">
<div class="row ">
<div class="col-lg-6">
<div class="row ">
<div class="col-sm-6">
<div class="listprod-text">
<img class="" src="<?php echo get_template_directory_uri();?>/assets/images/techsup.png" alt="techsup">
 <h2>Technical Support</h2>
</div>
 </div>
 
<div class="col-sm-6">
 <div class="listprod-text">
 <img class="" src="<?php echo get_template_directory_uri();?>/assets/images/24x7.png" alt="24x7">
 <h2>24/7 Support </h2>
 </div>
  </div>
 <div class="col-sm-6">
<div class="listprod-text">
<img class="" src="<?php echo get_template_directory_uri();?>/assets/images/staff.png" alt="staff">
 <h2>Staffing Service </h2>
</div>
 </div>
<div class="col-sm-6">	
 <div class="listprod-text">
 <img class="" src="<?php echo get_template_directory_uri();?>/assets/images/dollar.png" alt="dollar">
 <h2>Risk Assesment </h2>
 </div>
  </div>
 </div>
 </div>
<div class="col-lg-6">
  <img class="imgsive" src="<?php echo get_template_directory_uri();?>/assets/images/callset.png" alt="callset">
 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit. </p>
 </div>
 </div>
  </div>
</section>

<section class="ourclient">
<div class="container">
<div class="row ">
<div class="col-sm-12">
<h2>Here's what our clients say</h2>
<img class="text-bootom" src="<?php echo get_template_directory_uri();?>/assets/images/text-bootom.png" alt="text-bootom">
 </div>
  </div>
  <div id="myCarousel1" class="carousel slide" data-ride="carousel">
       
        <div class="carousel-inner">
          <div class="carousel-item active">
			  <div class="row clientwrap">
              <div class="ourclient-text col-md-6">
                <img src="<?php echo get_template_directory_uri();?>/assets/images/client1.png" alt="client1">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim 
ad minim veniam, quis nostrud exercitation ullamco 
laboris nisi ut aliquip .</p>
<p><strong>Luis Henery</strong></p>

              </div>
			  <div class="ourclient-text col-md-6">
                <img src="<?php echo get_template_directory_uri();?>/assets/images/client2.png" alt="client1">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim 
ad minim veniam, quis nostrud exercitation ullamco 
laboris nisi ut aliquip .</p>
<p><strong>Luis Henery</strong></p>
 </div>
              </div>
            </div>
          
        <div class="carousel-item ">
        
          
			  <div class="row clientwrap">
              <div class="ourclient-text col-md-6">
                <img src="<?php echo get_template_directory_uri();?>/assets/images/client2.png" alt="client1">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim 
ad minim veniam, quis nostrud exercitation ullamco 
laboris nisi ut aliquip .</p>
<p><strong>Luis Henery</strong></p>

              </div>
			  <div class="ourclient-text col-md-6">
                <img src="<?php echo get_template_directory_uri();?>/assets/images/client1.png" alt="client1">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim 
ad minim veniam, quis nostrud exercitation ullamco 
laboris nisi ut aliquip .</p>
<p><strong>Luis Henery</strong></p>
 </div>
              </div>
            </div>
          </div>
       
        <a class="carousel-control-prev" href="#myCarousel1" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel1" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
 </div>
 
</section>
<?php
get_footer();
?>