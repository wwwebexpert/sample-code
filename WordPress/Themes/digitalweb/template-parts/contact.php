<?php /*Template Name: Contact Page Template */?>
<?php get_header();?>
		<div class="banner" style="background:url('<?php echo the_field('banner_image');?>');">
			<h1><?php the_field('page_title');?></h1>
		</div>
	
	<section class="newcontactwrap">
	<div class="container">
	<div class="row">
	<div class="col-md-12">
	<h2><?php the_field('title');?></h2>
	<p><?php the_field('sub_content');?></p>
	</div>
	</div>
	

	<div class="row justify-content-center">
		<?php if( have_rows('contact_us') ):?>
			<?php while( have_rows('contact_us') ): the_row(); ?>
				<div class="col-md-6 contact-top1">	
					<img src="<?php the_sub_field('favicon_icon');?>">
					<p><?php the_sub_field('main_title');?></p>
					<label ><?php the_sub_field('contact_');?></label>
					<span><?php the_sub_field('extra_detail');?></span>
				</div>
			<?php endwhile;?>
		<?php endif;?>		
	</div>		
	<div class="row contactbottom1 justify-content-center">
		<div class="col-md-7">
			<h2><?php the_field('form_title');?></h2>
			<img class="grdborder" src="<?php the_field('icon');?>" alt="grdborder">
			<?php the_field('contact_us_form');?>		
		</div>
	</div>
	
	</div>
	
	</section>
				
		
		
			<div class="clearfix"></div>
		

<?php get_footer();?>