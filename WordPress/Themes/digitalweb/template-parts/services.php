<?php
	/*Template Name: Service Page Template*/

	//Header include  
	get_header();
?>
<div class="banner" style="background:url('<?php echo the_field('banner_image');?>');">
	<h1><?php the_field('main_title');?></h1>
</div>
<!--::::::::::::::::SERVICES ::::::::::::::::::::::-->	
<section class="ourservicess">
	<div class="container">
		<?php if( have_rows('service_content') ): $i=0;?>
			<?php while( have_rows('service_content') ): the_row(); ?>				
				<?php if($i%2==0){ ?>
					<div class="row servicess-inner">
						<div class="col-md-5">
							<img src="<?php the_sub_field('image');?>" alt="servicess1">
						</div>
						<div class="col-md-7">
							<h2><?php the_sub_field('title');?></h2>
							<p><?php the_sub_field('content');?></p>
						</div>
					</div>
				<?php }	else if($i%2!==0){ ?>
					<div class="row servicess-inner fcolumn-reverse">
				    	<div class="col-md-7">
							<h2><?php the_sub_field('title');?></h2>
							<p><?php the_sub_field('content');?></p>
						</div>
						<div class="col-md-5">
							<img src="<?php the_sub_field('image');?>" alt="servicess1">
						</div>
					</div>
				<?php }  
				$i++;?>
			<?php endwhile; ?>
		<?php endif;?>	
	</div>	
</section>
<!--::::::::::::::::CLIENTS DETAILS ::::::::::::::::::::::-->
<div class="clearfix"></div>		
<section class="client_imgs_background">
	<div class="content client text-center">
		<h1>OUR HAPPY CLIENTS</h1>
		<img class="text-bootom" src="http://digitalweb.mobilytedev.com/wp-content/uploads/2018/04/text-bootom.png" alt="text-bootom">
			<div class="container">
				<div class="row">
					<?php    // WP_Query arguments
					$args = array(
					'post_type'              => array( 'clients' ),
					'post_status'            => array( 'publish' ),
					'nopaging'               => false,
					'posts_per_page'         => '20',
					);

					// The Query
					$query = new WP_Query( $args );

					// The Loop
					if ( $query->have_posts() ) {
					while ( $query->have_posts() ) {
					$query->the_post();
					$thumbnail=get_post_thumbnail_ID($id);                                  
					$image=wp_get_attachment_image_url($thumbnail,'medium');
					$url=get_post_permalink($id) ;
					$author=get_the_author();
					?>	
								
						<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
							<div class="box">
								<img src="<?php echo $image;?>" alt="#">
							</div>
						</div>
					
					<?php }?>
					<?php } ?>
				</div>
			</div>
	</div>
</section>
<?php get_footer(); ?> 