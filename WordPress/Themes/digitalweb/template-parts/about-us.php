<?php
	/*
	Template Name: About us Page Template
	*/

	//Header include  
	get_header();
?>
<div class="banner" style="background:url('<?php echo the_field('banner_image');?>');">
	<h1><?php the_field('main_title');?></h1>
</div>
<!--:::::::::::::::: ABOUT COMPENY DETAILS ::::::::::::::::::::::-->
<section class="outhistory">
	<div class="container">
		<div class="row">			
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="bulb_img">
					<img src="<?php the_field('main_image');?>" alt="#"/>
				</div>
			</div>			
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="content">
					<h1><?php the_field('header');?></h1>
					<p><?php the_field('content');?>.</p>
				</div>				
				<div class="tick_lines">
					<?php if( have_rows('sub_content') ):?>
	    				<?php while( have_rows('sub_content') ): the_row(); ?>
							<div class="tick_heading">
								<?php the_sub_field('icon');?>
							</div>
							<div class="content tick_content">
								<p><?php the_sub_field('sub_points');?></p>
							</div>
						<?php endwhile;?>
					<?php endif;?>
				</div>				
			</div>
		</div>
	</div>
</section>
<div class="clearfix"></div>
<!--::::::::::::::::TEAM DETAILS ::::::::::::::::::::::-->
<section class="team_images">
	<div class="container">	
		<div class="row">
			<div class="col-sm-12  ">
				<h2><?php the_field('main_heading');?></h2>
				<img class="text-bootom" src="<?php the_field('favicon_icon');?>" alt="text-bootom">
			</div>
			<?php if( have_rows('team_details') ):?>
				<?php while( have_rows('team_details') ): the_row(); ?>
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
						<div class="team_img">
							<img src="<?php the_sub_field('member_picture');?>" alt="#">
							<div class="content team">
								<h4><?php the_sub_field('team_name');?></h4>
								<h6><?php the_sub_field('designation');?></h6>
							</div>
						</div>
					</div>
				<?php endwhile;?>
			<?php endif;?>	
		</div>
	</div>	
</section>	
<div class="clearfix"></div>
<!--:::::::::::::::: CLIENTS DETAILS ::::::::::::::::::::::-->
<!--::::::::::::::::CLIENTS DETAILS ::::::::::::::::::::::-->
<div class="clearfix"></div>		
<section class="client_imgs_background">
	<div class="content client text-center">
		<h1>OUR HAPPY CLIENTS</h1>
		<img class="text-bootom" src="http://digitalweb.mobilytedev.com/wp-content/uploads/2018/04/text-bootom.png" alt="text-bootom">
			<div class="container">
				<div class="row">
					<?php    // WP_Query arguments
					$args = array(
					'post_type'              => array( 'clients' ),
					'post_status'            => array( 'publish' ),
					'nopaging'               => false,
					'posts_per_page'         => '20',
					);

					// The Query
					$query = new WP_Query( $args );

					// The Loop
					if ( $query->have_posts() ) {
					while ( $query->have_posts() ) {
					$query->the_post();?>

					<!-- <div class="col-md-4 col-sm-6">
					<div class="box-image-text blog">
					<div class="top"> -->
					<?php    

					$thumbnail=get_post_thumbnail_ID($id);                                  
					$image=wp_get_attachment_image_url($thumbnail,'medium');
					$url=get_post_permalink($id) ;
					$author=get_the_author();


					?>	
				
					<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
						<div class="box">
							<img src="<?php echo $image;?>" alt="#">
						</div>
					</div>
					<?php }?>
					<?php } ?>
				</div>
			</div>
	</div>
</section>
<?php get_footer(); ?> 