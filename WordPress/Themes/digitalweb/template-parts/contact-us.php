<?php
	/*Template Name: Contact us Template*/

	//Header include  
	get_header();
?>
<!--::::::::::::::::PAGE TITLE ::::::::::::::::::::::-->
<div class="banner" style="background:url('<?php echo the_field('banner_image');?>');">
	<h1><?php the_field('page_title');?></h1>
</div>	
<!--:::::::::::::::: SERVICES SECTION ::::::::::::::::::::::-->
<section class="ourservicess">
	<div class="container">
		<div class="row d-flex justify-content-between">
			<div class="col-md-12">
				<h2><?php the_field('title');?></h2>
				<p><?php the_field('sub_content');?></p>
			</div>
			<?php if( have_rows('contact_us_') ):?>
			    <?php while( have_rows('contact_us_') ): the_row(); ?>
					<div class="col-md-4 contact-top">	
						<img src="<?php the_sub_field('favicon_icon');?>">
						<p><?php the_sub_field('main_title');?></p>
						<label><?php the_sub_field('contact_');?></label>
						<span><?php the_sub_field('extra_detail');?></span>
					</div>	
				<?php endwhile;?>
			<?php endif;?>		
		</div>
		<div class="row contactbottom1">
			<div class="col-md-6">
				<h2><?php the_field('quote_title');?></h2>
				<img class="grdborder" src="<?php the_field('sub_image');?>" alt="grdborder">
				<?php the_field('get_your_quotes');?>		
			</div>
			<div class="col-md-6">
				<h2><?php the_field('location_title');?></h2>
				<img class="grdborder" src="<?php the_field('sub_image1');?>" alt="grdborder">
				<?php the_field('location');?>				
			</div>
		</div>
	</div>	
</section>

				
<!--::::::::::::::::OTHERS SIGNUP ::::::::::::::::::::::-->			
<div class="clearfix"></div>
<section class="contatfooter">
	<div class="container">
		<div class="row d-flex justify-content-between ">
			<?php if( have_rows('sign__up_') ):?>
				<?php while( have_rows('sign__up_') ): the_row(); ?>
					<div class="col-md-4 contatfooter-inner">						
						<h2><?php the_sub_field('head');?></h2>
						<button><?php the_sub_field('button_title');?></button>							
					</div>
				<?php endwhile;?>
			<?php endif;?>
		</div>
	</div>
</section>			
<?php get_footer();?>