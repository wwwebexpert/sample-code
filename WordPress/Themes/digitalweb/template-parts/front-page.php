<?php
/*Template Name: Front Page Template*/
//Header include  
  get_header();
?>
<!--:::::::::::::::::::::::Slider:::::::::::::::::::::::::::-->
<section class="sliderpart">
  <div id="myCarousel" class="carousel" data-ride="carousel">      
    <div class="carousel-inner">            
          <div class="carousel-item active" >            
            <img src="<?php the_field('image');?>" alt="Banner image">
              <div class="container">
                <div class="carousel-text carousel-caption ">
                  <h1><?php the_field('title');?></h1>
                  <h2><?php the_field('content');?></h2>
                  <h3><?php the_field('sub_content');?></h3>
                  <p>
                    
                    <a class="btn btn-lg buttonmore" href="<?php the_field('button_url');?>" data-toggle="modal" data-target="#exampleModal" role="button"><?php the_field('button_name');?> <i class="fa fa-arrow-right" aria-hidden="true"></i>
                    </a>
                  </p>
                </div>
              </div>
          </div>
          
    </div>
   
  </div>
</section>
<!--::::::::::::::::::::::::OUR SERVICES SECTION::::::::::::::::::::::::::::::::-->
<section class="ourservice">
  <div class="container">
    <div class="row ">
      <div class="col-md-12">
        <h2><?php the_field('services_title');?></h2>
        <img class="text-bootom" src="<?php the_field('main_icon');?>" alt="text-bootom">
      </div>
      <?php if( have_rows('our_services') ): ?>
        <?php while( have_rows('our_services') ): the_row(); ?>
          <div class="col-sm-6 col-md-6 col-lg-4 ">
            <div class="row">
              <div class="col-sm-4">
                <img class="" src="<?php the_sub_field('icon');?>" alt="service1">
              </div>
              <div class="col-sm-8">
                <h3><?php the_sub_field('title');?></h3>
                <p><?php the_sub_field('content');?></p>
              </div>
            </div>
          </div>
        <?php endwhile; ?>
      <?php endif;?>
    </div>
  </div>
</section>
<!--:::::::::::::::::::::::::::::::SUPPORST SECTION:::::::::::::::::::::::::::::::::-->
<section class="listprod">
  <div class="container">
    <div class="row ">
      <div class="col-lg-6">
        <div class="row ">
          <?php if( have_rows('support_type') ): ?>
            <?php while( have_rows('support_type') ): the_row(); ?>
              <div class="col-sm-6">
                <div class="listprod-text">
                  <img class="" src="<?php the_sub_field('image');?>" alt="techsup">
                  <h2><?php the_sub_field('title');?></h2>
                </div>
              </div>
             <?php
              endwhile;
            endif;
          ?>
        </div>
      </div>
      <div class="col-lg-6">
        <img class="imgsive" src="<?php the_field('main_image');?>" alt="callset">
        <p><?php the_field('discription');?></p>
      </div>
     </div>
  </div>
</section>
<!--:::::::::::::::::::::::::::::::::::CLIENTS REVIEWS:::::::::::::::::::::::::::::::::::::-->
<section class="ourclient">
  <div class="container">
    <div class="row ">
      <div class="col-sm-12">
        <h2><?php the_field('main_header');?></h2>
        <img class="text-bootom" src="<?php the_field('header_image');?>" alt="text-bootom">
       </div>
      </div>
    <div id="myCarousel1" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
        <?php if( have_rows('clients_info') ) {$active = 'active'; $count=0;?>
          <?php while ( have_rows('clients_info') ) : the_row();?>
            <?php if ($count%2 == 0 ){ ?>
              <div class="carousel-item <?=$active?> <?=$count?>">
                <div class="row clientwrap">             
            <?php } ?>
                  <div class="ourclient-text col-md-6">
                    <img src="<?php the_sub_field('clients_picture');?>" alt="client1">
                    <p><?php the_sub_field('massage');?></p>
                    <p><strong><?php the_sub_field('name');?></strong></p>
                  </div>
                  <?php if ($count%2 == 1) { ?>
                </div>
              </div> 
              <?php } ?>
              <?php $active = ''; $count++; ?>
          <?php endwhile;  ?>
        <?php } ?>
      </div>
      <a class="carousel-control-prev" href="#myCarousel1" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#myCarousel1" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div> 
</section>
<?php get_footer();?>