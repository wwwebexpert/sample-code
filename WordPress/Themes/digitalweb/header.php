<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
    <title>
      <?php
        bloginfo('name');
        if (wp_title('', false)) {
            echo '|';
        } else {
            echo bloginfo('description');
        } wp_title('');
        ?>          
    </title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link href="<?= bloginfo('template_url');?>/assets/css/style.css" rel="stylesheet" >
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900" rel="stylesheet">  
  <?php wp_head(); ?>
  </head>
  <body>
  <header>
  <div class="header-top">
    <div class="container">
      <div class="row ">
        <ul class="col-md-12">
          <?php if( is_active_sidebar( 'header-widget' ) ) : ?>
            <div id="header-widget-area" class="hw-widget widget-area" role="complementary">
              <?php dynamic_sidebar( 'header-widget' ); ?>
            </div>
           <?php endif; ?>
        </ul>  
      </div>
    </div>
  </div>
  <div class="header-menu">
    <div class="container">
      <nav class="navbar navbar-expand-md navbar-dark">
        <a class="navbar-brand" href="<?php echo site_url();?>">          
          <?php   
            $custom_logo_id = get_theme_mod( 'custom_logo' );
            $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
            if(has_custom_logo())
            {
                echo '<img src="'.esc_url($image[0]).'" >';

            }
            else{
                echo'<h1>'.get_bloginfo('name').'</h1x>';
            }
          ?>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
        <!--   <ul class="navbar-nav ml-auto"> -->
          <?php
              wp_nav_menu( array( 
              'container'   => 'ul' ,
              'menu_class'  => 'navbar-nav ml-auto nav-item',
              'menu_id'     =>'primary-menu' ) );
            ?>
          <!-- <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="index.html">Home </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="services.html">Services</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="about-us.html">About Us</a>
          </li>
         <li class="nav-item lastnav">
            <a class="nav-link " href="contact-us.html">Contact Us</a>
          </li>
        </ul> -->
            
         <!--  </ul> -->
        </div>
      </nav>
    </div>
  </div>
  </header>
