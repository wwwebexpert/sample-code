<?php
/* Plugin Name: Mobilyte Plugin
Plugin URI: https://www.mobilyte.com
Description: Mobilyte Plugin for testing
Author: Harish Chauhan
Version: 1.0
Author URI: https://www.mobilyte.com */

//Include file in plugin
define( 'MY_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );

include( MY_PLUGIN_PATH . 'inc/mobilyte-functions.php');

//is_admin functionality
if ( is_admin() ) {
    //include_once( plugin_dir_path( __FILE__ ) . 'includes/admin-functions.php' );
} else {
    //include_once( plugin_dir_path( __FILE__ ) . 'includes/front-end-functions.php' );
}

//Add script in plugin
add_action('wp_enqueue_scripts', 'fwds_scripts');

function fwds_scripts() {

	//wp_enqueue_script('jquery');

	wp_register_script('mobilyte_jquery', plugins_url('js/mobilyte-jquery.js', __FILE__),array("jquery"));
	wp_enqueue_script('mobilyte_jquery');
}

add_action('wp_enqueue_scripts', 'fwds_styles');

//Add css to the plugin
function fwds_styles() {

	wp_register_style('mobilyte_css', plugins_url('css/mobilyte-css.css', __FILE__));
	wp_enqueue_style('mobilyte_css');
}

//register custom post type
function wporg_custom_post_type()
{
    register_post_type('wporg_product',
                       [
                           'labels'      => [
                               'name'          => __('Mobilyte Widget'),
                               'singular_name' => __('mobilyte'),
                           ],
                           'public'      => true,
                           'has_archive' => true,
                           'supports' => array( 'title', 'editor', 'custom-fields' ),
                           'taxonomies'   => array(
				           'post_tag',
				           'category',
			                )
                       ]
                     );
}
add_action('init', 'wporg_custom_post_type');

add_action('admin_menu', 'test_plugin_setup_menu');
function test_plugin_setup_menu(){
        add_menu_page( 'Test Plugin Page', 'Mobilyte Settings', 'manage_options', 'test-plugin', 'test_init' );
        
}
 
function test_init() {
     
    if (isset($_POST['action']) && $_POST['action'] == "update_theme") {

        if (wp_verify_nonce($_POST['theme_front_end'],'update-options')) {

            update_option('my_theme-style',$_POST['stylesheet']);
            update_option('mobilyte_widget_bg_color',$_POST['mobilyte_widget_bg_color']);
            update_option('mobilyte_widget_header_footer_color',$_POST['mobilyte_widget_header_footer_color']);
            //update_option('mobilyte_widget_header_footer_font',$_POST['mobilyte_widget_header_footer_font']);
        
        } else { ?><div class="error"><?php echo 'update failed'; ?></div><?php }
    } ?>
	<div align="">
	    <form id="save-theme" name="save-theme" action="" method="post">
			<h2 style="font-size: 30px;">Mobilyte Widget Settings</h2>
			<hr><br>
			<?php $mobilyte_widget_bg_color = get_option('mobilyte_widget_bg_color'); ?>
			<label style="font-size: 16px;font-weight: bold;">Bodybackground Color : </label>
			<input type="color" name="mobilyte_widget_bg_color" value="<?=$mobilyte_widget_bg_color;?>"><br>
			<label style="font-size: 16px;font-weight: bold;">Header And Footer Color : </label>
			<?php $mobilyte_widget_header_footer_color = get_option('mobilyte_widget_header_footer_color'); ?>
			<input  type="color" name="mobilyte_widget_header_footer_color" value="<?=$mobilyte_widget_header_footer_color;?>"><br>
			<?php wp_nonce_field('update-options','theme_front_end'); ?>
			<input type="hidden" name="action" value="update_theme">
			<input type="submit" name="update-options" value="Save">
			<hr>
			<h4>Shortcode of Custom Widget is : <span>[mobilyte_widgets]</span></h4>
		</form>
	</div>	    
<?php }

register_activation_hook(__FILE__, 'fwds_slider_activation');

//Plugin deactivation
function fwds_slider_deactivation() {
	//die('de activation hook');
}

register_deactivation_hook(__FILE__, 'fwds_slider_deactivation');

//Adding shortcode [my_shortcode]
add_shortcode("my_shortcode", "my_shortcode_function");

function my_shortcode_function() {
	return "<h1>Hello Shortcodes</h1>"; 
}

//Add advanced shortcode [mobilyte_widgets name="Nimesh" age="27"]
add_shortcode("mobilyte_widgets", "my_shortcode_advanced_function");

function my_shortcode_advanced_function( $atts, $content = null ) { 
?>
	<div class="col-xs-12  col-md-4 builder-column ">
	  	<div class="description">
	          <?php 
	          	$mobilyte_widget_bg_color = get_option('mobilyte_widget_bg_color'); 
	          	if ( $mobilyte_widget_bg_color == '' ) {
	          		$mobilyte_widget_bg_color = "#333";
	          	}
	          	?>
		      <div id="main" style="background: <?= $mobilyte_widget_bg_color ?> !important;">
		      	
		      	<?php $mobilyte_widget_header_footer_color = get_option('mobilyte_widget_header_footer_color'); 
		      		if ( $mobilyte_widget_header_footer_color == '' ) {
		          		$mobilyte_widget_header_footer_color = "#000";
		          	}
		      	?>

		          <div id="filed" style="background-color: <?=$mobilyte_widget_header_footer_color?> !important;padding: 20px !important;border: 1px solid #425a9c !important;">
			          <h5 style="margin: 0px; padding: 0; font-size: 20px; color: #fff;">FIELD STATUS</h5>
			          <!-- <i class="fa fa-angle-double-down" style="font-size: 36px; float: right; margin-top: -3px; margin-right: 15px;"></i> -->
		          </div>
	              <div id="wid">  
	                    <?php
						global $post;
						$args = array( 'posts_per_page' => 3 , 'post_type'=> 'wporg_product' ,'post_status' => 'publish,pending');
						$myposts = get_posts( $args );
						foreach ( $myposts as $post ) : 
						  setup_postdata( $post );
						   ?>
							<h2 style="color: white !important;font-size: 24PX !important;font-weight: bold !important;width: 100%;padding: 20px 22px 10px 22px;margin: 0;"><?php echo the_title(); ?></h2>
							<p style="float: left;color: white !important;font-size: 15px;font-weight: bold;width: 100%;padding: 0px 22px;">Updated: <?php the_modified_date('F j, Y');?>  <?php echo  the_modified_date('g:i a'); ?>
								<?php 
                                $category = get_the_category();?>
							<strong style="float: right; color: white !important; margin-left: 30px!important; font-size: 23px; "><?php echo get_post_meta( get_the_ID(), 'Status', true);?></strong></p>
						<?php endforeach;
						wp_reset_postdata(); ?>
			            <div style="background-color: <?=$mobilyte_widget_header_footer_color?> !important; color: white !important; padding: 20px !important; font-size: 20px; font-weight: bold; text-align: center; border: 1px solid #425a9c !important;clear: both;"><a href="#" style="color:#2A76D9;"> VIEW ALL </a></div>
	                </div>
	            </div>
	    </div>
	</div> 
<?php } ?>