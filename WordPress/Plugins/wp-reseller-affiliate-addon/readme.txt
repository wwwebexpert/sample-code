﻿Documentation
plugin documentation
- We use shortcode "[show_business]" to add button "Add New Business / School" and show list of all added business added
- Then create new page "Manage Details" with slug "manage-details" and insert shortcode [get_users] to show all users list, upload csv list,
show dashboard of user lists

Working process of Plugin
- Create new user with role "reseller"
- then login with "reseller" account details with url 
- open the link : https://staging.astrialibrary.com/reseller-area/
- add shortcode "[show_business]" in page "Manage"
- add business/ school and list will be display below and manage
- create new page "Manage Details" from backend and add shortcode "[get_users]" to show/ manage users of particular business
- then we can add new single user/ upload csv file of users and will display below
- all business are show in backend post_type "business"
- all users will display in "All users" with reseller name, id
- also users inserted in woocommerce subscriptions post_type "shop_subscription" with Items is
- Update membership plan from Affiliates->affiliate users list and then edit user of Reseller role
