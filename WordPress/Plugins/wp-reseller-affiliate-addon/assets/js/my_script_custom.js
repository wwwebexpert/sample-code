jQuery('#myform').validate({ // initialize the plugin
    rules: {
        school_business: {
            required: true,
        },
        address: {
            required: true,
        },
        country: {
            required: true,
        },
        contact_name: {
            required: true,
        },
        contact_email: {
            required: true,
        },
        contact_phone: {
            required: true,
        }
    }
});
jQuery('#userform').validate({ // initialize the plugin
        rules: {
            first_name: {
                required: true,
            },
            last_name: {
                required: true,
            },
            user_name: {
                required: true,
            },
            password: {
                required: true,
                minlength : 5
            },
            contact_email: {
                required: true,
            },
            contact_phone: {
                required: true,
            }
        }
});
jQuery(document).ready(function(){  
	//jquery for select membership enabled input[type="file"]
			 jQuery('.select_subscription').change(function(){
			 	var op = jQuery(this).val();
			 	if(op != ''){
			 		jQuery('input[name="uplaodfile"]').prop('disabled',false);
			 		jQuery('input[name="upload_file"]').prop('disabled',false);
			 	}
			 	else
			 	{
			 		jQuery('input[name="uplaodfile"]').prop('disabled',true);
			 		jQuery('input[name="upload_file"]').prop('disabled',true);
			 	}
			 }); 

	 //upload csv file

	 jQuery('#file_form').submit(function(event){
	 	event.preventDefault();
	 	var subscription      = jQuery('input[name="subscription"]:checked').val();
	 	var data 			  = new FormData(this);
	 	var business_id 	  = jQuery('#business_id').val();
	 	data.append("action", "add_csv_file_data"); 
	 	jQuery.ajax({
			        type: 'POST',
			        url: ajaxurl,
			        business_id : 'business_id',
			        subscription: 'subscription',
			        data:data,
			        processData: false,
                    contentType: false,
                    cache: false,
				        success:function(response) {
				            //console.log(data);
				            //alert(response);
				            jQuery('#the_response').html(response);
				            jQuery('#file_form')[0].reset();
				            location.reload();
				            if(response !== ''){
			                 }else{
			                 }
				        },
				        error: function(errorThrown){
				            //console.log(errorThrown);
				            //alert("fail");
				        }
			    	});
	 	return false;
	 	
	 });
		jQuery('#button1').on('click',function(event){
			event.preventDefault();
			if(jQuery("#myform").valid()){
				var user_id         = jQuery('#user_id').val();
				var school_business = jQuery('#school_business').val();
				var address 	    = jQuery('#address').val();
				var country 		= jQuery('#country').val();
				var contact_name 	= jQuery('#contact_name').val();
				var contact_email 	= jQuery('#contact_email').val();
				var contact_phone 	= jQuery('#contact_phone').val();
					jQuery.ajax({
			        type: 'POST',
			        url: ajaxurl,
			        data: {
				            'action':'add_school_business_function',
				            'user_id':user_id,
				            'school_business' : school_business,
				            'address' : address,
				            'country' : country,
				            'contact_name' : contact_name,
				            'contact_email':contact_email,
				            'contact_phone':contact_phone
				        },
				        success:function(response) {
				            //console.log(data);
				            //alert(response);
				            jQuery('#show_response').html(response);
				            jQuery('#myform')[0].reset();
				            location.reload();
				            if(response !== ''){
			                 }else{
			                 }
				        },
				        error: function(errorThrown){
				            //console.log(errorThrown);
				            //alert("fail");
				        }
			    	});
			    }
			});
		jQuery('#button2').on('click',function(event){
			event.preventDefault();
			if(jQuery("#userform").valid()){
            	var reseller_id     = jQuery('#reseller_id').val();
            	var curr_user_id    = jQuery('#curr_user_id').val();
				var first_name		= jQuery('#first_name').val();
				var last_name  		= jQuery('#last_name').val();
				var email  			= jQuery('#email').val();
				var password   		= jQuery('#password_name').val();
				var mobile_number   = jQuery('#mobile_number').val();
	            var subscription    = jQuery('input[name="subscription"]:checked').val();

				var add_address     = jQuery('#add_address').val();
				jQuery.ajax({
				type: 'POST',
			        url: ajaxurl,
			        data: {
				            'action':'add_user_db_function',
				            'reseller_id'   : reseller_id,
				            'curr_user_id'  : curr_user_id,
				            'first_name'	: first_name,
				            'last_name' 	: last_name,
				            'email' 		: email,
				            'password'  	: password,
				            'mobile_number' : mobile_number,
				            'subscription'  : subscription 
				        },
				        success:function(response) {
				            //console.log(data);
				            //alert(response);
				            jQuery('#get_response').html(response);
				            jQuery('#userform')[0].reset();
				            location.reload();
				            if(response !== ''){
			                 }else{
			                 }
				        },
				        error: function(errorThrown){
				            //console.log(errorThrown);
				            //alert("fail");
				        }	
				});
			}
		});
		jQuery('.user_delete').click(function(event){
			event.preventDefault();
			var el = this;
			var id = this.id;
			var splitid = id.split("_");

			// Delete id
			var delete_user_id = splitid[1];
			jQuery.ajax({
			type: 'POST',
			url: ajaxurl,
			data: {
			'action':'delete_user_business_function',
			'delete_user_id': delete_user_id
			},
			success:function(response) {
			//console.log(data);
			//alert(response);
			//jQuery('#delete_response').html(response);
			jQuery('#file_form')[0].reset();
			location.reload();
			if(response !== ''){
			}else{
			}
			},
			error: function(errorThrown){
			//console.log(errorThrown);
			alert("fail");
			}
		});	 
	});
});