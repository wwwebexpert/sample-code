<?php
//function
add_action( 'wp_ajax_add_school_business_function', 'add_school_business_function' );
add_action( 'wp_ajax_nopriv_add_school_business_function', 'add_school_business_function' );
function add_school_business_function() {
	global $wpdb;
	$table_name = $wpdb->prefix . "posts";
	extract($_POST);
	$user_id_1         = $_REQUEST['user_id'];
	$school_business_1 = $_REQUEST['school_business'];
	$address_1 		   = $_REQUEST['address'];
	$country_1 		   = $_REQUEST['country'];
	$contact_name_1    = $_REQUEST['contact_name'];
	$contact_email_1   = $_REQUEST['contact_email'];
	$contact_phone_1   = $_REQUEST['contact_phone'];
	//check condition already exists post title
	$query = $wpdb->get_results('SELECT * FROM '.$table_name.' WHERE post_type="business" AND post_status="publish" AND post_title = "'.$school_business_1.'"');
	if($query)
	{
		echo "School / Business Already Exists";
	}
	else
	{
		//add details into post type business
		$posts_data = array(
			'post_title'=>$school_business_1,
			'post_type'=>'business',
			'post_status'=>'publish'
		);
		$posts_id = wp_insert_post($posts_data);
		//add postmeta or custom field
		add_post_meta($posts_id,'post_author',$user_id_1);
		add_post_meta($posts_id,'school_business',$school_business_1);
		add_post_meta($posts_id,'address',$address_1);
		add_post_meta($posts_id,'country',$country_1);
		add_post_meta($posts_id,'contact_name',$contact_name_1);
		add_post_meta($posts_id,'contact_email',$contact_email_1);
		add_post_meta($posts_id,'contact_phone',$contact_phone_1);
		echo "Data Inserted Successfully!!";
	}
	die();
}
add_action( 'wp_ajax_add_user_db_function', 'add_user_db_function' );
add_action( 'wp_ajax_nopriv_add_user_db_function', 'add_user_db_function' );
function add_user_db_function() {
	global $wpdb;
	global $woocommerce;
	extract($_POST);
	$reseller_id    = $_REQUEST['reseller_id'];
	$curr_user_id   = $_REQUEST['curr_user_id'];
	$first_name 	= $_REQUEST['first_name']; 
	$last_name  	= $_REQUEST['last_name']; 
	$email  		= $_REQUEST['email']; 
	$password   	= MD5($_REQUEST['password']); 
	$mobile_number  = $_REQUEST['mobile_number'];
	$subscription   = $_REQUEST['subscription'];
	//print_r($subscription);die();
	//add user database
	$user_data = array(
		'user_login'=>$first_name,
		'user_pass'=>$password,
		'user_nicename'=>$first_name.''.$last_name,
		'user_email'=>$email,
		'role'=>'subscriber'
	);
	//user already exists
	if(!email_exists($email))
	{
		$user_id     = wp_insert_user($user_data);
 
 		//add subscription
		update_user_meta($user_id,'reseller_id',$reseller_id);
		update_user_meta($user_id,'curr_user_id',$curr_user_id);
		update_user_meta($user_id,'phone_number',$mobile_number);
		$get_curr_id = get_current_user_id();

		$product_id  = $subscription;

		$quantity = 1;
		$billing_state = '';
		$billing_post_code = '';

		$order = wc_create_order();
		update_post_meta( $order->id, '_billing_first_name', $first_name);
		update_post_meta( $order->id, '_billing_last_name', $last_name);
		update_post_meta( $order->id, '_billing_email', $email );
		update_post_meta( $order->id, '_billing_state', '');
		update_post_meta( $order->id, '_billing_postcode', '');
		update_post_meta( $order->id, '_shipping_first_name', $first_name);
		update_post_meta( $order->id, '_shipping_last_name', $last_name);
		$price = '';

		$order->add_product( wc_get_product( $product_id ), $quantity);
			  
		$order->set_address( $address, 'billing' );
		$order->set_address( $address, 'shipping' );
		$order->set_total( $price*$quantity , 'order_discount'); // not pennies (use dollar amount)
		$order->set_payment_method($this);
		$order->calculate_totals();
		update_post_meta( $order->id, '_customer_user', $user_id );
		update_post_meta( $order->id, '_add_user_id' , $user_id );


		$start_date = date( "Y-m-d H:i:s", strtotime( "now" ) );
		$product    = wc_get_product($product_id);
		$period     = WC_Subscriptions_Product::get_period($product);
		$interval   = WC_Subscriptions_Product::get_interval($product);

		$sub = wcs_create_subscription( array( 'order_id' => $order->id, 'billing_period' => $period, 'billing_interval' => $interval, 'start_date' => $start_date )) ;
			 
		$sub->add_product( wc_get_product( $product_id ), $quantity );
		$sub->calculate_totals();
		WC_Subscriptions_Manager::put_subscription_on_hold($order);
		//end subscription
		
		
		echo "Inserted User Successfully!!!";
		die();
	}
	else
	{
		echo "Email Exists";die();
	}
}

//csv file code starts
add_action( 'wp_ajax_add_csv_file_data', 'add_csv_file_data' );
add_action( 'wp_ajax_nopriv_add_csv_file_data', 'add_csv_file_data' );

function add_csv_file_data() 
{
	global $wpdb;
	global $woocommerce;
	//extract($_POST);
	//print_r($_FILES);
	$file_name  = $_FILES['uplaodfile']['name'];
	$tmp_name   = $_FILES['uplaodfile']['tmp_name'];
	//path
	$folderPath = '/wp-content/uploads/2018/06/';
	$target_path = ABSPATH.$folderPath . "/" .$file_name;
	if(move_uploaded_file($tmp_name,$target_path))
	{
		//$row = 1;
		if (($handle = fopen($target_path, "r")) !== FALSE) 
		{
		    $record_count = 0;
		    $recoed_inserted = 0;
		    $error_recoed_insert = 0;
		    $user_exists = array();
		    //get reseller and business id's
		    $business_id  = $_REQUEST['business_id'];
		    $reseller_id  = get_current_user_id();

		    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
		    {

		    	if($record_count > 0)
		    	{
			        $num = count($data);

			        $user_login    = $data[0];
			        $user_pass     = $data[1];
			        $user_nicename = $data[2];
			        $user_email    = $data[3];
			        $user_status   = $data[4];
			        $last_name     = $data[5];
			        $role 		   = $data[6];
			        $phone_number  = $data[7];

			        //insert into users table
			        $user_data = array(
			        	'user_login'   =>$user_login,
			        	'user_email'   =>$user_email,
			        	'user_pass'    =>$user_pass,
			        	'user_nicename'=>$user_nicename,
			        	'user_status'  =>$user_status,
			        	'last_name'	   =>$last_name,
			        	'role'         =>$role,
			        	'phone_number' =>$phone_number
			        );
		    	
			         $already_email_exists = email_exists($user_email);
				     if($already_email_exists)
				     {
				        $user_exists[] = $user_email;
				     }
				     else
				     {	
					    if($insert_user_id = wp_insert_user($user_data) ){

					    	//add subscription
					    	update_user_meta($insert_user_id,'reseller_id',$business_id);
							update_user_meta($insert_user_id,'curr_user_id',$reseller_id);
							update_user_meta($insert_user_id,'phone_number',$phone_number);

							$get_curr_id = get_current_user_id();
							$product_id  = $_REQUEST['subscription'];

								$quantity = 1;
								$billing_state = '';
								$billing_post_code = '';

								$order = wc_create_order();
								update_post_meta( $order->id, '_billing_first_name', $user_nicename);
								update_post_meta( $order->id, '_billing_last_name', $last_name);
								update_post_meta( $order->id, '_billing_email', $user_email );
								update_post_meta( $order->id, '_billing_state', '');
								update_post_meta( $order->id, '_billing_postcode', '');
								update_post_meta( $order->id, '_shipping_first_name', $user_nicename);
								update_post_meta( $order->id, '_shipping_last_name', $last_name);
								$price = '';

								$order->add_product( wc_get_product( $product_id ), $quantity);
								  
								$order->set_address( $address, 'billing' );
								$order->set_address( $address, 'shipping' );
								$order->set_total( $price*$quantity , 'order_discount'); // not pennies (use dollar amount)
								$order->set_payment_method($this);
								$order->calculate_totals();
								update_post_meta( $order->id, '_customer_user', $insert_user_id );
								update_post_meta( $order->id, '_add_user_id' , $insert_user_id );

								//add new subscription
								$start_date = date( "Y-m-d H:i:s", strtotime( "now" ) );
								$product    = wc_get_product($product_id);
								$period     = WC_Subscriptions_Product::get_period($product);
								$interval   = WC_Subscriptions_Product::get_interval($product);

								$sub = wcs_create_subscription( array( 'order_id' => $order->id, 'billing_period' => $period, 'billing_interval' => $interval, 'start_date' => $start_date )) ;
									
								$sub->add_product( wc_get_product( $product_id ), $quantity );
								$sub->calculate_totals();
								WC_Subscriptions_Manager::put_subscription_on_hold($order);
								//end subscription
	 						
					    	$recoed_inserted++;
					    } else {
					    	$error_recoed_insert++;
					    }
					    //echo "CSV Uploaded Successfully!!";
				    }
			    }
			     $record_count++;
		        echo "\n";
		    }

		    echo "<strong>Total Users</strong> :".$record_count."</br>";
		    echo "<strong>Users already exists</strong> :".count($user_exists)."</br>";
		    echo "<strong>Total users inserted</strong> :".$recoed_inserted."</br>";
		    echo "<strong>Error to insert user</strong> :".$error_recoed_insert."</br>";

		    //check user already exists
		    fclose($handle);
		}	
	}
	die();
}


add_action( 'wp_ajax_delete_user_business_function', 'delete_user_business_function' );
add_action( 'wp_ajax_nopriv_delete_user_business_function', 'delete_user_business_function' );

function delete_user_business_function() 
{
	global $wpdb;
	extract($_POST);
	$delete_user_id = $_REQUEST['delete_user_id'];

	//wordpress delete query
	wp_delete_user($delete_user_id);
	echo "User Deleted Successfully!!";
	die();
}

// get woocommerce subscriptions
function active_subscription_list($from_date=null, $to_date=null) {

    // Get all customer orders
    $logged_user = get_current_user_id();
    $subscriptions = get_posts( array(
    	'meta_value'=>$logged_user,
        'numberposts' => 1,
        'post_type'   => 'shop_subscription', // Subscription post type
        'post_status' => array('wc-active','wc-pending'), // Active subscription
        'orderby' => 'post_date', // ordered by date
        'order' => 'DESC',
        'date_query' => array( // Start & end date
            array(
                'after'     => $from_date,
                'before'    => $to_date,
                'inclusive' => true,
            ),
        ),
    ) );

    // Styles (temporary, only for demo display) should be removed
    echo "<style>
        .subscription_list th, .subscription_list td{border:solid 1px #666; padding:2px 5px;}
        .subscription_list th{font-weight:bold}
        .subscription_list td{text-align:center}
    </style>";

    // Displaying list in an html table
    echo "<table class='shop_table subscription_list subscription_table'>
        <tr>
            <th>" . __( 'Number ID', 'your_theme_domain' ) . "</th>
            <th>" . __( 'Date', 'your_theme_domain' ) . "</th>
            <th>" . __( 'User ID', 'your_theme_domain' ) . "</th>
            <th>" . __( 'User Name', 'your_theme_domain' ) . "</th>
            <th>" . __( 'Product ID', 'your_theme_domain' ) . "</th>
            <th>" . __( 'Subscription', 'your_theme_domain' ) . "</th>
            <th>" . __( 'Product Image', 'your_theme_domain' ) . "</th>
        </tr>
            ";
    // Going through each current customer orders
    foreach ( $subscriptions as $subscription ) {
        $subscription_id = $subscription->ID; // subscription ID
        $order = wc_get_order( $subscription_id );
        $items = $order->get_items();
       foreach($items as $item)
       {
       	$product_name = $item->get_name();
    	$product_id = $item->get_product_id();
    	 
    	$product_image =  wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'thumbnail', TRUE);
       
        $subscription_date = array_shift( explode( ' ', $subscription->post_date ) ); // Date
        $subscr_meta_data = get_post_meta($subscription->ID);
        $customer_id = $subscr_meta_data['_customer_user'][0]; // customer ID
        $customer_name = $subscr_meta_data['_billing_first_name'][0] . ' ' . $subscr_meta_data['_billing_last_name'][0];
        echo "</tr>
                <td>$subscription_id</td>
                <td>$subscription_date</td>
                <td>$customer_id</td>
                <td>$customer_name</td>
                <td>$product_id</td>
                <td>$product_name</td>
                <td><img src=".$product_image[0]." /></td>
            </tr>";
        }
    }
    echo '</table>';
}
add_shortcode('show_the_subscription','active_subscription_list');


//add function to add new field in backend users
function new_modify_user_table( $column ) {
    $column['phone'] = 'Reseller ID';
    return $column;
}
add_filter( 'manage_users_columns', 'new_modify_user_table' );
function new_modify_user_table_row( $val, $column_name, $user_id ) {
    switch ($column_name) {
        case 'phone' :
        $get_user_id = get_user_meta($user_id,'curr_user_id',true);
        $user_info   = get_userdata($get_user_id);
        if($get_user_id): 
			return $user_info->user_login.' ['.$get_user_id.']';
		endif;
            break;
        default:
    }
    return $val;
}
add_filter( 'manage_users_custom_column', 'new_modify_user_table_row', 10, 3 );

//add pugin activate condition
function my_text_strings( $translated_text, $text, $domain ) {
switch ( $translated_text ) {
    case 'Affiliates' :
        $translated_text = __( 'Reseller', 'affiliates' );
        break;
}
return $translated_text;
}
add_filter( 'gettext', 'my_text_strings', 20, 3 );


//add new field in users
add_action( 'edit_user_profile', 'crf_show_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'crf_update_profile_fields' );

function crf_show_extra_profile_fields( $user_id)
{?>
<h3><?php _e("Extra profile information", "blank"); ?></h3>	

 <table class="form-table">
    <tr>
        <th><label for="address"><?php _e("Select Subscription Plan"); ?></label></th></tr>
        <?php
        global $wpdb;
        global $woocommerce;

        $get_user_id = $_REQUEST['user_id'];
        $get_data    = get_user_meta($get_user_id,'subs_plan',true);
        $args = array(
	    'post_type'=>'product',
	    'shortby'=>'date',
	    'posts_per_page'=>12
	  );
	  $loop = new WP_Query($args);
	  if($loop->have_posts()) {
	    while($loop->have_posts()) {
	      $loop->the_post();
	      $plans = get_the_ID();
	      $subscriptions = get_posts( array(
	    	'meta_value'=>$get_user_id,
	        'numberposts' => 1,
	        'post_type'   => 'shop_subscription', // Subscription post type
	        'post_status' => array('wc-active','wc-pending','wc-completed'), // Active subscription
	        'orderby' => 'post_date', // ordered by date
	        'order' => 'DESC',
	        'date_query' => array( // Start & end date
	            array(
	                'after'     => $from_date,
	                'before'    => $to_date,
	                'inclusive' => true,
	            ),
	        ),
	    ) );
	    foreach ( $subscriptions as $subscription ) 
	    {
	        $subscription_id = $subscription->ID;
	        $order = wc_get_order( $subscription_id );
	        $items = $order->get_items();
	       foreach($items as $item)
	       {

	       	   	$product_name = $item->get_name();
		    	$product_id = array($item->get_product_id());
	       }
	    }
	      ?>
	      <td>
            <input type="checkbox" name="subs_plan[]" value="<?php echo get_the_ID(); ?>" <?php if(in_array($plans,$get_data)){ echo  "checked"; }  ?>  /><?php the_title(); ?>
        </td>
	      <?php
		}
	}
	  wp_reset_postdata();
        ?>
    </tr>
</table>
<?php } 
function crf_update_profile_fields( $user_id ) {
	
		update_user_meta( $user_id, 'subs_plan', $_POST['subs_plan']);
		global $wpdb;
        global $woocommerce;

        //$get_user_id = $_REQUEST['user_id'];
        $get_data    = get_user_meta($user_id,'subs_plan',true);

	    	foreach($get_data as $get_show_data)
	    	{
	    		$product_id[] = $get_show_data;
	    	}
			  $user_first_name   = get_user_meta($user_id,'first_name',true);
			  $user_last_name    = get_user_meta($user_id,'last_name',true);
			  $user_email 		 = get_user_meta($user_id,'billing_email',true);
			  $billing_email     = get_user_meta($user_id,'billing_email',true);
			  $billing_state     = get_user_meta($user_id,'billing_state',true);
			  $billing_post_code = get_user_meta($user_id,'billing_postcode',true);
			  $billing_country   = get_user_meta($user_id,'billing_country',true);
			  $quantity = 1;
			  	//update post subscription status before create new order
			    $subscriptions = get_posts( array(
			    	'meta_value'=>$user_id,
			        'numberposts' => -1,
			        'post_type'   => 'shop_subscription', // Subscription post type
			        'post_status' => array('wc-active','wc-pending','wc-on-hold'), // Active subscription
			        'orderby' => 'post_date', // ordered by date
			        'order' => 'DESC',
			        'date_query' => array( // Start & end date
			            array(
			                'after'     => $from_date,
			                'before'    => $to_date,
			                'inclusive' => true,
			            ),
			        ),
			    ) );
			    foreach($subscriptions as $subscription)
			    {

			    	$my_post = array(
				      'ID'           => $subscription->ID,
				      'post_status'  => 'wc-on-hold'
				  );
			    	//update the post into database
			    	echo wp_update_post( $my_post );
			    }

		    //create order
			  $order = wc_create_order();
			  update_post_meta( $order->id, '_billing_first_name', $user_first_name );
			  update_post_meta( $order->id, '_billing_last_name', $user_last_name );
			  update_post_meta( $order->id, '_billing_email', $billing_email );
			  update_post_meta( $order->id, '_billing_state', $billing_state );
			  update_post_meta( $order->id, '_billing_postcode', $billing_post_code );
			  update_post_meta( $order->id, '_shipping_first_name', $user_first_name );
			  update_post_meta( $order->id, '_shipping_last_name', $user_last_name );
			  $price = '';

			  for($i = 0 ; $i < count($product_id); $i++)
			  {
			  	$order->add_product( wc_get_product( $product_id[$i] ), $quantity);
			  }
			  
			  $order->set_address( $address, 'billing' );
			  $order->set_address( $address, 'shipping' );
			  $order->set_total( $price*$quantity , 'order_discount'); // not pennies (use dollar amount)
			  $order->set_payment_method($this);
			  $order->calculate_totals();
			  update_post_meta( $order->id, '_customer_user', $user_id );
			  //add new subscription
			  /*$start_date = '2018-06-22 16:02:36.000000';
			  $period = 'Year';
			  $interval = '2';*/
			$start_date = date( "Y-m-d H:i:s", strtotime( "now" ) );
			for($i = 0 ; $i < count($product_id); $i++)
			{
				$product    = wc_get_product($product_id[$i]);
			}
			$period     = WC_Subscriptions_Product::get_period($product);
			$interval   = WC_Subscriptions_Product::get_interval($product);
			$sub 		= wcs_create_subscription( array( 'order_id' => $order->id, 'billing_period' => $period, 'billing_interval' => $interval, 'start_date' => $start_date )) ;

			  for($i = 0 ; $i < count($product_id); $i++)
			  {
				  $sub->add_product( wc_get_product( $product_id[$i] ), $quantity );
			  }
				  $sub->calculate_totals();
				  //$sub->update_status( 'wc-pending-cancel');
				  WC_Subscriptions_Manager::put_subscription_on_hold($order);
}

// function for getting total resellers
function show_number_posts()
{
		ob_start();
		$args = array(
		'posts_per_page'   => -1,
		'offset'           => 0,
		'category'         => '',
		'category_name'    => '',
		'orderby'          => 'date',
		'order'            => 'DESC',
		'meta_key'         => '',
		'meta_value'       => '',
		'post_type'        => 'business'
	);
	$posts_array = get_posts( $args );
	echo count($posts_array);
}
add_shortcode('show_users','show_number_posts');

//shortcode to display total users in dashboard
function get_data_users()
{
	$users = get_users(array(
    'meta_key' => 'curr_user_id',
	));
	return count($users);
}
add_shortcode('all_users','get_data_users');

function get_reseller_id()
{
	return get_current_user_id();
}
add_shortcode('show_reseller','get_reseller_id');

function get_payment()
{
	ob_start();
	$subscriptions = get_posts( array(
    	'meta_value'=>get_current_user_id(),
        'numberposts' => 1,
        'post_type'   => 'shop_subscription', // Subscription post type
        'post_status' => array('wc-active','wc-pending'), // Active subscription
        'orderby' => 'post_date', // ordered by date
        'order' => 'DESC',
        'date_query' => array( // Start & end date
            array(
                'after'     => $from_date,
                'before'    => $to_date,
                'inclusive' => true,
            ),
        ),
    ) );
    foreach($subscriptions as $get_subscription)
    {
    	$subscription_id = $get_subscription->ID; // subscription ID
        $order = wc_get_order( $subscription_id );
        $items = $order->get_items();
        foreach($items as $get_item)
        {
        	return '$'.$get_item['subtotal'];
        }
    }
    return ob_get_clean();
}
add_shortcode('show_value','get_payment');

function get_reseller_subscription()
{
	ob_start();
	$subscriptions = get_posts( array(
    	'meta_value'=>get_current_user_id(),
        'numberposts' => 1,
        'post_type'   => 'shop_subscription', // Subscription post type
        'post_status' => array('wc-active','wc-pending'), // Active subscription
        'orderby' => 'post_date', // ordered by date
        'order' => 'DESC',
        'date_query' => array( // Start & end date
            array(
                'after'     => $from_date,
                'before'    => $to_date,
                'inclusive' => true,
            ),
        ),
    ) );
    foreach($subscriptions as $get_subscription)
    {
    	$subscription_id = $get_subscription->ID; // subscription ID
        $order = wc_get_order( $subscription_id );
        $items = $order->get_items();
        foreach($items as $get_item)
        {
        	return $get_item['order_id'];
        }
    }
    return ob_get_clean();
}
add_shortcode('show_res_subs','get_reseller_subscription');
?>