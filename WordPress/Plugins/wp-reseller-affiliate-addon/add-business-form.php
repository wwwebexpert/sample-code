<?php 
//get all business data
function raa_show_all_business( $atts, $content = null ) {

	ob_start();

	if(is_user_logged_in())
	{
		$ID = get_current_user_id();
		$user_meta=get_userdata($ID);
		$user_roles=$user_meta->roles;
		if($user_roles[0] == 'reseller')

		{
?>

	<div id="add_business">
		<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Add New Business/ School</button>
	</div>

<?php
		} 
	}
?>
	<div class="modal fade" id="myModal" role="dialog" style="z-index:99999">
	    <div class="modal-dialog">
	      <!-- Modal content-->
	      <div class="modal-content">
	        <div class="modal-header" id="heading">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h3 class="modal-title">Add Business / School</h3>
	        </div>
	        <div class="modal-body">
	          <form method="post" id="myform">
	          	<input type="hidden" name="user_id" value="<?php echo get_current_user_id(); ?>" />
			    <div class="form-group">
			      <label for="email">Name of School / Business</label>
			      <input type="text" class="form-control" id="school_business" name="school_business" placeholder="Enter School / Business" name="school_business" required />
			    </div>
			    <div class="form-group">
			      <label for="pwd">Address:</label>
			      <textarea class="form-control" id="address" name="address" placeholder="Enter Address" name="address" required></textarea>
			    </div>
			    <div class="form-group">
			      <label for="pwd">Country:</label>
			      <select class="form-control" id="country" name ="country" required></select>
			    </div>
			    <div class="form-group">
			      <h3 for="pwd">Contact Person Details</h3>
			    </div>
			    <div class="form-group">
			      <label for="pwd">Name:</label>
			      <input type="text" name="contact_name" id="contact_name" class="form-control" placeholder="Name" required />
			    </div>
			    <div class="form-group">
			      <label for="pwd">Email:</label>
			      <input type="email" name="contact_email" id="contact_email" class="form-control" placeholder="Email" required />
			    </div>
			    <div class="form-group">
			      <label for="pwd">Contact Phone:</label>
			      <input type="text" name="contact_phone" class="form-control" id="contact_phone" placeholder="Phone" required />
			    </div>
			    <!-- <button type="button" id="button1" class="btn btn-primary">Submit</button> -->
			    <input type="submit" name="button1" id="button1" value="Submit" class="btn btn-primary"  />
			    <div id="show_response"></div>
			  </form>
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        </div>
	      </div>      
	    </div>
	 </div>
	<?php

	echo "<form method='post' id='get_data'>";
	echo "<table class='reseller_table'>";
	echo "<tr>";
	echo "<th>Name of School / Business</th>";
	echo "<th>Address</th>";
	echo "<th>Country</th>";
	echo "<th>Contact Person</th>";
	echo "<th>Actions / Manage</th>";
	echo "<th></th>";
	echo "</tr>";
	$args = array(
		'posts_per_page'   => -1,
		'offset'           => 0,
		'category'         => '',
		'category_name'    => '',
		'orderby'          => 'date',
		'order'            => 'DESC',
		'include'          => '',
		'exclude'          => '',
		'meta_key'         => '',
		'meta_value'       => '',
		'post_type'        => 'business',
		'post_mime_type'   => '',
		'post_parent'      => '',
		'author'	   => '',
		'author_name'	   => '',
		'post_status'      => 'publish',
		'suppress_filters' => true,
		'fields'           => ''
	);
	$posts_array = get_posts( $args );
	$count = 0;
	foreach($posts_array as $get_posts_array)
	{
		echo "<tr>";
		echo "<td>".$get_posts_array->post_title."</td>";
		echo "<td>".get_post_meta($get_posts_array->ID,'address',true)."</td>";
		echo "<td>".get_post_meta($get_posts_array->ID,'country',true)."</td>";
	?>
		<td>
			<button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#toModal-<?php echo $count; ?>'>View Contact</button>
			<!-- view contact -->
			 <div class="modal fade" id="toModal-<?php echo $count ?>" role="dialog" style="z-index:99999">
			    <div class="modal-dialog">
				      <!-- Modal content-->
				      <div class="modal-content">
				        <div class="modal-header popup_table">
				        	<button type="button" class="close" data-dismiss="modal">&times;</button>
				          	<h3 class="modal-title">Contact Details</h3>
				          	<div class="modal-body">
				          		<div>
				          			<table>
				          				<tr>
				          					<th>Name</th>
				          					<td><?php echo get_post_meta($get_posts_array->ID,'contact_name',true); ?></td>
				          				</tr>
				          				<tr>
				          					<th>Email</th>
				          					<td><?php echo get_post_meta($get_posts_array->ID,'contact_email',true); ?></td>
				          				</tr>
				          				<tr>
				          					<th>Name</th>
				          					<td><?php echo get_post_meta($get_posts_array->ID,'contact_phone',true); ?></td>
				          				</tr>
				          			</table>
				        	</div>
				      </div>
				    </div>
				</div>
			</div>
			<!-- end contact -->
		</td>
		<td><a href="<?php echo site_url(); ?>/reseller-area/manage-details?id=<?php echo $get_posts_array->ID ?>" class="btn btn-primary">Manage</a></td>
	<?php
		echo "</tr>";
	$count++;
	}
	echo "</table>";
	echo "</form>";
	return ob_get_clean();

}
add_shortcode('show_business','raa_show_all_business');


//show manage details
function show_manage_details()
{
	if(!empty($_REQUEST['id']))
	{
	$ID 		      =  $_REQUEST['id'];
	$curr_user_id     =  get_current_user_id();
	$user_meta        =  get_userdata($curr_user_id);
	$user_roles       =  $user_meta->roles;
	if($user_roles[0] == 'reseller')
	{
?>
<div class="container">
	<div class="text-right"><button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#userModal'>Add New User</button>  <button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#csvModal'>Bulk Upload(CSV)</button></div>
 
 	<!-- modal for adding user -->
 	<div class="modal fade" id="userModal" role="dialog" style="z-index:99999">
		<div class="modal-dialog">
		<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" id="heading">
				    <button type="button" class="close" data-dismiss="modal">&times;</button>
				    <h3 class="modal-title">Add New User</h3>
				</div>
				<div class="modal-body">
					<form method="post" id="userform">
						<input type="hidden" id="reseller_id"  value="<?php  echo $_REQUEST['id'] ?>" />
						<input type="hidden" id="curr_user_id" value="<?php  echo $curr_user_id; ?>" />
						<div class="form-group">
							<label for="email">First Name</label>
							<input type="text" class="form-control" id="first_name" placeholder="First Name" name="first_name"  required />
						</div>
						<div class="form-group">
							<label for="pwd">Last Name:</label>
							<input type="text" class="form-control" id="last_name" placeholder="Last Name" name="last_name" required />
						</div>
						<div class="form-group">
							<label for="pwd">Email:</label>
							<input type="email" class="form-control" id="email" placeholder="Email" name="email" required />
						</div>
						<div class="form-group">
							<label for="pwd">Password:</label>
							<input type="password" name="password" id="password" class="form-control" placeholder="Password" required />
						</div>
						<div class="form-group">
							<label for="pwd">Select Subscription:</label><br/>
							<?php
							$subscriptions = get_posts( array(
								'meta_value'  =>$curr_user_id,
								'numberposts' => 1,
								'post_type'   => 'shop_subscription', // Subscription post type
								'post_status' => array('wc-active','wc-pending'), // Active subscription
								'orderby'     => 'post_date', // ordered by date
								'order'       => 'DESC',
								'date_query' => array( // Start & end date
													array(
										                'after'     => $from_date,
										                'before'    => $to_date,
										                'inclusive' => true,
										            ),
										        ),
										    ) );
									foreach ( $subscriptions as $subscription ) {
									$subscription_id = $subscription->ID; // subscription ID
									$order = wc_get_order( $subscription_id );
									$items = $order->get_items();
									foreach($items as $item)
									{
									       	$product_name = $item->get_name();
									    	$product_id = $item->get_product_id();
								    ?>
								    <input type="radio" name="subscription" id="subscription" value="<?php echo $product_id ?>"  /><?php echo $product_name; ?>
								    <?php
								      		}
								  		}
								      ?>
						</div>
						<div class="form-group">
							<label for="pwd">Mobile Number:</label>
							<input type="text" name="mobile_number" id="mobile_number" class="form-control" placeholder="Mobile Number"  required />
						</div>
							<input type="submit" name="button2" id="button2" value="Submit" class="btn btn-primary" />
							<!-- <button type="button" id="button2" class="btn btn-primary">Submit</button> -->
						<div id="get_response"></div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- modal for adding user end -->

	<!-- modal for adding csv user file -->
	 <div class="modal fade" id="csvModal" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" id="heading">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h3 class="modal-title">Import CSV File for Users</h3>
				</div>
				<div class="modal-body">
					<form method="post" enctype="multipart/form-data" id="file_form">
					    <div class="form-group">
					       	<p>Click on <a href="<?php echo site_url().'/C2ImportCalEventSample.csv'; ?>" download><strong>Follow the Link to upload csv</strong></a></p>
					    </div>
					    <div class="form-group">
							<label for="pwd">Select Subscription:</label>
							<?php
							$curr_user_id  = get_current_user_id();
							$subscriptions = get_posts( array(
								'meta_value'=>$curr_user_id,
								'numberposts' => 1,
								'post_type'   => 'shop_subscription', // Subscription post type
								'post_status' => array('wc-active','wc-pending'), // Active subscription
								'orderby' => 'post_date', // ordered by date
								'order' => 'DESC',
								'date_query' => array( // Start & end date
										          array(
										            'after'     => $from_date,
										             'before'    => $to_date,
										             'inclusive' => true,
										           ),
										        ),
										    ) );
										    foreach ( $subscriptions as $subscription ) {
									        $subscription_id = $subscription->ID; // subscription ID
									        $order = wc_get_order( $subscription_id );
									        $items = $order->get_items();
									       foreach($items as $item)
									       {
									       	$product_name = $item->get_name();
									    	$product_id = $item->get_product_id();
								      	?>
								      	<input type="radio" class="select_subscription" name="subscription" id="subscription" value="<?php echo $product_id ?>"  /><?php echo $product_name; ?>
								      	<?php
								      		}
								  		}
								      ?>
						</div>
					    <div class="form-group">
					        <label for="file">Upload CSV File</label>
					        <input type="file" id="upload_csv" name="uplaodfile" accept=".csv" required disabled="true" />
					        <input type="hidden" name="business_id" id="business_id" value="<?php echo $_REQUEST['id']; ?>">
					    </div>
					    <div class="form-group">
					        <input type="submit" name="upload_file" id="upload_file" value="Upload" disabled="true" />
					    	<div id="the_response"></div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- modal for adding user csv file end -->
  
 	<h2>User Management</h2>
 	<div><a class="btn btn-primary pull-right" href="<?php echo site_url(); ?>/reseller-area">Back To Reseller</a></div>
  	<ul class="nav nav-tabs">
    	<li class="active"><a data-toggle="tab" href="#home">Dashboard</a></li>
    	<li><a data-toggle="tab" href="#menu1" aria-expanded="true">Manage Users</a></li>
 	 </ul>

  	<div class="tab-content">
	    <div id="home" class="tab-pane fade in active">
			<div class="modal-header">
			    <table class="table_tag">
			      	<?php
			      	$args = array(
	    			'post__in' => array($_REQUEST['id']),
	    			'post_type'=> 'business'
					);
					$posts     = get_posts($args);
					foreach($posts as $show_posts)
					{
						$post_title = $show_posts->post_title;
						$user_data = get_userdata($show_posts->post_author);
					}
			      	?>
			      	<tr>
			      		<td>
			      			<p><strong>INSTITUTIONS</strong><br/>
			      				<?php echo $post_title; ?><br/>
			      			</p>
			      		</td>
			      		<td>
			      			<p><b>RESELLER ACCOUNT</b><br/>
			      				<?php echo $user_data->user_login; ?><br/>
			      			</p>
			      		</td>
			      	</tr>
			      	<tr>
			      		<td>
			      			<p><b>ADDRESS</b><br/>
			      				<?php echo get_post_meta($show_posts->ID,'address',true); ?><br/>
			      			</p>
			      		</td>
			      		<td>
			      			<p><b>COUNTRY</b><br/>
			      				<?php echo get_post_meta($show_posts->ID,'country',true); ?><br/>
			      			</p></td>
			      	</tr>
			    </table>
			</div>
	    </div>
	    <div id="menu1" class="tab-pane fade">
			<div class="modal-header">
			    <table class="table_tag">
			      	<tr>
			      		<th>Status</th>
			      		<th>Subscription</th>
			      		<th>Items</th>	      		
			      		<th>Total</th>
			      		<th>Start Date</th>
			      	</tr>
			      	<?php
			      	global $wpdb;
			      	$business_id = $_REQUEST['id'];
			      	$logged_user  = get_current_user_id();
			      	$args = array(
				        'meta_query'   =>
				            array(
				                'relation' => 'AND',

				            array(
				                'key' => 'reseller_id',
				                'value' => $business_id,
				                'compare' => "=",
				                'type' => 'numeric'
				            ),
				            array(
				                'key' => 'curr_user_id',
				                'value' =>  $logged_user,
				                'compare' => "=",
				                'type' => 'numeric'
				            )
				         )
				    );
				    $users = get_users( $args );
				    foreach($users as $get_users)
				    {
				    	$single_user_id = $get_users->ID;
				    	$subscriptions = get_posts( array(
					    	'meta_key'=>'_customer_user',
					    	'meta_value'=>$single_user_id,
					        'numberposts' => -1,
					        'post_type'   => 'shop_subscription', // Subscription post type
					        'post_status' => array('wc-active','wc-pending','wc-cancelled','wc-pending-cancel','wc-on-hold','wc-pending-cancel'), // Active subscription
					        'orderby' => 'post_date', // ordered by date
					        'order' => 'ASC',
					        'date_query' => array( // Start & end date
					            array(
					                'after'     => $from_date,
					                'before'    => $to_date,
					                'inclusive' => true,
					            ),
					        ),
					    ) );
					    foreach ( $subscriptions as $subscription ) 
					    {				    	
					    	$order = wc_get_order( $subscription->ID );
					        $items = $order->get_items();
					        $product_name = array();
					        foreach($items as $item)
					        {
					       		$product_name[] = $item->get_name();
					        }
					?>
			      	<tr>
			      		<?php if($subscription->post_status == 'wc-completed' || $subscription->post_status == 'wc-active' ){ ?>
			      		<td><span class="label label-success">Completed</span></td>
			      		<?php } elseif($subscription->post_status == 'wc-cancelled' || $subscription->post_status == 'wc-pending-cancel') {?>
			      		<td><span class="label label-default">Cancelled</span></td>
			      		<?php }else { ?>
			      		<td><span class="label label-default">Pending</span></td>
			      		<?php } ?>
			      		<td><?php echo '#'.$subscription->ID.' for '.$get_users->user_email.' '.get_post_meta($subscription->ID,'_billing_last_name',true)  ?></td>
			      		<td><?php echo implode(',', $product_name); ?></td>
			      		<td><?php echo '$'.get_post_meta($subscription->ID,'_order_total',true).' / '.get_post_meta($subscription->ID,'_billing_period',true); ?></td>
			      		<td><?php echo date('Y-m-d',strtotime($subscription->post_date)); ?></td>
			      		<!-- comment delete row <td><span class='user_delete' id='del_<?php echo $get_users->ID; ?>'><i class="fa fa-remove"></i></span></td> -->
			      	</tr>
			      	<?php
			      		}
			     	}
			      	?>
			    </table>
	  		</div>
	    </div>
</div>
</div>
<?php
	}
	else{echo "<p class='head_text'>To see the Affiliate Area, log in as an existing affiliate, or add your account as an affiliate.</p>";}
}
}
add_shortcode('get_users','show_manage_details');