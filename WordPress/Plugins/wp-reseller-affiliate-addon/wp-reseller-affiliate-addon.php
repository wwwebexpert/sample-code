<?php
/*
Plugin Name: WP Reseller Affiliate AddOn
Plugin URI: https://google.com/
Description: Enhance Affliate plugin functionality. Use shortcode <strong>[show_business]</strong> to add business / school. Create page with slug "manage-details" add shortcode <strong>[get_users]</strong> to show user manage dashboard. Use shortcode <strong>[show_the_subscription]</strong> to show subscription plan.
Author: Mobilyte
Version: 1.3.5
Author URI: https://google.com/
*/
//error_reporting(E_ALL);
define('RAA_PLUGIN_URL', plugins_url('', __FILE__));
include('add-business-form.php');
include('reseller-int-functions.php');
//include('show-form.php');
/*--function to add script--*/
function wp_reseller_scripts() {
    wp_enqueue_style( 'my_custom_css', RAA_PLUGIN_URL . '/assets/css/custom.css' );
    wp_enqueue_style( 'my_dataTables_css', RAA_PLUGIN_URL . '/assets/css/bootstrap.min.css' );
    wp_enqueue_script( 'my_dataTables_script', RAA_PLUGIN_URL . '/assets/js/jquery.min.js' );
    wp_enqueue_script( 'my_validate_script', RAA_PLUGIN_URL . '/assets/js/jquery.validate.min.js' );
    wp_enqueue_script( 'my_script_lib', RAA_PLUGIN_URL . '/assets/js/bootstrap.min.js' );
    wp_enqueue_script( 'my_custom_validatescript', RAA_PLUGIN_URL . '/assets/js/countries.js' );
    wp_enqueue_script( 'my_script_custom', RAA_PLUGIN_URL . '/assets/js/my_script_custom.js' );
}
add_action('wp_enqueue_scripts', 'wp_reseller_scripts');