<?php
/* Plugin Name: Seek Plugin
Plugin URI: https://www.mobilyte.com
Description: Seek Plugin for testing
Author: Manoj Sharma
Version: 1.0
Author URI: https://www.mobilyte.com */

//Include file in plugin
define( 'MY_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );

include( MY_PLUGIN_PATH . 'inc/mobilyte-functions.php');

//is_admin functionality
if ( is_admin() ) {
    //include_once( plugin_dir_path( __FILE__ ) . 'includes/admin-functions.php' );
} else {
    //include_once( plugin_dir_path( __FILE__ ) . 'includes/front-end-functions.php' );
}

//Add script in plugin
add_action('wp_enqueue_scripts', 'fwds_scripts');

function fwds_scripts() {

	//wp_enqueue_script('jquery');

	wp_register_script('mobilyte_jquery', plugins_url('js/mobilyte-jquery.js', __FILE__),array("jquery"));
	wp_enqueue_script('mobilyte_jquery');
}

register_activation_hook(__FILE__, 'fwds_slider_activation');

//Plugin deactivation
function fwds_slider_deactivation() {
	
}

register_deactivation_hook(__FILE__, 'fwds_slider_deactivation');

add_shortcode("seek_product", "my_shortcode_function");

function my_shortcode_function() {

	global $woocommerce, $post, $product;

	/*Getting attributes taxonomy*/

	$args = array(
    'hide_empty' => false
	);
	$terms = get_terms( 'pa_download-quality', $args );
	
	/*Setting up variation values*/

	$variations = array();

	foreach($terms as $key => $value){
		
		$variations[$key]['attributes']= array('download-quality' => $value->name);
		$variations[$key]['sku'] = $value->slug;
		if( $variations[$key]['sku'] == 'land007-c' ){
			$variations[$key]['price'] = $value->description;
		}elseif ( $variations[$key]['sku'] == 'land005-c' ) {
			$variations[$key]['price'] = $value->description;
		}else{
			$variations[$key]['credits_amount'] = $value->description;
		}
	}

	/*Adding/Updating product code*/

	if( isset( $_POST['add_product'] ) ){
		$post_status 		= isset($_POST['post_status']) ? $_POST['post_status'] : $_POST['hidden_post_status'];
		$post_visibility 	= isset($_POST['visibility']) ? $_POST['visibility'] : $_POST['hidden_post_visibility'];
		$product_visibility = isset($_POST['_visibility']) ? $_POST['_visibility'] : $_POST['current_visibility'];
		$product_featured 	= isset($_POST['_featured']) ? "yes" : "no";

		if($post_visibility == "password"){
			$post_password 	= $_POST['post_password'];
		}else{
			$post_password 	= "";
		}


		$post_month 	= isset($_POST['publish_on']) ? $_POST['publish_on'] : "" ;
		$post_day 		= isset($_POST['pub_day']) ? $_POST['pub_day'] : "" ;
		$post_year		= isset($_POST['pub_year']) ? $_POST['pub_year'] : "" ;
		$post_hour 		= isset($_POST['pub_hour']) ? $_POST['pub_hour'] : "" ;
		$post_minute 	= isset($_POST['pub_minute']) ? $_POST['pub_minute'] : "" ;
		$post_second 	= isset($_POST['ss']) ? $_POST['ss'] : "" ;

		if(empty($post_month) || empty($post_day) || empty($post_year) || empty($post_hour) || empty($post_minute) || empty($post_second)){
			$post_month		=	$_POST['cur_mm'] ;
			$post_day		=	$_POST['cur_jj'] ;
			$post_year		=	$_POST['cur_aa'] ;
			$post_hour		=	$_POST['cur_hh'] ;
			$post_minute	=	$_POST['cur_mn'] ;
			$post_second	=	$_POST['ss'];
		}

		$current_date = $_POST['cur_aa'].'-'.$_POST['cur_mm'].'-'.$_POST['cur_jj'].' '.$_POST['cur_hh'].':'.$_POST['cur_mn'].':'.$_POST['ss'];



		$post_date = $post_year.'-'.$post_month.'-'.$post_day.' '.$post_hour.':'.$post_minute.':'.$post_second;
		
		$seconds = strtotime($current_date) - strtotime($post_date);
		if($seconds < 0){
			$post_status = 'future';
		}
		if($post_visibility == 'private'){
			$post_status = 'private';
		}

		$product_sku = str_replace(' ','_',substr(strtolower($_POST['product_name']), 0,9));

		/*Setting up product attribues and it's variation in json encoded form*/

		$product_data = '[
						    {
						        "name"        : "'.$_POST['product_name'].'",
						        "sku"         : "'.$product_sku.'",
						        "description" : "'.$_POST['product_description'].'",
						        "available_attributes": [
						            "download-quality"
						        ],
						        "variations":'.json_encode($variations).'
						    } 
						]';
						
		/*Decoding product json data and inserting product*/

		$product_data_arr = json_decode($product_data);

		

		if(!empty($_POST['update_product'])){
			$post_id = $_POST['update_product'];

			$post = array( 
			        'ID'  			=> $post_id,
			        'post_content' 	=> $product_data_arr[0]->description,
			        'post_title'   	=> $product_data_arr[0]->name,
			        'post_parent'  	=> '',
			        'post_status'  	=> $post_status,
			        'post_date'  	=> $post_date,
			        'post_modified'	=> $current_date,
			        'post_password'	=> $post_password,
			        'post_type'    	=> 'product'
			    );
			// echo '<pre>';print_r($post);die;
		    wp_update_post($post);

		    if (!$post_id)
		    {
		        return false;
		    }

		    /*Setting up product as variable product*/

		    wp_set_object_terms($post_id, 'variable', 'product_type');

		    /*End*/

		    /*Updating watermark Image*/

			$watermark = basename($_FILES['watermark_img']['name']);
			if(!empty($watermark)){
				$meta_watermark = '<img src="'.get_site_url().'/wp-content/uploads/2018/07/'.$watermark.'" width="100%">';
				$target_file = ABSPATH . 'wp-content/uploads/2018/07/' . basename($watermark);

				if(move_uploaded_file($_FILES["watermark_img"]["tmp_name"], $target_file)){
					$watermar_meta_value = get_post_meta($post_id,'watermark');
					if(!empty($watermar_meta_value)){
						update_post_meta( $post_id, 'watermark', $meta_watermark );
					}else{
						add_post_meta( $post_id, 'watermark', $meta_watermark );
					}
				}
			}
			/*End*/

			/*Uploading and building variation values array*/

			$fileForVariations = array();
			$fileForVariations[] = $_FILES['fileForOriginal'];
			$fileForVariations[] = $_FILES['fileForOriginal'];
			$fileForVariations[] = $_FILES['watermark_img'];
			$fileForVariations[] = $_FILES['fileForSmall'];
			$fileForVariations[] = $_FILES['fileForMedium'];
			$fileForVariations[] = $_FILES['fileForLarge'];
			$fileForVariations[] = $_FILES['fileForOriginal'];

			$i = 0;
			foreach ($fileForVariations as $variationfileName) {

				$filename = $variationfileName['name'];
				$filetmpname = $variationfileName['tmp_name'];
				$target_fileForVariations = ABSPATH . 'wp-content/uploads/2018/07/' . basename($filename);

				if(!empty($filename)){
					$file_url  = get_site_url().'/wp-content/uploads/2018/07/' . basename($filename);
	        		$md5_num = md5( $file_url );
	        		move_uploaded_file($filetmpname, $target_fileForVariations);
				}else{
					$filename = "";
					$file_url  = "";
	        		$md5_num = md5( $file_url );
				}


				$abe_file[$i][$md5_num] = array(
			        'name'   =>  $filename,
			        'file'   =>  $file_url
			    );

				$i++;
			}

			/*End*/

		    /*Function for updating product variations*/
		    update_product_variations($post_id,$abe_file);

		    /*End*/

		    /*Success msg*/

		    echo '<div class="success_msg"><p>Product has been updated successfuly!!</p></div>';

		    /*End*/

		}else{
			$post = array( 
			        'post_author'  => get_current_user_id(),
			        'post_content' => $product_data_arr[0]->description,
			        'post_status'  	=> $post_status,
			        'post_date'  	=> $post_date,
			        'post_modified'	=> $current_date,
			        'post_title'   => $product_data_arr[0]->name,
			        'post_password'	=> $post_password,
			        'post_parent'  => '',
			        'post_type'    => 'product'
			    );
		  	$post_id = wp_insert_post($post);

		  	if (!$post_id)
		    {
		        return false;
		    }

		    /*Setting up product as variable product*/

		    wp_set_object_terms($post_id, 'variable', 'product_type');

		    /*End*/

		    /*Inserting watermark Image*/

			$watermark = basename($_FILES['watermark_img']['name']);
			if(!empty($watermark)){
				$meta_watermark = '<img src="'.get_site_url().'/wp-content/uploads/2018/07/'.$watermark.'" width="100%">';
				$target_file = ABSPATH . 'wp-content/uploads/2018/07/' . basename($watermark);

				if(move_uploaded_file($_FILES["watermark_img"]["tmp_name"], $target_file)){
					add_post_meta( $post_id, 'watermark', $meta_watermark );
				}
			}

			/*End*/

			/*Uploading and building variation values array*/

			$fileForVariations = array();
			$fileForVariations[] = $_FILES['fileForOriginal'];
			$fileForVariations[] = $_FILES['fileForOriginal'];
			$fileForVariations[] = $_FILES['watermark_img'];
			$fileForVariations[] = $_FILES['fileForSmall'];
			$fileForVariations[] = $_FILES['fileForMedium'];
			$fileForVariations[] = $_FILES['fileForLarge'];
			$fileForVariations[] = $_FILES['fileForOriginal'];

			$i = 0;
			foreach ($fileForVariations as $variationfileName) {

				$filename = $variationfileName['name'];
				$filetmpname = $variationfileName['tmp_name'];
				$target_fileForVariations = ABSPATH . 'wp-content/uploads/2018/07/' . basename($filename);

				if(!empty($filename)){
					$file_url  = get_site_url().'/wp-content/uploads/2018/07/' . basename($filename);
	        		$md5_num = md5( $file_url );
	        		move_uploaded_file($filetmpname, $target_fileForVariations);
				}else{
					$filename = "";
					$file_url  = "";
	        		$md5_num = md5( $file_url );
				}


				$abe_file[$i][$md5_num] = array(
			        'name'   =>  $filename,
			        'file'   =>  $file_url
			    );

				$i++;
			}

			/*End*/

		    /*Function for inserting product attributes*/

		    insert_product_attributes($post_id, $product_data_arr[0]->available_attributes, $product_data_arr[0]->variations);

		    /*End*/

		    /*Function for inserting product variations*/
		    
		    insert_product_variations($post_id, $product_data_arr[0]->variations,$abe_file);

		    /*End*/

		    /*Success msg*/

		    echo '<div class="success_msg"><p>Product has been added successfuly!!</p></div>';

		    /*End*/
		}


		/*Adding product sku and setting product visible*/

	    update_post_meta($post_id, '_sku', $product_data_arr[0]->sku); 

	    if($product_visibility == 'hidden'){
	    	//die('hidden');
	    	$terms = array( 'exclude-from-catalog', 'exclude-from-search' );
			wp_set_object_terms( $post_id, $terms, 'product_visibility' );
	    }elseif($product_visibility == 'catalog'){
	    	//die('catalog');
	    	$terms = 'exclude-from-search';
			wp_set_object_terms( $post_id, $terms, 'product_visibility' );
	    }elseif($product_visibility == 'search'){
	    	//die('search');
	    	$terms = 'exclude-from-catalog';
			wp_set_object_terms( $post_id, $terms, 'product_visibility' );
	    }elseif ($product_visibility == 'visible') {
	    	$terms = '';
			wp_set_object_terms( $post_id, $terms, 'product_visibility' );
	    }

	    if($product_featured == 'yes'){
	    	$terms = 'featured';
			wp_set_object_terms( $post_id, $terms, 'product_visibility',true );
	    }

	    

	    // update_post_meta( $post_id,'_visibility',$product_visibility); 
	    update_post_meta( $post_id,'_visibility','visible'); 
	    //update_post_meta( $post_id, '_featured', $product_featured );

	    /*End*/

	    /*Adding product tag*/

	    $product_tag = $_POST['product_tag'];

		foreach ($product_tag as $key=>$tag) {
			if(is_null($tag) || $tag == ''){
				unset($product_tag[$key]);
			}
		}
		wp_set_object_terms($post_id, $product_tag, 'product_tag');

		/*End*/

		/*Adding product categories*/

		$product_cat = $_POST['product_category'];
		wp_set_object_terms($post_id, $product_cat, 'product_cat');

		/*end*/

		/*Inserting featured image*/

		$product_featured_img = $_FILES['fileForOriginal']['name'];
		$upload_dir       = ABSPATH . 'wp-content/uploads/2018/07';
		$file = $upload_dir . '/' . $product_featured_img;
		$product_featured = $_FILES['fileForOriginal'];

		$attachment = array(
	        'post_mime_type' => $product_featured['type'],
	        'post_title'     => sanitize_file_name( $product_featured_img ),
	        'post_content'   => '',
	        'post_status'    => 'inherit'
	    );
	    // Create the attachment
	    $attach_id = wp_insert_attachment( $attachment, $file, $post_id );

	    require_once(ABSPATH . 'wp-admin/includes/image.php');

	    // Define attachment metadata
	    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );

	    // Assign metadata to attachment
	    wp_update_attachment_metadata( $attach_id, $attach_data );

	    // And finally assign featured image to post
	    set_post_thumbnail( $post_id, $attach_id );

		/*
		//vendors insert


		global $wpdb;
		$table = 'wp_term_relationships';
		$data = array('object_id' => $post_id, 'term_taxonomy_id' => 373, 'term_order' => 0);
		$format = array('%s','%d');
		$wpdb->insert($table,$data,$format);
		*/
	}
?>
<!doctype html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Product Form</title>
		<!-- <link rel="stylesheet" type="text/css" href="<?php echo plugin_dir_url( __FILE__ ) ?>css/normalize.css" /> -->
		<!-- <link rel="stylesheet" type="text/css" href="<?php echo plugin_dir_url( __FILE__ ) ?>css/demo.css" /> -->
		<!-- <link rel="stylesheet" type="text/css" href="<?php echo plugin_dir_url( __FILE__ ) ?>css/component.css" /> -->
		<link rel="stylesheet" type="text/css" href="<?php echo plugin_dir_url( __FILE__ ) ?>css/mobilyte-css.css" />
		<style type="text/css">
			.tags_col{
			  display: inline-block;
			  width: 50%;
			}
			.cat_col{
			  display: inline-block;
			  vertical-align: top;
			}
		</style>
	</head>

	<body>
		<div>
			<?php
			if($_GET['pid']){

				global $wpdb;
				$table = 'wp_posts';
				$p_id = $_GET['pid'];

				$data = $wpdb->get_row("SELECT post_date,post_password  FROM wp_posts WHERE ID =$p_id ");
				//echo $data->last_query();
				$post_date 		= $data->post_date;
				$post_password 	= $data->post_password;

				$re_date 	= date($post_date);
			 	$re_time 	= strtotime($re_date);
				
				$re_year 	= date('Y', $re_time);
				$re_month	= date('m', $re_time);
				$re_day 	= date('d', $re_time);
				$re_hour 	= date('h', $re_time);
				$re_minute 	= date('i', $re_time);
				$re_second 	= date('s', $re_time);


				$params = array(
								'p' 		=> $p_id,
								'post_type' => 'product'
								);
				$wc_query 	= new WP_Query($params);
				$product 	= wc_get_product( $p_id );
				$p_featured = $product->featured;

				$p_catalog_visibility = $product->catalog_visibility;

				$product_status = get_post_status($p_id);

				if( $wc_query->have_posts() ){
					while( $wc_query->have_posts() ){

						$wc_query->the_post();
						$p_title 	= get_the_title();
						$p_desc 	= get_the_content();

					}
				}

				$tags = get_the_terms( $p_id, 'product_tag' );
				$categories = get_the_terms( $p_id, 'product_cat' );
				$watermark_img = get_post_meta($p_id,'watermark');
				$img = $watermark_img[0];
				preg_match( '@src="([^"]+)"@' , $img, $match );
				$watermark_img_src = array_pop($match);
				$product = new WC_Product_Variable( $p_id );
				$variations = $product->get_available_variations();
				// echo '<pre>'; print_r($variations);die;
                $imgData = array();
                $allImagesData = array();
				foreach ($variations as $key=>$value) {
				 	//echo $key;
				 	foreach($value['downloads'] as $key1=>$val){
				 		$imgData[$val->get_id()] =  $val->get_file();
				 		$allImagesData[] 	= $val->get_file();		 		
				 		$allImagesNames[] 	= $val->get_name();		 		
				 		
				 	}
				}
				// echo '<pre>'; print_r($allImagesNames);
			}
			?>
<form id="product_addition_form" method="post" action="#" enctype="multipart/form-data">

	<div class="vc_general vc_tta vc_tta-tabs vc_tta-color-grey vc_tta-style-classic vc_tta-shape-rounded vc_tta-spacing-1 vc_tta-tabs-position-top vc_tta-controls-align-left">
		<div class="vc_tta-tabs-container">
			<ul class="vc_tta-tabs-list">
				<li class="vc_tta-tab vc_active" data-vc-tab="">
					<a href="#1528594831971-25a00175-fe50" data-vc-tabs="" data-vc-container=".vc_tta"><span class="vc_tta-title-text">Publishing</span></a>
				</li>
				<li class="vc_tta-tab" data-vc-tab="">
					<a href="#1528594773921-5b5b5c22-9ab5" data-vc-tabs="" data-vc-container=".vc_tta"><span class="vc_tta-title-text">Item Information</span></a>
				</li>
				<li class="vc_tta-tab" data-vc-tab="">
					<a href="#1528594773925-1208abc8-94c2" data-vc-tabs="" data-vc-container=".vc_tta"><span class="vc_tta-title-text">Uploads</span></a>
				</li>
			</ul>
		</div>
		<div class="vc_tta-panels-container">
			<div class="vc_tta-panels">
				<div class="vc_tta-panel vc_active" id="1528594831971-25a00175-fe50" data-vc-content=".vc_tta-panel-body">
					<div class="vc_tta-panel-heading">
						
						<h4 class="vc_tta-panel-title">
							<a href="#1528594831971-25a00175-fe50" data-vc-accordion="" data-vc-container=".vc_tta-container"><span class="vc_tta-title-text">Publishing</span></a>
						</h4>
					</div>
					<div class="vc_tta-panel-body">	
						<div class=" vc_custom_1528595242297">
							<div id="text-block-5" class="mk-text-block">
								<p>Set the status of your Photo.</p>
								<section class="sp-publishing">
									<h1>Publishing</h1>

									We can use the Publish Options, same as back-end Post<br />
									<br />

									<div class="post-status-options">
										<input name="hidden_post_status" id="hidden_post_status" value="draft" type="hidden">

										Status: <span class="current_status"><?php if($product_status == 'private'){echo "Privately published";}elseif(!empty($product_status)){echo ucfirst($product_status);}else{ echo "Draft";}?></span> <a id="edit_status" href="#post_status">Edit</a><br />
										<div class="post_status">
											<select class="p_status" name="post_status">
												<option value="draft" <?php if($product_status == 'draft'){echo "selected";} ?>>Draft</option>
												<option value="pending" <?php if($product_status == 'pending'){echo "selected";} ?>>Pending Review</option>
												<option value="publish" <?php if($product_status == 'publish' || $product_status == 'private'){echo "selected";} ?>>Published
												</option>
											</select>
											<button id="set_status">Ok</button>
											<a id="cancel_status" href="#post_status">Cancel</a>
										</div>
									</div>


									<div class="post-visibility-options">

										<input name="hidden_post_password" id="hidden-post-password" value="" type="hidden">
										<input name="hidden_post_visibility" id="hidden-post-visibility" value="public" type="hidden">
										
									 	Visibility: <span class="current_visibility"><?php if($product_status == 'publish'){echo "Public";}elseif($product_status == 'private'){echo "Private";}else{echo "Public";}?></span> <a id="edit_visibility" href="#visibility">Edit</a><br />

									 	<div class="post_visibility">
											<input class="visibility-radio" name="visibility" id="visibility-radio-public" value="public" checked="" type="radio"> 
											<label for="visibility-radio-public" class="selectit">Public</label>
											<br>

											<input name="visibility" class="visibility-radio" id="visibility-radio-password" value="password" type="radio" <?php if(!empty($post_password)){echo "checked";} ?>> 
											<label for="visibility-radio-password" class="selectit">Password protected</label>
											<br>
											<span id="password-span" style="display: none;">
												<label for="post_password">Password:</label> 
												<input name="post_password" id="post_password" value="<?php if(!empty($post_password)){echo $post_password;} ?>" maxlength="255" type="text">
												<br>
											</span>

											<input name="visibility" class="visibility-radio" id="visibility-radio-private" value="private" type="radio" <?php if($product_status == "private"){echo "checked";} ?>> 
											<label for="visibility-radio-private" class="selectit">Private</label>
											<br>

											<button id="set_visibility">Ok</button>
											<a id="cancel_visibility" href="#visibility">Cancel</a>

									 	</div>
									</div>

									<div class="post-time-options">

									 	<?php
										 	$date = date("Y/m/d h:i:sa");
										 	$time = strtotime($date);
											
											$year = date('Y', $time);
											$month= date('m', $time);
											$day = date('d', $time);
											$hour = date('h', $time);
											$minute = date('i', $time);
											$second = date('s', $time);

										?>


										<input id="ss" name="ss" value="<?=$second?>" type="hidden">
									 	<input id="hidden_mm" name="hidden_mm" value="<?=$month?>" type="hidden">
										<input id="cur_mm" name="cur_mm" value="<?=$month?>" type="hidden">
										<input id="hidden_jj" name="hidden_jj" value="<?=$day?>" type="hidden">
										<input id="cur_jj" name="cur_jj" value="<?=$day?>" type="hidden">
										<input id="hidden_aa" name="hidden_aa" value="<?=$year?>" type="hidden">
										<input id="cur_aa" name="cur_aa" value="<?=$year?>" type="hidden">
										<input id="hidden_hh" name="hidden_hh" value="<?=$hour?>" type="hidden">
										<input id="cur_hh" name="cur_hh" value="<?=$hour?>" type="hidden">
										<input id="hidden_mn" name="hidden_mn" value="<?=$minute?>" type="hidden">
										<input id="cur_mn" name="cur_mn" value="<?=$minute?>" type="hidden">

									 	<p class="post_pub_on">
									 		<span class="pub_or_schd">Published on : </span> 
									 		<span class="current_pub_on">
									 		<?php if(!empty($post_date)){
									 			echo $re_month.' '.$re_day.','.$re_year.' @ '.$re_hour.':'.$re_minute;
									 		}else{?>
									 			immediately
									 		<?php
									 		}?>
									 		</span> 
									 		<a id="edit_publishing" href="#edit_timestamp">Edit</a>
									 	</p>

									 	<div class="post_publishing">
									 		<select class="pub_on" name="publish_on">
									 			<option value="01" data-text="Jan" <?php if($re_month == '01'){echo "selected";}?>>01-Jan</option>
												<option value="02" data-text="Feb" <?php if($re_month == '02'){echo "selected";}?>>02-Feb</option>
												<option value="03" data-text="Mar" <?php if($re_month == '03'){echo "selected";}?>>03-Mar</option>
												<option value="04" data-text="Apr" <?php if($re_month == '04'){echo "selected";}?>>04-Apr</option>
												<option value="05" data-text="May" <?php if($re_month == '05'){echo "selected";}?>>05-May</option>
												<option value="06" data-text="Jun" <?php if($re_month == '06'){echo "selected";}?>>06-Jun</option>
												<option value="07" data-text="Jul" <?php if($re_month == '07'){echo "selected";}?>>07-Jul</option>
												<option value="08" data-text="Aug" <?php if($re_month == '08'){echo "selected";}?>>08-Aug</option>
												<option value="09" data-text="Sep" <?php if($re_month == '09'){echo "selected";}?>>09-Sep</option>
												<option value="10" data-text="Oct" <?php if($re_month == '10'){echo "selected";}?>>10-Oct</option>
												<option value="11" data-text="Nov" <?php if($re_month == '11'){echo "selected";}?>>11-Nov</option>
												<option value="12" data-text="Dec" <?php if($re_month == '12'){echo "selected";}?>>12-Dec</option>
									 		</select>
									 		<input type="text" class="pub_day" name="pub_day" value="<?php if($re_day){echo $re_day;} ?>" maxlength="2">
									 		<input type="text" class="pub_year" name="pub_year" value="<?php if($re_year){echo $re_year;} ?>" maxlength="4" minlength="3">
									 		@
									 		<input type="text" class="pub_hour" name="pub_hour" value="<?php if($re_hour){echo $re_hour;} ?>" maxlength="2">
									 		<input type="text" class="pub_minute" name="pub_minute" value="<?php if($re_minute){echo $re_minute;} ?>" maxlength="2"><br />

									 		<button id="publish">Ok</button>
											<a id="cancel_publishing" href="#edit_timestamp">Cancel</a>
									 	</div>

									</div>

									<div class="post-catalog-options">

									 	<input name="current_visibility" id="current_visibility" value="visible" type="hidden">
										<input name="current_featured" id="current_featured" value="no" type="hidden">

										Catalog visibility: <span class="current_catalog"><?php if(!empty($p_catalog_visibility)){echo $p_catalog_visibility;}else{echo "Visible";} ?></span> <a id="edit_catalog" href="#catalog-visibility">Edit</a><br />

										<div class="catalog_visibility">
											<p>Choose where this product should be displayed in your catalog. The product will always be accessible directly.</p>

											<input name="_visibility" class="_catalog_visibility" id="_visibility_visible" value="visible" data-label="Visible" checked="checked" type="radio"> 
											<label for="_visibility_visible" class="selectit">Visible</label><br>

											<input name="_visibility" class="_catalog_visibility" id="_visibility_catalog" value="catalog" data-label="Catalog" <?php if($p_catalog_visibility == 'catalog'){echo 'checked';} ?> type="radio"> 
											<label for="_visibility_catalog" class="selectit">Catalog</label><br>

											<input name="_visibility" class="_catalog_visibility" id="_visibility_search" value="search" data-label="Search" <?php if($p_catalog_visibility == 'search'){echo 'checked';} ?> type="radio"> 
											<label for="_visibility_search" class="selectit">Search</label><br>

											<input name="_visibility" class="_catalog_visibility" id="_visibility_hidden" value="hidden" data-label="Hidden" <?php if($p_catalog_visibility == 'hidden'){echo 'checked';} ?> type="radio"> 
											<label for="_visibility_hidden" class="selectit">Hidden</label><br>

											<p>Enable this option to feature this product.</p>

											<input name="_featured" id="_featured" type="checkbox" <?php if($p_featured == 'yes'){echo 'checked';} ?>> 
											<label for="_featured">Featured product</label><br />

											<button id="set_catalog">Ok</button>
											<a id="cancel_catalog" href="#catalog-visibility">Cancel</a>

										</div>
									</div>
									
								</section>
								<div class="clearboth"></div>
							</div>

						</div>
					</div>
				</div>
				<div class="vc_tta-panel" id="1528594773921-5b5b5c22-9ab5" data-vc-content=".vc_tta-panel-body">
					<div class="vc_tta-panel-heading">
						<h4 class="vc_tta-panel-title">
							<a href="#1528594773921-5b5b5c22-9ab5" data-vc-accordion="" data-vc-container=".vc_tta-container"><span class="vc_tta-title-text">Item Information</span></a>
						</h4>
					</div>
					<div class="vc_tta-panel-body">	
						<div class=" vc_custom_1528594980247">
							<div id="text-block-3" class="mk-text-block   ">
								<p>Use as many keywords as possible describing your creation.</p>
								<section class="sp-item">
									<input type="hidden" name="update_product" value="<?=$p_id?>">
									<input class="form-title" name ="product_name" value="<?php if(!empty($p_title)){echo $p_title;} ?>" placeholder="Enter your digital product name" type="text" /><br />
									<textarea  class="form-desc" name ="product_description" placeholder="describe your product"><?php if(!empty($p_desc)){echo $p_desc;} ?></textarea><br /><br /><br />
									<div class="tags_col">
										<div id="tags">
											<?php
											if(!empty($tags)){
												foreach ($tags as $tag) {?>
													<input class="form-tag" name ="product_tag[]" class="p_tag" value="<?php echo $tag->name; ?>" placeholder="tags" type="text" /><br />
												<?php
												}

											}else{
											?>
											<input class="form-tag" name ="product_tag[]" class="p_tag" value="" placeholder="tags" type="text" /><br />
											<?php
											}?>
										</div>
										<button class="form-add" id="more_tag">Add</button>
									</div>
									
									<div class="cat_col">
										<?php
										$terms = get_terms( array(
															    'taxonomy' => 'product_cat',
															    'hide_empty' => false,
															) );
										?>
										<label>Product Categories</label><br />
										<select name ="product_category[]" multiple="multiple">
											<?php foreach ($terms as $term) {?>
										  		<option value="<?=$term->name?>" <?php if( in_array($term, $categories) ){echo 'selected';}?>><?=$term->name?></option>
											<?php
											} ?>
										</select>
									</div>
								</section>

								<div class="clearboth"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="vc_tta-panel" id="1528594773925-1208abc8-94c2" data-vc-content=".vc_tta-panel-body">
					<div class="vc_tta-panel-heading">
						<h4 class="vc_tta-panel-title">
							<a href="#1528594773925-1208abc8-94c2" data-vc-accordion="" data-vc-container=".vc_tta-container"><span class="vc_tta-title-text">Uploads</span></a>
						</h4>
					</div>
					<div class="vc_tta-panel-body">	
						<div class=" vc_custom_1528595199160">
							<div id="text-block-4" class="mk-text-block   ">
								<p>Please avoid submitting super large files ( 50MB or less).</p>

								<section class="sp-uploads">
									<h1>Uploads</h1> - We need to make these forms Drag and Drop. <br/>
									<div>
										<input type="file" name="watermark_img" id="fileToUpload" /> Watermark 
										<?php
											if(!empty($watermark_img_src)){?>
												<input type="text" value="<?=$watermark_img_src?>">
											<?php
											}
										?>
									</div>
									<div>
										<input type="file" name="fileForSmall" id="fileToUpload1" /> Small 
										<?php
											if(!empty($allImagesData[3])){?>
												<input type="text" value="<?php echo !empty($allImagesData[3])? $allImagesData[3] : "";?>">
											<?php
											}
										?>
									</div>
									<div>
										<input type="file" name="fileForMedium" id="fileToUpload2" /> Medium 
										<?php
											if(!empty($allImagesData[4])){?>
												<input type="text" value="<?php echo !empty($allImagesData[4])? $allImagesData[4] : "";?>">
											<?php
											}
										?>
									</div>
									<div>
										<input type="file" name="fileForLarge" id="fileToUpload3" /> Large 
										<?php
											if(!empty($allImagesData[5])){?>
												<input type="text" value="<?php echo !empty($allImagesData[5])? $allImagesData[5] : "";?>">
											<?php
											}
										?>
									</div>
									<div>
										<input type="file" name="fileForOriginal" id="fileToUpload4" /> Original 
										<?php
											if(!empty($allImagesData[6])){?>
												<input type="text" value="<?php echo !empty($allImagesData[6])? $allImagesData[6] : "";?>">
											<?php
											}
										?>
									</div>
									<p>
										<input id="checkBox" name="six_mos" type="checkbox"> Commercial 6 months<br/>
										<input id="checkBox" name="twelve_mos" type="checkbox"> Commercial 12 months<br/>
									</p>
								</section>
								<input type="submit" name="add_product" value="Publish">
								<div class="clearboth"></div>
							</div>

						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</form>
</div>
		<!-- <script src="<?php echo plugin_dir_url( __FILE__ ) ?>js/mobilyte-jquery.js"></script> -->
		<script src="<?php echo plugin_dir_url( __FILE__ ) ?>js/custom-file-input.js"></script>
		<script src="<?php echo plugin_dir_url( __FILE__ ) ?>js/jquery.validate.js"></script>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/additional-methods.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery.validator.addMethod('filesize', function (value, element, param) {
				    return this.optional(element) || (element.files[0].size <= param)
				}, 'File size must be less');

				jQuery("#product_addition_form").validate({

		            rules: {
		                product_name: "required",
		                product_description: "required",
		                "product_tag[]": "required",
		                "product_category[]": "required",
		                /*day : {
		                	required: true,
          					number: true
		                },
		                year : {
		                	required: true,
          					digits: true
		                },
		                hour : {
		                	required: true,
          					digits: true
		                },
		                minute : {
		                	required: true,
          					digits: true
		                },*/
		             //    watermark_img:{
			            // 	required: true,
			            // 	extension: "jpg,jpeg,png",
	              //   		filesize: 2097152,
			            // },
			            // fileForSmall:{
			            // 	required: true,
			            // 	extension: "jpg,jpeg,png",
	              //   		filesize: 2097152,
			            // },
			            // fileForMedium:{
			            // 	required: true,
			            // 	extension: "jpg,jpeg,png",
	              //   		filesize: 2097152,
			            // },
			            // fileForLarge:{
			            // 	required: true,
			            // 	extension: "jpg,jpeg,png",
	              //   		filesize: 2097152,
			            // },
			            // fileForOriginal:{
			            // 	required: true,
			            // 	extension: "jpg,jpeg,png",
	              //   		filesize: 2097152,
			            // },
			            six_mos:{
			            	required: true,
			            },
			            twelve_mos:{
			            	required: true,
			            }
		            },
		            
		            messages: {
		                product_name: "Please enter Product name.",
		                product_description: "Please enter Product Description.",
		                "product_tag[]": "Please enter Product Tags.",
		                "product_category[]": "Please enter Product Category.",
		     //            watermark_img:{
							// required: "Please select Watermark image!",
			    //         },
			    //         fileForSmall:{
							// required: "Please select Small product image!",
			    //         },
			    //         fileForMedium:{
							// required: "Please select Medium product image!",
			    //         },
			    //         fileForLarge:{
							// required: "Please select Large product image!",
			    //         },
			    //         fileForOriginal:{
							// required: "Please select Original product image!",
			    //         },
			            six_mos:{
			            	required: 'Plese select',
			            },
			            twelve_mos:{
			            	required: 'Plese select',
			            }
		            },
		             

		            submitHandler: function(form) {
		                form.submit();
		            }
		        }).settings.ignore = "";

		        var getDaysInMonth = function(month,year) {
					  // Here January is 1 based
					  //Day 0 is the last day in the previous month
				 	return new Date(year, month, 0).getDate();
					// Here January is 0 based
					// return new Date(year, month+1, 0).getDate();
				};

	            jQuery(".pub_day").keyup(function (e) {

	            	var selected_month = jQuery('.pub_on').val();
	            	var selected_year = jQuery('.pub_year').val();
	            	var days = getDaysInMonth(selected_month,selected_year);
	            	
	            	var curr_day = jQuery(this).val();
	            	var curr_day_int = parseInt(curr_day);

	            	if(curr_day_int <= days){
	            		//if the letter is not digit then display error and don't type anything
					    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
					        //display error message
					        //$("#errmsg").html("Digits Only").show().fadeOut("slow");
					        return false;
					    }
	            	}else{
	            		jQuery(this).val("");
	            	}
				    
				});

				jQuery(".pub_year").keypress(function (e) {
				     //if the letter is not digit then display error and don't type anything
				     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
				        //display error message
				        //$("#errmsg").html("Digits Only").show().fadeOut("slow");
				        return false;
				    }
				});

				jQuery(".pub_hour").keyup(function (e) {
					var total_hour = parseInt('24');
					var curr_hour = jQuery(this).val();
					var curr_hour_int = parseInt(curr_hour);

					if(curr_hour_int <= total_hour){
						//if the letter is not digit then display error and don't type anything
					    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
					        //display error message
					        //$("#errmsg").html("Digits Only").show().fadeOut("slow");
					        return false;
					    }
					}else{
						jQuery(this).val("");
					}
					    
				});

				jQuery(".pub_minute").keyup(function (e) {
					var total_minutes = parseInt('60');
					var curr_minutes = jQuery(this).val();
					var curr_minutes_int = parseInt(curr_minutes);
					if(curr_minutes_int <= total_minutes){
						//if the letter is not digit then display error and don't type anything
				     	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
					        //display error message
					        //$("#errmsg").html("Digits Only").show().fadeOut("slow");
					        return false;
				   		}
					}else{
						jQuery(this).val("");
					}
				     
				});


				jQuery('#more_tag').click(function(event){
					event.preventDefault();
					jQuery('#tags').append('<input name ="product_tag[]" class="p_tag" value="" placeholder="tags" type="text" /><br />');
				});

			});
		</script>
	</body>
</html>
<?php
}

/*inserting product attributes function code*/

function insert_product_attributes ($post_id, $available_attributes, $variations)  
{
    foreach ($available_attributes as $attribute)
    {   
        $values = array();

        foreach ($variations as $variation)
        {
            $attribute_keys = $variation->attributes;
            
            foreach ($attribute_keys as $key=>$attribute_val)
            {
                if ($key === $attribute)
                {
                    $values[] = $variation->attributes->$key;
                }
            }
        }
        $values = array_unique($values);
        wp_set_object_terms($post_id, $values, 'pa_' . $attribute);
    }

    $product_attributes_data = array();

    foreach ($available_attributes as $attribute)
    {
        $product_attributes_data['pa_'.$attribute] = array(
											            'name'         => 'pa_'.$attribute,
											            'value'        => '',
											            'is_visible'   => '1',
											            'is_variation' => '1',
											            'is_taxonomy'  => '1'
											        );
    }

    update_post_meta($post_id, '_product_attributes', $product_attributes_data);
}

/*End*/

/*inserting product variations function code*/

function insert_product_variations ($post_id, $variations,$abe_file)  
{

	$i = 0;
    foreach ($variations as $index => $variation)
    {
    	
        $variation_post = array(

            'post_title'  => 'Variation #'.$index.' of '.count($variations).' for product#'. $post_id,
            'post_name'   => 'product-'.$post_id.'-variation-'.$index,
            'post_status' => 'publish',
            'post_parent' => $post_id,
            'post_type'   => 'product_variation',
            'guid'        => home_url() . '/?product_variation=product-' . $post_id . '-variation-' . $index
        );

        $variation_post_id = wp_insert_post($variation_post);
        
        foreach ($variation->attributes as $attribute => $value)
        {
            $attribute_term = get_term_by('name', $value, 'pa_'.$attribute);
            $slug = $attribute_term->slug;
        }

        update_post_meta($variation_post_id, 'attribute_pa_'.$attribute, $slug );
        update_post_meta( $variation_post_id, '_downloadable', 'yes' );
        update_post_meta( $variation_post_id, '_downloadable_files', $abe_file[$i] );
        update_post_meta($variation_post_id, '_price', $variation->price);
        update_post_meta($variation_post_id, '_regular_price', $variation->price);
        update_post_meta($variation_post_id, '_credits_amount', $variation->credits_amount);
        update_post_meta($variation_post_id, '_sku', $variation->sku);

        $i++;
    }
}

/*End*/

/*inserting product variations function code*/

function update_product_variations ($post_id,$abe_file)  
{
	$product = new WC_Product_Variable( $post_id );
	$variations = $product->get_available_variations();
	$i = 0;
	foreach ($variations as $variation) {
		$variation_post_id = $variation['id'];
		update_post_meta($variation_post_id, '_downloadable_files', $abe_file[$i] );
		$i++;
	}
}

/*End*/

/*End of adding product section*/

/*Seek inventory section*/

add_shortcode("seek-inventory-products", "inventory_products_function");

function inventory_products_function() {?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Product Inventory</title>
<style>
	ul {margin:0px; padding:0px; list-style-type: none; width: 100%;}
	.product_listing h2{font-size: 15px;}
	/*ul {margin:0px; padding:0px; list-style-type: none; width: 100%;}
	li {float:left;width:25%;}

	ul.skcreds li {float: initial!importantimportant;width: 100%}*/	
	
</style>

</head>

<body>
<div>


<!-- searches only for the current user products -->
	<input class="search-fld" type="text" placeholder="Search for products" value="" /> <br />
	<a href="/add-product" class="add-prod" style="text-align: center!important;margin-top: 10px;"><i class="fa fa-cloud-upload" style="font-size: 20px!important;"></i>&nbsp;&nbsp; New Product</a>

	<table class="product_listing">
		<thead>
			<tr>
				<th style="text-align: left!important;font-size:12px!important;">Item name</th>
				<th style="text-align: left!important;font-size:12px!important;">Analytics</th>
				<th> </th>
			</tr>
		</thead>
		<tbody>
			<?php
			$args = array(
		        'post_type'      => 'product',
		        /*'hide_empty' => false,*/
		        'post_status'  => array('draft','publish'),
		        'posts_per_page' => 10,
		        'author'    => get_current_user_id()
		    );

		    $loop = new WP_Query( $args );
		    // echo "<pre>";print_r($loop);die;
		    if( $loop->have_posts() ):
			    while( $loop->have_posts() ) :
			    	$loop->the_post();
			    	global $product;
			    	$image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'single-post-thumbnail' );
			    	// echo '<pre>';print_r($image);
				?>
				<tr class="p_row_list">
					<td class="p_col_1">
						<div class="pd_img">
							<a href="/add-product/?pid=<?=get_the_ID()?>"><img src="<?=$image[0]?>" width="100" height="100" title="" alt="Product Thumbnail" /></a>
						</div>
						<div class="pd_desc">
							<h2><?=the_title()?></h2>
							<button class="pub-prod">Published</button>
						</div>
					</td>
					<td class="p_col_2" style="text-align: left!important;">

						<i class="fa fa-eye"></i> 2,540<br />
						<i class="fa fa-cloud-download"></i> 25
					</td>
					<td class="p_col_3" >
						<ul class="skcreds" style="text-align: left!important;">
							<li><i class="fa fa-money"></i> $12,000 YTD</li>
							<li><i class="fa fa-cart-arrow-down"></i> 5</li>
						</ul>
					</td>
				</tr>
				<?php
				endwhile;
			else:
				echo "There are no Products under this user";
			endif;
			wp_reset_query();
				?>
		</tbody>
	</table>

	<!--div>
		<ul class="prd-menu">
		<li>&nbsp;</li>
		<li>Item</li>
		<li>Price</li>
		<li>Sold</li>
		</ul>
	</div-->

<!-- loop -->
		<!--div class="prod-lineup">
		<ul class="prd-menu">
		
			<li><img src="#" width="100" height="100" title="" /></li>

			<li><h2>Product Name</h2>
			<!-- if product is hidden, button label to change to Unpublish with a warning "You are about to deactivate this product. Click Okay to continue" --> 
			<!--button>Published</button>
			</li>

			<li>
			<h2>Analytics:</h2> 
			Views: 2,540<br />
			Time duration: 00:01:00		
			</li>

			<li>
			<h2>Sold</h2>
			<ul class="skcreds">
				<li>1 Credits: 5</li>
				<li>2 credits: 23</li>
				<li>3 credits: 3</li>
				<li>6 credits: 20</li>
				<li>Commercial 6 months: 12</li>
				<li>Commercial 12 months: 5</li>
			</ul>
			</li>
		</ul>		
	</div-->
<!-- end loop -->

</div>
</body>
</html>


<?php }?>
