<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Wallet_model extends MY_Model {

    function __construct() {
    	
        parent::__construct();
		

    }

    function get_single_data( $table_name, $column_name, $where_arr ) {
		
		$id = $this->session->userdata('id');			
		$this->db->select( $column_name);
		$this->db->from( $table_name );
		$this->db->where( $where_arr );

		return $this->db->get()->row_array();
	}

    function get_single_row( $table_name, $where_arr ) {
		
		$id = $this->session->userdata('id');			
		$this->db->select( $column_name);
		$this->db->from( $table_name );
		$this->db->where( $where_arr );
		$this->db->limit( 1 );

		return $this->db->get()->row_array();
	}
	
}