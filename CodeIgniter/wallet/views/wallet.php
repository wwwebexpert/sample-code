  
  <style type="text/css">
    input[type=number]::-webkit-inner-spin-button, 
    input[type=number]::-webkit-outer-spin-button { 
        -webkit-appearance: none; 
        margin: 0; 
    }
  </style>


      <!-- page start-->
     <div class="addshop">
        <h2 class="ui-heading ui-main-heading"><?=lang('my_wallet')?></h2>
        <div class="content-area  m-top-25 text-center">
          <div class="row billdetails">
            <div class="col-md-12">
              
              <?php //echo "<pre>"; print_r($cards); echo "</pre>"; ?>

              <div class="col-md-5">
                <div class="row"><div class="mywallet_heading"><?=lang('current_wallet_amount')?>
                  <span>$<?=$wallet_balance['wallet_balance']?></span> </div> </div>
              </div>

              <div class="col-md-5">
                <div class="row">
                  <div class="mywallet_heading"><?=lang('total_interest_earned')?>
                    <span>$<?=$total_wallet_interest_amount['total_wallet_interest_amount']?></span>
                  </div>
                </div>
              </div>

              <div class="col-md-2">

                <button type="button" redeemType="wallet" class="redeem btn btn-success" <?php if ($redeem == 'disabled') { echo "disabled"; }?>><?php if ( $redeem == 'enable' ) { lang('redeem'); } else { lang('redeem_requested'); } ?></button>                
              
              </div>
              
            </div>
          </div>

          <div class="row">
            <div class="my_subheading"><?=lang('add_amount_to_wallet')?></div>
          </div>

          <div class="row chooseac">
            <div class="col-md-5 padding">
              <div class="wallet_label"><?=lang('choose_an_amount')?></div>
              <ul class="chooseac_list">
                <li amount='50' class="defaultAmount active"><a href="">$50</a></li>
                <li amount='100' class="defaultAmount" ><a href="#">$100</a></li>
                <li amount='150' class="defaultAmount"><a href="">$150</a></li>
                <li amount='200' class="defaultAmount"><a href="">$200</a></li>
              </ul>

              <div class="clearfix"></div>
            </div>
            <div class="col-md-2 middlelabel"><?=lang('or')?></div>
            <div class="col-md-5 padding">
              <label class="wallet_label2 col-md-12 no-padding"><?=lang('enter_amount_yourself')?></label>
              <input type="text" class="form-control mytextbox" name="credit_amount" value="" required placeholder="Enter amount" />
            </div>
          </div>

          <div class="row chooseac">
            <div class="ui-focus-heading sub_heading2 ui-width-all ui-font-bold">
              <?=lang('add_a_card_or_select_a_card_from_already_added_cards_and_proceed')?>
            </div>
          </div>

          <div class="row chooseac">
            <div class="col-md-6 padding m-top-25">
           
              <!-- <div class="paygroup pull-left">
                <input type="radio" name="payment_method" value="credit-card" checked="checked"> Credit By Card
              </div> -->
        
              <div class="add-card col-md-9"> 
               <!-- CREDIT CARD FORM STARTS HERE -->
                  <div class="panel panel-default credit-card-box">
                      <div class="panel-heading display-table" >
                          <div class="row display-tr" >
                              <h3 class="panel-title display-td"><?=lang('add_card')?></h3>
                              <div class="display-td">
                                  <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">
                              </div>
                          </div>                 
                      </div>

                      <div class="panel-body">

                          <?php $attributes = array('class' => '', 'id' => 'braintreeCheckoutW'); echo form_open('wallet', $attributes); ?>

                              <div class="row">
                                  <div class="col-xs-12">
                                      <div class="form-group">
                                          <label for="cardNumber"><?=lang('card_number')?></label>
                                          <div class="input-group">
                                              <input 
                                                  data-braintree-name="number"
                                                  type="text" value=""
                                                  class="form-control"
                                                  name="number"
                                                  placeholder="<?=lang('card_number')?>"
                                                  autocomplete="cc-number" 
                                                  maxlength="16" required
                                              />
                                              <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                          </div>
                                      </div>                            
                                  </div>
                              </div>

                              <div class="row">
                                  <div class="col-xs-12 col-md-7">
                                      <div class="form-group">
                                          <label for="cardExpiry"><span class="hidden-xs"><?=lang('expiry_date')?></span></label>

                                          <input data-braintree-name="expiration_date"
                                              type="text" class="form-control" 
                                              name="expiration_date"
                                              placeholder="MM / YY"
                                              autocomplete="cc-exp" value="" maxlength="5" required>
                                          
                                      </div>
                                  </div>
                                  <div class="col-xs-12 col-md-5 pull-right">
                                      <div class="form-group">
                                          <label for="cvv" class="hidden-xs"><?=lang('cvv_code')?></label>
                                          <input data-braintree-name="cvv" type="password" value="" class="form-control m-top-5px"
                                              name="cvv" placeholder="<?=lang('cvv_code')?>" autocomplete="cc-cvv" maxlength="4" required/>
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-xs-12">
                                      <div class="form-group">
                                          <label for="holdername"><?=lang('postal_code')?></label>
                                          <input data-braintree-name="postal_code"  placeholder="<?=lang('postal_code')?>" 
                                          type="text" class="form-control" name="postal_code" value="" 
                                          maxlength="5" required/>
                                      </div>
                                  </div>                        
                              </div>
                              <div class="row">
                                  <div class="col-xs-12">
                                      <div class="form-group">
                                          <label for="cardholder_name"><?=lang('card_holder_name')?></label>
                                          <input data-braintree-name="cardholder_name" type="text" class="form-control" name="cardholder_name" placeholder="<?=lang('card_holder_name')?>" value="" maxlength="40" required/>
                                      </div>
                                  </div>                        
                              </div>
                              <div class="row">
                                  <div class="col-xs-12">
                                    <input type="hidden" name="account_type" value="card">
                                    <input type="hidden" name="submit" value="addCard">
                                      <input class="add btn btn-success btn-lg btn-block" type="submit" value="<?=lang('add_card')?>">
                                  </div>
                              </div>
                              <div class="row" style="display:none;">
                                  <div class="col-xs-12">
                                      <p class="payment-errors"></p>
                                  </div>
                              </div>

                          </form>

                      </div>
                    </div>            
                    <!-- CREDIT CARD FORM ENDS HERE -->
                  
                  </div>

                <div class="clearfix"> </div>

            </div>
            
            <div class="col-md-6 padding m-top-25">
              
              <?php if (!empty($cards)) { 

                  foreach ($cards as $card) { ?>
                  
                  <div class="bank_detailbox row x-card <?php if ( $card->is_default == 1) { echo 'default'; } ?>" data-card-id="<?=$card->payment_account_id?>">
                    <div class="bank_detailbox_sideimg col-xs-2">
                      <img src="<?=$card->image_url?>" alt="" />
                    </div>
                    <div class="col-xs-6">
                      <h5><?=$card->masked_number?></h5>
                    </div>
                    <div class="col-xs-4">
                      <span class="bank_detailbox_sideimg2 x-select"><img src="<?php echo base_url(); ?>resource/images/check.png" alt="" /></span>
                      <span data-card-id="<?=$card->payment_account_id?>" class="bank_detailbox_sideimg3 x-remove"><img src="<?php echo base_url(); ?>resource/images/close.png" alt="" /></span>
                    </div>
                  </div>    

              <?php } } else { ?>
                  
                  <div class="alert alert-danger" role="alert">
                    <?=lang('add_at_least_one_card_to_your_wallet')?>
                  </div>

              <?php } ?>
            
            </div>

            <div class="row" style="clear:left;float:none">

              <?php $attributes = array('class' => '', 'id' => 'braintreeCrWallet'); echo form_open('wallet', $attributes); ?>

                <input type="hidden" name="cr_amount" value="">
                <input type="hidden" name="card_payment_token_id" value="">
                <input type="hidden" name="submit" value="addCredit">

                <button class="display-none walletConfirm text-center donebutton btn btn-danger btn-sm"><?=lang('confirm_and_proceed')?></button>
                <a class="braintreeCrWallet text-center donebutton btn btn-info btn-sm"><?=lang('continue')?></a>
            
              <?=form_close()?>

            </div>
          </div>
        

          <!-- Transition History -->
          <div class="row chooseac">
            <div class="ui-focus-heading sub_heading2 ui-width-all ui-font-bold"><?=lang('transition_history')?></div>
          </div>

          <?php if (!empty($txn_history)) { 

                  foreach ($txn_history as $txn) { 

                      if($txn->wallet_txn_type == 'Cr'){ $transfer_mode = $this->lang->line("credit"); $icon = 'approve'; $txn_arrow ='left'; } 
                      else { $transfer_mode = $this->lang->line("debit"); $icon = 'remove'; $txn_arrow = 'right'; }?>

                    <div class="orderdetails wallet-txn-history" style="margin-top:10px;">
                      <div class="col-md-6 padding orderdetailsin">
                        <ul class="labeldetals">
                          <li><?=lang('transaction_id')?></li>
                          <li><?=lang('transition_date')?></li>
                        </ul>
                        <ul>
                          <li><?=$txn->txn_id?></li>

                          <li>
                            <?php 
                                $originalDate = $txn->created_on;
                                echo $newDate = date("m-d-Y H:i:s", strtotime($originalDate)); 
                              ?> 
                          </li>
                        
                        </ul>
                        <div class="clearfix"> </div>
                      </div>

                      <div class="col-md-6 padding orderdetailsin">
                        <ul class="labeldetals">
                          <li><?=lang('transition_mode')?></li>
                          <li><?=lang('amount')?></li>
                        </ul>
                        <ul>
                          <li><?=$transfer_mode?></li>
                          <li>$<?=$txn->amount?></li>
                        </ul>
                        
                        <div class="txn-in-out order-actions">
                          <button class="<?=$icon?>"><i class="fa fa-arrow-<?=$txn_arrow?>"></i></button>
                        </div>

                      </div>

                      <div class="clearfix"></div>

                    </div>

          <?php } } ?>

        </div>
      </div>

      <!-- PIN SETUP -->

      <div class="modal fade" id="my-pin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content">
            
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel"><?=lang('payment_pin')?></h4>
            </div>
            <div class="modal-body">
              
              <?php if ( !empty($is_pin) )
              { 
                if ($is_pin == 'yes')
                {
              ?>

              <form id="mySetupPinForm">
                <h5><?=lang('setup_payment_pin_message')?></h5><hr>
                <div class="form-group">
                  <label for="exampleInputPassword1"><?=lang('payment_pin')?></label>
                  <input type="password" name="pin" class="form-control" id="pin" placeholder="<?=lang('payment_pin')?>" autocomplete="off">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword2"><?=lang('confirm_payment_pin')?></label>
                  <input type="password" name="confirm_pin" id="confirmPin" class="form-control" placeholder="<?=lang('confirm_payment_pin')?>" autocomplete="off">
                </div>
                <input type="hidden" name="_5u8317" value="setup">
                <input type="submit" class="btn btn-primary" value="<?=lang('proceed')?>">
              </form>

              <?php
                }
                else
                {
              ?>
                  <form id="myPinForm">
                    <h5><?=lang('enter_your_payment_pin')?></h5><hr>
                    <div class="form-group">
                      <label for="exampleInputPassword1"><?=lang('payment_pin')?></label>
                      <input type="password" name="pin" class="form-control" id="exampleInputPassword1" placeholder="<?=lang('payment_pin')?>" autocomplete="off">
                    </div>

                    <input type="hidden" name="_5u8317" value="auth">
                <input type="submit" class="btn btn-primary" value="<?=lang('proceed')?>">
                  </form>
              
              <?php
                }
              } ?>

            </div>
            <div class="confirm modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal"><?=lang('close')?></button>
            </div>
          </div>
        </div>
      </div>
      <!-- /PIN SETUP -->

      <!-- page end--> 
   