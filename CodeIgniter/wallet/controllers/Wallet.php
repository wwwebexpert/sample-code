<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/* class.Wallet.php
.---------------------------------------------------------------------------.
|  Software: Braintree API - PHP Braintree JDK in CI                        |
|   Version: 1.0                                                            |
|   Contact: via sourceforge.net support pages (also www.worxware.com)      |
|      Info: http://phpmailer.sourceforge.net                               |
|   Support: http://sourceforge.net/projects/phpmailer/                     |
| ------------------------------------------------------------------------- |
|     Admin: Andy Prevost (project admininistrator)                         |
|   Authors: Harish Chauhan (Software Engineer) exe.harish@gmail.com        |
|          : Marcus Bointon (coolbru) coolbru@users.sourceforge.net         |
|   Founder: Brent R. Matzelle (original founder)                           |
| Copyright: (c) 2016-2017, Mobilyte India Tech Pvt Ltd.                    |
|             All Rights Reserved.                                          |
| Copyright (c) 2001-2003, Brent R. Matzelle                                |
| ------------------------------------------------------------------------- |
|   License: Distributed under the Lesser General Public License (LGPL)     |
|            http://www.gnu.org/copyleft/lesser.html                        |
| This program is distributed in the hope that it will be useful - WITHOUT  |
| ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
| FITNESS FOR A PARTICULAR PURPOSE.                                         |
| ------------------------------------------------------------------------- |
| We offer a number of paid services (www.worxware.com):                    |
| - Web Hosting on highly optimized fast and secure servers                 |
| - Technology Consulting                                                   |
| - Oursourcing (highly qualified programmers and graphic designers)        |
'---------------------------------------------------------------------------'
*/

/* ************************+*******
 * 
 * third_party
 *     - Braintree [folder]
 *     - ssl [folder]
 *     - Braintree.php [file]
 * 
 * application/library
 *     - Braintree_lib.php [file]
 *     
 * config
 *     - braintree.php [file]
 *
 * ************************+******* */

/**
 * PHPMailer - PHP email transport class
 * NOTE: Requires PHP version 5 or later
 * @package PHPMailer
 * @author Andy Prevost
 * @author Marcus Bointon
 * @copyright 2004 - 2009 Andy Prevost
 * @version $Id: class.phpmailer.php 447 2009-05-25 01:36:38Z codeworxtech $
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 */

class Wallet extends MX_Controller 
{
	public function __construct() 
    {
		parent::__construct();
		
        $this->load->model('Common_Model');
		$this->load->model('Wallet_model');
		$this->load->model('InitModel');

		$this->load->library("braintree_lib");
        
        if ( !$this->session->userdata('id') ) {        
            redirect('/');
        }
	}

    /**
     * @param \User $users
     * @return Response
    **/
	public function index() 
    {
        $current_user_id = $this->session->userdata('id');
        //echo "<pre>"; print_r($wallet_id); die(' Wallet ID');
        
        /*if ( !$this->session->userdata("payment_method_nonce")) {
            
            $payment_method_nonce = $this->braintree_lib->create_client_token();
            //$payment_method_nonce = 'BT-TOKEN0011';
            
            $this->session->set_userdata( array( 'payment_method_nonce'  => $payment_method_nonce ));
        }*/

        $payment_pin = $this->db->select('payment_pin')->from('ws_cscart_users')->where('user_id', $current_user_id)->get()->row();
        $payment_pin = $payment_pin->payment_pin;

        $query_rr = "SELECT status FROM `ws_redeem_request` WHERE `user_id` = $current_user_id AND redeem_request_type = 'wallet' AND (`status` = 1 OR `status` = 2); ";        
        $redeem_res_rr = $this->db->query($query_rr)->result();

        if (!empty($redeem_res_rr)) { $data['redeem'] = 'disable'; } 
        else { $data['redeem'] = 'enable'; }

        /* Funding Amount To User Account */
        /*$merchantAccountParams = [
          'individual' => [
            'firstName' => 'Jane',
            'lastName' => 'Doe',
            'email' => 'jane@14ladders.com',
            'phone' => '5553334444',
            'dateOfBirth' => '1981-11-19',
            'ssn' => '456-45-4567',
            'address' => [
              'streetAddress' => '111 Main St',
              'locality' => 'Chicago',
              'region' => 'IL',
              'postalCode' => '60622'
            ]
          ],
          'business' => [
            'legalName' => 'Jane\'s Ladders',
            'dbaName' => 'Jane\'s Ladders',
            'taxId' => '98-7654321',
            'address' => [
              'streetAddress' => '111 Main St',
              'locality' => 'Chicago',
              'region' => 'IL',
              'postalCode' => '60622'
            ]
          ],
          'funding' => [
            'descriptor' => 'Blue Ladders',
            'destination' => Braintree_MerchantAccount::FUNDING_DESTINATION_BANK,
            'email' => 'funding@blueladders.com',
            'mobilePhone' => '5555555555',
            'accountNumber' => '1123581321',
            'routingNumber' => '071101307'
          ],
          'tosAccepted' => true,
          'masterMerchantAccountId' => "14ladders_marketplace",
          'id' => "blue_ladders_store"
        ];
        $result = Braintree_MerchantAccount::create($merchantAccountParams);*/

        //$this->preDie($result);
        /* /Funding Amount To User Account */

        $current_time = date('Y-m-d H:i:s');

		//echo date('Y-m-d H:i:s', strtotime('+1 day', time() ));
		//echo " <pre>"; print_r($current_time); die(' TIME');

		$id = $this->session->userdata('id');

        $data['cards'] = $this->db->select('*')->from('ws_payment_accounts')->where('user_id', $current_user_id)->get()->result();

        //echo " <pre>"; print_r( $data['cards'] ); die(' TIME');


		if ($this->input->post('submit') == 'addCard' ) {

			//echo "<pre>"; print_r($this->input->post()); echo "</pre>"; die( ' Wallet | index');

			$formData = $this->input->post();
			//$this->preDie($this->input->post());

			$payment_method_nonce = $this->input->post('payment_method_nonce');
			$account_type = $this->input->post('account_type');

            //if user already added then add card to existing user account 
            if ( !empty($data['cards']) ) 
            {                
                //add card to existing user on braintree
                
                $customer_id = $data['cards'][0]->customer_id;


                $customer = Braintree_Customer::find($customer_id);
                $unique_ids = array_map( $this->extractUniqueId($customer->creditCards), $customer->creditCards);


                $result = \Braintree_PaymentMethod::create([
                    'customerId' => $customer_id,
                    'paymentMethodNonce' => $payment_method_nonce,
                    'options' => [
                        //'failOnDuplicatePaymentMethod' =>true,
                        'verifyCard' => true
                    ]
                ]);

                if($result->success) {

                    //$this->preDie($result);

                    $cardAlreadyExist = false;
                    $currentPaymentMethod = $this->extractUniqueId($result->paymentMethod);
                    foreach ($unique_ids as $key => $uid) {
                        if( $currentPaymentMethod  == $uid->uniqueNumberIdentifier)
                        {
                            $cardAlreadyExist = true;
                            $payment_token = $result->paymentMethod->token;
                            Braintree_PaymentMethod::delete($payment_token);
                        }
                    }
                    
                    if( $cardAlreadyExist) {

                        $this->session->set_flashdata('message', "Duplicate card exist for the user.");
                        $this->session->set_flashdata('message-class', 'danger');
                        redirect('wallet');
                        // redirect(base_url().'walletapp/error?msg=Please Try Again&tech_msg=Duplicate card exist for the user');
                    }



                    if($account_type == 'card') {

                        $data_arr = array(
                            'user_id' => $id,
                            'customer_id' => $customer_id,
                            'card_holder_name' => $result->paymentMethod->cardholderName,
                            'account_type' => $account_type,
                            'card_type' => $result->paymentMethod->cardType,
                            'card_identifier' => $result->paymentMethod->uniqueNumberIdentifier,
                            'masked_number' => $result->paymentMethod->maskedNumber,
                            'payment_token' => $result->paymentMethod->token,
                            'expiration_date' => $result->paymentMethod->expirationDate,
                            'image_url' => $result->paymentMethod->imageUrl,
                            'card_last_4' => $result->paymentMethod->last4,
                            'expired' => $result->paymentMethod->expired,
                            'payroll' => $result->paymentMethod->payroll,
                            'is_default' => $result->paymentMethod->default,
                            'updated_at' => date('Y-m-d H:i:s'),
                            'status' => $result->paymentMethod->verifications[0]['status']
                        );

                        //insert details into database
                        if ( !$this->db->insert('ws_payment_accounts', $data_arr)) {
                            
                            echo "Error Message: ".$this->db->_error_message().' | Error Number: '.$this->db->_error_number(); die(' | Wallet => index');
                        }

                        $this->session->set_flashdata('message', "Payment method saved successfully.");
                        $this->session->set_flashdata('message-class', 'success');
                    }

                } else {

                    $this->session->set_flashdata('message', "ERROR $result->message.");
                    $this->session->set_flashdata('message-class', 'danger');   
                }

            } else {

                //create user on braintree and add card
                $result = \Braintree_Customer::create([
                    'id' => 'EOC00'.$id,
                    'email' => 'harry@social.com',
                    'phone' => '+917814009418',
                    'paymentMethodNonce' => $payment_method_nonce,

                    'creditCard' => [
                        'options' => [
                            //'failOnDuplicatePaymentMethod' => true,
                            'verifyCard' => true
                        ]
                    ]

                ]);
                //$this->preDie($result);

                if($result->success) {

                    //$this->preDie($result);

                    if($account_type == 'card') {

                        $data_arr = array(
                            'user_id' => $id,
                            'customer_id' => $result->customer->creditCards[0]->customerId,
                            'card_holder_name' => $result->customer->creditCards[0]->cardholderName,
                            'account_type' => $account_type,
                            'card_type' => $result->customer->creditCards[0]->cardType,
                            'card_identifier' => $result->customer->creditCards[0]->uniqueNumberIdentifier,
                            'masked_number' => $result->customer->creditCards[0]->maskedNumber,
                            'payment_token' => $result->customer->creditCards[0]->token,
                            'expiration_date' => $result->customer->creditCards[0]->expirationDate,
                            'image_url' => $result->customer->creditCards[0]->imageUrl,
                            'card_last_4' => $result->customer->creditCards[0]->last4,
                            'expired' => $result->customer->creditCards[0]->expired,
                            'payroll' => $result->customer->creditCards[0]->payroll,
                            'is_default' => $result->customer->creditCards[0]->default,
                            'updated_at' => date('Y-m-d H:i:s'),
                            'status' => $result->customer->creditCards[0]->verifications[0]['status']
                        );

                        //insert details into database
                        if ( !$this->db->insert('ws_payment_accounts', $data_arr)) {
                            
                            echo "Error Message: ".$this->db->_error_message().' | Error Number: '.$this->db->_error_number(); die(' | Wallet => index');
                        }

                        $this->session->set_flashdata('message', "Payment method saved successfully.");
                        $this->session->set_flashdata('message-class', 'success');
                    }

                } else {

                    $this->session->set_flashdata('message', "ERROR $result->message.");
                    $this->session->set_flashdata('message-class', 'danger');   
                }
            }
    	}

        
        /* PAY */
        if ($this->input->post('submit') == 'addCredit' ) 
        {
            //die('braintreeCrWallet');

            $card_payment_token_id = $this->input->post('card_payment_token_id');

            $payment_token_arr = $this->db->select('payment_token')->from('ws_payment_accounts')->where('payment_account_id', $card_payment_token_id)->get()->result();

            if (!empty($payment_token_arr)) 
            {    
                    $result = Braintree_Transaction::sale([
                        'paymentMethodToken' => $payment_token_arr[0]->payment_token,
                        'amount' => $this->input->post('cr_amount')
                      ]
                    );

                    if ( $result->success == 1) {
                    
                    $txn['transaction_id'] = $result->transaction->id;
                    $txn['status'] = $result->transaction->status;
                    $txn['type'] = $result->transaction->type;
                    $txn['amount'] = $result->transaction->amount;
                    $txn['currency_code'] = $result->transaction->currencyIsoCode;
                    $txn['merchantAccountId'] = $result->transaction->merchantAccountId;
                    $txn['braintree_customer_id'] = $result->transaction->customer['id'];
                    $txn['card_type'] = $result->transaction->creditCard['cardType'];

                    $wallet_txn_type = 'Cr'; //Cr Or Dr

                    //find if user have a wallet
                    
                    $wallet_data = $this->db->select('wallet_id, wallet_balance')->from("ws_user_wallet_master")->where('status', '1')
                    ->where('user_id', $current_user_id)->get()->result();
                    
                    if (!empty($wallet_data)) {
                        
                        //echo "<pre>"; print_r($wallet_data); die(' Wallet ID');

                        //update wallet here
                        $wallet_id = $wallet_data[0]->wallet_id;
                        $wallet_balance = $wallet_data[0]->wallet_balance;

                        $update_last_flag = array('status' => '0', );

                        $this->db->where('wallet_id', $wallet_id);
                        $this->db->update('ws_user_wallet_master', $update_last_flag);
                        
                        $updated_amount = $wallet_balance + $txn['amount'];

                        $insert_update_data = array('txn_id' => $txn['transaction_id'],
                            'txn_type' => $txn['type'],
                            'user_id' => $current_user_id,
                            'braintree_customer_id' => $txn['braintree_customer_id'],
                            'card_type' => $txn['card_type'],
                            'card_payment_token' => $payment_token_arr[0]->payment_token,
                            'wallet_txn_type' => 'Cr', //Cr or Dr
                            'amount' => $txn['amount'],
                            'currency_code' => $txn['currency_code'],
                            'wallet_comments' => 'Amount added to Egoshop wallet',
                            'txn_status' => $txn['status'],
                            'wallet_balance' => $updated_amount,
                            );
                        
                        $this->db->insert( 'ws_user_wallet_master', $insert_update_data);

                        $txn_amount = $txn['amount'];
                        $query = "UPDATE `ws_cscart_users` SET `wallet_balance` = `wallet_balance` + $txn_amount WHERE `user_id` = $current_user_id; ";
                        $this->db->query($query);

                        $this->session->set_flashdata('message', "SUCCESS: Amount added to the wallet.");
                        $this->session->set_flashdata('message-class', 'success');  

                    } else {

                        //insert initial txn here
                        $insert_wallet = array('txn_id' => $txn['transaction_id'],
                            'txn_type' => $txn['type'],
                            'user_id' => $current_user_id,
                            'braintree_customer_id' => $txn['braintree_customer_id'],
                            'card_type' => $txn['card_type'],
                            'card_payment_token' => $payment_token_arr[0]->payment_token,
                            'wallet_txn_type' => 'Cr', //Cr or Dr
                            'amount' => $txn['amount'],
                            'currency_code' => $txn['currency_code'],
                            'wallet_comments' => 'Amount added to Egoshop wallet',
                            'txn_status' => $txn['status'],
                            'wallet_balance' => $txn['amount'],
                            );

                        $this->db->insert( 'ws_user_wallet_master', $insert_wallet);

                        $txn_amount = $txn['amount'];
                        $query = "UPDATE `ws_cscart_users` SET `wallet_balance` = `wallet_balance` + $txn_amount WHERE `user_id` = $current_user_id; ";
                        $this->db->query($query);

                        $this->session->set_flashdata('message', "SUCCESS: Wallet amount updated successfully.");
                        $this->session->set_flashdata('message-class', 'success');  
                    }
                
                } else {

                        $this->session->set_flashdata('message', "ERROR $result->message.");
                        $this->session->set_flashdata('message-class', 'danger');  
                }

            } else {

                //payment token is not valid or expired please use another card

                $this->session->set_flashdata('message', "ERROR: Payment token is not valid or expired please use another card.");
                $this->session->set_flashdata('message-class', 'danger');
            }

            //echo "<pre>"; print_r($result); echo "</pre>"; die(' PAY WALLET');
        }
        /* /PAY */

		$this->template->title('My Wallet', 'My Wallet');
		
		//$data['nonce_generated_time'] = $current_time;
		//$this->session->set_userdata( array( 'nonce_generated_time'  => $current_time ));

		$data['include_js'] = 'my-wallet';
		$data['include_css'] = 'my-wallet';
		$data['active_menu'] = 'my-wallet';
        $data['include_modal'] = 'yes';

        $data['is_pin'] = ($payment_pin == '' ? 'yes' : 'no'); //yes=generate the pin, no=already generated the pin

		$data['wallet_balance'] = $this->Wallet_model->get_single_data( $table_name='ws_cscart_users', $column_name='wallet_balance', $where_arr = array('user_id' => $id, ) ); 
		$data['total_wallet_interest_amount'] = $this->Wallet_model->get_single_data( $table_name='ws_cscart_users', $column_name='total_wallet_interest_amount', $where_arr = array('user_id' => $id, ) ); 
        $data['cards'] = $this->db->select('*')->from('ws_payment_accounts')->where('user_id', $current_user_id)->get()->result();
        $data['txn_history'] = $this->db->select('*')->from('ws_user_wallet_master')->where('user_id', $current_user_id)->order_by('wallet_id', 'DESC')->get()->result();
 		//$this->preDie($data);

		$this->template
		->set_layout('default') // application/views/layouts/two_col.php
		->build('wallet', $data); // views/welcome_message
    }

    /**
     * @param \User $users
     * @return Response
    **/
    public function deleteCard($value='')
    {
        $current_user_id = $this->session->userdata('id');
        $card_id = $this->input->post('card_id');

        $result_cc = $this->db->select('user_id')->from('ws_payment_accounts')->where('user_id', $current_user_id)->get()->result();
        $bt_user_data = $this->db->select('customer_id, payment_token')->from('ws_payment_accounts')->where('payment_account_id', $card_id)->get()->result();

        //echo "<pre>"; print_r($result_cc); echo "</pre>end"; //die('end');

        if (count($result_cc) == 1) {
            
            //delete user from braintree
            $result = Braintree_PaymentMethod::delete($bt_user_data[0]->payment_token);
            $result = Braintree_Customer::delete($bt_user_data[0]->customer_id);
            //echo "<pre>"; print_r($result->success); echo "</pre>"; die( 'Wallet | deleteCard' );

            if ($result->success == 1) {
                
                $this->db->delete('ws_payment_accounts', array('user_id' => $current_user_id));

                $this->session->set_flashdata('message', "SUCCESS: Card deleted successfully.");
                $this->session->set_flashdata('message-class', 'success'); 
            
            } else {

                $this->session->set_flashdata('message', "ERROR: $result->message.");
                $this->session->set_flashdata('message-class', 'success'); 
            }
             
        } else {

            //delete card only
            $result = Braintree_PaymentMethod::delete($bt_user_data[0]->payment_token);
            //echo "<pre>"; print_r($result); echo "</pre>"; die( 'Wallet | deleteCard' );


            if ($result->success == 1) {
                
                $this->db->delete('ws_payment_accounts', array('payment_account_id' => $card_id));

                $this->session->set_flashdata('message', "SUCCESS: Card deleted successfully.");
                $this->session->set_flashdata('message-class', 'success'); 
            
            } else {

                $this->session->set_flashdata('message', "ERROR: $result->message.");
                $this->session->set_flashdata('message-class', 'success'); 
            }
        }

        echo "success";
    } 


    public function redeem() {

        $user_id = $this->session->userdata('id');
        $redeem_type = $this->input->post('redeem_type');

        $txn_type = 'redeem';
        $currency_code = 'USD';
        $txn_status = 'authorized';
        $txn_id = 'NA';
        $braintree_customer_id = 'NA';
        $card_type = 'NA';
        $card_payment_token = 'NA';

        if ($this->input->post('redeem_type') == 'wallet') {
            
            $query = "SELECT status FROM `ws_redeem_request` WHERE `user_id` = $user_id AND `redeem_request_type` = 'wallet' AND (`status` = 1 OR `status` = 2); ";        
            $redeem_res = $this->db->query($query)->result();

            if (!empty($redeem_res)) {

                $data['res_status'] = 'info';
                $data['res_message'] = "INFO: You already applied for $redeem_type redeem and your request is in process.";

                echo json_encode($data); die();
            
            }

            //update wallet here
            $wallet_data = $this->db->select('wallet_id, wallet_balance')->from("ws_user_wallet_master")->where('status', '1')
                ->where('user_id', $user_id)->get()->result();

            //echo "<pre>"; print_r($wallet_data); echo "</pre>";

            $wallet_id = $wallet_data[0]->wallet_id;
            $wallet_balance = $wallet_data[0]->wallet_balance;

            $update_last_flag = array('status' => '0', );

            $this->db->where('wallet_id', $wallet_id);
            $this->db->update('ws_user_wallet_master', $update_last_flag);
            
            $updated_amount = $wallet_balance - $wallet_balance;

            $insert_update_data = array(
                'txn_id' => $txn_id,
                'txn_type' => 'redeem',
                'user_id' => $user_id,
                'braintree_customer_id' => $braintree_customer_id,
                'card_type' => $card_type,
                'card_payment_token' => $card_payment_token,
                'wallet_txn_type' => 'Dr', //Cr or Dr
                'amount' => $wallet_balance,
                'currency_code' => $currency_code,
                'wallet_comments' => 'Redeem wallet amount request',
                'txn_status' => $txn_status,
                'wallet_balance' => $updated_amount,
                );
            
            $this->db->insert( 'ws_user_wallet_master', $insert_update_data);

            $txn_amount = $wallet_balance;
            $query = "UPDATE `ws_cscart_users` SET `wallet_balance` = `wallet_balance` - $txn_amount WHERE `user_id` = $user_id; ";
            $this->db->query($query);

            $insert_redeem_req = array(
                'user_id' => $user_id,
                'redeem_amount' => $wallet_balance,
                'redeem_request_type' => 'wallet',
                'comment' => 'Redeem wallet amount request'
                );

            $this->db->insert( 'ws_redeem_request', $insert_redeem_req);

            $data['res_status'] = 'success';
            $data['res_message'] = 'Redeem wallet amount request sent successfully.';

            $this->session->set_flashdata('message', 'Redeem wallet amount request sent successfully.');
            $this->session->set_flashdata('message-class', 'success');
            echo json_encode($data); die();
        
        } elseif ($this->input->post('redeem_type') == 'credit-point') {
            
            $query = "SELECT status FROM `ws_redeem_request` WHERE `user_id` = $user_id AND `redeem_request_type` = 'credit-point' AND (`status` = 1 OR `status` = 2); ";        
            $redeem_res = $this->db->query($query)->result();

            if (!empty($redeem_res)) {

                $data['res_status'] = 'info';
                $data['res_message'] = "INFO: You already applied for $redeem_type redeem and your request is in process.";

                echo json_encode($data); die();
            
            }

            //update wallet here
            $credit_point_res = $this->db->select('credit_point')->from("ws_cscart_users")->where('user_id', $user_id)->get()->row();
            //echo "<pre>"; print_r($wallet_data); echo "</pre>";

            $credit_point = $credit_point_res->credit_point;

            $insert_redeem_req = array(
                'user_id' => $user_id,
                'redeem_credit_point' => $credit_point,
                'redeem_request_type' => 'credit-point',
                'comment' => 'Redeem credit point request.'
                );

            $this->db->insert( 'ws_redeem_request', $insert_redeem_req);

            $query = "UPDATE `ws_cscart_users` SET `credit_point` = `credit_point` - $credit_point WHERE `user_id` = $user_id; ";
            $this->db->query($query);

            $data['res_status'] = 'success';
            $data['res_message'] = 'Redeem credit point request sent successfully.';

            $this->session->set_flashdata('message', 'Redeem credit point request sent successfully.');
            $this->session->set_flashdata('message-class', 'success');

            echo json_encode($data); die();
        
        } else {

            $data['res_status'] = 'info';
            $data['res_message'] = 'ERROR: There is some error in this operation.';

        }

        echo json_encode($data); die();
    }

    /**
     * @param pin
     * @return Response
    **/
    public function pin($value='')
    {
        $user_id = $this->session->userdata('id');
        $_5u8317 =  $this->input->post('_5u8317');

        $pin =  $this->input->post('pin');

        if ($_5u8317 == 'setup')
        {
            $update_last_flag = array('payment_pin' => $pin, );

            $this->db->where('user_id', $user_id);
            $this->db->update('ws_cscart_users', $update_last_flag);
            echo "success";
        }

        if ($_5u8317 == 'auth')
        {
            $check_pin = $this->db->select('payment_pin')->from('ws_cscart_users')->where( array('user_id' => $user_id, 'payment_pin' => $pin))->get()->row();
            
            if ( !empty($check_pin)) {
                echo "success";
            }
            else
            {
                echo "Invalid payment pin";
            }
        }
    }

    public function extractUniqueId($creditCard){ 
        return $creditCard->uniqueNumberIdentifier;
    }       

    /**
     * @param \User $users
     * @return Response
    **/
    public function preDie( $arrayData )
    {
    	echo "<pre>"; print_r($arrayData); die();
    }

    /**
     * @param \User $users
     * @return Response
    **/
    public function pre( $arrayData )
    {
    	echo "<pre>"; print_r($arrayData); echo "</pre>";
    }
}