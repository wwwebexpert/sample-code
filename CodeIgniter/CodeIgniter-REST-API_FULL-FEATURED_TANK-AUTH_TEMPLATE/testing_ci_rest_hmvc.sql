-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 13, 2015 at 10:48 PM
-- Server version: 5.5.44-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `testing_ci_rest_hmvc`
--

-- --------------------------------------------------------

--
-- Table structure for table `access`
--

CREATE TABLE IF NOT EXISTS `access` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(40) NOT NULL DEFAULT '',
  `controller` varchar(50) NOT NULL DEFAULT '',
  `date_created` datetime DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `access`
--

INSERT INTO `access` (`id`, `key`, `controller`, `date_created`, `date_modified`) VALUES
(1, 'a152e84173914146e4bc4f391sd0f686ebc4f31', 'app', '2015-07-21 00:00:00', '2015-07-28 20:56:54');

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fix_key` varchar(200) NOT NULL,
  `value` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`id`, `fix_key`, `value`) VALUES
(1, 'project_name', 'Event Planner'),
(2, 'prefix', 'ws_');

-- --------------------------------------------------------

--
-- Table structure for table `keys`
--

CREATE TABLE IF NOT EXISTS `keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `is_private_key` tinyint(1) NOT NULL DEFAULT '0',
  `ip_addresses` text,
  `date_created` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `keys`
--

INSERT INTO `keys` (`id`, `key`, `level`, `ignore_limits`, `is_private_key`, `ip_addresses`, `date_created`) VALUES
(1, 'a152e84173914146e4bc4f391sd0f686ebc4f31', 0, 0, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `limits`
--

CREATE TABLE IF NOT EXISTS `limits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) NOT NULL,
  `count` int(10) NOT NULL,
  `hour_started` int(11) NOT NULL,
  `api_key` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `limits`
--

INSERT INTO `limits` (`id`, `uri`, `count`, `hour_started`, `api_key`) VALUES
(1, 'app/test', 1, 1439485402, 'a152e84173914146e4bc4f391sd0f686ebc4f31');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) NOT NULL,
  `method` varchar(6) NOT NULL,
  `params` text,
  `api_key` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `time` int(11) NOT NULL,
  `rtime` float DEFAULT NULL,
  `authorized` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=69 ;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `uri`, `method`, `params`, `api_key`, `ip_address`, `time`, `rtime`, `authorized`) VALUES
(1, 'app/test', 'post', 'a:1:{s:5:"input";s:17:"Input test asddas";}', '', '127.0.0.1', 1437409365, 0.032903, 1),
(2, 'app/test', 'post', 'a:1:{s:5:"input";s:17:"Input test asddas";}', '', '127.0.0.1', 1437410201, 0.030026, 1),
(3, 'app/test', 'post', 'a:1:{s:5:"input";s:17:"Input test asddas";}', '', '127.0.0.1', 1437410336, 0.0158498, 1),
(4, 'app/test', 'post', 'a:1:{s:5:"input";s:17:"Input test asddas";}', '', '127.0.0.1', 1437410605, 0.0256069, 0),
(5, 'app/test', 'post', 'a:2:{s:5:"input";s:17:"Input test asddas";s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1437410954, 0.0382349, 1),
(6, 'app/test', 'post', 'a:2:{s:5:"input";s:17:"Input test asddas";s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1437410970, 0.048897, 1),
(7, 'app/test', 'post', 'a:2:{s:5:"input";s:17:"Input test asddas";s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1437410971, 0.0209701, 1),
(8, 'app/test', 'post', 'a:2:{s:5:"input";s:17:"Input test asddas";s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1437411119, 0.0905762, 1),
(9, 'app/test', 'post', 'a:2:{s:5:"input";s:17:"Input test asddas";s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1437411119, 0.0685532, 1),
(10, 'app/test', 'post', 'a:2:{s:5:"input";s:17:"Input test asddas";s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1437412397, 0.0390561, 1),
(11, 'app/test', 'post', 'a:2:{s:5:"input";s:17:"Input test asddas";s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1437412823, 0.06794, 1),
(12, 'app/test', 'post', 'a:2:{s:5:"input";s:17:"Input test asddas";s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1437417439, 0.0897181, 1),
(13, 'app/test', 'post', 'a:2:{s:5:"input";s:17:"Input test asddas";s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1437417779, 0.09955, 1),
(14, 'app/test', 'post', 'a:2:{s:5:"input";s:17:"Input test asddas";s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1437417790, 0.0308368, 0),
(15, 'app/test', 'post', 'a:2:{s:5:"input";s:17:"Input test asddas";s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1437417862, 0.018904, 1),
(16, 'app/test', 'post', 'a:2:{s:5:"input";s:17:"Input test asddas";s:5:"token";s:40:"aa152e84173914146e4bc4f391sd0f686ebc4f31";}', '', '127.0.0.1', 1437417866, 0.0283618, 0),
(17, 'app/test', 'post', 'a:2:{s:5:"input";s:17:"Input test asddas";s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1437418215, 0.0996199, 1),
(18, 'app/test', 'post', 'a:2:{s:5:"input";s:17:"Input test asddas";s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1437418218, 0.070236, 1),
(19, 'app/test', 'post', 'a:2:{s:5:"input";s:17:"Input test asddas";s:5:"token";s:42:"afff152e84173914146e4bc4f391sd0f686ebc4f31";}', '', '127.0.0.1', 1437418220, 0.0436158, 0),
(20, 'app/test', 'post', 'a:2:{s:5:"input";s:17:"Input test asddas";s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1437418224, 0.0539708, 1),
(21, 'app/test', 'post', 'a:2:{s:5:"input";s:17:"Input test asddas";s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1437418310, 0.078671, 1),
(22, 'app/test', 'post', 'a:2:{s:5:"input";s:17:"Input test asddas";s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1437418726, 0.115022, 1),
(23, 'app/test', 'post', 'a:2:{s:5:"input";s:17:"Input test asddas";s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1437434413, 0.373295, 1),
(24, 'app', 'post', 'a:3:{s:9:"invite_id";s:2:"16";s:6:"accept";s:1:"1";s:6:"not_id";s:2:"19";}', '', '127.0.0.1', 1437603975, 0.157962, 0),
(25, 'app/test', 'post', 'a:3:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:6:"accept";s:1:"1";s:6:"not_id";s:2:"19";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1437603997, 0.061224, 1),
(26, 'app/test', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1437604006, 0.0928569, 1),
(27, 'app/test', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1437604053, 0.0896411, 1),
(28, 'app/test', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1438116883, 0.177685, 1),
(29, 'app/test', 'post', 'a:2:{s:5:"token";s:41:"sdf152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', '', '127.0.0.1', 1438116890, 0.0491269, 0),
(30, 'app/test', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1438116943, 0.100714, 1),
(31, 'app/test', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1438116944, 0.0672121, 1),
(32, 'app/test', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1438116944, 0.0754089, 1),
(33, 'app/test', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1438116945, 0.0468311, 1),
(34, 'app/test', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1438117004, 0.073329, 0),
(35, 'app/test', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1438717843, 0.238637, 1),
(36, 'app/test', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439313288, 0.092705, 1),
(37, 'app/pay_now', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439313350, 0.0490191, 1),
(38, 'app/pay_now', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439313359, 0.043128, 1),
(39, 'app/pay_now', 'get', NULL, '', '127.0.0.1', 1439317043, 0.100534, 0),
(40, 'app/pay_now', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439317051, 0.047498, 1),
(41, 'app/pay_now', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439318020, 0.0437829, 1),
(42, 'app/pay_now', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439319823, 0.0215249, 1),
(43, 'app/pay_now', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439320211, 0.0576749, 1),
(44, 'app/pay_now', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439320298, NULL, 1),
(45, 'app/pay_now', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439320509, 6.29585, 1),
(46, 'app/pay_now', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439320547, 1.7073, 1),
(47, 'app/pay_now', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439322221, 5.16643, 1),
(48, 'app/pay_now', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439322397, 1.91496, 1),
(49, 'app/pay_now', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439323880, 6.21449, 1),
(50, 'app/pay_now', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439325975, 1.95156, 1),
(51, 'app/pay_now', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439326181, 6.17859, 1),
(52, 'app/pay_now', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439326224, 1.78549, 1),
(53, 'app/pay_now', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439390844, 6.69629, 1),
(54, 'app/test', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:7:"FDtgrtr";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439479971, 0.188407, 1),
(55, 'app/test', 'post', 'a:2:{s:5:"token";s:36:"2e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:7:"FDtgrtr";}', '', '127.0.0.1', 1439479979, 0.071965, 0),
(56, 'app/test', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439485402, 0.0931199, 1),
(57, 'app/register', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:5:"input";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439485697, 0.0206549, 1),
(58, 'app/register', 'post', 'a:2:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:9:"user_name";s:2:"FD";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439485703, 0.0600271, 1),
(59, 'app/register', 'post', 'a:3:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:9:"user_name";s:2:"FD";s:8:"password";s:0:"";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439485708, 0.025224, 1),
(60, 'app/register', 'post', 'a:3:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:9:"user_name";s:2:"FD";s:8:"password";s:6:"a23423";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439485711, 0.036427, 1),
(61, 'app/register', 'post', 'a:3:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:9:"user_name";s:2:"FD";s:8:"password";s:6:"a23423";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439485828, 0.0448239, 1),
(62, 'app/register', 'post', 'a:3:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:8:"username";s:2:"FD";s:8:"password";s:6:"a23423";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439485840, 0.0751719, 1),
(63, 'app/register', 'post', 'a:3:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:8:"username";s:2:"FD";s:8:"password";s:6:"a23423";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439485873, NULL, 1),
(64, 'app/register', 'post', 'a:3:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:8:"username";s:2:"FD";s:8:"password";s:6:"a23423";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439485881, 0.057549, 1),
(65, 'app/register', 'post', 'a:3:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:8:"username";s:2:"FD";s:8:"password";s:6:"a23423";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439486009, 0.0699511, 1),
(66, 'app/login', 'post', 'a:3:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:8:"username";s:2:"FD";s:8:"password";s:6:"a23423";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439486203, 0.142041, 1),
(67, 'app/login', 'post', 'a:3:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:8:"username";s:2:"FD";s:8:"password";s:6:"a23423";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439486215, 0.0216382, 1),
(68, 'app/login', 'post', 'a:3:{s:5:"token";s:39:"a152e84173914146e4bc4f391sd0f686ebc4f31";s:8:"username";s:2:"FD";s:8:"password";s:6:"a23423";}', 'a152e84173914146e4bc4f391sd0f686ebc4f31', '127.0.0.1', 1439486245, 0.00983596, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ws_account_details`
--

CREATE TABLE IF NOT EXISTS `ws_account_details` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `fullname` varchar(160) DEFAULT NULL,
  `customer` varchar(150) NOT NULL,
  `city` varchar(40) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `address` varchar(64) NOT NULL,
  `phone` varchar(32) NOT NULL,
  `vat` varchar(32) NOT NULL,
  `language` varchar(40) DEFAULT 'english',
  `avatar` varchar(32) NOT NULL DEFAULT 'default_avatar.jpg',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=5 ;

--
-- Dumping data for table `ws_account_details`
--

INSERT INTO `ws_account_details` (`id`, `user_id`, `fullname`, `customer`, `city`, `country`, `address`, `phone`, `vat`, `language`, `avatar`) VALUES
(1, 1, 'Salman', '-', '', 'United States of America', '', '', '', 'english', 'default_avatar.jpg'),
(2, 2, 'navi', '0', '', NULL, '', '', '', 'english', 'default_avatar.jpg'),
(3, 3, 'Shakil', '0', NULL, NULL, '', '7133519083', '', 'english', 'default_avatar.jpg'),
(4, 4, 'Test Manager Account', '0', NULL, NULL, '', '', '', 'english', 'default_avatar.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `ws_ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ws_ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `ws_clients`
--

CREATE TABLE IF NOT EXISTS `ws_clients` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(40) NOT NULL,
  `last_name` varchar(40) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` bigint(12) NOT NULL,
  `about` varchar(400) NOT NULL,
  `profile_image` varchar(200) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '0-active|1-deactivated|2-reactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ws_login_attempts`
--

CREATE TABLE IF NOT EXISTS `ws_login_attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(40) COLLATE utf8_bin NOT NULL,
  `login` varchar(50) COLLATE utf8_bin NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ws_notes`
--

CREATE TABLE IF NOT EXISTS `ws_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `note_title` varchar(200) NOT NULL,
  `note_description` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `ws_notes`
--

INSERT INTO `ws_notes` (`id`, `note_title`, `note_description`, `created`, `status`) VALUES
(15, 'asssssssssss', 'aaaaaaaaaaaaaaaaa', '2015-07-22 16:55:12', 0),
(16, 'aaaaaa', 'dashj fshdf hsdfshf', '2015-07-22 16:55:16', 0),
(17, 'bbbbbb', 'asdhga dasd ashdga', '2015-08-04 19:51:07', 1),
(18, 'llllll', 'aaaa', '2015-07-21 23:36:28', 1),
(19, 'gggg', 'kkkk', '2015-07-21 23:36:37', 1),
(20, 'kkkk', 'ppp', '2015-07-22 16:55:14', 0),
(21, 'ccccggg', 'ssssdddwww', '2015-07-21 23:37:37', 1),
(22, 'oooooo', 'nnnnnn', '2015-07-21 23:37:49', 1),
(23, 'fffffff', 'hhhhhh', '2015-07-21 23:37:57', 1),
(24, 'test gggggg', 'hhhhhh uuuuu', '2015-07-21 23:38:18', 1),
(25, 'gggg tttttt', 'ggggg gggg rrrrrr', '2015-07-21 23:38:31', 1),
(26, 'uuuuuu iiiiiiii', 'wwww wjwww', '2015-07-21 23:38:56', 1),
(27, 'gggg kkkk', 'df fdg fdg dfg', '2015-07-21 23:54:37', 1),
(28, 'test note', 'descrip[tio kdjngfdgn dfkjg', '2015-07-22 17:24:27', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ws_users`
--

CREATE TABLE IF NOT EXISTS `ws_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_reason` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `new_password_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `new_password_requested` datetime DEFAULT NULL,
  `new_email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `new_email_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=5 ;

--
-- Dumping data for table `ws_users`
--

INSERT INTO `ws_users` (`id`, `username`, `password`, `email`, `activated`, `banned`, `ban_reason`, `new_password_key`, `new_password_requested`, `new_email`, `new_email_key`, `last_ip`, `last_login`, `created`, `modified`) VALUES
(1, 'admin', '$2a$08$A/H.8dbA7LXYcca.3/2RRO.Sp2x0ogeFBWCz0gkaTqWHRUsUz1UwW', 'admin@yopmail.com', 1, 0, NULL, NULL, NULL, 'admin@gmail.com', 'fe3ba297f1530055f66a068f21fb271b', '127.0.0.1', '2015-08-11 22:44:23', '2015-06-23 20:57:27', '2015-08-11 17:14:23'),
(2, 'FD', 'a23423', '', 1, 0, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00', '2015-08-13 22:40:40', '2015-08-13 17:10:40'),
(3, 'FD', 'ef49e0769ce9441394d3f217ec5546df', '', 1, 0, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00', '2015-08-13 22:41:21', '2015-08-13 17:11:21'),
(4, 'FD', 'ef49e0769ce9441394d3f217ec5546df', '', 1, 0, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00', '2015-08-13 22:43:29', '2015-08-13 17:13:29');

-- --------------------------------------------------------

--
-- Table structure for table `ws_user_autologin`
--

CREATE TABLE IF NOT EXISTS `ws_user_autologin` (
  `key_id` char(32) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `ws_user_profiles`
--

CREATE TABLE IF NOT EXISTS `ws_user_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `country` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
