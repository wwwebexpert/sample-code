<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require(APPPATH.'/libraries/REST_Controller.php'); // including rest api library

class App extends REST_Controller {

	function __construct() {

		parent::__construct();
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->baseurl = $this->config->item('base_url');	// App base URL
		$this->load->model('common_model');					// Adding your model to use for api


		// Limiting request per hour by using limit feature
		// Set $config['rest_enable_limits'] = true; and create limits tables as described in rest.php application/config/rest.php
		$this->methods['test_post']['limit'] = 500; //500 requests per hour per user / key
	}

	/**
	 *********************************************************************
	 *	Function Name :	check_empty() .
	 *	Functionality : check data validation
	 *********************************************************************
	 **/
	public function check_empty( $data, $message = '', $numeric = false )
	{
		$message = ( !empty( $message ) ) ? $message : 'Invalid data';
		if( empty( $data ) )
	    {
	   	
	    	$data = array( 'status' => 0, 'message' => $message );
	    	$this->response($data, 200);
	    }
	    if( $numeric )
	    {
	    	if( !is_numeric( $data ) )
		    {
		    	$data = array( 'status' => 0, 'message' => 'Invalid data' );
	    		$this->response($data, 200);
		    }
	    }
	}


	/**
	 ***********************************************************
	 *	Function Name :	test								
	 *	Functionality : Created for testing purpose.
	 *					checks if app contoller is properly integrated with rest architecture.
	 *	@access 		public 									
	 *	@param 		  : input 				
	 *	@return 	  : input data
	 ***********************************************************
	 **/
	public function test_post()
	{
		$input = $this->input->post('input');
		echo $input;
	}

	public function register_post()
	{
		$username = $this->input->post('username');
		$this->check_empty( $username, 'username is Empty' );

		$password = $this->input->post('password');
		$this->check_empty( $password, 'password is Empty' );

		$post_data=array(						
           'username'   => $username,
           'password'   => md5($password),
           'created'	=> $this->created
        );
		
		$last_id = $this->common_model->add( $table = 'ws_users', $post_data );
		if( $last_id )
		{
			$data = array( 'status' => 1, 'data' => 'Registered successfully' );
		}
		else
		{
			$data = array( 'status' => 0, 'data' => 'Error while adding to database.' );
		}
	    $this->response($data, 200);

	}

	public function login_post()
	{
		$username = $this->input->post('username');
		$this->check_empty( $username, 'username is Empty' );

		$password = $this->input->post('password');
		$this->check_empty( $password, 'password is Empty' );

		$where_data=array( 
			'username'=> $username,
			'password'=> md5($password)
			 );
		$valid_user = $this->common_model->findWhere('ws_users', $where_data, $multi_record = false);

		if( $valid_user )
		{
			unset($valid_user['password']);
			$data = array( 'status' => 1, 'data' => $valid_user );
		}
		else
		{
			$data = array( 'status' => 0, 'data' => 'Invalid details' );
		}
	    $this->response($data, 200);
	}

}
/* End of file welcome.php */
/* Location: ./application/modules/welcome/controllers/welcome.php */