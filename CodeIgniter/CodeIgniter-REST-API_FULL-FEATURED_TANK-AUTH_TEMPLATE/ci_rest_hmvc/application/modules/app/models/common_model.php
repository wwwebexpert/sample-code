<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
---------------------------------------------------------------
*	Class:	Admin_model extends Model defined in Core libraries
*	Author: Parveen Chauhan
*	Platform:	Codeigniter
*	Company:	Cogniter Technologies
*	Description:	Manage database functionality for content. 
---------------------------------------------------------------
*/
class Common_model extends MY_Model {
	


	/**
	 *********************************************************************
	 *	Function Name :	is_user_exists() .
	 *	Functionality : checks user exists already or not based on username
	 *	@param 		  : username string
	 *	@param 		  : id integer
	 *********************************************************************
	 **/
	function is_exists( $callback, $table, $field, $id = null )
	{ 
		if( !empty( $id ) )
		{			
			$this->db->where( $field );
			$this->db->where( array( 'id !=' => $id ) );
		}
		else
		{
			//log_message("error", "Its not working"); 
			$this->db->where( $field );
		}
	
		$this->db->from( $table );
		$data = $this->db->count_all_results();
	
		if( $data > 0 )
		{
			foreach($field as $field_name)
			$this->form_validation->set_message( $callback, "The %s $field_name is already exists.");
			return false;
		}
		else
			return true;
	}


	function getQuery( $query, $delete = FALSE )
	{
		$q = $this->db->query( $query );
		if( $delete )
		{
			return $q;
		}


		if ($q->num_rows() > 0)
		{			
       		$data = $q->result_array();
       		return $data;
		}
		else
		{
			return false;
		}
	}


	function get_files( $table, $offset=0, $count = true, $order = null )
	{
		if(  !empty( $order ) )
        {
        	$this->db->order_by( $order, 'desc' );
        }
		$this->db->from( $table );
		if( $count )
        {
            $data = $this->db->get();
            return count( $data->result() );
        }
        else
        {
        	$this->db->limit( 5, $offset );
            $data = $this->db->get();
            //echo $this->db->last_query();
            if(count($data->result()) > 0)
            {
                return $data->result();
            } 
            else
            {
                return false;
            }
        }
	}


	function findWhereData(  $table, $offset=0, $count = true, $where_data = array() )
	{
		$this->db->order_by("id", "desc");
		if( !empty( $where_data ) && is_array( $where_data ) )
        { 
            $this->db->where( $where_data );
        }
		$this->db->from( $table );
		if( $count )
        {
            $data = $this->db->get();
            //echo $this->db->last_query();
            return count( $data->result() );
        }
        else
        {
        	$this->db->limit( 5, $offset );
            $data = $this->db->get();
            //echo $this->db->last_query();
            if(count($data->result()) > 0)
            {
                return $data->result();
            } 
            else
            {
                return false;
            }
        }
	}

	function add(  $table = null, $data = null )
	{
	    if ( empty( $data ) || empty( $table ) )
	    {
	        return false;
	    }
		$this->db->insert( $table, $data);
		//echo $this->db->last_query();die;
		if( $this->db->insert_id() > 0 ) {
			return $this->db->insert_id();
		}
		else {
			return false;
		}
		
	}

	function delete( $table, $where_data = array() )
	{
		$this->db->where( $where_data );
		if ( $this->db->delete( $table ) ) {
			return TRUE;
		}else{
			FALSE;
		}
	}

	function fetch_all( $table )
    {
        if( empty( $table ) )
        {
            return false;
        }
        $this->db->from( $table );
        $data = $this->db->get();
        if(count($data->result()) > 0) 
        {
            return $data->result();
        }
        else
        {
            return false;
        } 
    }

    /**
     * Returns last insert id
     *
     * @return number
     */
    function lastId()
    {
        return $this->db->insert_id();
    }

     /**
     *********************************************************************
     *  Function Name : check_empty() .
     *  Functionality : check data validation
     *********************************************************************
     **/
    public function check_empty( $data, $message = '', $numeric = false )
    {
        $message = ( !empty( $message ) ) ? $message : 'Invalid data';
        if( empty( $data ) )
        {
            
            $data = array( 'status' => 0, 'message' => $message );
            $this->response($data, 200);
        }
        if( $numeric )
        {
            if( !is_numeric( $data ) )
            {
                $data = array( 'status' => 0, 'message' => 'Invalid data' );
                $this->response($data, 200);
            }
        }
    }


}
