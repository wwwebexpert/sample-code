<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MX_Controller
 *
 * @package	Clients
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */
class Clients extends MX_Controller {

	function __construct() {

		/**
		 * @author:	Harish Chauhan
		 *
		 * @desc: 	Parent constructors are not called implicitly if the child class defines a constructor.
		 * In order to run a parent constructor, a call to parent::__construct() within the child constructor is required.
		 * If the child does not define a constructor then it may be inherited from the parent class just like a normal class method 
		 * (if it was not declared as private).
		 */
		parent::__construct();
	}

	/**
	 * Loads th index page after login || The default login page
	 *
	 * @access	public
	 * @param	string
	 * @return	string
	 */
	public function index() {
		
		if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		} else {
			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();

			$this->load->module('layouts');
			$this->load->library('template');
			$this->template
			->set_layout('users')
			->build('list',isset($data) ? $data : NULL);
		
		}
	}

	/**
	 * Prints hello message on page load
	 *
	 * @access	public
	 * @param	string
	 * @return	string
	 */
	public function hello() {

		if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		} else {
			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();

			$this->load->module('layouts');
			$this->load->library('template');
			$this->template
			->set_layout('asd')
			->build('welcome_message',isset($data) ? $data : NULL);
		
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/modules/welcome/controllers/welcome.php */