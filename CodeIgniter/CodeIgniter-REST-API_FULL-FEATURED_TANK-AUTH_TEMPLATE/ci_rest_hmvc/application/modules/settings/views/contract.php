<section id="content"> <section class="vbox"> <section class="scrollable padder">
	<ul class="breadcrumb no-border no-radius b-b b-light pull-in">
		<li><a href="<?=base_url()?>"><i class="fa fa-home"></i> <?=lang('home')?></a></li>
		<li><a href="<?=base_url()?>settings/email"><?=lang('settings')?></a></li>
		<li class="active"><?=lang('email_settings')?></li>
	</ul>
	<?php  echo modules::run('sidebar/flash_msg');?>
	 <div class="row">
	<!-- Start Form -->

<div class="col-lg-12">
      
        <!-- Account Form -->
        <section class="panel panel-default">
      <header class="panel-heading font-bold">Event Contract Settings</header>
      <div class="panel-body">

       <?php     
$attributes = array('class' => 'bs-example form-horizontal');
echo form_open(base_url().'settings/update/contract', $attributes); ?>
        <div class="form-group">
				<label class="col-lg-2 control-label">Title <span class="text-danger">*</span></label>
				<div class="col-lg-10">
					<input type="text" class="form-control" name="contract_title" value="<?=$this->config->item('contract_title')?>" required>
				</div>
		</div>
        <div class="form-group">
				<label class="col-lg-2 control-label">Contract <span class="text-danger">*</span></label>
				<div class="col-lg-10">
					<textarea style="min-height:300px" class="form-control" name="contract" required><?=$this->config->item('contract')?></textarea>
				</div>
		</div>
		<div class="form-group">
				<label class="col-lg-2 control-label">Payment Terms <span class="text-danger">*</span></label>
				<div class="col-lg-10">
					<textarea style="min-height:300px" class="form-control" name="contract_payment_terms" required><?=$this->config->item('contract_payment_terms')?></textarea>
				</div>
		</div>	
        <div class="form-group">
				<label class="col-lg-2 control-label">Billing Instructions <span class="text-danger">*</span></label>
				<div class="col-lg-10">
					<textarea style="min-height:300px" class="form-control" name="contract_billing_instructions" required><?=$this->config->item('contract_billing_instructions')?></textarea>
				</div>
		</div>	
		<div class="form-group">
				<label class="col-lg-2 control-label">Cancellation <span class="text-danger">*</span></label>
				<div class="col-lg-10">
					<textarea class="form-control" name="contract_cancellation" required><?=$this->config->item('contract_cancellation')?></textarea>
				</div>
		</div>	
		<div class="form-group">
				<label class="col-lg-2 control-label">Change of Date <span class="text-danger">*</span></label>
				<div class="col-lg-10">
					<textarea style="min-height:300px" class="form-control" name="contract_change_of_date" required><?=$this->config->item('contract_change_of_date')?></textarea>
				</div>
		</div>
		<div class="form-group">
				<label class="col-lg-2 control-label">Additional Terms ans Conditions <span class="text-danger">*</span></label>
				<div class="col-lg-10">
					<textarea style="min-height:300px" class="form-control" name="contract_terms" required><?=$this->config->item('contract_terms')?></textarea>
				</div>
		</div>
		<div class="form-group">
				<label class="col-lg-2 control-label">MODIFICATIONS AND CHANGES TO THE CONTRACT <span class="text-danger">*</span></label>
				<div class="col-lg-10">
					<textarea class="form-control" name="contract_modifications" required><?=$this->config->item('contract_modifications')?></textarea>
				</div>
		</div>
		<div class="form-group">
				<label class="col-lg-2 control-label">OUTSIDE VENDORS:<span class="text-danger">*</span></label>
				<div class="col-lg-10">
					<textarea class="form-control" name="contract_outside_vendors" required><?=$this->config->item('contract_outside_vendors')?></textarea>
				</div>
		</div>
		<div class="form-group">
				<label class="col-lg-2 control-label">APPROVAL FOR OUTSIDE CATERING & POLICY FOR ALCOHOLIC BEVERAGES: <span class="text-danger">*</span></label>
				<div class="col-lg-10">
					<textarea style="min-height:300px" class="form-control" name="contract_policy" required><?=$this->config->item('contract_policy')?></textarea>
				</div>
		</div>
		<div class="form-group">
				<label class="col-lg-2 control-label">FORCE MAJEURE<span class="text-danger">*</span></label>
				<div class="col-lg-10">
					<textarea style="min-height:300px" class="form-control" name="contract_force_majeure" required><?=$this->config->item('contract_force_majeure')?></textarea>
				</div>
		</div>
		<div class="form-group">
				<label class="col-lg-2 control-label">WEDDING REHEARSAL<span class="text-danger">*</span></label>
				<div class="col-lg-10">
					<textarea style="min-height:300px" class="form-control" name="contract_wedding_rehearsal" required><?=$this->config->item('contract_wedding_rehearsal')?></textarea>
				</div>
		</div>
		<div class="form-group">
				<label class="col-lg-2 control-label">MANDATORY ADDITIONAL WAITERS/BARTENDERS/SECURITY TO BE ADDED:<span class="text-danger">*</span></label>
				<div class="col-lg-10">
					<textarea style="min-height:300px" class="form-control" name="contract_additional" required><?=$this->config->item('contract_additional')?></textarea>
				</div>
		</div>
		<div class="form-group">
				<label class="col-lg-2 control-label">EVENT DETAILS AND WHATS INCLUDED IN YOUR STERLING BANQUET HALL PACKAGE :<span class="text-danger">*</span></label>
				<div class="col-lg-10">
					<textarea style="min-height:300px" class="form-control" name="contract_event_details" required><?=$this->config->item('contract_event_details')?></textarea>
				</div>
		</div>
        <button type="submit" class="btn btn-sm btn-success pull-right"><?=lang('save_changes')?></button>
      </form>




    </div>
  </section>
  <!-- /Account form -->
  
    </div>



</div>
</section>
</section>
<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
</section>