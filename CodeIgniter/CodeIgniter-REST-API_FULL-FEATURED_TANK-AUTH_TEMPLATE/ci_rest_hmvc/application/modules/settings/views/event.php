<section id="content"> <section class="vbox"> <section class="scrollable padder">
	<ul class="breadcrumb no-border no-radius b-b b-light pull-in">
		<li><a href="<?=base_url()?>"><i class="fa fa-home"></i> <?=lang('home')?></a></li>
		<li><a href="<?=base_url()?>settings/update/general"><?=lang('settings')?></a></li>
		<li class="active">Event Settings</li>
	</ul>
	<?php  echo modules::run('sidebar/flash_msg');?>
	 <div class="row">
	 
				<div class="col-lg-3">				  
				<ul class="nav">	
				<li class="b-b b-light bg-light dk"><a href="<?=base_url('settings/update/event')?>">Event Settings</a></li>
				<li class="b-b b-light bg-light dk"><a href="<?=base_url('admin')?>">Event Types</a></li>
				<li class="b-b b-light bg-light dk"><a href="<?=base_url('admin/event_status')?>">Event Status</a></li>
				<li class="b-b b-light bg-light dk"><a href="<?=base_url('admin/event_description')?>">Item Description</a></li>
				<li class="b-b b-light bg-light dk"><a href="<?=base_url('admin/service_type')?>">Service Type</a></li>
				<li class="b-b b-light bg-light dk"><a href="<?=base_url('rooms')?>">Create / Edit Venues</a></li>
				</ul>
				</div>

	<div class="col-lg-9">

	<section class="panel panel-default">
	<header class="panel-heading font-bold"><i class="fa fa-cogs"></i> <?=lang('general_settings')?></header>
	<div class="panel-body">
	  <?php     
$attributes = array('class' => 'bs-example form-horizontal');
echo form_open(uri_string(), $attributes); ?>
<input type="hidden" name="r_url" value="<?=uri_string()?>">
			<div class="form-group">
				<label class="col-lg-4 control-label">Event Start Time<span class="text-danger">*</span></label> 
				<div class="col-lg-8">
				<select class="form-control" name="start_time">
						<?php $from_time=1;
						$to_time=24;		
						for ($i = $from_time; $i <= $to_time; $i++) 
						{ 						
						$time=date('H:i:s', mktime($i, 0, 0, 0, 2006));?>
						<option <?php if($this->config->item('start_time')==$i) echo "selected='selected'"?> value="<?php echo $i ?>"><?php echo $time ?></option>
						<?php  if($i==$to_time)
						break;
						} ?>
				</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-lg-4 control-label">Event End Time<span class="text-danger">*</span></label> 
				<div class="col-lg-8">
					<select class="form-control" name="end_time">
						<?php $from_time=1;
						$to_time=24;		
						for ($i = $from_time; $i <= $to_time; $i++) 
						{ 						
						$time=date('H:i:s', mktime($i, 0, 0, 0, 2006));?>
						<option <?php if($this->config->item('end_time')==$i) echo "selected='selected'"?> value="<?php echo $i ?>"><?php echo $time ?></option>
						<?php  if($i==$to_time)
						break;
						} ?>
				</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-lg-4 control-label">Security Deposit<span class="text-danger">*</span></label> 
				<div class="col-lg-8">
					<input type="text" name="security_deposit" class="form-control" value="<?=$this->config->item('security_deposit')?>" required>
				</div>
			</div>
			<div class="form-group">
				<div class="col-lg-offset-4 col-lg-8">
				<button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i> <?=lang('save_changes')?></button>
				</div>
			</div>
		</form>

	</div> </section>
</div>
<!-- End Form -->




</div>

</section>
</section>
<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
</section>
