<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Sidebar extends MX_Controller {

	function __construct() {

			parent::__construct();
		}

		public function left_sidebar() {

			$this->load->view('left_sidebar',isset($data) ? $data : NULL);
		}

		public function footer() {

			$this->load->view('footer',isset($data) ? $data : NULL);
		}

		public function top_header() {

			$this->load->view('top_header',isset($data) ? $data : NULL);
		}

		public function flash_msg() {

			$this->load->view('flash_msg',isset($data) ? $data : NULL);
		}
}
/* End of file sidebar.php */