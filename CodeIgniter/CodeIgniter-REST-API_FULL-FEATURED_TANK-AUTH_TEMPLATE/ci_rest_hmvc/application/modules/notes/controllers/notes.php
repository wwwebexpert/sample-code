<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MX_Controller
 *
 * @package	Package Name
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */
class Notes extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('tank_auth');
		if (!$this->tank_auth->get_username()) {
			$this->session->set_flashdata('message', 'You are not allowed to access this page. Please contact the system admin for assistance.');
			redirect('');
		}
		$this->load->library('form_validation');
		$this->load->model('note_model');
		$this->prefix = $this->config->item('db_table_prefix');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->table = $this->prefix.'notes';
	}

	function index(){
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$data['notes'] 	= $this->note_model->get_notes();
		//pr($data['notes'], true);
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('notes')
		->build('notes',isset($data) ? $data : NULL);
	}
	function add()
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('notes')
		->build('notes_add',isset($data) ? $data : NULL);


		if ($this->input->post() ) 
		{	

			$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
			$this->form_validation->set_rules('note_title', 'Note title', 'required');
			$this->form_validation->set_rules('note_description', 'Note description', 'required');		
			if ( $this->form_validation->run() == FALSE )
			{
				$data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('error', 'Please add required fieds');
				$_POST = '';

				$this->load->module('layouts');
				$this->load->library('template');
				$this->template
				->set_layout('notes')
				->build('notes_add',isset($data) ? $data : NULL);
			
			}else{

				$note_title =$this->input->post('note_title');
				$note_description = $this->input->post('note_description');

				$notes_data=array(
                   'note_title'       	=> $note_title,
                   'note_description' 	=> $note_description,
                   'created'	=> $this->created
			    );
				if ( $this->note_model->add(  $this->table, $notes_data ) )  // success
				{
					$this->session->set_flashdata('success', 'Note added successfully');
					$_POST = '';
					redirect('notes/add');		
				}else{			// Failed password not matched					
					$this->session->set_flashdata('error', 'Error in adding database.');
					$_POST = '';
					redirect('notes/add');
				}
			}			
		}
	}

	function edit()
	{
		$note_id = $this->uri->segment(4);
		$note='';
		$note = $this->note_model->get_note( $note_id );

		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$data['note']		= $note;
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('notes')
		->build('notes_edit',isset($data) ? $data : NULL);


		//echo $note_id;	die();
		if ($this->input->post() ) 
		{	
			//echo $note_id;	die();

			$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
			$this->form_validation->set_rules('note_title', 'Note title', 'required');
			$this->form_validation->set_rules('note_description', 'Note description', 'required');		
			if ( $this->form_validation->run() == FALSE )
			{
				$data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('error', 'Please add required fieds');
				$_POST = '';
				redirect('notes/edit/id/'.$note_id);		
			
			}else{

				$note_title =$this->input->post('note_title');
				$note_description = $this->input->post('note_description');



				$notes_data=array(
                   'note_title'       	=> $note_title,
                   'note_description' 	=> $note_description,
                   'created'	=> $this->created
			    );

			    $where_data=array( 'id' => $note_id );

				if ( $this->note_model->updateWhere(  $this->table, $where_data, $notes_data ) )  // success
				{
					$this->session->set_flashdata('success', 'Note Updated successfully');
					$_POST = '';
					redirect('notes/edit/id/'.$note_id);		
				}else{			// Failed password not matched					
					$this->session->set_flashdata('error', 'Error in adding database.');
					$_POST = '';
					redirect('notes/edit/id/'.$note_id);
				}
			}			
		}
	}
	function delete()
	{
		$note_id = $this->uri->segment(4);
		$this->note_model->delete_note($note_id);
		header('Location: '.base_url().'notes');
	}



}

/* End of file welcome.php */
/* Location: ./application/modules/welcome/controllers/welcome.php */