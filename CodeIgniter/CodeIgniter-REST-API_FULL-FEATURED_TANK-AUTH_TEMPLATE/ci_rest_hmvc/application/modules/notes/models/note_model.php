<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Note_model extends MY_Model
{	
	function __construct() {
		$this->prefix = $this->config->item('db_table_prefix');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->table = $this->prefix.'notes';
	}

	function get_notes()
	{
		$where_data = array('status' => 1 );
		return $this->findWhere( $this->table, $where_data, $multi_record = TRUE );
	}
	function get_note($note_id)
	{
		$where_data = array('id' => $note_id );
		return $this->findWhere($this->table, $where_data, $multi_record = FALSE);

	}
	function delete_note($note_id)
	{
		$where_data 	= array('id' => $note_id );
		$post 	= array('status' => 0 );
		return $this->updateWhere($this->table, $where_data, $post);
	}


}

/* End of file model.php */