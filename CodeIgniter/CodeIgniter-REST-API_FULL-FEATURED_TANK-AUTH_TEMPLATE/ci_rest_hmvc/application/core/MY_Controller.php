<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * VCI_Controller extends Controller default controller of codeigniter
 * overriding the default controller. provides additional 
 * functionality for layout and views and handles unauthorized user access
 * to admin panel and provides common functionality
 *
 * @author Vince Balrai
 */
class MY_Controller extends CI_Controller {
	
	function __construct() 
	{
		parent::__construct();
	}
}
