<?php
$login = array(
    'name'  => 'login',
    'id'    => 'login',
    'class' => 'form-control',
    'value' => set_value('login'),
    'placeholder' => 'User Id',
    'maxlength' => 80,
    'size'  => 30,
);
if ($login_by_username AND $login_by_email) {
    $login_label = 'Email or login';
} else if ($login_by_username) {
    $login_label = 'Login';
} else {
    $login_label = 'Email';
}
$password = array(
    'name'  => 'password',
    'id'    => 'password',
    'class' => 'form-control',
    'placeholder' => 'Password',
    'size'  => 30,
);
$remember = array(
    'name'  => 'remember',
    'id'    => 'remember',
    'value' => 1,
    'checked'   => set_value('remember')
);
$captcha = array(
    'name'  => 'captcha',
    'id'    => 'captcha',
    'maxlength' => 8,
);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="#" type="image/png">

    <title>Login</title>

    <link href="<?=base_url('assets') ?>/css/style.css" rel="stylesheet">
    <link href="<?=base_url('assets') ?>/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?=base_url('assets') ?>/js/html5shiv.js"></script>
    <script src="<?=base_url('assets') ?>/js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="login-body">

<div class="container">

    <?php 
        $form_attributes = array( 'class' => 'form-signin' );
        echo form_open( $this->uri->uri_string(), $form_attributes ); 
    ?>
        <div class="form-signin-heading text-center">
            <h1 class="sign-title">Sign In</h1>
            <img src="<?=base_url('assets') ?>/images/login-logo.png" alt=""/>
        </div>
        <div class="login-wrap">

            <?php 
                echo form_input($login);

                if( form_error($login['name']) ){
                    echo '<p>'. form_error($login['name']); 
                    echo isset($errors[$login['name']])?$errors[$login['name']]:''.'</p>'; 
                }
            ?>

            <?php 
                echo form_password($password); 

                if( form_error($password['name']) ){ 
                    echo isset($errors[$password['name']])?$errors[$password['name']]:''; 
                }
            ?>

            <!--Captcha -->
            <?php if ($show_captcha) {
                if ($use_recaptcha) { ?>
            <tr>
                <td colspan="2">
                    <div id="recaptcha_image"></div>
                </td>
                <td>
                    <a href="javascript:Recaptcha.reload()">Get another CAPTCHA</a>
                    <div class="recaptcha_only_if_image"><a href="javascript:Recaptcha.switch_type('audio')">Get an audio CAPTCHA</a></div>
                    <div class="recaptcha_only_if_audio"><a href="javascript:Recaptcha.switch_type('image')">Get an image CAPTCHA</a></div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="recaptcha_only_if_image">Enter the words above</div>
                    <div class="recaptcha_only_if_audio">Enter the numbers you hear</div>
                </td>
                <td><input type="text" id="recaptcha_response_field" name="recaptcha_response_field" /></td>
                <td style="color: red;"><?php echo form_error('recaptcha_response_field'); ?></td>
                <?php echo $recaptcha_html; ?>
            </tr>
            <?php } else { ?>
            <tr>
                <td colspan="3">
                    <p>Enter the code exactly as it appears:</p>
                    <?php echo $captcha_html; ?>
                </td>
            </tr>
            <tr>
                <td><?php echo form_label('Confirmation Code', $captcha['id']); ?></td>
                <td><?php echo form_input($captcha); ?></td>
                <td style="color: red;"><?php echo form_error($captcha['name']); ?></td>
            </tr>
            <?php }
            } ?>
            <!-- -->


            <button class="btn btn-lg btn-login btn-block" type="submit">
                <i class="fa fa-check"></i>
            </button>

            <div class="registration">
                Not a member yet?
                <a class="" href="<?=base_url()?>auth/register">
                    Signup
                </a>
            </div>
            <label class="checkbox">
                <?php echo form_checkbox($remember); ?>
                Remember me
                <span class="pull-right">
                    <!--<a data-toggle="modal" href="#myModal"> Forgot Password?</a>-->
                    <a href="<?=base_url()?>auth/forgot_password"> Forgot Password?</a>
                </span>
            </label>

        </div>

        <!-- Modal -->
        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Forgot Password ?</h4>
                    </div>
                    <div class="modal-body">
                        <p>Enter your e-mail address below to reset your password.</p>
                        <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">

                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                        <button class="btn btn-primary" type="button">Submit</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- modal -->

    <?php echo form_close(); ?>

</div>



<!-- Placed js at the end of the document so the pages load faster -->

<!-- Placed js at the end of the document so the pages load faster -->
<script src="<?=base_url('assets') ?>/js/jquery-1.10.2.min.js"></script>
<script src="<?=base_url('assets') ?>/js/bootstrap.min.js"></script>
<script src="<?=base_url('assets') ?>/js/modernizr.min.js"></script>

</body>
</html>
