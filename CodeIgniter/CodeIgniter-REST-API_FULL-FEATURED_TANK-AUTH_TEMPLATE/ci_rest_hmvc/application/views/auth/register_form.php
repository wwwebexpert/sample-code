<?php
if ($use_username) {
    $username = array(
        'name'  => 'username',
        'id'    => 'username',
        'class' => 'form-control',
        'value' => set_value('username'),
        'maxlength' => $this->config->item('username_max_length', 'tank_auth'),
        'size'      => 30,
        'autofocus' => 'autofocus',
        'placeholder'   => 'User Name'
    );
}
$email = array(
    'name'  => 'email',
    'id'    => 'email',
    'class' => 'form-control',
    'placeholder' => 'Email',
    'autofocus' => 'autofocus',
    'value'     => set_value('email'),
    'maxlength' => 80,
    'size'      => 30,
);
$password = array(
    'name'  => 'password',
    'id'    => 'password',
    'class' => 'form-control',
    'placeholder' => 'Password',
    'value' => set_value('password'),
    'maxlength' => $this->config->item('password_max_length', 'tank_auth'),
    'size'      => 30,
);
$confirm_password = array(
    'name'  => 'confirm_password',
    'id'    => 'confirm_password',
    'class' => 'form-control',
    'placeholder' => 'Confirm Password',
    'value' => set_value('confirm_password'),
    'maxlength' => $this->config->item('password_max_length', 'tank_auth'),
    'size'      => 30,
);
$captcha = array(
    'name'  => 'captcha',
    'id'    => 'captcha',
    'maxlength' => 8,
    'class' => 'form-control',
);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="#" type="image/png">

    <title>Registration</title>

    <link href="<?=base_url('assets')?>/css/style.css" rel="stylesheet">
    <link href="<?=base_url('assets')?>/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?=base_url('assets')?>/js/html5shiv.js"></script>
    <script src="<?=base_url('assets')?>/js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="login-body">

<div class="container">

    <?php 
        $form_attributes = array( 'class' => 'form-signin' );
        echo form_open( $this->uri->uri_string(), $form_attributes ); 
    ?>
        <div class="form-signin-heading text-center">
            <h1 class="sign-title">Registration</h1>
            <img src="<?=base_url('assets')?>/images/login-logo.png" alt=""/>
        </div>


        <div class="login-wrap">
            <p>PLEASE ENTER YOUR DETAILS BELOW TO REGISTER</p>

            <?php 

            if ($use_username) { 
                echo form_label('Username', $username['name']); 
                echo form_input($username); 
                echo form_error($username['name']);
                echo isset($errors[$username['name']])?$errors[$username['name']]:'';
            }
            echo form_label('Email Address', $email['id']); 
            echo form_input($email);
            echo form_error($email['name']);
            echo isset($errors[$email['name']])?$errors[$email['name']]:''; 
        
            echo form_label('Password', $password['id']); 
            echo form_password($password); 
            echo form_error($password['name']);
            
            echo form_label('Confirm Password', $confirm_password['id']);
            echo form_password($confirm_password);
            echo form_error($confirm_password['name']); ?>

            <?php if ($captcha_registration) {
                if ($use_recaptcha) { ?>
            <tr>
                <td colspan="2">
                    <div id="recaptcha_image"></div>
                </td>
                <td>
                    <a href="javascript:Recaptcha.reload()">Get another CAPTCHA</a>
                    <div class="recaptcha_only_if_image"><a href="javascript:Recaptcha.switch_type('audio')">Get an audio CAPTCHA</a></div>
                    <div class="recaptcha_only_if_audio"><a href="javascript:Recaptcha.switch_type('image')">Get an image CAPTCHA</a></div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="recaptcha_only_if_image">Enter the words above</div>
                    <div class="recaptcha_only_if_audio">Enter the numbers you hear</div>
                </td>
                <td><input type="text" id="recaptcha_response_field" name="recaptcha_response_field" /></td>
                <td style="color: red;"><?php echo form_error('recaptcha_response_field'); ?></td>
                <?php echo $recaptcha_html; ?>
            </tr>
            <?php } else { ?>
            <tr>
                <td colspan="3">
                    <p>Enter the code exactly as it appears:</p>
                    <?php echo $captcha_html; ?>
                </td>
            </tr>
            <tr>
                <td><?php echo form_label('Confirmation Code', $captcha['id']); ?></td>
                <td><?php echo form_input($captcha); ?></td>
                <td style="color: red;"><?php echo form_error($captcha['name']); ?></td>
            </tr>
            <?php }
            } ?>

            <label class="checkbox">
                <input required type="checkbox" value="agree this condition"> I agree to the Terms of Service and Privacy Policy
            </label>

            <button type="submit" class="btn btn-lg btn-login btn-block" name='register'>
                <i class="fa fa-check"></i>
            </button>

            <div class="registration">
                Already Registered.
                <a href="<?=base_url()?>auth/login" class="">
                    Login
                </a>
            </div>

        </div>

    <?php echo form_close(); ?>

</div>

<!-- Placed js at the end of the document so the pages load faster -->

<!-- Placed js at the end of the document so the pages load faster -->
<script src="<?=base_url('assets')?>/js/jquery-1.10.2.min.js"></script>
<script src="<?=base_url('assets')?>/js/bootstrap.min.js"></script>
<script src="<?=base_url('assets')?>/js/modernizr.min.js"></script>

</body>
</html>