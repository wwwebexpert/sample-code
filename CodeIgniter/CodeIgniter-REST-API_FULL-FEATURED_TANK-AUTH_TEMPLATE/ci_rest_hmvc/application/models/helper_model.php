<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Helper_model extends MY_Model {

	public function check_user_password($password)
	{
			$user_id = $this->tank_auth->get_user_id();
			if (!is_null($user = $this->users->get_user_by_id($user_id, TRUE))) 
			{
				$user_id = $this->session->userdata('user_id');
				// Check if password correct
				$hasher = new PasswordHash(
						$this->config->item('phpass_hash_strength', 'tank_auth'),
						$this->config->item('phpass_hash_portable', 'tank_auth'));
				if ($hasher->CheckPassword($password, $user->password)) 
				{
					return TRUE;
				}else{
				 	return FALSE;
				}
			}else{
				return FALSE;
			}
	}
	public function prefix()
	{

		$this->db->where('fix_key', 'prefix');
		$query = $this->db->get('config');
		if ($query->num_rows() == 1) {
			$data = $query->row_array();
			return $data['value'];
		}else{

		}
		return NULL;
	}

}
