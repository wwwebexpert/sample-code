<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
class ControllerRestapiCheckoutCart extends Controller {
	public function index() {
		$this->load->language('checkout/cart');
		$this->load->model('restapi/restapimodel');
		$postData = $user_id = '';
		$postData =	$this->request->post;
		//print_r($postData);
		$user_id 	 		= $postData["user_id"];
		$cust_session_id    = (!empty($postData["session_id"]) ? (($postData["session_id"]==1) ?  $postData["session_id"]:''): '');

		if(empty($user_id) || empty($cust_session_id)){
			$data['message'] = "User or session id Is Empty";
			$data['status'] = 201;
			$this->response->setOutput(json_encode($data));
			return;
		}else{
				$profile_data = $this->model_restapi_restapimodel->get_profile($user_id);				
				if(empty(@$profile_data)){
					$data["message"] = "This user  Does not Exist";
					$data['status'] = 201;
					$this->response->setOutput(json_encode($data));
					return;
				}
				

		}
		
		$this->document->setTitle($this->language->get('heading_title'));

		/*$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'href' => $this->url->link('restapi/common/home'),
			'text' => $this->language->get('text_home')
		);

		$data['breadcrumbs'][] = array(
			'href' => $this->url->link('restapi/checkout/cart'),
			'text' => $this->language->get('heading_title')
		);*/

		//$data['text_count'] = $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0);
		//print_r($this->cart->hasProducts($user_id)); 
		$expData  = $data = $mdata = array(); 
		//t($this->cart->hasProducts($user_id),1);
		if ($this->cart->hasProducts($user_id,$cust_session_id)) {
			//die('if');
		//	$data['heading_title'] = $this->language->get('heading_title');

			/*$data['text_recurring_item'] = $this->language->get('text_recurring_item');
			$data['text_next'] = $this->language->get('text_next');
			$data['text_next_choice'] = $this->language->get('text_next_choice');

			$data['column_image'] = $this->language->get('column_image');
			$data['column_name'] = $this->language->get('column_name');
			$data['column_model'] = $this->language->get('column_model');
			$data['column_quantity'] = $this->language->get('column_quantity');
			$data['column_price'] = $this->language->get('column_price');
			$data['column_total'] = $this->language->get('column_total');

			$data['button_update'] = $this->language->get('button_update');
			$data['button_remove'] = $this->language->get('button_remove');
			$data['button_shopping'] = $this->language->get('button_shopping');
			$data['button_checkout'] = $this->language->get('button_checkout');*/
				
			if (!$this->cart->hasStock() && (!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning'))) {
				//$data['error_warning'] = $this->language->get('error_stock');
			} elseif (isset($this->session->data['error'])) {
			//	$data['error_warning'] = $this->session->data['error'];

				unset($this->session->data['error']);
			} else {
				//$data['error_warning'] = '';
			}

			if ($this->config->get('config_customer_price') && $user_id ) {
				//$data['attention'] = sprintf($this->language->get('text_login'), $this->url->link('restapi/account/login'), $this->url->link('restapi/account/register'));
			} else {
				//$data['attention'] = '';
			}

			if (isset($this->session->data['success'])) {
				//$data['success'] = $this->session->data['success'];

				unset($this->session->data['success']);
			} else {
				//$data['success'] = '';
			}

			//$data['action'] = $this->url->link('restapi/checkout/cart/edit', '', true);
			$customerData = $this->db->query("SELECT oc_customer.firstname as firstname,oc_customer.lastname as lastname ,
			oc_address.address_id,oc_address.address_1,oc_address.city,oc_address.postcode,oc_country.name as country ,
			oc_zone.name as zone
			FROM `oc_customer` 
			left Join oc_address on oc_customer.address_id =oc_address.address_id 
			left Join oc_country on oc_country.country_id =oc_address.country_id 
			left Join oc_zone on oc_zone.zone_id =oc_address.zone_id 
			WHERE oc_customer.`customer_id` = '".$user_id."'");

		   $expData["customer_default_address_detail"] = $customerData->rows[0];
			if ($this->config->get('config_cart_weight')) {
				$expData['weight'] = $this->weight->format($this->cart->getWeight(), $this->config->get('config_weight_class_id'), $this->language->get('decimal_point'), $this->language->get('thousand_point'));
			} else {
				$expData['weight'] = '';
			}

			$this->load->model('tool/image');
			$this->load->model('tool/upload');

			//$data['products'] = array();

			$products = $this->cart->getProducts($user_id,$cust_session_id);
			//print_r($products); die('oklajkoa');
			$color=array();
			foreach ($products as $product) {
				$product_total = 0;
				
				foreach ($products as $product_2) {
					if ($product_2['product_id'] == $product['product_id']) {
						$product_total += $product_2['quantity'];
					}
				}

				if ($product['minimum'] > $product_total) {
					$data['error_warning'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
				}

				if ($product['image']) {
					//$image = $this->model_tool_image->resize($product['image'], $this->config->get($this->config->get('config_theme') . '_image_cart_width'), $this->config->get($this->config->get('config_theme') . '_image_cart_height'));
					//$image = $this->model_tool_image->resize($product['image']);
					
					$image = HTTP_SERVER.'image/'.$product['image'];
					
				} else {
					$image = '';
				}

				/*if(empty($image)){
						 		$image = $this->config->get('config_url') .'image/cache/placeholder-279x400.png';;
						 	}*/
				//$option_data = array();
				

				foreach ($product['option'] as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}
					//$size='';
					//$color='';
					if(strtolower($option['name'])=='size'){
						$size = (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value);
					}
					if(empty($size)){
						$size = '';
					}
					if(strtolower($option['name'])=='color'){

						$color = (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value);
					}
				     
					/*if(empty($color)){
						$color = '';
					}*/
				   
					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
					);
				}
				// echo "Color12".$color[0];
				 
				 //echo"<pre>";
				 //print_r($option_data);

							/**
                             * add seller information on the product page through code end
                             */

                          
                            	$this->load->model('account/customerpartner');
                                $this->load->language('customerpartner/profile');

                                $check_seller = $this->model_account_customerpartner->getProductSellerDetails($product['product_id']);
                                
                                 $tempArr["product_seller_detail"] =[];
                            if ($check_seller) {

                                            $this->load->model('customerpartner/master');
                                            $seller["seller_id"] = $check_seller['customer_id'];
                                            $partner = $this->model_customerpartner_master->getProfile($check_seller['customer_id']);
                                          
                                            switch ($this->config->get('marketplace_product_name_display')) {
                                                case 'sn':
                                                    $seller['seller_name'] = $partner['firstname']. ' ' .$partner['lastname'];
                                                    break;

                                                case 'cn':
                                                    $seller['seller_name'] = $partner['companyname'];
                                                    break;

                                                case 'sncn':
                                                    $seller['seller_name'] = $partner['firstname']. ' ' .$partner['lastname'].' '.'And'.' '.$partner['companyname'];
                                                    break;  
                                            }
                                        }
				if(empty($seller['seller_name'])){
					$seller['seller_name'] = '';
				}
				// Display prices
				if ($user_id /*|| !$this->config->get('config_customer_price')*/) {
					//$price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					$price = $product['price'];
				} else {
					$price = 0.0;
				}

				// Display prices
				
				if ($user_id /*|| !$this->config->get('config_customer_price')*/) {
					$total = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'], $this->session->data['currency']);
				} else {
					$total = 0;
				}

				$recurring = '';

				if ($product['recurring']) {
					$frequencies = array(
						'day'        => $this->language->get('text_day'),
						'week'       => $this->language->get('text_week'),
						'semi_month' => $this->language->get('text_semi_month'),
						'month'      => $this->language->get('text_month'),
						'year'       => $this->language->get('text_year'),
					);

					if ($product['recurring']['trial']) {
						$recurring = sprintf($this->language->get('text_trial_description'), $this->currency->format($this->tax->calculate($product['recurring']['trial_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['trial_cycle'], $frequencies[$product['recurring']['trial_frequency']], $product['recurring']['trial_duration']) . ' ';
					}

					if ($product['recurring']['duration']) {
						$recurring .= sprintf($this->language->get('text_payment_description'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
					} else {
						$recurring .= sprintf($this->language->get('text_payment_cancel'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
					}
				}
				
				$expData['cart_product_list'][] = array(
					'cart_item_id'   => $product['cart_id'],
					'product_image'     =>($image==null)?'':$image, 
					'product_id'      => $product['product_id'],					
					'product_name'      => $product['name'],
					'product_price'      => $price,
					'product_size'     => ($size==null)?'':$size,
					'product_color'     => ($color==null)?'':$color,
					'seller_name' => $seller['seller_name'],
					'currency' => 'NGN',
					"quantity" => $product['quantity']
				//	'option'    => $option_data,
				//  'recurring' => $recurring,
					/*'product_qty'  => $product['quantity'],
					'stock'     => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
					'reward'    => ($product['reward'] ? sprintf($this->language->get('text_points'), $product['reward']) : ''),
					
					'total'     => $total,
					'href'      => $this->url->link('restapi/product/product', 'product_id=' . $product['product_id'])*/
				);
				
			}
			$expData["product_count"] = $this->cart->countProducts($user_id,$cust_session_id);
			// Gift Voucher
			/*$data['vouchers'] = array();

			if (!empty($this->session->data['vouchers'])) {
				foreach ($this->session->data['vouchers'] as $key => $voucher) {
					$data['vouchers'][] = array(
						'key'         => $key,
						'description' => $voucher['description'],
						'amount'      => $this->currency->format($voucher['amount'], $this->session->data['currency']),
						'remove'      => $this->url->link('restapi/checkout/cart', 'remove=' . $key)
					);
				}
			}*/

			// Totals
			$this->load->model('extension/extension');
			//print_r("dataa".$this->cart->getTaxes());
			$totals = array();
			$taxes = $this->cart->getTaxes();
			$total = 0;
			
			// Because __call can not keep var references so we put them into an array. 			
			$total_data = array(
				'totals' => &$totals,
				'taxes'  => &$taxes,
				'total'  => &$total
			);
			
			/*// Display prices
			if ( !$this->config->get('config_customer_price')) {
				$sort_order = array();

				$results = $this->model_extension_extension->getExtensions('total');

				foreach ($results as $key => $value) {
					$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
				}

				array_multisort($sort_order, SORT_ASC, $results);
				// print_r($data);
				foreach ($results as $result) {

					if (!empty($result['code']) && ($this->config->get($result['code'] . '_status'))) {
						$this->load->model('extension/total/' . $result['code']);
						
						// We have to put the totals in an array so that they pass by reference.
						//echo "<hr>total_data:";print_r($total_data);
						
						$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
					}
				}*/

				/*$sort_order = array();

				foreach ($totals as $key => $value) {
					$sort_order[$key] = $value['sort_order'];
				}

				array_multisort($sort_order, SORT_ASC, $totals);
			}*/

			//$data['totals'] = array();

			/*foreach ($totals as $total) {
				$data['totals'][] = array(
					'title' => $total['title'],
					'text'  => $this->currency->format($total['value'], $this->session->data['currency'])
				);
			}*/
			$opdata = array();
			$opdata["status"]  	 = 200;
			$opdata["message"] 	 = "Success";
			$opdata["data"] 	 = $expData;
			
			/*$data['continue'] = $this->url->link('restapi/common/home');

			$data['checkout'] = $this->url->link('restapi/checkout/checkout', '', true);*/

			$this->load->model('extension/extension');

			/*$data['modules'] = array();
			
			$files = glob(DIR_APPLICATION . '/controller/restapi/total/*.php');

			if ($files) {
				foreach ($files as $file) {
					$result = $this->load->controller('restapi/total/' . basename($file, '.php'));
					//print_r($result);
					// unset(@$result["countries"]);
					if ($result) {
						$data['modules'][] = $result;
					}
				}
			}*/		
			// unset($this->session->data["customer_id"]);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($opdata));
		} else {
				
			$mdata['heading_title'] = $this->language->get('heading_title');

			$mdata['text_error'] = $this->language->get('text_empty');

			$mdata['button_continue'] = $this->language->get('button_continue');

			$mdata['continue'] = $this->url->link('restapi/common/home');

			unset($this->session->data['success']);
			$data["status"] 	= 201;
			$data["message"] 	= "failure";
			$data["data"] 		= $mdata;				
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($data));		
		}
	}

	public function add() {
		$this->load->language('checkout/cart');

		$json = array();

		if (isset($this->request->post['product_id'])) {
			$product_id = (int)$this->request->post['product_id'];
		} else {
			$product_id = 0;
		}

		$this->load->model('catalog/product');

		$product_info = $this->model_catalog_product->getProduct($product_id);

		if ($product_info) {
			if (isset($this->request->post['quantity']) && ((int)$this->request->post['quantity'] >= $product_info['minimum'])) {
				$quantity = (int)$this->request->post['quantity'];
			} else {
				$quantity = $product_info['minimum'] ? $product_info['minimum'] : 1;
			}

			if (isset($this->request->post['option'])) {
				$option = array_filter($this->request->post['option']);
			} else {
				$option = array();
			}

			$product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);

			foreach ($product_options as $product_option) {
				if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
					$json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
				}
			}

			if (isset($this->request->post['recurring_id'])) {
				$recurring_id = $this->request->post['recurring_id'];
			} else {
				$recurring_id = 0;
			}

			$recurrings = $this->model_catalog_product->getProfiles($product_info['product_id']);

			if ($recurrings) {
				$recurring_ids = array();

				foreach ($recurrings as $recurring) {
					$recurring_ids[] = $recurring['recurring_id'];
				}

				if (!in_array($recurring_id, $recurring_ids)) {
					$json['error']['recurring'] = $this->language->get('error_recurring_required');
				}
			}

			if (!$json) {
				$this->cart->add($this->request->post['product_id'], $quantity, $option, $recurring_id);

$json['success'] = sprintf($this->language->get('text_success'), '#', $product_info['name'], '#');

				// Unset all shipping and payment methods
				unset($this->session->data['shipping_method']);
				unset($this->session->data['shipping_methods']);
				unset($this->session->data['payment_method']);
				unset($this->session->data['payment_methods']);

				// Totals
				$this->load->model('extension/extension');

				$totals = array();
				$taxes = $this->cart->getTaxes();
				$total = 0;
		
				// Because __call can not keep var references so we put them into an array. 			
				$total_data = array(
					'totals' => &$totals,
					'taxes'  => &$taxes,
					'total'  => &$total
				);

				// Display prices
				if ($user_id /*|| !$this->config->get('config_customer_price')*/) {
					$sort_order = array();

					$results = $this->model_extension_extension->getExtensions('total');

					foreach ($results as $key => $value) {
						$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
					}

					array_multisort($sort_order, SORT_ASC, $results);

					foreach ($results as $result) {
						if ($this->config->get($result['code'] . '_status')) {
							$this->load->model('total/' . $result['code']);

							// We have to put the totals in an array so that they pass by reference.
							$this->{'model_total_' . $result['code']}->getTotal($total_data);
						}
					}

					$sort_order = array();

					foreach ($totals as $key => $value) {
						$sort_order[$key] = $value['sort_order'];
					}

					array_multisort($sort_order, SORT_ASC, $totals);
				}

				$json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total, $this->session->data['currency']));
			} else {
				$json['redirect'] = str_replace('&amp;', '&', $this->url->link('restapi/product/product', 'product_id=' . $this->request->post['product_id']));
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function edit() {
		$this->load->language('checkout/cart');

		$json = array();

		// Update
		if (!empty($this->request->post['quantity'])) {
			foreach ($this->request->post['quantity'] as $key => $value) {
				$this->cart->update($key, $value);
			}

			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['reward']);

			$this->response->redirect($this->url->link('restapi/checkout/cart'));
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function remove() {
		$this->load->language('checkout/cart');

		$json = array();

		// Remove
		if (isset($this->request->post['key'])) {
			$this->cart->remove($this->request->post['key']);

			unset($this->session->data['vouchers'][$this->request->post['key']]);

			$this->session->data['success'] = $this->language->get('text_remove');

			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['reward']);

			// Totals
			$this->load->model('extension/extension');

			$totals = array();
			$taxes = $this->cart->getTaxes();
			$total = 0;

			// Because __call can not keep var references so we put them into an array. 			
			$total_data = array(
				'totals' => &$totals,
				'taxes'  => &$taxes,
				'total'  => &$total
			);

			// Display prices
			if ($user_id /*|| !$this->config->get('config_customer_price')*/) {
				$sort_order = array();

				$results = $this->model_extension_extension->getExtensions('total');

				foreach ($results as $key => $value) {
					$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
				}

				array_multisort($sort_order, SORT_ASC, $results);

				foreach ($results as $result) {
					if ($this->config->get($result['code'] . '_status')) {
						$this->load->model('total/' . $result['code']);

						// We have to put the totals in an array so that they pass by reference.
						$this->{'model_total_' . $result['code']}->getTotal($total_data);
					}
				}

				$sort_order = array();

				foreach ($totals as $key => $value) {
					$sort_order[$key] = $value['sort_order'];
				}

				array_multisort($sort_order, SORT_ASC, $totals);
			}

			$json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total, $this->session->data['currency']));
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
