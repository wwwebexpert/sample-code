<?php
class ControllerRestapiTotalCoupon extends Controller {
	public function index() {
		
		if ($this->config->get('coupon_status')) {
			$this->load->language('total/coupon');

			$data['heading_title'] = $this->language->get('heading_title');

			$data['text_loading'] = $this->language->get('text_loading');

			$data['entry_coupon'] = $this->language->get('entry_coupon');

			$data['button_coupon'] = $this->language->get('button_coupon');

			if (isset($this->session->data['coupon'])) {
				$data['coupon'] = $this->session->data['coupon'];
			} else {
				$data['coupon'] = '';
			}

			return $data;
		}
	}

	public function coupon() {
		error_reporting(E_ALL);
ini_set('display_errors', 1);
		$this->load->language('total/coupon');
		$postData =	$this->request->post;

		$json = array();

		$this->load->model('extension/total/couponmobi');
		
		
		if (@$postData['coupon']) {
			$data['coupon'] = $postData['coupon'];
			$data['user_id'] = $postData['user_id'];
			$this->session->data['customer_id'] = $data['user_id'];
		} else {
			$data['coupon']  = '';
		}

		$coupon_info = $this->model_extension_total_couponmobi->getCoupon($data);
	print_r($coupon_info);die;
		if (empty($data['coupon'])) {
			$json['error'] = $this->language->get('error_empty');

			unset($this->session->data['coupon']);
		} elseif ($coupon_info) {
			$this->session->data['coupon'] = $data['coupon'];
			

			$this->session->data['success'] = $this->language->get('text_success');

			//$json['redirect'] = $this->url->link('restapi/checkout/cart');
		} else {
			$json['error'] = $this->language->get('error_coupon');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
