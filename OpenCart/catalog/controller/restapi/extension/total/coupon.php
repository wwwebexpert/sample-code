<?php  
class ControllerRestapiExtensionTotalCoupon extends Controller {
	public function index() {
		if ($this->config->get('coupon_status')) {
			$this->load->language('extension/total/coupon');

			$data['heading_title'] = $this->language->get('heading_title');

			$data['text_loading'] = $this->language->get('text_loading');

			$data['entry_coupon'] = $this->language->get('entry_coupon');

			$data['button_coupon'] = $this->language->get('button_coupon');

			if (isset($this->session->data['coupon'])) {
				$data['coupon'] = $this->session->data['coupon'];
			} else {
				$data['coupon'] = '';
			}

			return $this->load->view('extension/total/coupon', $data);
		}
	}

	public function coupon() {
		$this->load->language('extension/total/coupon');
		$this->load->model('restapi/restapimodel');
		$this->response->addHeader('Content-Type: application/json');

		$data = array();

		$this->load->model('extension/total/coupon');
		$user_id =$this->request->post['user_id'];
		$coupon  = $this->request->post['coupon'];
		
		if(empty($user_id) || empty($coupon)){
			$data["status"] = 201;
            $data["message"] = "Please enter the seller id or coupon.";
		    $this->response->setOutput(json_encode($data)); die;
		}

		if(!empty($user_id)){
				$profile_data = $this->model_restapi_restapimodel->get_profile($user_id);
				
				if(empty(@$profile_data)){
					$data["message"] = "This user  Does not Exist";
					$data['status'] = 201;
					$this->response->setOutput(json_encode($data));
					return;
				}else{
					$this->session->data['customer_id'] = $user_id;
					$coupon_info = $this->model_restapi_restapimodel->getCoupon($coupon,$user_id);

					if (empty($coupon)) {
						$data['message'] = $this->language->get('error_empty');
						$data['status'] = 201;

						unset($this->session->data['coupon']);
					} elseif ($coupon_info) {
						$this->session->data['coupon'] = $coupon;

						//$this->session->data['success'] = $this->language->get('text_success');
						$data["message"] = $this->language->get('text_success');
						$data['status'] = 200;
						$data["data"]  = $coupon_info;
						//$json['redirect'] = $this->url->link('checkout/cart');
					} else {							
						$data['message'] = $this->language->get('error_coupon');
						$data['status'] = 201;
					}


				}
			}else{
				    $data['message'] = "User Id Is Empty";
					$data['status'] = 201;
					$this->response->setOutput(json_encode($data));
					return;

			}
		
		$this->response->setOutput(json_encode($data));
	}
}
