<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

class ControllerRestapiAccountReset extends Controller {
	private $error = array();

	public function index() {
		
		/*if ($this->customer->isLogged()) {
			$this->response->redirect($this->url->link('restapi/account/account', '', true));
		}*/

		if (isset($this->request->post['code'])) {
			$code = $this->request->post['code'];
		} else {
			$code = '';
		}

		$this->load->model('account/customer');

		$customer_info = $this->model_account_customer->getCustomerByCode($code);

		if ($customer_info) {

			$this->load->language('api-global-lang');
			$this->load->language('account/reset');

			$data['status'] 	= $this->language->get('_api_success_code');
			$data['message'] 	= $this->language->get('_text_match_success');

			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($data));
		
		} else {

			$this->load->language('api-global-lang');
			$this->load->language('account/reset');

			$data['status'] 	= $this->language->get('_api_error_code');
			$data['message'] 	= $this->language->get('error_code');

			$this->session->data['error'] = $this->language->get('error_code');

			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($data));

			//return new Action('account/login');
		}
	}

	protected function validate() {
		if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
			$this->error['password'] = $this->language->get('error_password');
		}

		if ($this->request->post['confirm'] != $this->request->post['password']) {
			$this->error['confirm'] = $this->language->get('error_confirm');
		}

		return !$this->error;
	}
}