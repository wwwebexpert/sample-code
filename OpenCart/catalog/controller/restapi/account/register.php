<?php
class ControllerRestapiAccountRegister extends Controller {
	private $error = array();

	public function index() {
		$data		 = array();    		
		$this->load->model('account/customer');
		$this->load->model('restapi/restapimodel');
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) { 
			$customer_id =	$this->session->data['customer_id'];					
		}

		$errMess   = array();
		
		if (isset($this->error['firstname'])) {
			$errMess[] = 'Firstname';
		}

		if (isset($this->error['lastname'])) {			
			$errMess[] = 'Lastname';
		}

		if (isset($this->error['email'])) {
			$errMess[] = 'E-mail';			
		}
		
		if (isset($this->error['telephone'])) {
			$errMess[] = 'Telephone';			
		}

		if (isset($this->error['password'])) {
			$errMess[] = 'Password';			
		}
		;
		
		if(count($errMess)>0){			
			$str 			 = implode(", ",$errMess);
			$data["message"] = "Required parameters ".$str." are missing!";
	  		$data["status"]  = "201";	 
	  		
					
		} else if (isset($this->error['duplicate_email'])){							
				$data["message"] = "Email address already exist!";
		  		$data["status"]  = "201";	 		  		
			
		} else {
			$this->load->language('account/register');	 
	      	$customer_id 		 = $this->model_restapi_restapimodel->addCustomer($this->request->post);	  		
	      	if(!empty($customer_id)){
	      		$data["message"] = " Customer register successfully!";
			  	$data["status"]  = "200";
			} else {
				$data["message"] = " Customer not register successfully!";
		  		$data["status"] = "201";
	      	}

		}
						
		$this->response->setOutput(json_encode($data));
	}


	private function validate() {

		if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {			
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if ($this->model_restapi_restapimodel->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['duplicate_email'] = $this->language->get('error_email');
		}

		if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
			$this->error['telephone'] = $this->language->get('error_telephone');
		}

		if ((utf8_strlen(trim($this->request->post['address_1'])) < 3) || (utf8_strlen(trim($this->request->post['address_1'])) > 128)) {
			$this->error['address_1'] = $this->language->get('error_address_1');
		}

		if ((utf8_strlen(trim($this->request->post['city'])) < 2) || (utf8_strlen(trim($this->request->post['city'])) > 128)) {
			$this->error['city'] = $this->language->get('error_city');
		}

		$this->load->model('localisation/country');
		
		if ($this->request->post['country_id'] == '') {
			$this->error['country'] = $this->language->get('error_country');
		}

		if ($this->request->post['zone_id'] == '') {
			$this->error['zone'] = $this->language->get('error_zone');
		}

		
		// Custom field validation
		$this->load->model('account/custom_field');

		if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
			$this->error['password'] = $this->language->get('error_password');
		}

		if ($this->request->post['password'] != $this->request->post['password']) {
			$this->error['confirm'] = $this->language->get('error_confirm');
		}
		
		return !$this->error;
	}


	public function customfield() {
		$json = array();

		$this->load->model('account/custom_field');

		// Customer Group
		if (isset($this->request->get['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->get['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $this->request->get['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		$custom_fields = $this->model_account_custom_field->getCustomFields($customer_group_id);

		foreach ($custom_fields as $custom_field) {
			$json[] = array(
				'custom_field_id' => $custom_field['custom_field_id'],
				'required'        => $custom_field['required']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}