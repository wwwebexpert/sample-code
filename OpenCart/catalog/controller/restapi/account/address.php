<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

class ControllerRestapiAccountAddress extends Controller {
	private $error = array();

	public function index() {

		$this->load->language('account/address');
		$this->load->language('api-global-lang');

		$this->load->model('account/address');
		$this->load->model('restapi/restapimodel');

		$this->getList();
	}

	public function add() {

		$this->load->language('api-global-lang');
		$this->load->language('account/address');
		$this->load->model('account/address');
		$this->load->model('restapi/restapimodel');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			
			$address_id = $this->model_restapi_restapimodel->addAddress($this->request->post);
			
			//$this->session->data['success'] = $this->language->get('text_add');
			$data['status'] 	= $this->language->get('_api_success_code');
			$data['message'] 	= $this->language->get('text_add');

			$data['data']['firstname'] 	= $this->request->post['firstname'];
			$data['data']['lastname'] 	= $this->request->post['lastname'];
			$data['data']['address_1'] 	= $this->request->post['address_1'];
			$data['data']['city'] 		= $this->request->post['city'];
			$data['data']['postcode'] 	= $this->request->post['postcode'];
			$data['data']['country_id'] 	= $this->request->post['country_id'];
			$data['data']['zone_id'] 		= $this->request->post['zone_id'];
			$data['data']['address_id'] 	= $address_id;

			// Add to activity log
			$this->load->model('account/activity');
			//$this->response->redirect($this->url->link('restapi/account/address', '', true));

			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($data));
		
		} else {
			$this->getForm();
		}
	}

	public function edit() {
		/*if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('restapi/account/address', '', true);

			$this->response->redirect($this->url->link('restapi/account/login', '', true));
		}*/

		$this->load->language('api-global-lang');
		$this->load->language('account/address');

		$this->load->model('account/address');
		$this->load->model('restapi/restapimodel');

		/*$this->document->setTitle($this->language->get('heading_title'));

		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

		$this->load->model('account/address');*/
		
		//echo $this->request->post['firstname']; die();

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			
			//die('in popst');

			$this->model_restapi_restapimodel->editAddress($this->request->post['address_id'], $this->request->post, $customer_id = $this->request->post['user_id']);

			// Default Shipping Address
			if (isset($this->session->data['shipping_address']['address_id']) && ($this->request->get['address_id'] == $this->session->data['shipping_address']['address_id'])) {
				$this->session->data['shipping_address'] = $this->model_restapi_restapimodel->getAddress($this->request->get['address_id']);

				unset($this->session->data['shipping_method']);
				unset($this->session->data['shipping_methods']);
			}

			// Default Payment Address
			if (isset($this->session->data['payment_address']['address_id']) && ($this->request->get['address_id'] == $this->session->data['payment_address']['address_id'])) {
				$this->session->data['payment_address'] = $this->model_restapi_restapimodel->getAddress($this->request->get['address_id']);

				unset($this->session->data['payment_method']);
				unset($this->session->data['payment_methods']);
			}

			$this->session->data['success'] = $this->language->get('text_edit');

			// Add to activity log
			$this->load->model('account/activity');

			/*$activity_data = array(
				'customer_id' => $this->customer->getId(),
				'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
			);

			$this->model_account_activity->addActivity('address_edit', $activity_data);*/

			//$this->response->redirect($this->url->link('restapi/account/address', '', true));

			$data['status'] 	= $this->language->get('_api_success_code');
			$data['message'] 	= 'Updated Successfull!';

			$data['data']['firstname'] 	= $this->request->post['firstname'];
			$data['data']['lastname'] 	= $this->request->post['lastname'];
			$data['data']['address_1'] 	= $this->request->post['address_1'];
			$data['data']['city'] 		= $this->request->post['city'];
			$data['data']['postcode'] 	= $this->request->post['postcode'];
			$data['data']['country_id'] 	= $this->request->post['country_id'];
			$data['data']['zone_id'] 		= $this->request->post['zone_id'];
			$data['data']['address_id'] 	= $this->request->post['address_id'];

			// Add to activity log
			//$this->load->model('account/activity');
			//$this->response->redirect($this->url->link('restapi/account/address', '', true));

			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($data));
		
		} else {
			$this->getForm();
		}
	}

	public function delete() {

		/*if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('restapi/account/address', '', true);

			$this->response->redirect($this->url->link('restapi/account/login', '', true));
		}*/

		$this->load->language('api-global-lang');
		$this->load->language('account/address');
		$this->load->model('restapi/restapimodel');

		//$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('account/address');

		if ( $this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateDelete( $user_id = $this->request->post['user_id'] )) {

			$this->model_restapi_restapimodel->deleteAddress($this->request->post['address_id'], $user_id = $this->request->post['user_id']);

			// Default Shipping Address
			if (isset($this->session->data['shipping_address']['address_id']) && ($this->request->post['address_id'] == $this->session->data['shipping_address']['address_id'])) {
				unset($this->session->data['shipping_address']);
				unset($this->session->data['shipping_method']);
				unset($this->session->data['shipping_methods']);
			}

			// Default Payment Address
			if (isset($this->session->data['payment_address']['address_id']) && ($this->request->post['address_id'] == $this->session->data['payment_address']['address_id'])) {
				unset($this->session->data['payment_address']);
				unset($this->session->data['payment_method']);
				unset($this->session->data['payment_methods']);
			}

			$this->session->data['success'] = $this->language->get('text_delete');

			// Add to activity log
			$this->load->model('account/activity');

			$activity_data = array(
				'customer_id' => $user_id,
				'name'        => $user_id
			);
			
			$this->model_account_activity->addActivity('address_delete', $activity_data);

			/**/
			$data['status'] 	= $this->language->get('_api_success_code');
			$data['message'] 	= $this->language->get('text_delete');

			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($data));
			/**/

			//$this->response->redirect($this->url->link('restapi/account/address', '', true));
		}

		if (isset($this->error['address_id'])) {
			//$data['error_firstname'] = $this->error['firstname'];
			$data['status'] 	= $this->language->get('_api_error_code');
			$data['message'] = $this->error['address_id'];
		} else {
			//$data['error_firstname'] = '';
		}

		if (isset($this->error['user_id'])) {
			//$data['error_lastname'] = $this->error['lastname'];
			$data['status'] 	= $this->language->get('_api_error_code');
			$data['message'] = $this->error['user_id'];
		} else {
			//$data['error_lastname'] = '';
		}

		if (isset($this->error['warning'])) {
			//$data['error_lastname'] = $this->error['lastname'];
			$data['status'] 	= $this->language->get('_api_error_code');
			$data['message'] = $this->error['warning'];
		} else {
			//$data['error_lastname'] = '';
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));

		//$this->getList();
	}

	protected function getList() {

		if (isset($this->error['warning'])) {
			//$data['error_warning'] = $this->error['warning'];

			$data['status'] 	= $this->language->get('_api_error_code');

		} else {
			//$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			//$data['success'] 	= $this->session->data['success'];
			$data['status'] 	= $this->language->get('_api_success_code');

			unset($this->session->data['success']);
		} else {
			//$data['success'] = '';
		}

		//$data['addresses'] = array();
		$results = $this->model_restapi_restapimodel->getAddresses( $user_id = $this->request->post['user_id'] );

		if (empty($results)) {
			# code...
			$data['status'] 	= $this->language->get('_api_error_code');
			$data['message'] = 'No Addresses in your address book.';
		} else {
			

			$data['status'] 	= $this->language->get('_api_success_code');
			$data['message'] = 'Successfull!';

			foreach ($results as $result) {

				//echo "<pre>"; print_r($result); die();

				if ($result['address_format']) {
					$format = $result['address_format'];
				} else {
					$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
				}

				$find = array(
					'{firstname}',
					'{lastname}',
					'{company}',
					'{address_1}',
					'{address_2}',
					'{city}',
					'{postcode}',
					'{zone}',
					'{zone_code}',
					'{country}'
				);

				$replace = array(
					'firstname' => $result['firstname'],
					'lastname'  => $result['lastname'],
					'company'   => $result['company'],
					'address_1' => $result['address_1'],
					'address_2' => $result['address_2'],
					'city'      => $result['city'],
					'postcode'  => $result['postcode'],
					'zone'      => $result['zone'],
					'zone_code' => $result['zone_code'],
					'country'   => $result['country']
				);

				//$data['address_book'][] = array('' => , );

				$data['data']['address_book'][] = array( 'address_id' => $result['address_id'],
					'firstname' => $result['firstname'], 
					'lastname' 	=> $result['lastname'],
					'address_1' => $result['address_1'],
					'city' 		=> $result['city'],
					'postcode' 	=> $result['postcode'],
					'zone' 		=> $result['zone'],
					'zone_code' => $result['zone_code'],
					'country' 	=> $result['country']
				);

				/*$data['addresses'][] = array(
					'address_id' => $result['address_id'],
					'address'    => str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format)))),
					'update'     => $this->url->link('restapi/account/address/edit', 'address_id=' . $result['address_id'], true),
					'delete'     => $this->url->link('restapi/account/address/delete', 'address_id=' . $result['address_id'], true)
				);*/
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	protected function getForm() {

		/*if (!isset($this->request->get['address_id'])) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_edit_address'),
				'href' => $this->url->link('restapi/account/address/add', '', true)
			);
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_edit_address'),
				'href' => $this->url->link('restapi/account/address/edit', 'address_id=' . $this->request->get['address_id'], true)
			);
		}*/

		/*$data['text_edit_address'] = $this->language->get('text_edit_address');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_firstname'] = $this->language->get('entry_firstname');
		$data['entry_lastname'] = $this->language->get('entry_lastname');
		$data['entry_company'] = $this->language->get('entry_company');
		$data['entry_address_1'] = $this->language->get('entry_address_1');
		$data['entry_address_2'] = $this->language->get('entry_address_2');
		$data['entry_postcode'] = $this->language->get('entry_postcode');
		$data['entry_city'] = $this->language->get('entry_city');
		$data['entry_country'] = $this->language->get('entry_country');
		$data['entry_zone'] = $this->language->get('entry_zone');
		$data['entry_default'] = $this->language->get('entry_default');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_back'] = $this->language->get('button_back');
		$data['button_upload'] = $this->language->get('button_upload');*/

		if (isset($this->error['firstname'])) {
			//$data['error_firstname'] = $this->error['firstname'];
			$data['status'] 	= $this->language->get('_api_error_code');
			$data['message'] = $this->error['firstname'];
		} else {
			//$data['error_firstname'] = '';
		}

		if (isset($this->error['lastname'])) {
			//$data['error_lastname'] = $this->error['lastname'];
			$data['status'] 	= $this->language->get('_api_error_code');
			$data['message'] = $this->error['lastname'];
		} else {
			//$data['error_lastname'] = '';
		}

		if (isset($this->error['address_1'])) {
			//$data['error_address_1'] = $this->error['address_1'];
			$data['status'] 	= $this->language->get('_api_error_code');
			$data['message'] = $this->error['address_1'];
		} else {
			//$data['error_address_1'] = '';
		}

		if (isset($this->error['city'])) {
			//$data['error_city'] = $this->error['city'];
			$data['status'] 	= $this->language->get('_api_error_code');
			$data['message'] = $this->error['city'];
		} else {
			//$data['error_city'] = '';
		}

		if (isset($this->error['postcode'])) {
			//$data['error_postcode'] = $this->error['postcode'];
			$data['message'] = $this->error['postcode'];
			$data['status'] 	= $this->language->get('_api_error_code');
		} else {
			//$data['error_postcode'] = '';
		}

		if (isset($this->error['country'])) {
			//$data['error_country'] = $this->error['country'];
			$data['message'] = $this->error['country'];
			$data['status'] 	= $this->language->get('_api_error_code');
		} else {
			//$data['error_country'] = '';
		}

		if (isset($this->error['zone'])) {
			//$data['error_zone'] = $this->error['zone'];
			$data['message'] = $this->error['zone'];
			$data['status'] 	= $this->language->get('_api_error_code');
		} else {
			//$data['error_zone'] = '';
		}

		if (isset($this->error['custom_field'])) {
			//$data['error_custom_field'] = $this->error['custom_field'];
			$data['message'] = $this->error['custom_field'];
			$data['status'] 	= $this->language->get('_api_error_code');
		} else {
			// $data['error_custom_field'] = array();
		}
		
		/*if (!isset($this->request->get['address_id'])) {
			$data['action'] = $this->url->link('restapi/account/address/add', '', true);
		} else {
			$data['action'] = $this->url->link('restapi/account/address/edit', 'address_id=' . $this->request->get['address_id'], true);
		}*/

		/*if (isset($this->request->get['address_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$address_info = $this->model_account_address->getAddress($this->request->get['address_id']);
		}

		if (isset($this->request->post['firstname'])) {
			$data['data']['firstname'] = $this->request->post['firstname'];
			$data['message'] 	= $this->language->get('error_firstname');
			$data['message'] 	= $this->language->get('error_firstname');
			$data['status'] 	= $this->language->get('_api_error_code');
		} elseif (!empty($address_info)) {
			$data['data']['firstname'] = $address_info['firstname'];
		} else {
			$data['data']['firstname'] = '';
		}

		if (isset($this->request->post['lastname'])) {
			$data['data']['lastname'] = $this->request->post['lastname'];
		} elseif (!empty($address_info)) {
			$data['data']['lastname'] = $address_info['lastname'];
		} else {
			$data['data']['lastname'] = '';
		}*/

		/*if (isset($this->request->post['company'])) {
			$data['data']['company'] = $this->request->post['company'];
		} elseif (!empty($address_info)) {
			$data['data']['company'] = $address_info['company'];
		} else {
			$data['data']['company'] = '';
		}*/

		if (isset($this->request->post['address_1'])) {
			$data['data']['address_1'] = $this->request->post['address_1'];
		} elseif (!empty($address_info)) {
			$data['data']['address_1'] = $address_info['address_1'];
		} else {
			$data['data']['address_1'] = '';
		}

		/*if (isset($this->request->post['address_2'])) {
			$data['data']['address_2'] = $this->request->post['address_2'];
		} elseif (!empty($address_info)) {
			$data['data']['address_2'] = $address_info['address_2'];
		} else {
			$data['data']['address_2'] = '';
		}*/

		if (isset($this->request->post['postcode'])) {
			$data['data']['postcode'] = $this->request->post['postcode'];
		} elseif (!empty($address_info)) {
			$data['data']['postcode'] = $address_info['postcode'];
		} else {
			$data['data']['postcode'] = '';
		}

		if (isset($this->request->post['city'])) {
			$data['data']['city'] = $this->request->post['city'];
		} elseif (!empty($address_info)) {
			$data['data']['city'] = $address_info['city'];
		} else {
			$data['data']['city'] = '';
		}

		if (isset($this->request->post['country_id'])) {
			$data['data']['country_id'] = (int)$this->request->post['country_id'];
		}  elseif (!empty($address_info)) {
			$data['data']['country_id'] = $address_info['country_id'];
		} else {
			$data['data']['country_id'] = $this->config->get('config_country_id');
		}

		if (isset($this->request->post['zone_id'])) {
			$data['data']['zone_id'] = (int)$this->request->post['zone_id'];
		}  elseif (!empty($address_info)) {
			$data['data']['zone_id'] = $address_info['zone_id'];
		} else {
			$data['data']['zone_id'] = '';
		}

		//$this->load->model('localisation/country');

		//$data['countries'] = $this->model_localisation_country->getCountries();

		// Custom fields
		$this->load->model('account/custom_field');

		//$data['custom_fields'] = $this->model_account_custom_field->getCustomFields($this->config->get('config_customer_group_id'));

		/*if (isset($this->request->post['custom_field'])) {
			$data['address_custom_field'] = $this->request->post['custom_field'];
		} elseif (isset($address_info)) {
			$data['address_custom_field'] = $address_info['custom_field'];
		} else {
			$data['address_custom_field'] = array();
		}*/

		/*if (isset($this->request->post['default'])) {
			$data['data']['default'] = $this->request->post['default'];
		} elseif (isset($this->request->get['address_id'])) {
			$data['data']['default'] = $this->customer->getAddressId() == $this->request->get['address_id'];
		} else {
			$data['data']['default'] = false;
		}*/

		//$data['back'] = $this->url->link('restapi/account/address', '', true);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	protected function validateForm() {

		//die('12345');

		if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen(trim($this->request->post['address_1'])) < 3) || (utf8_strlen(trim($this->request->post['address_1'])) > 128)) {
			$this->error['address_1'] = $this->language->get('error_address_1');
		}

		if ((utf8_strlen(trim($this->request->post['city'])) < 2) || (utf8_strlen(trim($this->request->post['city'])) > 128)) {
			$this->error['city'] = $this->language->get('error_city');
		}

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->request->post['country_id']);

		if ($country_info && $country_info['postcode_required'] && (utf8_strlen(trim($this->request->post['postcode'])) < 2 || utf8_strlen(trim($this->request->post['postcode'])) > 10)) {
			$this->error['postcode'] = $this->language->get('error_postcode');
		}

		if ($this->request->post['country_id'] == '' || !is_numeric($this->request->post['country_id'])) {
			$this->error['country'] = $this->language->get('error_country');
		}

		if (!isset($this->request->post['zone_id']) || $this->request->post['zone_id'] == '' || !is_numeric($this->request->post['zone_id'])) {
			$this->error['zone'] = $this->language->get('error_zone');
		}

		// Custom field validation
		/*$this->load->model('account/custom_field');

		$custom_fields = $this->model_account_custom_field->getCustomFields($this->config->get('config_customer_group_id'));

		foreach ($custom_fields as $custom_field) {
			if (($custom_field['location'] == 'address') && $custom_field['required'] && empty($this->request->post['custom_field'][$custom_field['custom_field_id']])) {
				$this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
			} elseif (($custom_field['type'] == 'text' && !empty($custom_field['validation'] && $custom_field['location'] == 'address')) && !filter_var($this->request->post['custom_field'][$custom_field['custom_field_id']], FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => $custom_field['validation'])))) {
                $this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field_validate'), $custom_field['name']);
            }
		}*/

		return !$this->error;
	}

	protected function validateDelete( $user_id ) {

		if ($this->model_restapi_restapimodel->getTotalAddresses( $user_id ) == 1) {
			$this->error['warning'] = $this->language->get('error_delete');
		}

		if ($this->request->post['address_id'] == '') {
			$this->error['address_id'] = "Please enter address id!";
		}

		/*if ($this->customer->getAddressId() == $this->request->get['address_id']) {
			$this->error['warning'] = $this->language->get('error_default');
		}*/

		return !$this->error;
	}
}
