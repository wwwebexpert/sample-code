<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

class ControllerRestapiAccountCountry extends Controller {

	private $error = array();

	public function index() {

		$this->load->language('account/register');
		$this->load->language('api-global-lang');

		$this->load->model('account/customer');

		if (($this->request->server['REQUEST_METHOD'] == 'GET') ) {
			
			$this->load->model('localisation/country');

			$data['status'] 	= $this->language->get('_api_success_code');
			$data['message'] 	= 'Country List Fetched Successfully.';
			$data['data'] = $this->model_localisation_country->getCountries();
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	public function state($arg='')
	{
		$this->load->language('account/register');
		$this->load->language('api-global-lang');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

			$this->load->model('localisation/zone');
			$this->load->model('localisation/country');

			$data['status'] 	= $this->language->get('_api_success_code');
			$data['message'] 	= 'State List Fetched Successfully.';
			$data['data'] = $this->model_localisation_zone->getZonesByCountryId( $this->request->post['country_id'] );
		}

		if (isset($this->error['country'])) {
			$data['status'] = $this->language->get('_api_error_code');
			$data['message'] = $this->error['country'];
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	private function validate() {
		
		if ($this->request->post['country_id'] == '') {
			$this->error['country'] = $this->language->get('error_country');
		}

		if ($this->request->post['country_id'] != '') {
			# code...
			$this->load->model('localisation/country');
			$country_info = $this->model_localisation_country->getCountry( $this->request->post['country_id']);

			if (empty($country_info)) {
				# code...
				$this->error['country'] = $this->language->get('error_country');
			}
		}

		return !$this->error;
	}
}