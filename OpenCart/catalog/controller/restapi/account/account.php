<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

class ControllerRestapiAccountAccount extends Controller {

	private $error = array();

	public function index() {

		/*if (!$this->customer->isLogged()) {

			$this->session->data['redirect'] = $this->url->link('restapi/account/account', '', true);
			$this->response->redirect($this->url->link('restapi/account/login', '', true));
		}*/

		$this->load->language('account/account');

		$this->document->setTitle($this->language->get('heading_title'));

		/*$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('restapi/common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('restapi/account/account', '', true)
		);*/

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
			$data['status'] = true;
			
		} else {
			$data['success'] = '';
		} 

		$data['heading_title'] = $this->language->get('heading_title');

		$data['logged_in'] = true;

		/*$data['text_my_account'] = $this->language->get('text_my_account');
		$data['text_my_orders'] = $this->language->get('text_my_orders');
		$data['text_my_newsletter'] = $this->language->get('text_my_newsletter');
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_password'] = $this->language->get('text_password');
		$data['text_address'] = $this->language->get('text_address');
		$data['text_credit_card'] = $this->language->get('text_credit_card');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_reward'] = $this->language->get('text_reward');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
		$data['text_recurring'] = $this->language->get('text_recurring');

		$data['edit'] = $this->url->link('restapi/account/edit', '', true);
		$data['password'] = $this->url->link('restapi/account/password', '', true);
		$data['address'] = $this->url->link('restapi/account/address', '', true);*/
		
		$data['credit_cards'] = array();
		
		$files = glob(DIR_APPLICATION . 'controller/credit_card/*.php');
		
		foreach ($files as $file) {
			$code = basename($file, '.php');
			
			if ($this->config->get($code . '_status') && $this->config->get($code)) {
				$this->load->language('credit_card/' . $code);

				$data['credit_cards'][] = array(
					'name' => $this->language->get('heading_title'),
					'href' => $this->url->link('restapi/credit_card/' . $code, '', true)
				);
			}
		}
		
		/*$data['wishlist'] = $this->url->link('restapi/account/wishlist');
		$data['order'] = $this->url->link('restapi/account/order', '', true);
		$data['download'] = $this->url->link('restapi/account/download', '', true);*/
		
		if ($this->config->get('reward_status')) {
			$data['reward'] = $this->url->link('restapi/account/reward', '', true);
		} else {
			$data['reward'] = '';
		}		
		
		/*$data['return'] = $this->url->link('restapi/account/return', '', true);
		$data['transaction'] = $this->url->link('restapi/account/transaction', '', true);
		$data['newsletter'] = $this->url->link('restapi/account/newsletter', '', true);
		$data['recurring'] = $this->url->link('restapi/account/recurring', '', true);
		

		$data['column_right'] = $this->load->controller('restapi/common/column_right');*/
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	public function country() {

		$json = array();

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

		if ($country_info) {
			$this->load->model('localisation/zone');

			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
				'status'            => $country_info['status']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	/*
	 * @ Auther: Vaibhav
	 * @ Get the profile data
	 *
	 ***/
	public function profile(){

		
		$user_id =	$this->request->post["user_id"];

		$this->load->model('restapi/restapimodel');
		
		 $profile_data = $this->model_restapi_restapimodel->get_profile($user_id);
	//print_r($profile_data);
		 if(!empty($profile_data)){

		 		$resp["f_name"]  = $profile_data["firstname"];
				$resp["l_name"]  = $profile_data["lastname"];
				$resp["email"]  = $profile_data["email"];
				$resp["contact"]  = $profile_data["telephone"];
				$data['data'] = $resp;
				$data['message'] = "Successful";
				$data['status'] = 200;
		 }else{
		 		
				$data['message'] = "Failure";
				$data['status'] = 201;
		 }
		

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));


	}
	/*
	 * @ Auther: Vaibhav
	 * @ Update the profile data
	 *
	 ***/
	public function edit_profile(){
		$this->response->addHeader('Content-Type: application/json');
		$this->load->model('restapi/restapimodel');
		$postData =	$this->request->post;
		if(!empty($postData)){
			$user_id = $postData["user_id"];
			if(!empty($user_id)){
				$profile_data = $this->model_restapi_restapimodel->get_profile($user_id);
				
				if(empty(@$profile_data)){
					$data["message"] = "This Id Does not Exist";
					$data['status'] = 201;
				}else{

				    $response = $this->model_restapi_restapimodel->edit_profile($postData);
				    if($response==1){
				    	unset($postData['user_id']);
						$data['data'] = $postData;
						$data['message'] = "Successful";
						$data['status'] = 200;
					}else{
						$data['message'] = "Something went wrong";
						$data['status'] = 201;
					}
			    }
			}
			$this->response->setOutput(json_encode($data));
		}
	
		
	}

	/*
	 * @ Auther: Vaibhav
	 * @ Update the Password
	 *
	 ***/
	public function edit_password(){
		$this->response->addHeader('Content-Type: application/json');
		$this->load->model('restapi/restapimodel');
		$postData =	$this->request->post;
		if(!empty($postData)){
			$user_id = $postData["user_id"];
			if(!empty($user_id)){
				$profile_data = $this->model_restapi_restapimodel->get_profile($user_id);
				
				if(empty(@$profile_data)){
					$data["message"] = "This Id Does not Exist";
					$data['status'] = 201;
				}else{
					if($this->validate($profile_data["email"],$postData["old_password"])){

						$response = $this->model_restapi_restapimodel->edit_password($postData["user_id"],$postData["password"]);
					
				    if($response==1){
				    	//unset($postData['user_id']);
						//$data['data'] = $postData;
						$data['message'] = "Successful";
						$data['status'] = 200;
					}else{
						$data['message'] = "Something went wrong";
						$data['status'] = 201;
					}
				  }else{
				  		$data['message'] = "User Id  or Password is wrong";
						$data['status'] = 201;
				  } 
			    }
			}else{
						$data['message'] = "Please fill user Id";
						$data['status'] = 201;
			}
			$this->response->setOutput(json_encode($data));
		}
	}

	/*
	 * @ Auther: Vaibhav
	 * @ Update the Password
	 *
	 ***/
	public function dashboard() {

		static $module = 0;	

		$this->load->language('api-global-lang');

		$this->load->model('design/banner');
		$this->load->model('tool/image');

		//$data['banner_images'] = array();

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateDashboard()) {

			$this->load->model('restapi/restapimodel');
			$postData 		=	$this->request->post;
			$user_id 		= $postData["user_id"];
			$profile_data 	= $this->model_restapi_restapimodel->get_profile($user_id);
			
			if(empty(@$profile_data)){
				
				$data["message"] 	= "This Id Does not Exist";
				$data['status'] 	= 201;
			
			} else {

			    $dashboard_data = $this->model_restapi_restapimodel->dashboard($user_id);
			    
			    if($dashboard_data) {
			    	//unset($postData['user_id']);
			    	$content_top = $this->load->controller('restapi/common/content_top');
			    	//print_r($content_top );
			    	$banners = $content_top["modules"][0]["banners"];
			    	$products = $content_top["modules"][1]["products"];
			    	foreach($products as $key=>$product){
			    		
			    		if($product["thumb"]==null){
			    			$product["thumb"] = '';
			    		}
			    		$productsAlias[] = $product;
			    	}

					$data['data'] 	= $dashboard_data;
					$data['data']["banner_images"] = $banners;
					$data['data']["featured_products"] = $productsAlias;
					
					/*$results = $this->model_design_banner->getBanner(7);

					foreach ($results as $result) {
						if (is_file(DIR_IMAGE . $result['image'])) {

							$data['banner_images'][] = array(
								//'title' => $result['title'],
								//'link'  => $result['link'],
								'image' => $this->model_tool_image->resize($result['image'], 600, 212)
							);

							$my_banner_images[] = $this->model_tool_image->resize($result['image'], 600, 212);

						}
					}*/

					//$data['data']['banner_images'] 	= $my_banner_images;
					$data['message'] 	= "Successful";
					$data['status'] 	= 200;


				} else {
					$data['message'] = "Failure";
					$data['status'] = 201;
				}
		    }
		}

		if (isset($this->error['user_id'])) {
			//$data['error_warning'] = $this->error['warning'];
			$data['status'] 	= $this->language->get('_api_error_code');
			$data['message'] 	= $this->error['user_id'];
		}

		if (isset($this->error['warning'])) {
			//$data['error_warning'] = $this->error['warning'];
			$data['status'] 	= $this->language->get('_api_error_code');
			$data['message'] 	= $this->error['warning'];
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}


	/*
	 * @ Auther: Harish
	 * @ Validate dashboard API
	 *
	 ***/
	protected function validateDashboard() {

		if( empty($this->request->post) ) {
			$this->error['warning'] = 'Please enter input data!';
		}

		if ($this->request->post['user_id'] == '' || !is_numeric($this->request->post['user_id'])) {
			$this->error['user_id'] = 'Please enter an valid user_id!';
		}

		return !$this->error;
	}

	/*
	 * @ Auther: Vaibhav
	 * @ list the product
	 *
	 ***/
	public function product_list(){
		$this->response->addHeader('Content-Type: application/json');
		$this->load->model('restapi/restapimodel');
		$this->load->model('tool/image');
		$this->load->model('account/customerpartner');
		$postData =	$this->request->post;
		
		if(!empty($postData)){
			$user_id = $postData["user_id"];

			if(!empty($user_id)){
				$profile_data = $this->model_restapi_restapimodel->get_profile($user_id);
				
				if(empty(@$profile_data)){
					$data["message"] = "This Id Does not Exist";
					$data['status'] = 201;
				}else{
					/*if (!empty($postData["category_id"])) {*/
						// $product_list = $this->model_restapi_restapimodel->product_list($postData['user_id'],$postData["category_id"]);
						if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_model'])) {
			$filter_model = $this->request->get['filter_model'];
		} else {
			$filter_model = null;
		}

		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = null;
		}

		if (isset($this->request->get['filter_quantity'])) {
			$filter_quantity = $this->request->get['filter_quantity'];
		} else {
			$filter_quantity = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'pd.name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}		

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if($this->config->get('wkmpuseseo'))
			$url = '';
				    $data = array(
						'filter_name'	  => @$filter_name, 
						'filter_model'	  => @$filter_model,
						'filter_price'	  => @$filter_price,
						'filter_quantity' => @$filter_quantity,
						'filter_status'   => @$filter_status,
						'sort'            => @$sort,
						'order'           => @$order,
						'start'           => @($page - 1) * 10,
						'limit'           => @10,
						'customer_id' => $user_id
					);
				 

		$product_total = $this->model_account_customerpartner->getTotalProductsSeller($data);

		$results = $this->model_account_customerpartner->getProductsSeller($data);

		foreach ($results as $key => $result) {

			if(!$results[$key]['product_id'])
				$results[$key]['product_id'] = $result['product_id'] = $key;

			$action = array();
            
             // membership codes starts here
			if($this->config->get('wk_seller_group_status')) {
				$action[] = array(
					'text_edit' => $this->language->get('text_edit'),
					'text_relist' => $this->language->get('text_relist'),
					'text_publish' => $this->language->get('text_publish'),
					'text_unpublish' => $this->language->get('text_unpublish'),
					'text_clone_product' => $this->language->get('text_clone_product'),
					'href_edit' => $this->url->link('account/customerpartner/addproduct', '' . '&edit&product_id=' . $result['product_id'] , 'SSL'),
					'href_relist' => $this->url->link('account/customerpartner/addproduct', '' . '&relist&product_id=' . $result['product_id'] , 'SSL'),
					'href_active_deactive' => $this->url->link('account/customerpartner/addproduct', '' . '&active_deactive&product_id=' . $result['product_id'] , 'SSL'),
					'href_clone' => $this->url->link('account/customerpartner/productlist/copy', '' . '&clone=1&product_id=' . $result['product_id'] , 'SSL'),
					'href_publish' => $this->url->link('account/customerpartner/productlist/publish', '' . '&product_id=' . $result['product_id'] , 'SSL'),
					'href_unpublish' => $this->url->link('account/customerpartner/productlist/unpublish', '' . '&product_id=' . $result['product_id'] , 'SSL'),
				);
			} else {
				$action[] = array(
					'text' => $this->language->get('text_edit'),
					'href' => $this->url->link('account/customerpartner/addproduct', '' . '&product_id=' . $result['product_id'] , 'SSL')
				);
			}

			 // membership codes ends here

			if ($result['image'] && file_exists(DIR_IMAGE . $result['image'])) {
				$thumb = $this->model_tool_image->resize($result['image'], 40, 40);
			} else {
				$thumb = $this->model_tool_image->resize('no_image.jpg', 40, 40);
			}	
			
			$product_sold_quantity = array();
			$sold = $totalearn = 0;

			$product_sold_quantity = $this->model_account_customerpartner->getProductSoldQuantity($result['product_id']);

			if($product_sold_quantity){
				$sold = $product_sold_quantity['quantity'] ? $product_sold_quantity['quantity'] : 0;
				$totalearn = $product_sold_quantity['total'] ? $product_sold_quantity['total'] : 0;
			}
			$results[$key]['price'] = $this->currency->format($result['price'],$this->session->data['currency']);
			$results[$key]['special'] = $result['special'] ? $this->currency->format($result['special'],$this->session->data['currency']): '';
			$results[$key]['thumb'] = $thumb;
			$results[$key]['sold'] = $sold;
			$results[$key]['soldlink'] = $this->url->link('account/customerpartner/soldlist&product_id='.$result['product_id'],'','SSL');
			$results[$key]['totalearn'] = $this->currency->format($totalearn,$this->session->data['currency']);
			$results[$key]['selected'] =  isset($this->request->post['selected']) && in_array($result['product_id'], $this->request->post['selected']);
			$results[$key]['totalearn'] = $this->currency->format($totalearn,$this->session->data['currency']);
			$results[$key]['action'] = $action;
			$results[$key]['productLink'] = $this->url->link('product/product' , 'product_id='.$key, 'SSL');
		}

	$data['products'] = $results;
				    	//unset($postData['user_id']);
						//$data['data']["product_list"] = $product_list;
						$data['message'] = "Successful";
						$data['status'] = 200;
					
			    }/*else{
						$data['message'] = "Provide category Id";
						$data['status'] = 201;
					}*/
					

				   
			}else{
						$data['message'] = "Provide user Id";
						$data['status'] = 201;
					}
			$this->response->setOutput(json_encode($data));
		}
	}


	/*
	 * @ Auther: Vaibhav
	 * @ Categories wise product listing 
	 * @ Parameters:category Id
	 *
	 ***/
	Public function category_wise_product_listing(){
		$this->response->addHeader('Content-Type: application/json');
		$this->load->model('restapi/restapimodel');
		$this->load->model('tool/image');
		$postData =	$this->request->post;
		$category_id=$postData["category_id"];
		$currency_code=$postData["currency_code"];
		$currency = ['GBP','USD','NGN','EUR'];
		if(!in_array($currency_code, $currency)){
			$currency_code = "NGN"; //default currency 	
		}
// echo $currency_code. $category_id;
			if(!empty($category_id)){
				$category_row = $this->db->query("SELECT * FROM `oc_category` WHERE `category_id` = $category_id");
				if($category_row->num_rows ==0){
					$data['message'] = "Category Id is Not valid .";
					$data['status'] = 201;
				}else{
					$product_Rows = $this->db->query("SELECT  `oc_product`.`product_id`,`oc_product`.`tax_class_id`,`oc_product`.`image` as product_image,`oc_product`.`price` as product_price,`oc_product_description`.`name` as product_name ,
									round(AVG(`oc_review`.`rating`), 1) as product_rating
									FROM `oc_product`

									LEFT JOIN `oc_product_to_category` ON `oc_product_to_category`.`product_id` = `oc_product`.`product_id`   

									LEFT JOIN `oc_product_description` ON `oc_product_description`.`product_id` = `oc_product`.`product_id`

									LEFT JOIN `oc_review` ON `oc_review`.`product_id` = `oc_product`.`product_id`

									WHERE `oc_product_to_category`.`category_id`  = $category_id group by `oc_product`.`product_id`");

					$currency_row =  $this->db->query(" SELECT * FROM oc_currency WHERE code = '".$currency_code."' ");

						if($product_Rows->num_rows>0){
							foreach($product_Rows->rows as $product){
								$img = $product["product_image"];
								if (!empty($img)) {

									$image = $this->model_tool_image->resize($img, $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
								} else { 
									
									$image = '';//$this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
								}
								$product["product_image"]  = empty($image)?'':$image;
								$product["currency_code"]  = empty($currency_row->row["code"])?'NGN':$currency_row->row["code"];
								$product["product_price"]  = $this->currency->format_for_app($this->tax->calculate($product["product_price"], $product['tax_class_id'], $this->config->get('config_tax') ? 'P' : false),$currency_row->row["code"],false );
								
								if($product["product_rating"]==null){
									$product["product_rating"] = number_format((float)0,1,".","");
								}else{
									$product["product_rating"] =  number_format((float)$product["product_rating"],2,'.',"");
								}
								$mainArr[] = $product;
							}
							
							$data["data"]["product_list"] = $mainArr;
							$data['message'] = "Successful.";
							$data['status'] = 200; 

						}else{
						    $data['message'] = "No product found for this category.";
							$data['status'] = 200;
							$data["data"]["product_list"] = [];
						}
				}
			}else{
				$data['message'] = "Category Id is empty .";
				$data['status'] = 201;
				$this->response->setOutput(json_encode($data));
				return;
			}
			$this->response->setOutput(json_encode($data));
	}

	/*
	 * @ Author: Vaibhav
	 * @ ADD to cart API 
	 * @ Parameters:user_id,product_id,color_id,size_id
	 *
	 ***/

	public function add_to_cart(){
		$this->response->addHeader('Content-Type: application/json');
		$this->load->model('restapi/restapimodel');
		$postData =	$this->request->post;
		if(!empty($postData)){
			$user_id = $postData["user_id"];
			$product_id = $postData["product_id"];
			if(!empty($product_id)){
				$product_rows = $this->model_restapi_restapimodel->check_product($product_id);
				
				if($product_rows=='0'){  
					$data['message'] = "product not available .";
					$data['status'] = 201;
					$this->response->setOutput(json_encode($data));
					return;
				}
			}else{
					$data['message'] = "Product Id is empty .";
					$data['status'] = 201;
					$this->response->setOutput(json_encode($data));
					return;
			}
			if(!empty($user_id)){
				$profile_data = $this->model_restapi_restapimodel->get_profile($user_id);
				
				if(empty(@$profile_data)){
					$data["message"] = "This User Does Not Exist";
					$data['status'] = 201;
				}else{

				    $response = $this->model_restapi_restapimodel->add_to_cart($postData);
				    
				    if($response>0){
				    	//unset($postData['user_id']);
						//$data['data']["product_list"] = $product_list;
						$data['message'] = "Successful";
						$data['status'] = 200;
					}else{
						$data['message'] = "Failure";
						$data['status'] = 201;
					}
			    }  
			}else{
						$data['message'] = "User id is empty";
						$data['status'] = 201;
					}
			$this->response->setOutput(json_encode($data));
		}
	}
	/*
	 * @ Auther: Vaibhav
	 * @ Cart detail Api:
	 *
	 ***/
	public function cart_details(){

		
		$this->response->addHeader('Content-Type: application/json');
		$this->load->model('restapi/restapimodel');
		$postData =	$this->request->post;
		$currency_code=$postData["currency_code"];
		$currency = ['GBP','USD','NGN','EUR'];
		if(!in_array($currency_code, $currency)){
			$currency_code = "NGN"; //default currency 	
		}
		if(!empty($postData)){
			$user_id = $postData["user_id"];

			if(!empty($user_id)){
				$profile_data = $this->model_restapi_restapimodel->get_profile($user_id);
				
				if(empty(@$profile_data)){
					$data["message"] = "This Id Does not Exist";
					$data['status'] = 201;
				  }else{
				    $cart_details= $this->model_restapi_restapimodel->cart_details($user_id,$currency_code);
				    $data['products'] = array();
					$products = $this->cart->getProducts($user_id);
					print_r($products);
				   if ($this->config->get('config_cart_weight')) {
						$data['weight'] = $this->weight->format($this->cart->getWeight(), $this->config->get('config_weight_class_id'), $this->language->get('decimal_point'), $this->language->get('thousand_point'));
					} else {
						$data['weight'] = '';
					}

					
					    if($cart_details){

					    	//unset($postData['user_id']);
							$data['data'] = $cart_details;
							$data['message'] = "Successful";
							$data['status'] = 200;
						}else{
							$data['message'] = "No item found in your cart!";
							$data['status'] = 201;
						}
			     }

				}
				else{
							$data['message'] = "Provide User Id";
							$data['status'] = 201;
						}
				$this->response->setOutput(json_encode($data));
			}else{
						$data['message'] = "Provide User Id";
						$data['status'] = 201;
						$this->response->setOutput(json_encode($data));
					}
			
		}
	/*
	 * @ Auther: Vaibhav
	 * @ 12/23/2017
	 * @ Cart Edit Api(quantity):
	 ***/	
	public function edit_cart(){
		$this->response->addHeader('Content-Type: application/json');
		$this->load->model('restapi/restapimodel');
		$this->load->model('tool/image');
		$postData =	$this->request->post;
		$product_id = $postData["product_id"];
		if(!empty($postData)){
			if(!empty(@$postData["user_id"])){
				$user_id = $postData["user_id"];

			}else{
				$data['message'] = "User Id is empty";
				$data['status'] = 201;
				$this->response->setOutput(json_encode($data));
				return;
			}
			if(empty(@$postData["product_id"])){
				$data['message'] = "Product Id is empty";
				$data['status'] = 201;
				$this->response->setOutput(json_encode($data));	
				return;

			}else{
				$prodata = $this->db->query("SELECT product_id FROM oc_product WHERE product_id = $product_id");
				if($prodata->num_rows==0){
					$data['message'] = "Product Does not exist";
					$data['status'] = 201;
					$this->response->setOutput(json_encode($data));	
					return;
				}
			}
			if(empty(@$postData["cart_item_id"])){
				$data['message'] = "Cart Id is empty";
				$data['status'] = 201;
				$this->response->setOutput(json_encode($data));	
				return;

			}else{
				$cartid =$postData["cart_item_id"];
				$cartData = $this->db->query("SELECT cart_id FROM oc_cart WHERE cart_id = $cartid");
				if($cartData->num_rows==0){
					$data['message'] = "Cart Does not exist";
					$data['status'] = 201;
					$this->response->setOutput(json_encode($data));	
					return;
				}
			}
			if(!empty($user_id)){
				$profile_data = $this->model_restapi_restapimodel->get_profile($user_id);
				
				if(empty(@$profile_data)){
					$data["message"] = "User Id Does not Exist";
					$data['status'] = 201;
				}else{ 
					$checkCart = $this->db->query("SELECT cart_id FROM oc_cart WHERE cart_id = $cartid AND product_id = $product_id AND customer_id = $user_id");
					if($checkCart->num_rows==0){
						$data['message'] = "There is no cart with these data.";
						$data['status'] = 201;
						$this->response->setOutput(json_encode($data));	
						return;
					}
				    $response = $this->model_restapi_restapimodel->edit_cart($postData); //update only quantity
				   
				    $this->load->model('catalog/product');
					$product_info = $this->model_catalog_product->getProduct($product_id);
					if (!empty($product_info['product_image'])) {
							$image = $this->model_tool_image->resize($product_info['product_image'], $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
						} else {
							$image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
						}
				   	/*if($response == "error"){
				   		$data['message'] = "Something went wrong.";
						$data['status'] = 201;
						$this->response->setOutput(json_encode($data));
						return;
				   	}*/
				    if($response==1){
				    	//unset($postData['user_id']);
				    	$data['message'] = "Successful";
						$data['status'] = 200;
						$data['data']["cart_item_id"] = $postData["cart_item_id"];
						$data['data']["product_image"] = $image;
						$data['data']["product_name"] = $product_info["name"];
						$data['data']["product_price"] = $product_info["price"];
						$data['data']["product_size"] = $product_info["length"] ."*".$product_info["width"]."*".$product_info["height"];
						$data['data']["product_color"] = "";
						$data['data']["product_qty"] = $postData["product_qty"];
						
					}else{
						$data['message'] = "Something went wrong.";
						$data['status'] = 201;
					}
			    }
			}
			$this->response->setOutput(json_encode($data));
		}
	}

	/*
	 * @ Auther: Vaibhav
	 * @ 12/23/2017
	 * @ (13). Cart Delete Api:
	 ***/
	public function delete_cart(){
		$this->response->addHeader('Content-Type: application/json');
		$this->load->model('restapi/restapimodel');
		$postData =	$this->request->post;

		if(!empty($postData)){
			$user_id = $postData["user_id"];
			$product_id = $postData["product_id"];
			$cartid =$postData["cart_item_id"];
					if(empty(@$postData["product_id"])){
						$data['message'] = "Product Id is empty";
						$data['status'] = 201;
						$this->response->setOutput(json_encode($data));	
						return;

					}else{
						$prodata = $this->db->query("SELECT product_id FROM oc_product WHERE product_id = $product_id");
						if($prodata->num_rows==0){
							$data['message'] = "Product Does not exist";
							$data['status'] = 201;
							$this->response->setOutput(json_encode($data));	
							return;
						}
					}
					if(empty(@$postData["cart_item_id"])){
						$data['message'] = "Cart Id is empty";
						$data['status'] = 201;
						$this->response->setOutput(json_encode($data));	
						return;

					}else{
						
						$cartData = $this->db->query("SELECT cart_id FROM oc_cart WHERE cart_id = $cartid");
						if($cartData->num_rows==0){
							$data['message'] = "Cart Does not exist";
							$data['status'] = 201;
							$this->response->setOutput(json_encode($data));	
							return;
						}
					}


			
			if(!empty($user_id)){
				$profile_data = $this->model_restapi_restapimodel->get_profile($user_id);
				
				if(empty(@$profile_data)){
					$data["message"] = "This user  Does not Exist";
					$data['status'] = 201;
				}else{

				    $response = $this->model_restapi_restapimodel->delete_cart($postData); //update only quantity
				   
					
				   	
				    if($response>0){
				    	//unset($postData['user_id']);
				    	$data['message'] = "Successful";
						$data['status'] = 200;
						
					}else{
						$data['message'] = "Something went wrong.";
						$data['status'] = 201;
					}
			    }
			}
			$this->response->setOutput(json_encode($data));
		}

	}

	
	/*
	 * @ Auther: Vaibhav
	 * @ 12/23/2017
	 * @ Order History customer Api
	 * @ Parameters:user_id,gift_code
	 ***/
	public function order_history(){
		$this->response->addHeader('Content-Type: application/json');
		$this->load->model('restapi/restapimodel');
		$this->load->language('account/order');
		$this->load->model('account/order');
		$postData =	$this->request->post;
		$postData = $this->request->post = json_decode(file_get_contents('php://input'), true);  
		if(!empty($postData)){
			$user_id = $postData["user_id"];
			if(!empty($user_id)){
				$profile_data = $this->model_restapi_restapimodel->get_profile($user_id);
				
				if(empty(@$profile_data)){
					$data["message"] = "This user  Does not Exist";
					$data['status'] = 201;
				}else{

				    $order_history = $this->model_restapi_restapimodel->order_history($user_id);
				
				    if($order_history){
				    	$i=0;
				    	$data['message'] = "Successful";
						$data['status']  = 200;
				    	foreach($order_history as $key=>$order){
				    		$order_h["id"] 			 		= $i;
				    		$order_h["order_id"] 	 		= "#".$order["order_id"] ;
				    		$order_h["customer"] 	 		= $order["firstname"]." ".$order["lastname"];
				    		$order_h["order_status"] 		= $order["order_status"];
				    		$order_h["order_date_added"] 	= $order["order_date"];
				    		$order_h["total"] 				= $order["order_amount"];				    		
				    		$order_h["product_name"] 		= $order["pname"];				    		
				    		$order_h["currency_code"] 		= $order['symbol_left'];
				    		$order_h["product_numbers"] 	= $order['product_numbers'];
				    		$order_hdata[]					= $order_h;
				    		$i++;
				    	}							
					    	$data['data']["order_detail"]   = $order_hdata;
				    								
					}else{
						$data["data"]['order_detail'] = [];
						$data['message'] = "No order found!";
						$data['status'] = 201;
					}
			    }
			}else{
						$data['message'] = "User id is empty";
						$data['status'] = 201;
					}
			$this->response->setOutput(json_encode($data));
		}

	}

	/*
	 * @ Auther: Vaibhav
	 * @ 12/23/2017
	 * @ Get Reviews Api
	 * @ Parameters:user_id,gift_code
	 ***/
	public function get_reviews(){
		$this->response->addHeader('Content-Type: application/json');
		$this->load->model('restapi/restapimodel');
		$postData =	$this->request->post;

		if(!empty($postData)){
			$user_id = $postData["user_id"];
			$product_id = $postData["product_id"];
			if(!empty($user_id)){
				$profile_data = $this->model_restapi_restapimodel->get_profile($user_id);
				
				if(empty(@$profile_data)){
					$data["message"] = "This user  Does not Exist";
					$data['status'] = 201;
				}else{
						if (!empty($product_id)) {
							$get_reviews = $this->model_restapi_restapimodel->get_reviews($postData); 
				   
				    if($get_reviews){
				    	//unset($postData['user_id']);
				    	$data["data"] = $get_reviews;
				    	$data['message'] = "Successful";
						$data['status'] = 200;
						
					}else{
						$data['message'] = "No Reviews Found For This Product";
						$data['status'] = 201;
					}
						}else{
						$data['message'] = "Product Id Is Empty";
						$data['status'] = 201;
					}
				    
			    }
			}else{
						$data['message'] = "User Id Is Empty";
						$data['status'] = 201;
					}
			$this->response->setOutput(json_encode($data));
		}

	}

	/*
	 * @ Auther: Vaibhav
	 * @ 12/23/2017
	 * @ Send Reviews Api
	 * @ Parameters:user_id,product_id,review_message,review_rating
	 ***/
	public function send_reviews(){
		$this->response->addHeader('Content-Type: application/json');
		$this->load->model('restapi/restapimodel');
		$postData =	$this->request->post;

		if(!empty($postData)){
			$user_id = $postData["user_id"];
			$product_id = $postData["product_id"];
			$review_message = $postData["review_message"];
			$review_rating = $postData["review_rating"];

			if(!empty($user_id)){
				$profile_data = $this->model_restapi_restapimodel->get_profile($user_id);
				
				if(empty(@$profile_data)){
					$data["message"] = "This user  Does not Exist";
					$data['status'] = 201;
				}else{
					if (!empty($product_id)) {
						$postData["author"] = $profile_data["firstname"].' '.$profile_data["lastname"];
				    $send_reviews = $this->model_restapi_restapimodel->send_reviews($postData); 
				   
				    if($send_reviews>0){
				    	//unset($postData['user_id']);
				    	//$data["data"] = $get_reviews;
				    	$data['message'] = "Review submitted successfuly for approval";
						$data['status'] = 200;
						
					}else{
						$data['message'] = "Somthing went wrong";
						$data['status'] = 201;
					}
					}else{
						$data['message'] = "Product Id Is Empty";
						$data['status'] = 201;
					}
			    }
			}else{
						$data['message'] = "User Id Is Empty";
						$data['status'] = 201;
					}
			$this->response->setOutput(json_encode($data));
		}

	}	

	/*
	 * @ Auther: Vaibhav
	 * @ 12/27/2017
	 * @ Product Detail Api: 
	 * @ Parameters:user_id,product_id
	 ***/
	public function product_details(){
		$this->response->addHeader('Content-Type: application/json');
		$this->load->model('restapi/restapimodel');
		$this->load->model('tool/image');
		$postData =	$this->request->post;

		if(!empty($postData)){
			$user_id = $postData["user_id"];
			$product_id = $postData["product_id"];
			

			if(!empty($user_id)){
				$profile_data = $this->model_restapi_restapimodel->get_profile($user_id);
				
				if(empty(@$profile_data)){
					$data["message"] = "This user  Does not Exist";
					$data['status'] = 201;
				}else{
					if (!empty($product_id)) {
						$product_id = $postData["product_id"];
			
					
						$prodata = $this->db->query("SELECT product_id FROM oc_product WHERE product_id = $product_id");
						if($prodata->num_rows==0){
							$data['message'] = "Product Does not exist";
							$data['status'] = 201;
							$this->response->setOutput(json_encode($data));	
							return;
						}
					
						$this->load->model('catalog/product');

						 $product_details = $this->model_catalog_product->getProduct($product_id);
						
						 $data["data"]['product_images'] = array();
						 $data["data"]['product_color'] = array();

						 $productImages = $this->model_catalog_product->getProductImages($product_id);
						// print_r( $productImages);
						 foreach ($productImages as $result) {
						 	$popup = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height'));
						 	if(empty($popup)){
						 		$popup = $this->config->get('config_url') .'image/cache/placeholder-279x400.png';;
						 	} else {
						 		$popup = HTTP_SERVER.'image/'.$result['image'];
						 	}


						 	$thumb = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_additional_width'), $this->config->get($this->config->get('config_theme') . '_image_additional_height'));
						 	if(empty($thumb)){
						 		$thumb = $this->config->get('config_url') .'image/cache/placeholder-279x400.png';;
						 	}
								$data["data"]['product_images'][] = array(
									'popup' => $popup,
									'thumb' => $thumb
								);
							}
							
						if (!empty($product_details['image'])) {
							$image = $this->model_tool_image->resize($product_details['image'], $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
						} else {
							$image = '';//$this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
						}
					
						
				   		// $product_details = $this->model_restapi_restapimodel->product_details($postData); 
				       $product_details['image'] = empty($image)?'':$image;
				       $data["data"]["product_size"] =[];
				       $data["data"]["product_color"] =[];
						foreach ($this->model_catalog_product->getProductOptions($product_id) as $option) {
							$product_option_value_data = array();

							foreach ($option['product_option_value'] as $option_value) {
								if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
									if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
										/*$price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);*/
										$price = $option_value['price'];
									} else {
										$price = 0.0;
									}
									if (!empty($option_value['image'])) {
										$image = $this->model_tool_image->resize($option_value['image'], $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
									} else {
										$image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
									}
									$product_option_value_data[] = array(
										'product_option_value_id' => $option_value['product_option_value_id'],
										'option_value_id'         => $option_value['option_value_id'],
										'name'                    => $option_value['name'],
										'image'                   => empty($image)?"":$image,//$this->model_tool_image->resize($option_value['image'], 50, 50),
										'price'                   => !empty($price)?$price:"",
										'currency'                => 'NGN',
										'price_prefix'            => $option_value['price_prefix']
									);
								}
							}

							if(strtolower($option['name'])=="size"){
								
								$data["data"]["product_size"] = empty($product_option_value_data)?array():$product_option_value_data;
							}
							if(strtolower($option['name'])=="color"){
								
								$data["data"]["product_color"] =empty($product_option_value_data)?array():$product_option_value_data; 
							}
							
							/*$data['options'][] = array(
								'product_option_id'    => $option['product_option_id'],
								'product_option_value' => $product_option_value_data,
								'option_id'            => $option['option_id'],
								'name'                 => $option['name'],
								'type'                 => $option['type'],
								'value'                => $option['value'],
								'required'             => $option['required']
							);*/
						}

			//$this->response->setOutput(json_encode($data));
				 //  echo "<pre>";print_r($product_details); echo "</pre>";
				    if($product_details){
				    	//unset($postData['user_id']);
				    	//print_r($product_details);
				    		$product_rating			 	= !empty($product_details["rating"]) ? $product_details["rating"] :'';
				    		$tempArr["product_name"] 	= !empty($product_details["name"]) ? $product_details["name"] : '';
				    		$tempArr["product_model"] 	= !empty($product_details["model"]) ? $product_details["model"] : '';
				    		$tempArr["product_price"] 	= !empty($product_details["price"]) ? $product_details["price"] :'';
				    		$tempArr["product_rating"] 	= !empty($product_details["rating"]) ? (string)$product_details["rating"] :'';
				    		$tempArr["product_description"] =  $this->url->link("restapi/account/account/description&product_id=$product_id",'', true);
				    		//$tempArr["product_specification"] = $product_detail["name"];
				    		$tempArr['product_specification'] = $this->url->link("restapi/account/account/specification&product_id=$product_id",'', true);
				    		
		    		 		/**
                             * add seller information on the product page through code end
                             */

                          
                            	$this->load->model('account/customerpartner');
                                $this->load->language('customerpartner/profile');

                                $check_seller = $this->model_account_customerpartner->getProductSellerDetails($product_id);
                                
                                 $tempArr["product_seller_detail"] = null;
                            if ($check_seller) {

                                            $this->load->model('customerpartner/master');
                                            $seller["seller_id"] = $check_seller['customer_id'];
                                            $partner = $this->model_customerpartner_master->getProfile($check_seller['customer_id']);
                                          
                                            switch ($this->config->get('marketplace_product_name_display')) {
                                                case 'sn':
                                                    $seller['seller_name'] = $partner['firstname']. ' ' .$partner['lastname'];
                                                    break;

                                                case 'cn':
                                                    $seller['seller_name'] = $partner['companyname'];
                                                    break;

                                                case 'sncn':
                                                    $seller['seller_name'] = $partner['firstname']. ' ' .$partner['lastname'].' '.'And'.' '.$partner['companyname'];
                                                    break;  
                                            }
                                            $img = $partner['avatar'];
                                            if (!empty($img)) {

													$image = $this->model_tool_image->resize($img, $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
												} else {
													
													$image = '';//$this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
												}
											$seller["seller_image"] = !empty($image)?$image:"";
											$seller["seller_shop_name"] = empty($partner["screenname"])?"":$partner["screenname"];
											$seller["seller_rating"] = '';

                             $tempArr["product_seller_detail"] = $seller; 
                         }
                         
				    		//$expData[]=$tempArr;
                        /*product_detail_map*/ 
                        if(empty($data["data"]["product_color"])){
                        	$data["data"]["product_detail_map"]["is_product_color"]=false;
                        }else{

                        	$data["data"]["product_detail_map"]["is_product_color"]=true;
                        }
                        if(empty($data["data"]["product_size"])){
                        	$data["data"]["product_detail_map"]["is_product_size"]=false;
                        }else{
                        	$data["data"]["product_detail_map"]["is_product_size"]=true;
                        }
                        if(empty($tempArr["product_description"])){
                        	$data["data"]["product_detail_map"]["is_product_description"]=false;
                        }else{
                        	$data["data"]["product_detail_map"]["is_product_description"]=true;
                        }

                        if(empty($tempArr["product_specification"])){
                        	$data["data"]["product_detail_map"]["is_product_specification"]=false;
                        }else{
                        	$data["data"]["product_detail_map"]["is_product_specification"]=true;
                        }

                        $tempArr["currency_code"] = "NGN";
				    	$data["data"]["product_details"] = $tempArr;
				    	$data['message'] = "Success";
						$data['status'] = 200;
						
					}else{
						$data['message'] = "Somthing went wrong";
						$data['status'] = 201;
					}
					}else{
						$data['message'] = "Product Id Is Empty";
						$data['status'] = 201;
					}
			    }
			}else{
						$data['message'] = "User Id Is Empty";
						$data['status'] = 201;
					}
			$this->response->setOutput(json_encode($data));
		}

	}

	/*
	 * @ Auther: Vaibhav
	 * @ 12/23/2017
	 * @ Seller Registration Api:
	 * @ Parameters:Parameters:user_id,street,city,country_id,state_id,business_name,passport_no,national_id_no,driver_license_no,
account_type,account_name,account_number,bank_name,bank_address
	 ***/
	public function seller_registration(){
		$this->response->addHeader('Content-Type: application/json');
		$this->load->model('restapi/restapimodel');
				$postData 	 =	$this->request->post;

		if(!empty($postData)){
			$user_id		 = $postData["user_id"];
			$product_id 	 = $postData["product_id"];
			$review_message  = $postData["review_message"];
			$review_rating   = $postData["review_rating"];

			if(!empty($user_id)){
				$profile_data = $this->model_restapi_restapimodel->get_profile($user_id);
				
				if(empty(@$profile_data)){
					$data["message"] 	= "This user  Does not Exist";
					$data['status'] 	= 201;
				}else{
					$is_seller 			= $this->db->query("SELECT customer_id FROM oc_customerpartner_to_customer WHERE customer_id = '".$user_id."' and is_partner = 1");
					if($is_seller->num_rows > 0 ){
						$data['message'] 	= "This user is already registered as seller.";
						$data['status'] 	= 201;
						$this->response->setOutput(json_encode($data));
						return;
					}

					$sellerResponse 		= $this->model_restapi_restapimodel->seller_registration($postData);
					if($sellerResponse == "error"){
				   		$data['message'] 	= "Something went wrong.";
						$data['status'] 	= 201;
						$this->response->setOutput(json_encode($data));
						return;
				   	}else{
				   		$data['message'] 	= "Success";
						$data['status'] 	= 200;
						$data["data"] 		= array("shop_id"=>$user_id);
						$this->response->setOutput(json_encode($data));
						return;
				   	}					
			    }

			}else{
					$data['message'] 	= "User Id Is Empty";
					$data['status'] 	= 201;

				 }
			$this->response->setOutput(json_encode($data));
		}

	}




	function uploadSellerShopImage($getData=null,$mainPath=null){
		$this->load->model('tool/image');
		$data  = array();
		if(!empty($getData) && !empty($mainPath)){
			$allowed =  array('gif','png' ,'jpg','jpeg');				
			if(!empty($getData)){													
					$name 	  	= str_replace(" ", "_", $getData['name']);					
					$tmp_name 	= $getData['tmp_name'];					
					$ext 		= pathinfo($name, PATHINFO_EXTENSION);
					
					if(!in_array($ext,$allowed) ) {
					    return false;
					}

					if (!file_exists(DIR_IMAGE.'wkseller/'.$mainPath)) {
						mkdir(DIR_IMAGE.'wkseller/'.$mainPath, 0777, true);
					}
					
					$target_file  = DIR_IMAGE .'wkseller/'.$mainPath.'/'. basename($name);
					$imagePath="wkseller/".$mainPath."/". basename($name); 

					if(move_uploaded_file($tmp_name, $target_file)){							
						$this->model_tool_image->resize($name, 100, 100);			 			
						return $imagePath;		 				
		 			}				
							
			} else {
				return false;
			} 			
		}

	}



	/*
	* Function    : seller_shop_profile_update
	* Description : Function use to update the seller shop profile details 
	* Params 	  : user_id,shop_id + updated info,
	* Return 	  : Return result of array() in json	
	* Author 	  : N/A
	* Updated By  : Yuvraj Singh <yuvraj.singh@mobilyte.com> 
	* Date 		  : 08-Feb-2018
	*/

	public function seller_shop_profile_update(){
		//$this->response->addHeader('Content-Type: application/json');
		$this->load->model('restapi/restapimodel');		
		$postData 	 	=	$this->request->post;		
		$user_id 		= !empty($postData['user_id']) ? $postData['user_id'] : $postData['shop_id'];
		if(!empty($user_id)){
		
			$is_seller 	= $this->db->query("SELECT ctc.customer_id,c.customer_id as ids,c.firstname FROM  ".DB_PREFIX."customerpartner_to_customer as ctc LEFT JOIN ".DB_PREFIX."customer as c  ON  ctc.customer_id = c.customer_id WHERE ctc.customer_id = '".$user_id."' and ctc.is_partner = 1");			
			if($is_seller->num_rows > 0 ){					
						
							$updateStr = '';

							$mainPath = trim($is_seller->row['firstname']).''.$is_seller->row['ids'];

						if(!empty($_FILES['banner_image']['name'])){													
							$imagePath = $this->uploadSellerShopImage($_FILES['banner_image'],ucfirst($mainPath));
							if($imagePath!=false){
								$updateStr .= "companybanner='".$imagePath."', ";	
							}
							
						}

						if(!empty($_FILES['logo_image']['name'])){
							$imagePath = $this->uploadSellerShopImage($_FILES['logo_image'],ucfirst($mainPath));
							if($imagePath!=false){
								$updateStr .= "companylogo='".$imagePath."', ";
							}
						}
						
						if(!empty($_FILES['profile_image']['name'])){
							$imagePath = $this->uploadSellerShopImage($_FILES['profile_image'],ucfirst($mainPath));
							if($imagePath!=false){
								$updateStr .= "avatar='".$imagePath."', ";
							}
						}


						if(!empty($postData['short_profile'])){													
							$updateStr .= "shortprofile='".$postData['short_profile']."', ";
						}
						if(!empty($postData['company_name'])){
							$updateStr .= "companyname='".$postData['company_name']."', ";

						}
						if(!empty($postData['twitter_id'])){							
							$updateStr .= "twitterid='".$postData['twitter_id']."', ";
						}

						if(!empty($postData['facebook_id'])){						
							$updateStr .= "facebookid='".$postData['facebook_id']."', ";
						}

						 if(!empty($postData['country_id'])){							
							$this->load->model('localisation/country');
							$country_info = $this->model_localisation_country->getCountry($postData['country_id']);
							if ($country_info) {								
								$updateStr .= "country='".$country_info['iso_code_2']."', ";
							}
						}

						if(!empty($postData['company_address'])){							
							$updateStr .= "companylocality='".$postData['company_address']."', ";
						}

						if(!empty($postData['company_description'])){							
							$updateStr .= "companydescription='".$postData['company_description']."', ";
						}
						
						if(!empty($postData['account_name'])){							
							$updateStr .= "vchAccountName='".$postData['account_name']."', ";
						}

						if(!empty($postData['account_type_id'])){							
							$updateStr .= "vchIFSCCode='".$postData['account_type_id']."', ";
						}
						
						if(!empty($postData['account_number'])){							
							$updateStr .= "vchAccountNumber='".$postData['account_number']."', ";
						}
						
						if(!empty($postData['bank_name'])){							
							$updateStr .= "vchBankName='".$postData['bank_name']."', ";
						}
						
						if(!empty($postData['bank_address'])){							
							$updateStr .= "vchBankAddress='".$postData['bank_address']."', ";
						}  
						
						if(!empty($updateStr)){

							$updateStr  =  substr($updateStr,0,-2);

							$issellUpd 	= $this->db->query("UPDATE ".DB_PREFIX."customerpartner_to_customer set ".$updateStr." where is_partner = 1 and customer_id=".$user_id);							
							$this->shop_profile_details($user_id);
						} else {
							$data['message'] 	= "Information not updated!";
							$data['status'] 	= 201;
							$this->response->setOutput(json_encode($data));

						}										

			} else {
				$data['message'] 	= "Invalid user id or shop id !";
				$data['status'] 	= 201;
				$this->response->setOutput(json_encode($data));
			}	
		} else {
				$data['message'] 	= "User id or shop id is required!";
				$data['status'] 	= 201;
				$this->response->setOutput(json_encode($data));
		}
	}	

	/*
	* Function    : shop_profile_details
	* Description : Function use to get the profile details customer 
	* Params 	  : user_id,shop_id,
	* Return 	  : Return result of array() in json	
	* Author 	  : N/A
	* Updated By  : Yuvraj Singh <yuvraj.singh@mobilyte.com> 
	* Date 		  : 30-Jan-2018
	*/

	public function shop_profile_details($callbackId=null){
		
		$this->load->model('restapi/restapimodel');
		$this->load->model('tool/image');	
		if(!empty($callbackId)){
			$shop_id 		=  $postData["user_id"] = $callbackId;			
		} else {
			$this->response->addHeader('Content-Type: application/json');
				$postData 		=  json_decode(file_get_contents('php://input'), true);	
			if(empty($postData)){
				$postData 	 	=	$this->request->post;	
			}
		}	
		
		if(!empty($postData)){
			$shop_id 	= !empty($postData["user_id"]) ? $postData["user_id"] :$postData["shop_id"];
			if(empty($shop_id)){
				$data['message'] 	= "Shop Id or User Id Is Empty";
				$data['status'] 	= 201;
				$this->response->setOutput(json_encode($data));
				return;
			}
			
			$shop_details = $this->db->query("SELECT oc_customerpartner_to_customer.*,oc_customer.*,oc_country.country_id FROM oc_customerpartner_to_customer LEFT JOIN oc_customer ON oc_customer.customer_id =  oc_customerpartner_to_customer.customer_id LEFT JOIN oc_country ON oc_customerpartner_to_customer.country=oc_country.iso_code_2  where oc_customer.customer_id = '".$shop_id."' and is_partner  = 1");
				
				if($shop_details->num_rows>0){

					$shopDetails["shop_id"] 	= $shop_id;
					$shopDetails["shop_name"] 	= $shop_details->row["screenname"];
					

					if (!empty($shop_details->row["companybanner"]) && empty($callbackId)) {
					 	$companybanner 			= $this->model_tool_image->resize($shop_details->row["companybanner"], $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
					} else {
						if(empty($shop_details->row['companybanner'])){
							$companybanner  				= '';
						} else {
							$companybanner  				= HTTP_SERVER.'image/'.$shop_details->row['companybanner'] ;
						}
					}

					$shopDetails["shop_banner_image"] = empty($companybanner)?'':$companybanner ;
					if (!empty($shop_details->row["companylogo"]) && empty($callbackId)) {
					 	$companylogo 			= $this->model_tool_image->resize($shop_details->row["companylogo"], $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
					} else {						

						if(empty($shop_details->row['companylogo'])){
							$companylogo  				= '';
						} else {
							$companylogo  				= HTTP_SERVER.'image/'.$shop_details->row['companylogo'] ;
						}
					}

					$shopDetails["shop_logo_image"] = empty($companylogo)?'':$companylogo ;

					if (!empty($shop_details->row["avatar"]) && empty($callbackId)) {
					 	$avatar = $this->model_tool_image->resize($shop_details->row["avatar"], $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
					} else {
						if(empty($shop_details->row['avatar'])){
							$avatar  				= '';
						} else {
							$avatar  				= HTTP_SERVER.'image/'.$shop_details->row['avatar'] ;
						}
						
					}




					$shopDetails["shop_avatar"] 	= empty($avatar)?'':$avatar ;
					$shopDetails["first_name"] 		= $shop_details->row["firstname"] ;
					$shopDetails["last_name"] 		=$shop_details->row["lastname"] ;
					$shopDetails["email"] 			= $shop_details->row["email"] ;
					$shopDetails["short_profile"] 	= $this->getExactHtml($shop_details->row["shortprofile"]);
					$shopDetails["company_name"] 	= $shop_details->row["companyname"] ;
					$shopDetails["twitter_id"] 		= $shop_details->row["twitterid"] ;  
					$shopDetails["facebook_id"] 	= $shop_details->row["facebookid"] ;
					$shopDetails["company_address"] = $shop_details->row["companylocality"] ;
					$shopDetails["country_id"] 	    = !empty($shop_details->row["country_id"]) ? $shop_details->row["country_id"] :'';
					$shopDetails["company_description"] = $this->getExactHtml($shop_details->row["companydescription"]) ;
					$shopDetails["account_name"] 	= $shop_details->row["vchAccountName"] ;
					$shopDetails["account_type_id"] = $shop_details->row["vchIFSCCode"];
					$shopDetails["account_number"] 	= $shop_details->row["vchAccountNumber"];
					$shopDetails["bank_name"] 		= $shop_details->row["vchBankName"];
					$shopDetails["bank_address"] 	= $shop_details->row["vchBankAddress"];

					$bankDetails					= array();
					$bankDetails[]['name']			= 'Current Account';
					$bankDetails[]['name']			= 'Saving Account';

					$shopDetails["account_type_list"] = $bankDetails;

					$data['message'] 	= "Success";
					$data['status'] 	= 200;
					$data['data'] 		= $shopDetails; 

				}else{

					$data['message'] = "No Data";
					$data['status'] = 201;
				
				}
		}else{
			$data['message'] = "Shop Id or User Id Is Empty";
			$data['status'] = 201;
		}
		$this->response->setOutput(json_encode($data));
	}


	/*
	* Function    : get_shop_detail
	* Description : Function use to get the detail of shop on behalf of the shop id or user id 
	* Params 	  : user_id,shop_id
	* Return 	  : Return result in json	
	* Author 	  : Yuvraj Singh <yuvraj.singh@mobilyte.com>
	* Date 		  : 30-Jan-2018
	*/
    
    function get_shop_detail(){
    	$this->response->addHeader('Content-Type: application/json');
		$this->load->model('restapi/restapimodel');
		$this->load->model('tool/image');		
		$postData 		= json_decode(file_get_contents('php://input'), true);
		if(!empty($postData)){
			$shop_id 	= !empty($postData["user_id"]) ? $postData["user_id"] :$postData["shop_id"];
			if(empty($shop_id)){
				$data['message'] = "Shop Id or User Id Is Empty";
				$data['status']  = 201;
				$this->response->setOutput(json_encode($data));
				return;
			}

				$shop_details 	 = $this->db->query("SELECT oc_customerpartner_to_customer.*,oc_customer.* FROM oc_customerpartner_to_customer LEFT JOIN oc_customer ON oc_customer.customer_id =  oc_customerpartner_to_customer.customer_id where oc_customer.customer_id = '".$shop_id."' and is_partner  = 1");
				
				if($shop_details->num_rows>0){
					 if (!empty($shop_details->row["companybanner"])) {
					 	$companybanner = $this->model_tool_image->resize($shop_details->row["companybanner"], $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
						}
					$shopDetails["shop_banner_image"] = empty($companybanner)?'':$companybanner ;

					 if (!empty($shop_details->row["companylogo"])) {
					 	$companylogo 	= $this->model_tool_image->resize($shop_details->row["companylogo"], $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
						}
					$shopDetails["shop_logo_image"] = empty($companylogo)?'':$companylogo ;

					if (!empty($shop_details->row["avatar"])) {
					 	$avatar 		= $this->model_tool_image->resize($shop_details->row["avatar"], $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
						}

					$shopDetails["shop_avatar"]  		= empty($avatar)?'':$avatar ;
					$shopDetails["company_name"] 		= $shop_details->row["companyname"] ;
					$shopDetails["paypal_id"] 	 		= $shop_details->row["paypalid"] ;
					$shopDetails["other_payment_info"] 	= $shop_details->row["otherpayment"] ;


					$data['message'] = "Success";
					$data['status']  = 200;
					$data['data'] 	 = $shopDetails; 

				}else{

					$data['message'] = "No Data";
					$data['status']  = 201;
				
				}
		}else{
			$data['message'] = "Shop Id or User Id Is Empty";
			$data['status']  = 201;
		}
		$this->response->setOutput(json_encode($data));
    }



    /*
	* Function    : update_paypal_detail
	* Description : Function use to update the paypal detail in oc_customerpartner_to_customer tbl behalf of the shop id/user id 
	* Params 	  : user_id,shop_id,paypal_id,other_payment_info
	* Return 	  : Return result of array() in json	
	* Author 	  : Yuvraj Singh <yuvraj.singh@mobilyte.com>
	* Date 		  : 30-Jan-2018
	*/
    
    function update_paypal_detail(){
    	$this->response->addHeader('Content-Type: application/json');
		$this->load->model('restapi/restapimodel');
		$this->load->model('tool/image');		
		$postData 			= json_decode(file_get_contents('php://input'), true);
		if(!empty($postData)){
			$shop_id 		= !empty($postData["user_id"]) ? $postData["user_id"] :$postData["shop_id"];
			$paypalid 		= !empty($postData["paypal_id"]) ? $postData["paypal_id"] :'';
			$otherpayment 	= !empty($postData["other_payment_info"]) ? $postData["other_payment_info"] :'';
			if(empty($shop_id)){
				$data['message'] = "Shop Id or User Id Is Empty";
				$data['status']  = 201;
				$this->response->setOutput(json_encode($data));
				return;
			}


			$shop_details 	= $this->db->query("SELECT oc_customerpartner_to_customer.customer_id,oc_customer.customer_id FROM oc_customerpartner_to_customer LEFT JOIN oc_customer ON oc_customer.customer_id =  oc_customerpartner_to_customer.customer_id where oc_customer.customer_id = '".$shop_id."' and is_partner  = 1");
			

			if($shop_details->num_rows>0){
				$result 	= $this->db->query("update oc_customerpartner_to_customer set paypalid='$paypalid',otherpayment='$otherpayment' where oc_customerpartner_to_customer.customer_id = '".$shop_id."' and is_partner  = 1");
				
				if($result){						
						$shopDetails["paypal_id"] 	 		= $paypalid;
						$shopDetails["other_payment_info"] 	= $otherpayment;
						$data['message'] 					= "Success";
						$data['status'] 					= 200;
						$data['data'] 						= $shopDetails; 	
				} else {
					$data['message'] = "Data not update successfully";
					$data['status']  = 201;
				}
				

			}else{

				$data['message'] = "No Data";
				$data['status']  = 201;
			
			}
		}else{
			$data['message'] = "Shop Id or User Id Is Empty";
			$data['status']  = 201;
		}
		$this->response->setOutput(json_encode($data));
    }

    
    function getExactHtml($description=null){
			if(!empty($description)){
					$description 		 = html_entity_decode(htmlspecialchars_decode($description));
					$description 		 = str_replace("\r\n", "", $description);
					$description 		 = str_replace("\r", "", $description);
					$description 		 = str_replace("\n", "", $description);
					$description 		 = str_replace("", "", $description);					
					$description 		 = str_replace("\"", "'", $description);					
					return strip_tags($description);
			} else {
				return "";
			}    			
    }

	/*
	* Function    : get_seller_product_detail
	* Description : Use to get single product detail correspond to the product id 
	* Params 	  : user_id,shop_id,product_id
	* Return 	  : Return result in json	
	* Author 	  : Yuvraj Singh <yuvraj.singh@mobilyte.com>
	* Date 		  : 30-Jan-2018
	*/
    
    function get_seller_product_detail(){    	
    	$this->response->addHeader('Content-Type: application/json');
		$this->load->model('restapi/restapimodel');
		$this->load->model('tool/image');		
		$postData = json_decode(file_get_contents('php://input'), true);

		if(!empty($postData)){
			$shop_id 			 = !empty($postData["user_id"]) ? $postData["user_id"] :$postData["shop_id"];
			$product_id 		 = !empty($postData["product_id"]) ? $postData["product_id"] :'';			
			if(empty($shop_id)){
				$data['message'] = "Shop Id or User Id Is Empty";
				$data['status']  = 201;
				$this->response->setOutput(json_encode($data));
				return;
			}

			$pd 	 = $this->db->query("SELECT oc_customerpartner_to_product.*,oc_product.*,oc_product_description.*,oc_manufacturer.name as manufacturer,oc_weight_class_description.title as weight_class_name,oc_length_class_description.title as length_class_name,oc_stock_status.name as subtract_stock_name,oss.name as out_of_stock_status_name FROM oc_customerpartner_to_product LEFT JOIN oc_product ON oc_customerpartner_to_product.product_id =  oc_product.product_id 
				LEFT JOIN oc_product_description ON oc_customerpartner_to_product.product_id =  oc_product_description.product_id 
				LEFT JOIN oc_manufacturer ON oc_product.manufacturer_id =  oc_manufacturer.manufacturer_id 				
				LEFT JOIN oc_weight_class_description ON oc_product.weight_class_id =  oc_weight_class_description.weight_class_id 
				LEFT JOIN oc_length_class_description ON oc_product.length_class_id =  oc_length_class_description.length_class_id 
				LEFT JOIN oc_stock_status ON oc_product.subtract =  oc_stock_status.stock_status_id 
				LEFT JOIN oc_stock_status as oss ON oc_product.stock_status_id =  oss.stock_status_id 				
				where oc_customerpartner_to_product.product_id = $product_id and oc_customerpartner_to_product.customer_id  = $shop_id"); 


			if($pd->num_rows>0){					
				$pImgDetails = $this->db->query("SELECT product_image_id,image FROM oc_product_image where oc_product_image.product_id = $product_id");
				$getCats 	 = $this->db->query("SELECT category_id FROM oc_product_to_category where product_id = $product_id");
					
					
					$pdata["product_name"] 				= $pd->row['name'];
					$pdata["product_description"] 		= $this->getExactHtml($pd->row['description']);					
					$pdata["meta_tag_title"] 			= $pd->row['meta_title'];
					$pdata["meta_teg_description"] 		= $pd->row['meta_description'];					
					$pdata["meta_teg_keyword"] 			= $this->getExactHtml($pd->row['meta_keyword']);
					$pdata["product_tags"] 				= $pd->row['tag'];
					$pdata["model"] 					= $pd->row['model'];
					$pdata["sku"] 						= $pd->row['sku'];
					$pdata["upc"] 						= $pd->row['upc'];					
					$pdata["ean"] 						= $pd->row['ean'];
					$pdata["jan"] 						= $pd->row['jan'];
					$pdata["isbn"] 						= $pd->row['isbn'];
					$pdata["mpn"] 						= $pd->row['mpn'];
					$pdata["location"] 					= $pd->row['location'];
					$pdata["price"] 					= $pd->row['price'];
					$pdata["quantity"] 					= $pd->row['quantity'];
					$pdata["subtract_stock_id"] 		= $pd->row['subtract'];		
					$pdata["date_available"] 			= $pd->row['date_available'];
					$pdata["weight"] 					= $pd->row['weight'];									
					$pdata["weight_class_id"] 			= $pd->row['weight_class_id'];					
					$pdata["length"] 					= $pd->row['length'];
					$pdata["width"] 					= $pd->row['width'];
					$pdata["height"] 					= $pd->row['height'];
					$pdata["length_class_id"] 			= $pd->row['length_class_id'];										
					$pdata["sort_order"] 				= $pd->row['sort_order'];
					$pdata["manufacturer_id"] 			= $pd->row['manufacturer_id'];
					$pdata["manufacturer"] 				= !empty($pd->row['manufacturer']) ? $pd->row['manufacturer']:'';
					$pdata["weight_class_name"] 		= !empty($pd->row['weight_class_name']) ? $pd->row['weight_class_name']:'';
					$pdata["required_shipping"] 		= !empty($pd->row['shipping']) ? 'Yes' : 'No';
					$pdata["length_class_name"] 		= $pd->row['length_class_name'];
					$pdata["status_id"] 				= $pd->row['status'];
					$pdata["status_name"] 				= !empty($pd->row['status']) ? 'Enable': 'Disable';
					$pdata["seo_keyword"] 				= "";
					$pdata["subtract_stock_name"] 		= !empty($pd->row['subtract']) ?'Yes':'No';
					$pdata["out_of_stock_status_id"] 	= $pd->row['stock_status_id'];
					$pdata["out_of_stock_status_name"] 	= !empty($pd->row['out_of_stock_status_name']) ? $pd->row['out_of_stock_status_name']:'';	


					if(!empty($getCats->rows)){						
						foreach ($getCats->rows as $key => $gcat) {												
										$catids[]		= $gcat['category_id'];														
						}					 
						$pdata["categories_id"]			= implode(",",$catids);
					} else {
						$pdata["categories_id"]			= "";
					}
					
					if(!empty($pImgDetails)){						
						foreach ($pImgDetails->rows as $key => $pid) {												
							$product_images['image_id']		= $pid['product_image_id'];							
							$product_images['image_path']	= HTTP_SERVER.'image/'.$pid['image'];						
							$pdata['product_images'][]		= $product_images;
						}
					}

					
					$data['message'] 					= "Success";
					$data['status'] 					= 200;
					$data['data'] 						= $pdata; 	
				} else {
					$data['message'] = "No Data";
					$data['status'] = 201;
				} 
				 

			}else{

				$data['message'] = "Shop Id or User Id Is Empty";
				$data['status'] = 201;			
			}
		
		$this->response->setOutput(json_encode($data));

    }


	public function description(){

		$this->load->language('product/product');
		$this->load->model('catalog/product');
		if (isset($this->request->get['product_id'])) {
 			$product_id = $this->request->get["product_id"];
		}
		$product_details = $this->model_catalog_product->getProduct($product_id);
		$data["description"] = htmlspecialchars_decode($product_details["description"]);
		//print_r($data["description"]);
		$this->response->setOutput($this->load->view('product/description', @$data));
		return ;

	}

	public function specification(){

		$this->load->language('product/product');
		$this->load->model('catalog/product');
		if (isset($this->request->get['product_id'])) {
 			$product_id = $this->request->get["product_id"];
		}
		$product_specification = $this->model_catalog_product->getProductAttributes($product_id);
		$data["attribute_groups"] = $product_specification;
		//print_r($data["description"]);
		$this->response->setOutput($this->load->view('product/specification', @$data));
		return ;
	}

	protected function validate($email,$password) {
		$this->load->model('account/customer');

		// Check how many login attempts have been made.
		/*$login_info = $this->model_account_customer->getLoginAttempts($this->request->post['email']);

		if ($login_info && ($login_info['total'] >= $this->config->get('config_login_attempts')) && strtotime('-1 hour') < strtotime($login_info['date_modified'])) {
			$this->error['warning'] = $this->language->get('error_attempts');
		}

		// Check if customer has been approved.
		$customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

		if ($customer_info && !$customer_info['approved']) {
			$this->error['warning'] = $this->language->get('error_approved');
		}*/

		if (!$this->error) {   
			if (!$this->customer->login($email, $password)) {
				$this->error['warning'] = $this->language->get('error_login');
				$this->model_account_customer->addLoginAttempt($email);

			} else { 

				$this->model_account_customer->deleteLoginAttempts($email);
			}
		}
		return !$this->error;
	}





	public function seller_product_details(){
		$this->response->addHeader('Content-Type: application/json');
		$this->load->model('restapi/restapimodel');
		$this->load->model('tool/image');
		$postData =	$this->request->post;

		if(!empty($postData)){
			$user_id = $postData["user_id"];
			$product_id = $postData["product_id"];
			

			if(!empty($user_id)){
				$profile_data = $this->model_restapi_restapimodel->get_profile($user_id);
				
				if(empty(@$profile_data)){
					$data["message"] = "This user  Does not Exist";
					$data['status'] = 201;
				}else{
					if (!empty($product_id)) {
						$product_id = $postData["product_id"];
			
					
						$prodata = $this->db->query("SELECT product_id FROM oc_product WHERE product_id = $product_id");
						if($prodata->num_rows==0){
							$data['message'] = "Product Does not exist";
							$data['status'] = 201;
							$this->response->setOutput(json_encode($data));	
							return;
						}
					
						$this->load->model('catalog/product');

						 $product_details = $this->model_catalog_product->getProduct($product_id);
						
						 $data["data"]['product_images'] = array();
						 $data["data"]['product_color'] = array();

						 $productImages = $this->model_catalog_product->getProductImages($product_id);
						// print_r( $productImages);
						 foreach ($productImages as $result) {
						 	$popup = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height'));
						 	if(empty($popup)){
						 		$popup = $this->config->get('config_url') .'image/cache/placeholder-279x400.png';;
						 	}
						 	$thumb = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_additional_width'), $this->config->get($this->config->get('config_theme') . '_image_additional_height'));
						 	if(empty($thumb)){
						 		$thumb = $this->config->get('config_url') .'image/cache/placeholder-279x400.png';;
						 	}
								$data["data"]['product_images'][] = array(
									'popup' => $popup,
									'thumb' => $thumb
								);
							}
							
						if (!empty($product_details['image'])) {
							$image = $this->model_tool_image->resize($product_details['image'], $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
						} else {
							$image = '';//$this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
						}
					
						
				   		// $product_details = $this->model_restapi_restapimodel->product_details($postData); 

				       $product_details['image'] = empty($image)?'':$image;
				       $data["data"]["product_size"] =[];
				       $data["data"]["product_color"] =[];
						foreach ($this->model_catalog_product->getProductOptions($product_id) as $option) {
							$product_option_value_data = array();

							foreach ($option['product_option_value'] as $option_value) {
								if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
									if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
										/*$price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);*/
										$price = $option_value['price'];
									} else {
										$price = 0.0;
									}
									if (!empty($option_value['image'])) {
										$image = $this->model_tool_image->resize($option_value['image'], $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
									} else {
										$image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
									}
									$product_option_value_data[] = array(
										'product_option_value_id' => $option_value['product_option_value_id'],
										'option_value_id'         => $option_value['option_value_id'],
										'name'                    => $option_value['name'],
										'image'                   => empty($image)?"":$image,//$this->model_tool_image->resize($option_value['image'], 50, 50),
										'price'                   => !empty($price)?$price:"",
										'currency'                => 'NGN',
										'price_prefix'            => $option_value['price_prefix']
									);
								}
							}

							if(strtolower($option['name'])=="size"){
								
								$data["data"]["product_size"] = empty($product_option_value_data)?array():$product_option_value_data;
							}
							if(strtolower($option['name'])=="color"){
								
								$data["data"]["product_color"] =empty($product_option_value_data)?array():$product_option_value_data; 
							}
							
							/*$data['options'][] = array(
								'product_option_id'    => $option['product_option_id'],
								'product_option_value' => $product_option_value_data,
								'option_id'            => $option['option_id'],
								'name'                 => $option['name'],
								'type'                 => $option['type'],
								'value'                => $option['value'],
								'required'             => $option['required']
							);*/
						}

			//$this->response->setOutput(json_encode($data));
				 //  echo "<pre>";print_r($product_details); echo "</pre>";
				    if($product_details){
				    	//unset($postData['user_id']);
				    	//print_r($product_details);
				    		$tempArr = $product_details;
				    		unset($tempArr["description"]);
				    		unset($tempArr["name"]);
				    		unset($tempArr["model"]);
				    		unset($tempArr["price"]);
				    		unset($tempArr["rating"]);
				    		unset($tempArr["description"]);
				    		unset($tempArr["description"]);
				    		
            				$tempArr["manufacturer_id"] = empty($tempArr["manufacturer_id"])?'':$tempArr["manufacturer_id"];
            				$tempArr["manufacturer"] = empty($tempArr["manufacturer"])?'':$tempArr["manufacturer"];
            				$tempArr["special"] = empty($tempArr["special"])?'':$tempArr["special"];
            				$tempArr["reward"] = empty($tempArr["reward"])?'':$tempArr["reward"];

				    		$tempArr["product_name"] = $product_details["name"];
				    		$tempArr["product_model"] = $product_details["model"];
				    		$tempArr["product_price"] = $product_details["price"];
				    		
				    		$tempArr["product_rating"] = "'".$product_details["rating"]."'";
				    		$tempArr["product_description"] =  $this->url->link("restapi/account/account/description&product_id=$product_id",'', true);
				    		//$tempArr["product_specification"] = $product_detail["name"];
				    		$tempArr['product_specification'] = $this->url->link("restapi/account/account/specification&product_id=$product_id",'', true);
				    		
		    		 		/**
                             * add seller information on the product page through code end
                             */

                             
                            	$this->load->model('account/customerpartner');
                                $this->load->language('customerpartner/profile');

                                $check_seller = $this->model_account_customerpartner->getProductSellerDetails($product_id);
                                
                                 $tempArr["product_seller_detail"] =[];
                            if ($check_seller) {

                                            $this->load->model('customerpartner/master');
                                            $seller["seller_id"] = $check_seller['customer_id'];
                                            $partner = $this->model_customerpartner_master->getProfile($check_seller['customer_id']);
                                          
                                            switch ($this->config->get('marketplace_product_name_display')) {
                                                case 'sn':
                                                    $seller['seller_name'] = $partner['firstname']. ' ' .$partner['lastname'];
                                                    break;

                                                case 'cn':
                                                    $seller['seller_name'] = $partner['companyname'];
                                                    break;

                                                case 'sncn':
                                                    $seller['seller_name'] = $partner['firstname']. ' ' .$partner['lastname'].' '.'And'.' '.$partner['companyname'];
                                                    break;  
                                            }
                                            $img = $partner['avatar'];
                                            if (!empty($img)) {

													$image = $this->model_tool_image->resize($img, $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
												} else {
													
													$image = '';//$this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
												}
											$seller["seller_image"] = !empty($image)?$image:"";
											$seller["seller_shop_name"] = empty($partner["screenname"])?"":$partner["screenname"];
											$seller["seller_rating"] = '';

                             $tempArr["product_seller_detail"] = $seller; 
                         }
                         
				    		//$expData[]=$tempArr;
                        /*product_detail_map*/ 
                        if(empty($data["data"]["product_color"])){
                        	$data["data"]["product_detail_map"]["is_product_color"]=false;
                        }else{

                        	$data["data"]["product_detail_map"]["is_product_color"]=true;
                        }
                        if(empty($data["data"]["product_size"])){
                        	$data["data"]["product_detail_map"]["is_product_size"]=false;
                        }else{
                        	$data["data"]["product_detail_map"]["is_product_size"]=true;
                        }
                        if(empty($tempArr["product_description"])){
                        	$data["data"]["product_detail_map"]["is_product_description"]=false;
                        }else{
                        	$data["data"]["product_detail_map"]["is_product_description"]=true;
                        }

                        if(empty($tempArr["product_specification"])){
                        	$data["data"]["product_detail_map"]["is_product_specification"]=false;
                        }else{
                        	$data["data"]["product_detail_map"]["is_product_specification"]=true;
                        }

                        $tempArr["currency_code"] = "NGN";
				    	$data["data"]["product_details"] = $tempArr;
				    	$data['message'] = "Success";
						$data['status'] = 200;
						
					}else{
						$data['message'] = "Somthing went wrong";
						$data['status'] = 201;
					}
					}else{
						$data['message'] = "Product Id Is Empty";
						$data['status'] = 201;
					}
			    }
			}else{
						$data['message'] = "User Id Is Empty";
						$data['status'] = 201;
					}
			$this->response->setOutput(json_encode($data));
		}

	}

}

