<?php
class ControllerAccountCustomerpartnerProfile extends Controller {

	private $error = array();

	public function index() {
		

		$data = array();
		$data = array_merge($data, $this->language->load('account/customerpartner/profile'));

		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/customerpartner/profile', '', 'SSL');
			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}

		$this->load->model('account/customerpartner');

		$data['chkIsPartner'] = $this->model_account_customerpartner->chkIsPartner();	
			
		if(!$data['chkIsPartner'])
			$this->response->redirect($this->url->link('account/account'));
		
		$this->document->setTitle($data['heading_title']);
			
		$this->document->addScript('catalog/view/javascript/wk_summernote/summernote.js');
		$this->document->addStyle('catalog/view/javascript/wk_summernote/summernote.css');		
		$this->document->addStyle('catalog/view/theme/default/stylesheet/MP/sell.css');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

			
			$this->load->model('account/customerpartner');			
			$this->model_account_customerpartner->updateProfile($this->request->post);			
			$this->session->data['success'] = $data['text_success'];
			$this->response->redirect($this->url->link('account/customerpartner/profile', '', 'SSL'));
		}

		$partner = $this->model_account_customerpartner->getProfile();
		if($partner) {

			$this->load->model('tool/image');

			if (isset($this->request->post['avatar'])) {

				if ($this->request->post['avatar']) {
					$partner['avatar_img'] = $this->request->post['avatar'];
			        $partner['avatar'] = $this->model_tool_image->resize($this->request->post['avatar'], 100, 100);
				}else{
                    $partner['avatar_img'] = '';
			        $partner['avatar'] = '';
				}
			}elseif ($partner['avatar'] && file_exists(DIR_IMAGE . $partner['avatar'])) {
			    $partner['avatar_img'] = $partner['avatar'];
				$partner['avatar'] = $this->model_tool_image->resize($partner['avatar'], 100, 100);
			} else if($this->config->get('marketplace_default_image_name') && file_exists(DIR_IMAGE . $this->config->get('marketplace_default_image_name'))) {
				$partner['avatar'] = $this->model_tool_image->resize($this->config->get('marketplace_default_image_name'), 100, 100);
				$partner['avatar_img'] = '';
			} else {
				$partner['avatar_img'] = '';
				$partner['avatar'] = '';
			}

			if (isset($this->request->post['companybanner'])) {
				if ($this->request->post['companybanner']) {
					$partner['companybanner_img'] = $this->request->post['companybanner'];
			        $partner['companybanner'] = $this->model_tool_image->resize($this->request->post['companybanner'], 100, 100);
				}else{
                    $partner['companybanner_img'] = '';
			        $partner['companybanner'] = '';
				}
			}elseif ($partner['companybanner'] && file_exists(DIR_IMAGE . $partner['companybanner'])) {
			    $partner['companybanner_img'] = $partner['companybanner'];
				$partner['companybanner'] = $this->model_tool_image->resize($partner['companybanner'], 100, 100);
			} else if($this->config->get('marketplace_default_image_name') && file_exists(DIR_IMAGE . $this->config->get('marketplace_default_image_name'))) {
                $partner['companybanner'] = $this->model_tool_image->resize($this->config->get('marketplace_default_image_name'), 100, 100);
                $partner['companybanner_img'] = '';
			} else {
				$partner['companybanner_img'] = '';
				$partner['companybanner'] = '';
			}

			if (isset($this->request->post['companylogo'])) {
				if ($this->request->post['companylogo']) {
					$partner['companylogo_img'] = $this->request->post['companylogo'];
			        $partner['companylogo'] = $this->model_tool_image->resize($this->request->post['companylogo'], 100, 100);
				}else{
                    $partner['companylogo_img'] = '';
			        $partner['companylogo'] = '';
				}
			}elseif ($partner['companylogo'] && file_exists(DIR_IMAGE . $partner['companylogo'])){
				$partner['companylogo_img'] = $partner['companylogo'];
				$partner['companylogo'] = $this->model_tool_image->resize($partner['companylogo'], 100, 100);
			} else if($this->config->get('marketplace_default_image_name') && file_exists(DIR_IMAGE . $this->config->get('marketplace_default_image_name'))) {
				$partner['companylogo'] = $this->model_tool_image->resize($this->config->get('marketplace_default_image_name'), 100, 100);
				$partner['companylogo_img'] = '';
			} else {
				$partner['companylogo_img'] = '';
				$partner['companylogo'] = '';
			}
			
			$partner['countrylogo'] = $partner['countrylogo'];
			$data['storeurl'] =$this->url->link('customerpartner/profile&id='.$this->customer->getId(),'','SSL');
		}
		
		if($this->config->get('wk_seller_group_status')) {
			$this->load->model('account/customer_group');
			$isMember = $this->model_account_customer_group->getSellerMembershipGroup($this->customer->getId());
			if($isMember) {
				$allowedAccountMenu = $this->model_account_customer_group->getprofileOption($isMember['gid']);
				if($allowedAccountMenu['value']) {
					$accountMenu = explode(',',$allowedAccountMenu['value']);
					if($accountMenu) {
						foreach ($accountMenu as $key => $value) {
							$values = explode(':',$value);
							$data['allowed'][$values[0]] = $values[1];
						}
					}
				}
			}
		} else if($this->config->get('marketplace_allowedprofilecolumn')) {
			$data['allowed']  = $this->config->get('marketplace_allowedprofilecolumn');
		}



		$data['partner'] = $partner;

		$data['countries'] = $this->model_account_customerpartner->getCountry();

      	$data['breadcrumbs'] = array();

      	$data['breadcrumbs'][] = array(
        	'text'      => $data['text_home'],
			'href'      => $this->url->link('common/home'),     	
        	'separator' => false
      	); 

      	$data['breadcrumbs'][] = array(
        	'text'      => $data['text_account'],
			'href'      => $this->url->link('account/account'),     	
        	'separator' => false
      	);
		
      	$data['breadcrumbs'][] = array(
        	'text'      => $data['heading_title'],
			'href'      => $this->url->link('account/customerpartner/profile', '', 'SSL'),       	
        	'separator' => false
      	);
		
		$data['customer_details'] = array(
			'firstname' => $this->customer->getFirstName(),
			'lastname' => $this->customer->getLastName(),
			'email' => $this->customer->getEmail()
		);
		
		
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['screenname_error'])) {
			$data['screenname_error'] = $this->error['screenname_error'];
		} else {
			$data['screenname_error'] = '';
		}

		if (isset($this->error['companyname_error'])) {
			$data['companyname_error'] = $this->error['companyname_error'];
		} else {
			$data['companyname_error'] = '';
		}

		if (isset($this->error['paypal_error'])) {
			$data['paypal_error'] = $this->error['paypal_error'];
		} else {
			$data['paypal_error'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';			
		}
	
		$data['action'] = $this->url->link('account/customerpartner/profile', '', 'SSL');
		$data['back'] = $this->url->link('account/account', '', 'SSL');
		$data['view_profile'] = $this->url->link('customerpartner/profile&id='.$this->customer->getId(), '', 'SSL');

		$data['isMember'] = true;
		if($this->config->get('wk_seller_group_status')) {
      		$data['wk_seller_group_status'] = true;
      		$this->load->model('account/customer_group');
			$isMember = $this->model_account_customer_group->getSellerMembershipGroup($this->customer->getId());
			if($isMember) {
				$allowedAccountMenu = $this->model_account_customer_group->getaccountMenu($isMember['gid']);
				if($allowedAccountMenu['value']) {
					$accountMenu = explode(',',$allowedAccountMenu['value']);
					if($accountMenu && !in_array('profile:profile', $accountMenu)) {
						$data['isMember'] = false;
					}
				}
			} else {
				$data['isMember'] = false;
			}
      	} else {
      		if(!in_array('profile', $this->config->get('marketplace_allowed_account_menu'))) {
      			$this->response->redirect($this->url->link('account/account','', 'SSL'));
      		}
      	}
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/customerpartner/profile', $data));
			
	}

	public function validateForm() {
		$error = false;
		$this->language->load('account/customerpartner/profile');
		if(strlen($this->request->post['screenName']) < 1) {
			$this->error['screenname_error'] = $this->language->get('error_seo_keyword');
			$this->error['warning'] = $this->language->get('error_check_form');
			$error = true;
		}

		if (isset($this->request->post['paypalid']) && $this->request->post['paypalid']) {
			if(!filter_var($this->request->post['paypalid'], FILTER_VALIDATE_EMAIL)) {
				$this->error['paypal_error'] = $this->language->get('error_paypal');
				$this->error['warning'] = $this->language->get('error_check_form');
				$error = true;
			}			
		}

		if(strlen($this->request->post['companyName']) < 1) {
			$this->error['companyname_error'] = $this->language->get('error_company_name');
			$this->error['warning'] = $this->language->get('error_check_form');
			$error = true;
		}else{
			$this->load->model('customerpartner/master');
			$check_companyname = $this->model_customerpartner_master->getShopData($this->request->post['companyName']);
			if ($check_companyname && $check_companyname['customer_id'] != $this->customer->getId()) {
				$this->error['companyname_error'] = $this->language->get('error_company_name_exists');
				$this->error['warning'] = $this->language->get('error_check_form');
				$error = true;
			}
		}
		
		if($error) {
			return false;
		} else {
			return true;
		}

	}
	
}
?>
