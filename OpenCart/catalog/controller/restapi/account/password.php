<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

class ControllerRestapiAccountPassword extends Controller {
	private $error = array();

	public function index() {
		/*if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('restapi/account/password', '', true);

			$this->response->redirect($this->url->link('restapi/account/login', '', true));
		}*/

		$this->load->language('api-global-lang');
		$this->load->language('account/password');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->load->model('account/customer');

			$customer_info = $this->model_account_customer->getCustomer($this->request->post['user_id']);
			
			if (!empty($customer_info)) {
				# code...
				$user_email = $customer_info['email'];
				$firstname  = $customer_info['firstname'];
				$lastname   = $customer_info['lastname'];

				$this->model_account_customer->editPassword( $user_email, $this->request->post['password']);

				// Add to activity log
				$this->load->model('account/activity');

				$activity_data = array(
					'customer_id' => $this->request->post['user_id'],
					'name'        => $firstname . ' ' . $lastname
				);

				$this->model_account_activity->addActivity('password', $activity_data);

				$data['status'] 	= $this->language->get('_api_success_code');
				$data['message'] 	= $this->language->get('text_success');
			
			} else {

				$data['status'] 	= $this->language->get('_api_error_code');
				$data['message'] 	= 'Error: User with customer id '.$this->request->post['user_id'].' not exists.';
			}
		}

		if (isset($this->error['password'])) {
			$data['status'] 	= $this->language->get('_api_error_code');
			$data['message'] 	= $this->error['password'];
		}

		if (isset($this->error['confirm'])) {
			$data['status'] 	= $this->language->get('_api_error_code');
			$data['message'] 	= $this->error['confirm'];
		}

		if (isset($this->error['user_id'])) {
			$data['status'] 	= $this->language->get('_api_error_code');
			$data['message'] 	= $this->error['user_id'];
		} 

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	protected function validate() {

		if ( utf8_strlen($this->request->post['user_id']) == '' ) {
			$this->error['user_id'] = $this->language->get('error_user_id');
		}

		if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
			$this->error['password'] = $this->language->get('error_password');
		}

		if ($this->request->post['password'] != $this->request->post['password']) {
			$this->error['confirm'] = $this->language->get('error_confirm');
		}

		return !$this->error;
	}
}