<?php
class ControllerExtensionModuleOnwebchat extends Controller {
	public function index() {
		$this->load->language('extension/module/onwebchat');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['code'] = html_entity_decode($this->config->get('onwebchat_chat_id'));

		return $this->load->view('extension/module/onwebchat.tpl', $data);
	}
}
