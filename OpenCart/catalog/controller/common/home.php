<?php

class ControllerCommonHome extends Controller {
private $error = array();
	public function index() {

		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (isset($this->request->get['route'])) {
			$this->document->addLink($this->config->get('config_url'), 'canonical');
		}
		/************ Feature Product section Here ******************/
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		/***** End Here ********************************************/
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['slider'] = $this->load->controller('common/slider');

/**** Get Total Testimonial ****************/
		$this->load->model('catalog/review');
                $results = $this->model_catalog_review->getReviews();

		$data['reviews']=array();
		foreach ($results as $result) {
  

		$ProductImage = $this->model_catalog_product->getProductImages($result['product_id']);
  
		
		
				$ProductImage[0]['thumb']= $this->model_tool_image->resize($ProductImage[0]['image'], 205, 198);
				
			

		  
	
			$data['reviews'][] = array(
				'author'     => $result['author'],
				'text'       => nl2br($result['text']),
				'rating'     => (int)$result['rating'],
				'image'=>$ProductImage[0]['thumb'], 
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
			);
		}
  
		
	/************ End Here ***********************************************************************************************/
		$page=1;
		$sort='';
		$filter='';
		$order='DESC';
		$limit=7;
		$filter_data = array(
				'filter_category_id' => '20',
				'filter_filter'      => $filter,
				'sort'               => $sort,
				'order'              => $order,
				'start'              => ($page - 1) * $limit,
				'limit'              => 7
			);
		$results=$this->model_catalog_product->getProducts($filter_data);

foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}
$url = '';
$this->request->get['path']='20';
				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'rating'      => $result['rating'],
					'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
				);
			}

  /****************************** Get All Category *****************************************************************************/
    $this->load->model('catalog/category');
     $category_id = 0;
    $data['category_info'] = $this->model_catalog_category->getCategoriesinfo($category_id);
    
   
/******************************* End Here ***************************************************************************************/
/******************************** Get All Seller Link **************************************************************************/
  $sellerinfo=$this->model_catalog_product->getSellerProduct();
$data['sellerinfo']=array();
$this->load->model('tool/image');
for($i=0;$i<count($sellerinfo);$i++){
 $partner['avatar'] = $this->model_tool_image->resize($sellerinfo[$i]['avatar'], 231, 231);
if($partner['avatar']=='')
{
 $image='image/cache/catalog/store-100x100.jpeg';
}
else
{
$image=$partner['avatar'];
}
$data['sellerinfo'][$i]=array('customer_id'=>$sellerinfo[$i]['customer_id'],'images'=>$image,'name'=>$sellerinfo[$i]['name']);
}

/******************************** End Here *************************************************************************************/
		
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
  
		$this->response->setOutput($this->load->view('common/home', $data));
	}

function newsletter()
{
   $this->load->model('catalog/category');
   $this->load->language('account/register');
  $this->load->model('tool/image');
  if(($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate())
  {
   
   $email=$this->request->post['email'];

    $this->model_catalog_category->addNewsletter($email);
   
  }
  if (isset($this->error['email'])) {
			$data['error_email'] = $this->error['email'];
		} else {
			$data['error_email'] = '';
		}
		if (isset($this->error['email'])) {
		$data['email'] = $this->request->post['email'];
		} else {
			$data['email'] = '';
		}
		$data['entry_email'] = $this->language->get('entry_email');

/************ Feature Product section Here ******************/
		$this->load->model('catalog/product');
		/***** End Here ********************************************/
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['slider'] = $this->load->controller('common/slider');

/**** Get Total Testimonial ****************/
		$this->load->model('catalog/review');
                $results = $this->model_catalog_review->getReviews();
		$data['reviews']=array();
		foreach ($results as $result) {
   
		$ProductImage = $this->model_catalog_product->getProductImages($result['product_id']);
  
		
		
				$ProductImage[0]['thumb']= $this->model_tool_image->resize($ProductImage[0]['image'], 205, 198);
				
			

		  
	
			$data['reviews'][] = array(
				'author'     => $result['author'],
				'text'       => nl2br($result['text']),
				'rating'     => (int)$result['rating'],
				'image'=>$ProductImage[0]['thumb'], 
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
			);
		}
  
		
	/************ End Here ***********************************************************************************************/
		$page=1;
		$sort='';
		$filter='';
		$order='DESC';
		$limit=7;
		$filter_data = array(
				'filter_category_id' => '20',
				'filter_filter'      => $filter,
				'sort'               => $sort,
				'order'              => $order,
				'start'              => ($page - 1) * $limit,
				'limit'              => 7
			);
		$results=$this->model_catalog_product->getProducts($filter_data);

foreach ($results as $result) {


				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}
$url = '';
$this->request->get['path']='20';
				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'rating'      => $result['rating'],
					'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
				);
			}

  /****************************** Get All Category *****************************************************************************/
    $this->load->model('catalog/category');
     $category_id = 0;
    $data['category_info'] = $this->model_catalog_category->getCategoriesinfo($category_id);
    
   
/******************************* End Here ***************************************************************************************/
 		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
  
		$this->response->setOutput($this->load->view('common/home', $data));
   
}

private function validate() {
	
	
		if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$this->error['email'] = $this->language->get('error_email');
		}
 
    		return !$this->error;
}


}
