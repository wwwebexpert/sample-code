<?php
class ControllerCommonHeader extends Controller {
	public function index() {
 
		// Analytics
		$this->load->model('extension/extension');

		$data['analytics'] = array();

		$analytics = $this->model_extension_extension->getExtensions('analytics');

		foreach ($analytics as $analytic) {
			if ($this->config->get($analytic['code'] . '_status')) {
				$data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get($analytic['code'] . '_status'));
			}
		}

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
		}

		$data['title'] = $this->document->getTitle();
		$data['base'] = $server;
		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts();
		$data['lang'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');
		$data['name'] = $this->config->get('config_name');
		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}
		$this->load->language('common/header');
		$data['text_home'] = $this->language->get('text_home');
		// Wishlist
		if ($this->customer->isLogged()) {
  
			$this->load->model('account/wishlist');
 		$this->load->model('account/customer');
                $customer_id=$this->session->data['customer_id'];
                $customerinfo=$this->model_account_customer->getCustomerInfo($customer_id);
                $data['customerinfo']=$customerinfo;

			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
		} else {
			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		}

		$data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
		$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));

		$data['text_account'] = $this->language->get('text_account');
		$data['text_register'] = $this->language->get('text_register');
		$data['text_login'] = $this->language->get('text_login');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_buyermessage'] = $this->language->get('text_buyermessage');
		$data['text_logout'] = $this->language->get('text_logout');
		$data['text_checkout'] = $this->language->get('text_checkout');
		$data['text_category'] = $this->language->get('text_category');
		$data['text_all'] = $this->language->get('text_all');

		$data['home'] = $this->url->link('common/home');
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['logged'] = $this->customer->isLogged();
		$data['ispartner'] = @$this->session->data['is_partner'];
		$data['customer_id'] = @$this->session->data['customer_id'];
		$data['account'] = $this->url->link('account/account', '', true);
		$data['register'] = $this->url->link('account/register/customer', '', true);
		$data['seller'] = $this->url->link('account/register', '', true);
		$data['login'] = $this->url->link('account/login', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['transaction'] = $this->url->link('account/transaction', '', true);
		$data['download'] = $this->url->link('account/download', '', true);
		$data['buyermessage'] = $this->url->link('account/buyermessage', '', true);
		$data['logout'] = $this->url->link('account/logout', '', true);
		$data['shopping_cart'] = $this->url->link('checkout/cart');
		$data['checkout'] = $this->url->link('checkout/checkout', '', true);
		$data['contact'] = $this->url->link('information/contact');
		$data['telephone'] = $this->config->get('config_telephone');

                //Get Customer information//
               
                
                //End Here //

		// Menu
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			if ($category['top']) {
				// Level 2
				$children_data = array();
				$subchildren_data = array();
				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach ($children as $child) {
					
					//Level Two Sub Category //
					
					

				$subchildren = $this->model_catalog_category->getCategories($child['category_id']);
				
				if(count($subchildren)>0){
				foreach($subchildren as $subchild)
				{
					
				$filter_data = array(
						'filter_category_id'  => $subchild['category_id'],
						'filter_sub_category' => true
					);

					$subchildren_data[] = array(
					    'parent_id'=>$subchild['parent_id'],
						'name'  => $subchild['name'],
						'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']. '_' . $subchild['category_id'])
					);	
				}
				}
				
				
					//End here//
					
					$filter_data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);

					$children_data[] = array(
						'child_category'=>$child['category_id'],
						'name'  => $child['name'],
						'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
					);
				}

				// Level 1
				$data['categories'][] = array(
					'name'     => $category['name'],
					'children' => $children_data,
					'subchildren'=>$subchildren_data,
					'column'   => $category['column'] ? $category['column'] : 1,
					'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
				);
			}
		}
  
		$data['language'] = $this->load->controller('common/language');
		$data['currency'] = $this->load->controller('common/currency');
		$data['search'] = $this->load->controller('common/search');
		$data['cart'] = $this->load->controller('common/cart');

		// For page specific css
		if (isset($this->request->get['route'])) {
			if (isset($this->request->get['product_id'])) {
				$class = '-' . $this->request->get['product_id'];
			} elseif (isset($this->request->get['path'])) {
				$class = '-' . $this->request->get['path'];
			} elseif (isset($this->request->get['manufacturer_id'])) {
				$class = '-' . $this->request->get['manufacturer_id'];
			} elseif (isset($this->request->get['information_id'])) {
				$class = '-' . $this->request->get['information_id'];
			} else {
				$class = '';
			}

			$data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
		} else {
			$data['class'] = 'common-home';
		}

		return $this->load->view('common/header', $data);
	}
/************** Text chat section Code Here Ramesh Kamboj*************************/
function sendChat() {
$this->load->model('account/customer');
       $customer_id=$this->session->data['customer_id'];


$customerinfo=$this->model_account_customer->getCustomer($customer_id);
               
session_start();
	 $from = $customerinfo['firstname'].$customerinfo['lastname'].$customerinfo['customer_id'];

	//echo $from;
	 $to = $_POST['to'];
	
	 $message = addslashes($_POST['message']);
	$this->model_account_customer->insertchatsession($from,$to,$message);

	$_SESSION['openChatBoxes'][$_POST['to']] = date('Y-m-d H:i:s', time());
	
	 $messagesan = $message;

	if (!isset($_SESSION['chatHistory'][$_POST['to']])) {
		$_SESSION['chatHistory'][$_POST['to']] = '';
	}

	$_SESSION['chatHistory'][$_POST['to']] .= <<<EOD
					   {
			"s": "1",
			"f": "{$to}",
			"m": "{$messagesan}"
	   },
EOD;


	unset($_SESSION['tsChatBoxes'][$_POST['to']]);
  	
	echo "1";
	exit(0);
}
public function startChatSession()
{
  $items='';
header('Content-type: application/json');
?>
{
		"username": "<?php echo"Me"; // $this->session->userdata('user_name');?>",
		"items": [
			<?php echo $items;?>
        ]
}

<?php
exit(0);
 
}

function chatBoxSession($chatbox) {
	
	$items = '';
	if (isset($_SESSION['chatHistory'][$chatbox])) {
		$items = $_SESSION['chatHistory'][$chatbox];
	}
	return $items;
}

function chatHeartbeat() {
$this->load->model('account/customer');
 $customer_id=$this->session->data['customer_id'];


$customerinfo=$this->model_account_customer->getCustomer($customer_id);
               //session_start();
	 $from = $customerinfo['firstname'].$customerinfo['lastname'].$customerinfo['customer_id'];
//$from = $this->session->data['firstname'].$this->session->data['lastname'].$this->session->data['customer_id'];
	session_start();
	//session_destroy();
	$date=date('Y-m-d');
	   $sql = "select * from ". DB_PREFIX ."chat where (tos = '".$from."' AND recd = 0   )  order by id asc ";

	$query = $this->db->query($sql);

	
	$items = '';

	$chatBoxes = array();

	foreach ($query->rows as $chat) {

//exit;

 if (!isset($_SESSION['openChatBoxes'][$chat['froms']]) && isset($_SESSION['chatHistory'][$chat['froms']])) {
			$items = $_SESSION['chatHistory'][$chat['froms']];
		}
  
		 $chat['message'] = $chat['message'];

		$items .= <<<EOD
					   {
			"s": "2",
			"f": "{$chat['froms']}",
			"m": "{$chat['message']}"
	   },
EOD;

	if (!isset($_SESSION['chatHistory'][$chat['froms']])) {
		$_SESSION['chatHistory'][$chat['froms']] = '';
	}

	$_SESSION['chatHistory'][$chat['froms']] .= <<<EOD
						   {
			"s": "0",
			"f": "{$chat['froms']}",
			"m": "{$chat['message']}"
	   },
EOD;
		
		unset($_SESSION['tsChatBoxes'][$chat['froms']]);
		$_SESSION['openChatBoxes'][$chat['froms']] = $chat['sent'];
	}
	
	if (!empty($_SESSION['openChatBoxes'])) {
	foreach ($_SESSION['openChatBoxes'] as $chatbox => $time) {
		if (!isset($_SESSION['tsChatBoxes'][$chatbox])) {
			$now = time()-strtotime($time);
			$time = date('g:iA M dS', strtotime($time));

			$message = "Sent at $time";
			if ($now > 180) {
				/*$items .= <<<EOD
{
"s": "2",
"f": "$chatbox",
"m": "{$message}"
},
EOD;*/

	if (!isset($_SESSION['chatHistory'][$chatbox])) {
		$_SESSION['chatHistory'][$chatbox] = '';
	}

	$_SESSION['chatHistory'][$chatbox] .= <<<EOD
		{
"s": "2",
"f": "$chatbox",
"m": "{$message}"
},
EOD;
			$_SESSION['tsChatBoxes'][$chatbox] = 1;
		}
		}
	}
}


	  $sql = "update ". DB_PREFIX ."chat set recd = 1 where tos = '".$from."' and recd = 0";
	
	$query = $this->db->query($sql);

	if ($items != '') {
		$items = substr($items, 0, -1);
	}
	
header('Content-type: application/json');
?>
{
		"items": [
			<?php echo $items;?>
        ]
}

<?php
			exit(0);
}


/*********************** Chat History Function Here ******************************/


/*********************** Chat History Function Here ******************************/

 function chatHeartbeatHistory() {
$this->load->model('account/customer');
	session_start();
	//session_destroy();
	$date=date('Y-m-d');
	  
$title=$_REQUEST['name'];
$customer_id=$this->session->data['customer_id'];


$customerinfo=$this->model_account_customer->getCustomer($customer_id);
               
//session_start();
	 $from = $customerinfo['firstname'].$customerinfo['lastname'].$customerinfo['customer_id'];
 $sql="SELECT * FROM ". DB_PREFIX ."chat WHERE (froms='".$from."' and  tos='".$title."') or (tos='".$from."' and froms='".$title."') and recd = 1";

//exit;

	$query = $this->db->query($sql);

	
	$items = '';

	$chatBoxes = array();

	foreach ($query->rows as $chat) {

 if (!isset($_SESSION['openChatBoxes'][$chat['froms']]) && isset($_SESSION['chatHistory'][$chat['froms']])) {
			$items = $_SESSION['chatHistory'][$chat['froms']];
		}
  
		 $chat['message'] = $chat['message'];
$messageinfo=$chat['froms'].":".$chat['message'];
		$items .= <<<EOD
					   {
			"s": "2",
			"f": "{$chat['froms']}",
			"m": "{$messageinfo}"
	   },
EOD;

	if (!isset($_SESSION['chatHistory'][$chat['froms']])) {
		$_SESSION['chatHistory'][$chat['froms']] = '';
	}

	$_SESSION['chatHistory'][$chat['froms']] .= <<<EOD
						   {
			"s": "0",
			"f": "{$chat['froms']}",
			"m": "{$chat['message']}"
	   },
EOD;
		
		unset($_SESSION['tsChatBoxes'][$chat['froms']]);
		$_SESSION['openChatBoxes'][$chat['froms']] = $chat['sent'];
	}
	
	if (!empty($_SESSION['openChatBoxes'])) {
	foreach ($_SESSION['openChatBoxes'] as $chatbox => $time) {
		if (!isset($_SESSION['tsChatBoxes'][$chatbox])) {
			$now = time()-strtotime($time);
			$time = date('g:iA M dS', strtotime($time));

			$message = "Sent at $time";
			if ($now > 180) {
				/*$items .= <<<EOD
{
"s": "2",
"f": "$chatbox",
"m": "{$message}"
},
EOD;*/

	if (!isset($_SESSION['chatHistory'][$chatbox])) {
		$_SESSION['chatHistory'][$chatbox] = '';
	}

	$_SESSION['chatHistory'][$chatbox] .= <<<EOD
		{
"s": "1",
"f": "$chatbox",
"m": "{$message}"
},
EOD;
			$_SESSION['tsChatBoxes'][$chatbox] = 1;
		}
		}
	}
}


	 

	if ($items != '') {
		$items = substr($items, 0, -1);
	}
	
header('Content-type: application/json');
?>
{
		"items": [
			<?php echo $items;?>
        ]
}

<?php
			exit(0);
}


/********************** End Here *************************************************/
/************** End Here *********************************************************/


}
