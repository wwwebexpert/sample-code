<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1>View Message</h1>
      <?php if ($message) { ?>
      <div class="table-responsive">
        <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <td class="text-right">ID</td>
              <td class="text-left">Seller Name</td>
	     <td class="text-right">Buyer Question</td>
              <td class="text-left">Seller Response</td>
             <td class="text-left">Action</td>
             
            
            </tr>
          </thead>
          <tbody>
            <?php foreach ($message as $order) { ?>
            <tr>
              <td class="text-left">#<?php echo $order['id']; ?></td>
              <td class="text-left"><?php echo $order['name']; ?></td>
	       <td class="text-left"><?php echo $order['question']; ?></td>
              <td class="text-left"><?php echo $order['message']; ?></td>
              <td class="text-left"> <a data-target="#myModal-seller-mail" data-toggle="modal">Reply to Seller</a></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
      
      <?php } else { ?>
      <p style="color:red;"><?php echo $text_empty; ?></p>
      <?php } ?>
      
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>


<?php if($logged){ ?>
<div class="modal fade" id="myModal-seller-mail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?php echo $text_close; ?></span></button>
        <h3 class="modal-title"><?php echo $text_ask_seller; ?></h3>
      </div>
      <form id="seller-mail-form">
        <div class="modal-body">
          <div class="form-group required">
            <label class="control-label" for="input-subject"><?php echo $text_subject; ?></label>
            <input type="text" name="subject" id="input-subject" class="form-control" />
            <?php if(isset($partner)){ ?>
            <input type="hidden" name="seller" value="<?php echo $seller_id;?>"/>           
            <?php } ?>
          </div>
          <div class="form-group required">
            <label class="control-label" for="input-message"><?php echo $text_ask; ?></label>
            <textarea class="form-control" name="message" rows="3" id="input-message"></textarea>
          </div>
          <div class="error text-center text-danger"></div>
        </div>
      </form>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $text_close; ?></button>
        <button type="button" class="btn btn-primary" id="send-mail"><?php echo $text_send; ?></button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php } ?>


<?php if($logged){ ?>
$('#send-mail').on('click',function(){
  f = 0;
  $('#myModal-seller-mail input[type=\'text\'],#myModal-seller-mail textarea').each(function () {
    if ($(this).val() == '') {
      $(this).parent().addClass('has-error');
      f++;
    }else{
      $(this).parent().removeClass('has-error');
    }
  });

  if (f > 0) {
    $('#myModal-seller-mail .error').text('<?php echo $text_error_mail; ?>').slideDown('slow').delay(2000).slideUp('slow');
  } else {
    $('#send-mail').addClass('disabled');
    $('#myModal-seller-mail').addClass('mail-procss');
    $('.alert-success').remove();
    $('#myModal-seller-mail .modal-body').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $text_success_mail; ?><button type="button" class="close" data-dismiss="alert">&times;</button></div>');
    $.ajax({
      url: '<?php echo $send_mail; ?>',
      data: $('#seller-mail-form').serialize()+'<?php echo $mail_for; ?>',
      type: 'post',
      dataType: 'json',
      complete: function () {
        $('#send-mail').removeClass('disabled');
        $('#myModal-seller-mail input,#myModal-seller-mail textarea').each(function () {
          $(this).val(''); 
          $(this).text(''); 
        });
      }
    });
  }
});
<?php } ?>
