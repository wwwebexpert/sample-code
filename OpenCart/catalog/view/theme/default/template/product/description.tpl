<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<div class="container">
<div class="row">
<div id="content" class="col-sm-12">    
<div class="row">
<div class="col-sm-12">
          <div class="tab-content">
        <center><h2>Product Description</h2></center>
            <div class="tab-pane active" id="tab-description"><p> <?=$description; ?> </p>
            </div>
                               
          </div>
        </div>
            </div>
            </div>
  </div>
            </div>                   
 <style>
                .tab-content{
                  border: 1px solid black;
                  padding: 17px;
                }
                #wk_seller_info_container{
                  width: 100%;
                  margin: 10px 0;
                  /*padding: 20px;*/
                  /*background-color: #eee;*/
                }

                #wk_seller_info_profpic{
                  margin-right: 20px;
                  display: inline-block;
                  width: 80px;
                  height: 80px;
                }

                #wk_seller_info_box{
                  display: inline-block;
                }

                #wk_seller_info_rating{
                  position: relative;
                  cursor: pointer;
                }

                #wk_seller_info_rating > .fa{
                  font-size: 10px;
                }

                #wk_seller_info_rating > .fa > .fa-stack-1x {
                    color:#3B9911;
                }

                #wk_seller_info_rating_details{
                  position: absolute;
                  min-width: 250px;
                  height: auto;
                  background: #FFFFFF;
                  top: 23px;
                  padding: 20px;
                  box-shadow: 0 2px 8px 2px #868282;
                  display: none;
                  z-index: 100000;
                }

                .wk_rating_details_box > .wk_rating_details > .fa{
                  font-size: 10px;
                }

                .wk_rating_details_box > .wk_rating_details > .fa > .fa-stack-1x {
                    color:#3B9911;
                }

                .wk_rating_details{
                  padding-left: 5px;
                }

                @media only screen and (max-width: 700px) {
                    
                    #wk_seller_info_rating_details{

                        min-width: 200px;
                        /*height: 100px;*/
                    }
                }

  </style>
              
            
              
              <script>
                $("#wk_seller_info_rating, #wk_seller_info_rating_details").on('mouseover', function(){
                    $("#wk_seller_info_rating_details").show();
                });

                $("#wk_seller_info_rating, #wk_seller_info_rating_details").on('mouseout', function(){
                    $("#wk_seller_info_rating_details").hide();
                });
              </script>
                                            
      

          