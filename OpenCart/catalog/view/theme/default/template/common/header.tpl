<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<!-- Bootstrap -->
   
    <!-- Bootstrap -->
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
    <link href="catalog/view/theme/default/stylesheet/bootstrap.min.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/style.css" rel="stylesheet">
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Vollkorn" rel="stylesheet"> 

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="catalog/view/theme/default/js/bootstrap.min.js"></script>
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>

<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
  </head>
  <body>
  
    <header>
    
      <div class="header-top-bar">
  <div class="container">
  <div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  


  <div class="header-top-text">

  <ul class=" list-inline">
 
 <?php if ($logged) { ?>
<li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <span class="caret"></span></a>

          <ul class="dropdown-menu dropdown-menu-right">
           
            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
            <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
            <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
	    
            <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
            
          </ul>
        </li>
<style>
.dropdown-menu1 {
    background-clip: padding-box;
    background-color: #fff;
    border: 1px solid rgba(0, 0, 0, 0.15);
    border-radius: 4px;
    box-shadow: 0 6px 12px rgba(0, 0, 0, 0.176);
    display: none;
    float: left;
    font-size: 14px;
    left: 0;
    list-style: outside none none;
    margin: 2px 0 0;
    min-width: 160px;
    padding: 5px 0;
    position: absolute;
    text-align: left;
    top: 100%;
    z-index: 1000;
}
</style>
<li  style="position: relative;"><a href="index.php?route=customerpartner/sell" title="<?php echo $text_account; ?>" ><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md">Online Customer</span> <!--span class="caret"></span--></a>

          <!--ul class="dropdown-menu dropdown-menu-right">
           <?php foreach($customerinfo as $customer){
$username=str_replace(' ','',$customer['firstname']."".$customer['lastname']."".$customer['customer_id']);
?>
            <li><a href="#" onclick="javascript:chatWith('<?php echo $username;?>');"><?php echo $customer['firstname']." " .$customer['lastname']; ?></a></li>
            <?php } ?>
            
            
          </ul-->
        </li>
<?php if(@$ispartner!='1'){ ?>
             <li><a href="<?php echo $seller; ?>">Open a Shop </a></li>
	    <?php } ?>
<?php } ?>
   	<?php if (!$logged) { ?>
		

            <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
            <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>

            <?php } ?>
<?php if(@$ispartner!='1'){ ?>
 <li><a href="<?php echo $buyermessage; ?>">Message<button class="btn btn-green-cicle-notification" type="button">0</button></a></li>
<?php } else { ?>
<li><a href="index.php?route=customerpartner/profile&id=<?php echo $customer_id;?>">Message<button class="btn btn-green-cicle-notification" type="button">0</button></a></li>
<?php } ?>

<li><a href="<?php echo $checkout; ?>" title="<?php echo $text_checkout; ?>"><?php echo $text_checkout; ?></a></li>
  <li>

 <?php echo $currency; ?></li>
  </ul>

 
  </div>
  
  </div>
  

  
  </div>
  
  </div>
  
  </div>
  
  
  <div class="header-mid-bar">
  
  
  <div class="container">
  <div class="row">
  <div class="col-xs-12 col-sm-4 colmd-4">
  
  <div class="header-search-field">
 <div class="media" id="search">
  <span class="media-left pull-left">
  <button class="btn btn-green-cicle" type="button"><i class="fa fa-search"></i></button>
  
  </span>
  
   <input type="text" name ="search" class="form-control pull-left" placeholder="Search our entire store...">

  </div>

  <!--div id="search" class="input-group">
  <input name="search" value="" placeholder="Search" class="form-control input-lg" type="text">
  <span class="media-left">
    <button type="button" class="btn  btn-green-cicle"><i class="fa fa-search"></i></button>
  </span>
</div-->




     </div>
   
  </div>
  
  
   <div class="col-xs-12 col-sm-4 colmd-4">
   
   <div class="logo">
 <?php if ($logo) { ?>
          <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
          <?php } else { ?>
          <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>

</div>
   
   </div>
  
  
  <div class="col-xs-12 col-sm-4 colmd-4">
  
  <div class="header-search-field">
 <div class="media">
  
  <div class="media-body">
   <p class="noofitems"><?php echo $cart; ?></p>


  </div>
  <div class="media-left">

  <a href="index.php?route=checkout/cart" title="View Cart"><button class="btn btn-green-cicle" type="button"><i class="fa fa-shopping-cart"></i></button></a>
   
  </div>
  
</div>
  



     </div>
   
  </div>
  
  </div>
  
  </div>
  
  
  
  
  </div>
  
  
  <div class="header-bottom-bar">
  <!--nav class="navbar navbar-default" data-spy="affix" data-offset-top="197" style="z-index:20;">
      
         
          <?php if ($categories) { ?>
<div class="container">
  <nav id="menu" class="navbar">
    <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
      <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav">
<li><a href="index.php"><i class="fa fa-home"></i></a></li>
        <?php foreach ($categories as $category) { ?>
        <?php if ($category['children']) { ?>
        <li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?></a>
          <div class="dropdown-menu">
            <div class="dropdown-inner">
              <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
              <ul class="list-unstyled">
                <?php foreach ($children as $child) { ?>
                <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                <?php } ?>
              </ul>
              <?php } ?>
            </div>
            <a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a> </div>
        </li>
        <?php } else { ?>
        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
        <?php } ?>
        <?php } ?>
      </ul>
    </div>
  </nav>
</div>
<?php } ?>
      
      </nav-->
      
     
      
	   <?php if ($categories) { ?>
	  <div class="container">
      <a class="toggleMenu" href="#">Menu</a>
<ul class="nav">

<li>
		<a href="#">
    
   <i class="fa fa-home"></i>

</a>
	</li>
<?php foreach ($categories as $category) {


 ?>
        <?php if ($category['children']) { ?>
    <li  class="test">
		<a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
		<?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
		<ul>
		   <?php foreach ($children as $child) { 
		   
		   ?>
		   <?php if ($category['subchildren']) { ?>
			<li>
				<a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
				<?php foreach (array_chunk($category['subchildren'], ceil(count($category['subchildren']) / $category['column'])) as $subchildren) { 
				?>
				<?php if(count($subchildren)>0){?>
				<ul class="list-inline inner" >
				   <?php foreach ($subchildren as $subchild) { ?>
				   <?php if($child['child_category']==$subchild['parent_id']){?>
					<li><a href="<?php echo $subchild['href']; ?>"><?php echo $subchild['name']; ?></a></li>
					
					<?php }  } ?>
				</ul>
				<?php }  } ?>
			</li>
			<?php }
			else { ?>
        <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
        <?php } ?>
			<?php } ?>
			
		</ul>
		<?php } ?>
	</li>
	<li>
		 <?php }  else { ?>
        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
        <?php } ?>
        <?php } ?>
</ul>
      </div>
	   <?php } ?>
	  
	  
	  
	  
  </div>  
  </header>


