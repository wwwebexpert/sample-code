
 
 <footer>
  
  <div class="container">
  <div class="row">
  
  <?php if ($_SERVER['QUERY_STRING']!='route=common/home') {?>
  <div class="col-xs-12">
  <div class="foot-bordertop">
  <br></div>
  
  
  </div>
  <?php } ?>
  <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
  <div class="row">
  <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
   <h3 class="foot-heading"><?php echo $text_information; ?></h3>
 
   
   <ul class="list-unstyled foot-content">
 <?php foreach ($informations as $information) { ?>
   <li>&gt;<a href="<?php echo $information['href']; ?>"> <?php echo $information['title']; ?></a></li>
  <?php } ?>
   </ul>
   
   
   
   
   
  </div>
  
  <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
   <h3 class="foot-heading"><?php echo $text_service; ?></h3>
 
   
   <ul class="list-unstyled foot-content">
   <li>&gt;<a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>

          <li>&gt;<a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
          <li>&gt;<a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
   </ul>
   
   
   
   
   
  </div>
  
  <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
   <h3 class="foot-heading"><?php echo $text_extra; ?></h3>
 
   
   <ul class="list-unstyled foot-content">
 <li>&gt;<a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
          <li>&gt;<a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
          <li>&gt;<a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
          <li>&gt;<a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
   
   </ul>
   
   
   
   
   
  </div>
  
  
  
  
  
  
  </div>
  
  
  
  </div>
  
  
  <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
  <div class="row">
  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
   <h3 class="foot-heading">Join the community</h3>
 
   
   <ul class="list-unstyled foot-content">
   <li>&gt;<a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
          <li>&gt;<a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
          <li>&gt;<a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
          <li>&gt;<a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
   </ul>
   
   
   
   
   
  </div>
  
  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
   <h3 class="foot-heading">Follow us</h3>
 
   
   <ul class="list-inline foot-content social-icons">
   <li><a href="https://www.facebook.com/Products-of-Nigeria-1061529660650069/?ref=aymt_homepage_panel"> <img src="image/catalog/social1.png"  alt=""/></a></li>
   <li><a href="https://twitter.com/P_O_Nigeria"><img src="image/catalog/social2.png"  alt=""/></a></li>
   <li><a href="https://plus.google.com/u/0/"> <img src="image/catalog/social3.png"  alt=""/></a></li>
   <li><a href="https://www.instagram.com/productsofnigeria/"> <img src="image/catalog/Instagram.png"  alt=""/></a></li>

  
   </ul>
   
   
   
   
   
  </div>
  
  
  
  
  
  
  
  
  </div>
  
  
  
  </div>
  
  
  </div>
  
  <div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12">
  <hr />
  
   <div class="row">
  <div class="col-xs-12 col-sm-12 col-md-6">
  
  <ul class="list-inline">
  
  <li>
  <p>Copyright © 2014, Product of Nigeria, All Rights Reserved</p></li>
  
  </ul>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-6">
  <ul class="list-inline payment-icons">
  <li><a href="#"><img src="images/payment-1.jpg"  alt=""/></a></li>
    <li><a href="#"><img src="images/payment-2.jpg"  alt=""/></a></li>
      <li><a href="#"><img src="images/payment-3.jpg"  alt=""/></a></li>
        <li><a href="#"><img src="images/payment-4.jpg"  alt=""/></a></li>
          <li><a href="#"><img src="images/payment-5.jpg"  alt=""/></a></li>
            <li><a href="#"><img src="images/payment-6.jpg"  alt=""/></a></li>
            
  
  
  </ul>
  
  
  
  </div>
  
  
  </div>
  
  </div>
  
  
  
  </div>
  </div>
  
  
  </footer>

     <script>
    
    var ww = document.body.clientWidth;

$(document).ready(function() {
    $(".nav li a").each(function() {
    	if ($(this).next().length > 0) {
			$(this).addClass("parent");
		};
	})
	
	$(".toggleMenu").click(function(e) {
		e.preventDefault();
		$(this).toggleClass("active");
		$(".nav").toggle();
	});
	adjustMenu();
})

$(window).bind('resize orientationchange', function() {
	ww = document.body.clientWidth;
	adjustMenu();
});

var adjustMenu = function() {
	if (ww < 1199) {
		$(".toggleMenu").css("display", "inline-block");
		if (!$(".toggleMenu").hasClass("active")) {
			$(".nav").hide();
		} else {
			$(".nav").show();
		}
		$(".nav li").unbind('mouseenter mouseleave');
		$(".nav li a.parent").unbind('click').bind('click', function(e) {
			// must be attached to anchor element to prevent bubbling
			e.preventDefault();
			$(this).parent("li").toggleClass("hover");
		});
	} 
	else if (ww >= 1199) {
		$(".toggleMenu").css("display", "none");
		$(".nav").show();
		$(".nav li").removeClass("hover");
		$(".nav li a").unbind('click');
		$(".nav li").unbind('mouseenter mouseleave').bind('mouseenter mouseleave', function() {
		 	// must be attached to li so that mouseleave is not triggered when hover over submenu
		 	$(this).toggleClass('hover');
		});
	}
}
    
    </script>
    




  </body>

<script src="catalog/view/javascript/chat.js" type="text/javascript"></script>
<link href="catalog/view/javascript/chat.css" type="text/css" rel="stylesheet"/>
</html>
