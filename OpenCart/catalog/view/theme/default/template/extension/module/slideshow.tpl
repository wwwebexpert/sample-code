
 <section class="banner">

      <div class="carousel slide" id="carousel-example-captions" data-ride="carousel">
        <ol class="carousel-indicators hidden-xs hidden-sm hidden-md">
<?php for($i=0;$i<count($banners);$i++){?>
          <li data-target="#carousel-example-captions" data-slide-to="<?php echo $i;?>" class="active"></li>
<?php } ?>
         
        </ol>
        <div class="carousel-inner" role="listbox">

 <?php 
 $count=1;
foreach ($banners as $banner) { ?>
          <div class="item <?php if($count==1){ ?> active <?php } ?>">
          
          <img  src="<?php echo $banner['image']; ?>" width="100%">
   
          <?php if($count==1){?>
          
         <div class="carousel-caption">
              <h3>
Every Product is Unique<br>

 Create your own online store with us and start selling.</h3>
 
 <p class="for-hide"><button class="btn btn-explore">EXPLORE</button></p>
              
           
          
          
          </div>
<?php } ?>

 <?php if($count==2){?>
          
         <div class="carousel-caption">
              <h3>
Top Notch Products and Deals<br>

 Buying and Selling online was never so effortless.</h3>
 
 <p class="for-hide"><button class="btn btn-explore">Let’s Get Started</button></p>
              
           
          
          
          </div>
<?php } ?>
           
          </div>
          <?php $count++; } ?>
         
         
        <a href="#carousel-example-captions" class="left carousel-control" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a href="#carousel-example-captions" class="right carousel-control" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
    
  </div>
  
  </section>
  
