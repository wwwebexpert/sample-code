  
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Checkout</title>

<!-- Bootstrap -->
   
    <!-- Bootstrap -->
    <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
    <link href="catalog/view/theme/default/stylesheet/bootstrap.min.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/style.css" rel="stylesheet">
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Vollkorn" rel="stylesheet"> 

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="catalog/view/theme/default/js/bootstrap.min.js"></script>
<link href="catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen">
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<link href="http://pon.mobilytedev.com/image/catalog/cart.png" rel="icon">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<script src="catalog/view/javascript/jquery/datetimepicker/moment.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
</head>

<body>
  <div class="panel-body">
<form class="form-horizontal">
  <fieldset id="payment">
    <legend>Credit Card Details</legend>
    <div class="form-group required">
      <label class="col-sm-2 control-label" for="input-cc-type">Card Type</label>
      <div class="col-sm-10">
        <select name="cc_type" id="input-cc-type" class="form-control">
                    <option value="VISA">Visa</option>
                    <option value="MASTERCARD">MasterCard</option>
                    <option value="DISCOVER">Discover Card</option>
                    <option value="AMEX">American Express</option>
                    <option value="SWITCH">Maestro</option>
                    <option value="SOLO">Solo</option>
                  </select>
      </div>
    </div>
    <div class="form-group required">
      <label class="col-sm-2 control-label" for="input-cc-number">Card Number</label>
      <div class="col-sm-10">
        <input type="text" name="cc_number" value="" placeholder="Card Number" id="input-cc-number" class="form-control">
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label" for="input-cc-start-date"><span data-toggle="tooltip" title="" data-original-title="(if available)">Card Valid From Date</span></label>
      <div class="col-sm-3">
        <select name="cc_start_date_month" id="input-cc-start-date" class="form-control">
                    <option value="01">January</option>
                    <option value="02">February</option>
                    <option value="03">March</option>
                    <option value="04">April</option>
                    <option value="05">May</option>
                    <option value="06">June</option>
                    <option value="07">July</option>
                    <option value="08">August</option>
                    <option value="09">September</option>
                    <option value="10">October</option>
                    <option value="11">November</option>
                    <option value="12">December</option>
                  </select>
      </div>
      <div class="col-sm-3">
        <select name="cc_start_date_year" class="form-control">
                    <option value="2008">2008</option>
                    <option value="2009">2009</option>
                    <option value="2010">2010</option>
                    <option value="2011">2011</option>
                    <option value="2012">2012</option>
                    <option value="2013">2013</option>
                    <option value="2014">2014</option>
                    <option value="2015">2015</option>
                    <option value="2016">2016</option>
                    <option value="2017">2017</option>
                    <option value="2018">2018</option>
                  </select>
      </div>
    </div>
    <div class="form-group required">
      <label class="col-sm-2 control-label" for="input-cc-expire-date">Card Expiry Date</label>
      <div class="col-sm-3">
        <select name="cc_expire_date_month" id="input-cc-expire-date" class="form-control">
                    <option value="01">January</option>
                    <option value="02">February</option>
                    <option value="03">March</option>
                    <option value="04">April</option>
                    <option value="05">May</option>
                    <option value="06">June</option>
                    <option value="07">July</option>
                    <option value="08">August</option>
                    <option value="09">September</option>
                    <option value="10">October</option>
                    <option value="11">November</option>
                    <option value="12">December</option>
                  </select>
      </div>
      <div class="col-sm-3">
        <select name="cc_expire_date_year" class="form-control">
                    <option value="2018">2018</option>
                    <option value="2019">2019</option>
                    <option value="2020">2020</option>
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                    <option value="2023">2023</option>
                    <option value="2024">2024</option>
                    <option value="2025">2025</option>
                    <option value="2026">2026</option>
                    <option value="2027">2027</option>
                    <option value="2028">2028</option>
                  </select>
      </div>
    </div>
    <div class="form-group required">
      <label class="col-sm-2 control-label" for="input-cc-cvv2">Card Security Code (CVV2)</label>
      <div class="col-sm-10">
        <input type="text" name="cc_cvv2" value="" placeholder="Card Security Code (CVV2)" id="input-cc-cvv2" class="form-control">
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label" for="input-cc-issue"><span data-toggle="tooltip" title="" data-original-title="(for Maestro and Solo cards only)">Card Issue Number</span></label>
      <div class="col-sm-10">
        <input type="text" name="cc_issue" value="" placeholder="Card Issue Number" id="input-cc-issue" class="form-control">
      </div>
    </div>
  </fieldset>
</form>
<div class="buttons">
  <div class="pull-right">
    <input type="button" value="Confirm Order" id="button-confirm" data-loading-text="Loading..." class="btn btn-primary">
  </div>
</div>
</div>
</body>
</html> 
<script type="text/javascript"><!--
$('#button-confirm').bind('click', function() {
  $.ajax({
    url: 'index.php?route=extension/payment/pp_pro/send',
    type: 'post',
    data: $('#payment :input'),
    dataType: 'json',
    beforeSend: function() {
      $('#button-confirm').attr('disabled', true);
      $('#payment').before('<div class="alert alert-info"><i class="fa fa-info-circle"></i> Please wait!</div>');
    },
    complete: function() {
      $('.alert').remove();
      $('#button-confirm').attr('disabled', false);
    },
    success: function(json) {
      if (json['error']) {
        alert(json['error']);
      }
    
      if (json['success']) {
        location = json['success'];
      }
    }
  });
});
//--></script>