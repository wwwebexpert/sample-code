$(document).delegate('.wk_upload_img','click', function(){
  $('#modal-image').remove();
  $.ajax({
    url : 'index.php?route=common/filemanager&target=' + $(this).parent().find('input').attr('id') + '&thumb=' + $(this).find('img').attr('id'),
    dataType: 'html',
    success: function(html) {
      $('body').append('<div class="modal fade" id="modal-image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">' + html + '</div>');

      $('#modal-image').modal('show');
    }
  });
});

$(document).delegate('.delete_img','click', function() { 
  $(this).parent().parent().find('img').attr('src', '');
  $(this).parent().parent().find('input').attr('value', '');
});

$(document).ready(function(){
  $('button[data-event=\'showImageDialog\']').attr('data-toggle', 'image').removeAttr('data-event');
});

$(document).delegate('button[data-toggle=\'image\']', 'click', function() {
  $('#modal-image').remove();

  $(this).parents('.note-editor').find('.note-editable').focus();

  $.ajax({
    url: 'index.php?route=common/filemanager',
    dataType: 'html',
    success: function(html) {
      $('body').append('<div class="modal fade" id="modal-image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">' + html + '</div>');

      $('#modal-image').modal('show');
    }
  });
});