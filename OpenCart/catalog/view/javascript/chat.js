/*
	Handle the chat process by passing parameters in post method

*/

var windowFocus 		= true;
var username;
var chatHeartbeatCount 	= 0;
var minChatHeartbeat 	= 5000;
var maxChatHeartbeat 	= 33000;
var chatHeartbeatTime 	= minChatHeartbeat;
var originalTitle;
var blinkOrder 			= 0;

var chatboxFocus 		= new Array();
var newMessages 		= new Array();
var newMessagesWin 		= new Array();
var chatBoxes 			= new Array();



//jQuery.noConflict();

jQuery(document).ready(function(){ 

	originalTitle = document.title;
	startChatSession();

	jQuery([window, document]).blur(function(){
		windowFocus = false;
	}).focus(function(){
		windowFocus = true;
		document.title = originalTitle;
	});
});





//**********************************************************************************************************
	// restructureChatBoxes
//**********************************************************************************************************

function restructureChatBoxes() {
	align = 0;
	for (x in chatBoxes) {
		chatboxtitle = chatBoxes[x];

		if (jQuery("#chatbox_"+chatboxtitle).css('display') != 'none') {
			if (align == 0) {
				jQuery("#chatbox_"+chatboxtitle).css('left', '20px');
			} else {
				width = (align)*(225+7)+20;
				jQuery("#chatbox_"+chatboxtitle).css('left', width+'px');
			}
			align++;
		}
	}
}



//**********************************************************************************************************
	// chatWith
//**********************************************************************************************************

function chatWith(chatuser) {
  
	createChatBox(chatuser);
	jQuery("#chatbox_"+chatuser+" .chatboxtextarea").focus();
	chathistory(chatuser);
}






//**********************************************************************************************************
	// createChatBox
//**********************************************************************************************************


function createChatBox(chatboxtitle,minimizeChatBox) {
  
	if (jQuery("#chatbox_"+chatboxtitle).length > 0) {
		if (jQuery("#chatbox_"+chatboxtitle).css('display') == 'none') {
			jQuery("#chatbox_"+chatboxtitle).css('display','block');
			restructureChatBoxes();
		}
		jQuery("#chatbox_"+chatboxtitle+" .chatboxtextarea").focus();
		return;
	}

	jQuery(" <div />" ).attr("id","chatbox_"+chatboxtitle)
	.addClass("chatbox")
	.html('<div class="chatboxhead"><div class="chatboxtitle">'+chatboxtitle+'</div><div class="chatboxoptions"><a data-target="#myModal-seller-file" data-toggle="modal"><i class="fa fa-envelope-o"></i></a><a href="javascript:void(0)" onclick="javascript:toggleChatBoxGrowth(\''+chatboxtitle+'\')">-</a> <a href="javascript:void(0)" onclick="javascript:closeChatBox(\''+chatboxtitle+'\')">X</a></div><br clear="all"/></div><div class="chatboxcontent"></div><div class="chatboxinput"><textarea class="chatboxtextarea"  id="txt" name="txt" onkeydown="javascript:return checkChatBoxInputKey(event,this,\''+chatboxtitle+'\');"></textarea></div>')
	.appendTo(jQuery( "body" ));
			   
	jQuery("#chatbox_"+chatboxtitle).css('bottom', '0px');
	
	chatBoxeslength = 0;

	for (x in chatBoxes) {
		if (jQuery("#chatbox_"+chatBoxes[x]).css('display') != 'none') {
			chatBoxeslength++;
		}
	}

	if (chatBoxeslength == 0) {
		jQuery("#chatbox_"+chatboxtitle).css('right', '20px');
	} else {
		width = (chatBoxeslength)*(225+7)+20;
		jQuery("#chatbox_"+chatboxtitle).css('left', width+'px');
	}
	
	chatBoxes.push(chatboxtitle);

	if (minimizeChatBox == 1) {
		minimizedChatBoxes = new Array();

		if (jQuery.cookie('chatbox_minimized')) {
			minimizedChatBoxes = jQuery.cookie('chatbox_minimized').split(/\|/);
		}
		minimize = 0;
		for (j=0;j<minimizedChatBoxes.length;j++) {
			if (minimizedChatBoxes[j] == chatboxtitle) {
				minimize = 1;
			}
		}

		if (minimize == 1) {
			jQuery('#chatbox_'+chatboxtitle+' .chatboxcontent').css('display','none');
			jQuery('#chatbox_'+chatboxtitle+' .chatboxinput').css('display','none');
		}
	}

	chatboxFocus[chatboxtitle] = false;
  
	jQuery("#chatbox_"+chatboxtitle+" .chatboxtextarea").blur(function(){
		chatboxFocus[chatboxtitle] = false;
		jQuery("#chatbox_"+chatboxtitle+" .chatboxtextarea").removeClass('chatboxtextareaselected');
	}).focus(function(){
		chatboxFocus[chatboxtitle] = true;
		newMessages[chatboxtitle] = false;
		jQuery('#chatbox_'+chatboxtitle+' .chatboxhead').removeClass('chatboxblink');
		jQuery("#chatbox_"+chatboxtitle+" .chatboxtextarea").addClass('chatboxtextareaselected');
	});

	jQuery("#chatbox_"+chatboxtitle).click(function() {
		if (jQuery('#chatbox_'+chatboxtitle+' .chatboxcontent').css('display') != 'none') {
			jQuery("#chatbox_"+chatboxtitle+" .chatboxtextarea").focus();
		}
	});

	jQuery("#chatbox_"+chatboxtitle).show();
	///////////////
	//$("#chatbox_"+chatboxtitle).draggable();
	//jQuery(".chatbox_").draggable();
	///////////
}




//**********************************************************************************************************
	// chatHeartbeat
//**********************************************************************************************************


function chatHeartbeat(){

 
	var itemsfound = 0;
	//alert(windowFocus);
	if (windowFocus == false) {
		
		var blinkNumber = 0;
		var titleChanged = 0;
		for (x in newMessagesWin) {
			
			if (newMessagesWin[x] == true) {
				++blinkNumber;
				if (blinkNumber >= blinkOrder) {
					document.title = x+' says...';
					titleChanged = 1;
					break;	
				}
			}
		}
		
		if (titleChanged == 0) {
			document.title = originalTitle;
			blinkOrder = 0;
		} else {
			++blinkOrder;
		}

	} else {
		for (x in newMessagesWin) {
			newMessagesWin[x] = false;
		}
	}

	for (x in newMessages) {
		if (newMessages[x] == true) {
			if (chatboxFocus[x] == false) {
				//FIXME: add toggle all or none policy, otherwise it looks funny
				jQuery('#chatbox_'+x+' .chatboxhead').toggleClass('chatboxblink');
			}
		}
	}
	
	jQuery.ajax({
	  url: "http://pon.mobilytedev.com/index.php?route=common/header/chatheartbeat",
	  cache: false,
	  dataType: "json",
	  success: function(data) {
      
		jQuery.each(data.items, function(i,item){
			if (item)	{ // fix strange ie bug

				chatboxtitle = item.f;

				if (jQuery("#chatbox_"+chatboxtitle).length <= 0) {
					createChatBox(chatboxtitle);
				}
				if (jQuery("#chatbox_"+chatboxtitle).css('display') == 'none') {
					jQuery("#chatbox_"+chatboxtitle).css('display','block');
					restructureChatBoxes();
				}
				
				if (item.s == 1) {
					item.f = username;
				}
				items=replaceURLWithHTMLLinks(item.m);
				items=replacetext(items);

 
				if (item.s == 2) {
					jQuery("#chatbox_"+chatboxtitle+" .chatboxcontent").append('<div class="chatboxmessage"><span class="chatboxinfo"><b><span style="color:blue;">'+item.f+'</span></b>:&nbsp;&nbsp;</span><span class="chatboxmessagecontent">'+items+'</span></div>');
				} else {
					newMessages[chatboxtitle] = true;
					newMessagesWin[chatboxtitle] = true;
					jQuery("#chatbox_"+chatboxtitle+" .chatboxcontent").append('<div class="chatboxmessage"><span class="chatboxmessagefrom"><b>'+item.f+'</b>:&nbsp;&nbsp;</span><span class="chatboxmessagecontent">'+items+'</span></div>');
				}

				jQuery("#chatbox_"+chatboxtitle+" .chatboxcontent").scrollTop(jQuery("#chatbox_"+chatboxtitle+" .chatboxcontent")[0].scrollHeight);
				itemsfound += 1;
			}
		});

		chatHeartbeatCount++;

		if (itemsfound > 0) {
			chatHeartbeatTime = minChatHeartbeat;
			chatHeartbeatCount = 1;
		} else if (chatHeartbeatCount >= 10) {
			chatHeartbeatTime *= 2;
			chatHeartbeatCount = 1;
			if (chatHeartbeatTime > maxChatHeartbeat) {
				chatHeartbeatTime = maxChatHeartbeat;
			}
		}
// alert(chatHeartbeatCount);
 if(chatHeartbeatCount==1){
		//chatHeartbeathistory();
		}
		setTimeout('chatHeartbeat();',chatHeartbeatTime);
		
		
	}});
}

/*********************************************************************************************************/

 //Chat History Section 
/*********************************************************************************************************/

function chathistory(chatboxtitle)
{
  jQuery("#chatbox_"+chatboxtitle+" .chatboxcontent").html('');
  var itemsfound = 0;
   jQuery.ajax({
	  url: "http://pon.mobilytedev.com/index.php?route=common/header/chatHeartbeatHistory",
	  cache: false,
	 data: {
	  name:chatboxtitle
	  },
	  dataType: "json",
	  success: function(data) {
      
		jQuery.each(data.items, function(i,item){
			if (item)	{ // fix strange ie bug

				//chatboxtitle = item.f;
				//alert(chatboxtitle);

				/*if (jQuery("#chatbox_"+chatboxtitle).length <= 0) {
					//createChatBox(chatboxtitle);
				}*/
				/*if (jQuery("#chatbox_"+chatboxtitle).css('display') == 'none') {
					jQuery("#chatbox_"+chatboxtitle).css('display','block');
					restructureChatBoxes();
				}*/
				
				/*if (item.s == 1) {
					item.f = username;
				}*/
				items=replaceURLWithHTMLLinks(item.m);
				items=replacetext(items);
			       
 
				
				//alert(chatboxtitle);
					jQuery("#chatbox_"+chatboxtitle+" .chatboxcontent").append('<div class="chatboxmessage"><span class="chatboxinfo">'+items+'</span></div>');
				
				itemsfound += 1;
			}
		});
}});
}
//End Here //
function replaceURLWithHTMLLinks(text) {
    var exp = /(\b(http?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
    return text.replace(exp,"<a href='$1' target='_blank'>$1</a>");
}
function replacetext(text)
{
 //alert(text);
   str=text.replace(':)','<img src=https://www.culturesconverge.com/smiley/smiley-cool.png>');
   str=str.replace(':hehe','<img src=https://www.culturesconverge.com/smiley/smiley-hehe.png>');
   str=str.replace(':s','<img src=https://www.culturesconverge.com/smiley/smiley-puzzled.png>');
   str=str.replace(':((','<img src=https://www.culturesconverge.com/smiley/smiley-sad.png>');
   str=str.replace(':z','<img src=https://www.culturesconverge.com/smiley/smiley-sleeping.png>');
   str=str.replace(':smoke','<img src=https://www.culturesconverge.com/smiley/smiley-smoker.png>');
   str=str.replace(':x','<img src=https://www.culturesconverge.com/smiley/smiley-speechless.png>');
  
   str=str.replace(':love','<img src=https://www.culturesconverge.com/smiley/smiley-love.png>');
   str=str.replace(':kiss','<img src=https://www.culturesconverge.com/smiley/smiley-kiss.png>');
   str=str.replace(';)','<img src=https://www.culturesconverge.com/smiley/smiley-wink.png>');
	str=str.replace(':)','<img src=https://www.culturesconverge.com/smiley/smiley-cool.png>');
	str=str.replace(':p','<img src=https://www.culturesconverge.com/smiley/smiley-lick.png>');
	str=str.replace(':D','<img src=https://www.culturesconverge.com/smiley/smiley-laugh.png>');
	str=str.replace(':))','<img src=https://www.culturesconverge.com/smiley/smiley-cheerful.png>');
	str=str.replace(':sx','<img src=https://www.culturesconverge.com/smiley/smiley-depressed.png>');
	str=str.replace(':(', '<img src=https://www.culturesconverge.com/smiley/smiley-devious.png>');
	str=str.replace(':)))','<img src=https://www.culturesconverge.com/smiley/smiley-ditsy.png>');
	str=str.replace(':dog','<img src=https://www.culturesconverge.com/smiley/smiley-dog.png>');
	str=str.replace(':fuck','<img src=https://www.culturesconverge.com/smiley/smiley-fuck.png>');
	str=str.replace(':oops','<img src=https://www.culturesconverge.com/smiley/smiley-bigeyes.png>');
	

	return str;
  
}
//**********************************************************************************************************
	// closeChatBox
//**********************************************************************************************************

function closeChatBox(chatboxtitle) {

	jQuery('#chatbox_'+chatboxtitle).css('display','none');
	restructureChatBoxes();

	jQuery.post("https://www.culturesconverge.com/application/views/chat.php?action=closechat", { chatbox: chatboxtitle} , function(data){	
	});

}


//**********************************************************************************************************
	// toggleChatBoxGrowth
//**********************************************************************************************************

function toggleChatBoxGrowth(chatboxtitle) {
 
	if (jQuery('#chatbox_'+chatboxtitle+' .chatboxcontent').css('display') == 'none') {  
		jQuery('#chatbox_'+chatboxtitle+' .chatboxcontent').css('display','block');
		jQuery('#chatbox_'+chatboxtitle+' .chatboxinput').css('display','block');
		var minimizedChatBoxes = new Array();
		
		if (jQuery.cookie('chatbox_minimized')) {
			minimizedChatBoxes = jQuery.cookie('chatbox_minimized').split(/\|/);
		}

		var newCookie = '';

		for (i=0;i<minimizedChatBoxes.length;i++) {
			if (minimizedChatBoxes[i] != chatboxtitle) {
				newCookie += chatboxtitle+'|';
			}
		}

		newCookie = newCookie.slice(0, -1)


		jQuery.cookie('chatbox_minimized', newCookie);
		jQuery('#chatbox_'+chatboxtitle+' .chatboxcontent').css('display','block');
		jQuery('#chatbox_'+chatboxtitle+' .chatboxinput').css('display','block');
		jQuery("#chatbox_"+chatboxtitle+" .chatboxcontent").scrollTop(jQuery("#chatbox_"+chatboxtitle+" .chatboxcontent")[0].scrollHeight);
	} else {
		jQuery('#chatbox_'+chatboxtitle+' .chatboxcontent').css('display','none');
		jQuery('#chatbox_'+chatboxtitle+' .chatboxinput').css('display','none');
		
		var newCookie = chatboxtitle;

		if (jQuery.cookie('chatbox_minimized')) {
			newCookie += '|'+jQuery.cookie('chatbox_minimized');
		}


		jQuery.cookie('chatbox_minimized',newCookie);
		jQuery('#chatbox_'+chatboxtitle+' .chatboxcontent').css('display','none');
		jQuery('#chatbox_'+chatboxtitle+' .chatboxinput').css('display','none');
	}
	
}




//**********************************************************************************************************
	// checkChatBoxInputKey
//**********************************************************************************************************

function checkChatBoxInputKey(event,chatboxtextarea,chatboxtitle) {
	
	if(event.keyCode == 13 && event.shiftKey == 0)  {
		message = jQuery(chatboxtextarea).val();
		message = message.replace(/^\s+|\s+$/g,"");

		jQuery(chatboxtextarea).val('');
		jQuery(chatboxtextarea).focus();
		jQuery(chatboxtextarea).css('height','44px');
		if (message != '') {
		 
			jQuery.post("http://pon.mobilytedev.com/index.php?route=common/header/sendChat", {to: chatboxtitle, message: message} , function(data){
			    
				message = message.replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/\"/g,"&quot;");
                                message=replacetext(message);
				// alert("hi"+username);
				jQuery("#chatbox_"+chatboxtitle+" .chatboxcontent").append('<div class="chatboxmessage"><span class="chatboxmessagefrom"><b><span style="color:#b93d47;">'+username+'</span></b>:&nbsp;&nbsp;</span><span class="chatboxmessagecontent">'+message+'</span></div>');
				jQuery("#chatbox_"+chatboxtitle+" .chatboxcontent").scrollTop(jQuery("#chatbox_"+chatboxtitle+" .chatboxcontent")[0].scrollHeight);
			});
		}
		chatHeartbeatTime = minChatHeartbeat;
		chatHeartbeatCount = 1;

		return false;
	}

	var adjustedHeight = chatboxtextarea.clientHeight;
	var maxHeight = 94;

	if (maxHeight > adjustedHeight) {
		adjustedHeight = Math.max(chatboxtextarea.scrollHeight, adjustedHeight);
		if (maxHeight)
			adjustedHeight = Math.min(maxHeight, adjustedHeight);
		if (adjustedHeight > chatboxtextarea.clientHeight)
			jQuery(chatboxtextarea).css('height',adjustedHeight+8 +'px');
	} else {
		jQuery(chatboxtextarea).css('overflow','auto');
	}
	 
}



//**********************************************************************************************************
	// startChatSession
//**********************************************************************************************************

function startChatSession(){
 
 
	jQuery.ajax({
	  url: "http://pon.mobilytedev.com/index.php?route=common/header/startchatsession",
	  cache: false,
	  dataType: "json",
	  success: function(data) {
    
		username = data.username;
         
		jQuery.each(data.items, function(i,item){
			if (item)	{ // fix strange ie bug

				chatboxtitle = item.f;

				if (jQuery("#chatbox_"+chatboxtitle).length <= 0) {
					createChatBox(chatboxtitle,1);
				}
				
				if (item.s == 1) {
					item.f = username;
				}
     
				if (item.s == 2) {
					jQuery("#chatbox_"+chatboxtitle+" .chatboxcontent").append('<div class="chatboxmessage"><span class="chatboxinfo"><b>'+item.m+'</b></span></div>');
				} else {
					jQuery("#chatbox_"+chatboxtitle+" .chatboxcontent").append('<div class="chatboxmessage"><span class="chatboxmessagefrom"><b>'+item.f+'</b>:&nbsp;&nbsp;</span><span class="chatboxmessagecontent">'+item.m+'</span></div>');
				}
			}
		});
		
		for (i=0;i<chatBoxes.length;i++) {
			chatboxtitle = chatBoxes[i];
			jQuery("#chatbox_"+chatboxtitle+" .chatboxcontent").scrollTop(jQuery("#chatbox_"+chatboxtitle+" .chatboxcontent")[0].scrollHeight);
			setTimeout('jQuery("#chatbox_"+chatboxtitle+" .chatboxcontent").scrollTop(jQuery("#chatbox_"+chatboxtitle+" .chatboxcontent")[0].scrollHeight);', 100); // yet another strange ie bug
		}
	// smith
		//$("#chatbox_"+chatboxtitle).draggable();
		//$(".chatbox_").draggable();
	////////////////
chatHeartbeat()
//	setTimeout('chatHeartbeat();',chatHeartbeatTime);
	
	}});
}




/**
 * Cookie plugin
 *
 * Copyright (c) 2006 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */

jQuery.cookie = function(name, value, options) {
    if (typeof value != 'undefined') { // name and value given, set cookie
        options = options || {};
        if (value === null) {
            value = '';
            options.expires = -1;
        }
        var expires = '';
        if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
            var date;
            if (typeof options.expires == 'number') {
                date = new Date();
                date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
            } else {
                date = options.expires;
            }
            expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
        }
        // CAUTION: Needed to parenthesize options.path and options.domain
        // in the following expressions, otherwise they evaluate to undefined
        // in the packed version for some reason...
        var path = options.path ? '; path=' + (options.path) : '';
        var domain = options.domain ? '; domain=' + (options.domain) : '';
        var secure = options.secure ? '; secure' : '';
        document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
    } else { // only name given, get cookie
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
};
