<?php

#catalog/language/en-gb/account/login

// Heading
$_['heading_title']                = 'Account Login';

// Text
$_['text_account']                 = 'Account';
$_['text_login']                   = 'Login';
$_['text_new_customer']            = 'New Customer';
$_['text_register']                = 'Register Account';
$_['text_register_account']        = 'By creating an account you will be able to shop faster, be up to date on an order\'s status, and keep track of the orders you have previously made.';
$_['text_returning_customer']      = 'Returning Customer';
$_['text_i_am_returning_customer'] = 'I am a returning customer';
$_['text_forgotten']               = 'Forgotten Password';

// Entry
$_['entry_email']                  = 'E-Mail Address';
$_['entry_password']               = 'Password';

// Error
$_['error_login']                  = 'Warning: No match for E-Mail Address and/or Password.';
$_['error_attempts']               = 'Warning: Your account has exceeded allowed number of login attempts. Please try again in 1 hour.';
$_['error_approved']               = 'Warning: Your account requires approval before you can login.';


// REST API

$_['_api_login_success']		= 'Login success'; 
$_['_api_login_error']		= 'Login error'; 
$_['_api_login_success_code']	= 200; 
$_['_api_login_error_code']		= 201; 