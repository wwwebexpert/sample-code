<?php
class ModelRestapiRestapimodel extends Model {
	
	public function addAddress($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int)$data['user_id'] . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', company = '" . $this->db->escape($data['company']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', city = '" . $this->db->escape($data['city']) . "', zone_id = '" . (int)$data['zone_id'] . "', country_id = '" . (int)$data['country_id'] . "', custom_field = '" . $this->db->escape(isset($data['custom_field']) ? json_encode($data['custom_field']) : '') . "'");

		$address_id = $this->db->getLastId();

		if (!empty($data['default'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$data['user_id'] . "'");
		}

		return $address_id;
	}

	public function editAddress($address_id, $data, $customer_id) {
		$this->db->query("UPDATE " . DB_PREFIX . "address SET firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', company = '" . $this->db->escape($data['company']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', city = '" . $this->db->escape($data['city']) . "', zone_id = '" . (int)$data['zone_id'] . "', country_id = '" . (int)$data['country_id'] . "', custom_field = '" . $this->db->escape(isset($data['custom_field']) ? json_encode($data['custom_field']) : '') . "' WHERE address_id  = '" . (int)$address_id . "' AND customer_id = '" . (int)$customer_id . "'");

		if (!empty($data['default'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
		}
	}

	public function deleteAddress($address_id, $customer_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "address WHERE address_id = '" . (int)$address_id . "' AND customer_id = '" . (int)$customer_id . "'");
	}

	public function getAddress($address_id) {
		die('inside000');
		$address_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "address WHERE address_id = '" . (int)$address_id . "' AND customer_id = '" . (int)$this->customer->getId() . "'");

		if ($address_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$address_query->row['country_id'] . "'");

			if ($country_query->num_rows) {
				$country = $country_query->row['name'];
				$iso_code_2 = $country_query->row['iso_code_2'];
				$iso_code_3 = $country_query->row['iso_code_3'];
				$address_format = $country_query->row['address_format'];
			} else {
				$country = '';
				$iso_code_2 = '';
				$iso_code_3 = '';
				$address_format = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$address_query->row['zone_id'] . "'");

			if ($zone_query->num_rows) {
				$zone = $zone_query->row['name'];
				$zone_code = $zone_query->row['code'];
			} else {
				$zone = '';
				$zone_code = '';
			}

			$address_data = array(
				'address_id'     => $address_query->row['address_id'],
				'firstname'      => $address_query->row['firstname'],
				'lastname'       => $address_query->row['lastname'],
				'company'        => $address_query->row['company'],
				'address_1'      => $address_query->row['address_1'],
				'address_2'      => $address_query->row['address_2'],
				'postcode'       => $address_query->row['postcode'],
				'city'           => $address_query->row['city'],
				'zone_id'        => $address_query->row['zone_id'],
				'zone'           => $zone,
				'zone_code'      => $zone_code,
				'country_id'     => $address_query->row['country_id'],
				'country'        => $country,
				'iso_code_2'     => $iso_code_2,
				'iso_code_3'     => $iso_code_3,
				'address_format' => $address_format,
				'custom_field'   => json_decode($address_query->row['custom_field'], true)
			);

			return $address_data;
		} else {
			return false;
		}
	}

	public function getAddresses( $user_id = "" ) {
		
		if ($user_id == '') {
			# code...
			$user_id = $this->customer->getId();
		}

		$address_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$user_id . "'");

		foreach ($query->rows as $result) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$result['country_id'] . "'");

			if ($country_query->num_rows) {
				$country = $country_query->row['name'];
				$iso_code_2 = $country_query->row['iso_code_2'];
				$iso_code_3 = $country_query->row['iso_code_3'];
				$address_format = $country_query->row['address_format'];
			} else {
				$country = '';
				$iso_code_2 = '';
				$iso_code_3 = '';
				$address_format = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$result['zone_id'] . "'");

			if ($zone_query->num_rows) {
				$zone = $zone_query->row['name'];
				$zone_code = $zone_query->row['code'];
			} else {
				$zone = '';
				$zone_code = '';
			}

			$address_data[$result['address_id']] = array(
				'address_id'     => $result['address_id'],
				'firstname'      => $result['firstname'],
				'lastname'       => $result['lastname'],
				'company'        => $result['company'],
				'address_1'      => $result['address_1'],
				'address_2'      => $result['address_2'],
				'postcode'       => $result['postcode'],
				'city'           => $result['city'],
				'zone_id'        => $result['zone_id'],
				'zone'           => $zone,
				'zone_code'      => $zone_code,
				'country_id'     => $result['country_id'],
				'country'        => $country,
				'iso_code_2'     => $iso_code_2,
				'iso_code_3'     => $iso_code_3,
				'address_format' => $address_format,
				'custom_field'   => json_decode($result['custom_field'], true)
			);
		}

		return $address_data;
	}

	public function getTotalAddresses( $user_id = "" ) {

		if ($user_id == '') {
			# code...
			$user_id = $this->customer->getId();
		}

		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$user_id . "'");
		return $query->row['total'];
	}


	public function get_profile($user_id){
		$query = $this->db->query("SELECT firstname,lastname,email,telephone FROM oc_customer WHERE customer_id = '".(int)$user_id."'");
		return @$query->rows[0];
	}
	public function check_product($product_id){
		$query = $this->db->query("SELECT product_id FROM oc_product WHERE product_id = $product_id");

		
		return $query->num_rows;
	}

	public function edit_profile($postData){
		$query = $this->db->query("UPDATE `oc_customer` SET `firstname`= '".$postData['f_name']."',
			`lastname`='".$postData['l_name']."',
			`telephone`='".$postData['contact']."'
			 where customer_id = '".$postData['user_id']."'");

		if(empty($this->db->error)){
			return $query;
		}else{
		    return "error";
		}
		
	}

	


	public function edit_password($user_id, $password) {
		$response=$this->db->query("UPDATE " . DB_PREFIX . "customer SET salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($password)))) . "', code = '' WHERE LOWER(customer_id) = '" .$user_id. "'");
		if(empty($this->db->error)){
			return $response;
		}else{
		    return "error";
		}
	}

	public function dashboard($user_id){
		
		$seller_data=$this->db->query("SELECT oc_customer.`customer_id` as user_id,is_partner as is_seller,count(oc_cart.customer_id) as cart_count/*,oc_customerpartner_to_customer.avatar as banner_images*/  FROM `oc_customer` 
		left join oc_customerpartner_to_customer on oc_customerpartner_to_customer.customer_id = oc_customer.`customer_id` 
		left join oc_cart on oc_cart.customer_id = oc_customer.`customer_id` 
		where oc_customer.`customer_id` = $user_id and is_partner=1");
		//return $seller_data;
		
		$featured_products = $this->db->query("SELECT oc_customerpartner_to_product.product_id,oc_product.image,oc_product.model as product_name,oc_product.price as product_price  FROM oc_customerpartner_to_product 
			left join oc_product on oc_product.product_id = oc_customerpartner_to_product.`product_id` 
			WHERE oc_customerpartner_to_product.customer_id = $user_id");
		
		if(!empty($seller_data)){

			$data=array(
				"user_id"=>$user_id,
				"cart_count"=>$seller_data->rows[0]["cart_count"],
				//"banner_images"=>$this->config->get('config_url').'/image'.$seller_data->rows[0]["banner_images"],
			);
				

			if ( $seller_data->rows[0]["is_seller"] == '') {
				$data['is_seller'] = '0';
			} else {
				$data['is_seller'] = $seller_data->rows[0]["is_seller"];
			}
				
		}
		
		if(!empty($featured_products)){
			//$data["featured_products"]=$featured_products->rows;
		}
		
		return $data;
	}
	/*
	 * Fetch Products 
	 */
	public function product_list($user_id,$category_id){
		$this->load->model('tool/image');
			$query = "SELECT oc_product.product_id,oc_customerpartner_to_product.currency_code,oc_currency.value as currency_value, oc_product_description.name as product_name ,oc_product.price as product_price,oc_review.rating ,oc_product.image as product_image  FROM `oc_product` 
						left join oc_product_to_category on oc_product.product_id =  oc_product_to_category.product_id  
						left join oc_product_description on oc_product.product_id =  oc_product_description.product_id  
						left join oc_review on oc_product.product_id =  oc_review.product_id  
						left join oc_customerpartner_to_product
						 on oc_customerpartner_to_product
						.product_id =  oc_product.product_id 
						left join oc_currency on oc_currency.code =  oc_customerpartner_to_product.currency_code  
						WHERE oc_customerpartner_to_product.customer_id = $user_id ";
			if(!empty($category_id)){
			$query	.= " AND  oc_product_to_category.category_id = $category_id";
			}
			$products_data=$this->db->query($query);
			$productList = $products_data->rows;
			
			$data = [];
			if($productList){
				foreach($productList as $key=>$product){

					$data["product_id"] = $product["product_id"];
					$data["product_name"] = $product["product_name"];
					$data["product_price"] = $this->currency->format($product["product_price"], $product['currency_code'], $product['currency_value']);
					$data["rating"] = (int)$product["rating"];
					$img = $product['product_image'];
					if (!empty($img)) {
						
						
							$image = $this->model_tool_image->resize($img, $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
						} else {
							
							$image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
						}

						
					$data["product_image"] = $image;
					
					$mainR[] = $data;
				}
			}
						

						
			return $mainR;
	}

	public function add_to_cart($postData){

		
		$customer_id=$postData["user_id"];
		$product_id=$postData["product_id"];
		$date_added = date('Y-m-d H:i:s');
		$productOpData = $this->db->query("SELECT product_option_id,oc_option_description.name,oc_option.type,oc_option.option_id FROM `oc_product_option` 
								left join oc_option_description on oc_option_description.option_id = oc_product_option.option_id 
								left join oc_option on oc_option.option_id = oc_product_option.option_id

								WHERE product_id = $product_id ");
		
		foreach($productOpData->rows as $op){
			if(strtolower($op["name"])=="color"){
				$optionArr[] = array($op["product_option_id"]=>$postData['color_id']);
			}
			if(strtolower($op["name"])=="size"){
				$optionArr[] = array($op["product_option_id"]=>$postData['size_id']);
			}

		}
		//$option=json_encode(array(""=>$postData['color_id'],"size_id"=>$postData['size_id']));
		$product_qtyi='1';

		if(!empty($optionArr)){
			foreach ($optionArr as $key => $value) {					
				foreach ($value as $key1 => $value) {
					if(!empty($value)){							
						$opArr[$key1]=$value; 	
					}						
				}					
			}

			$query = $this->db->query("INSERT INTO `oc_cart`(`customer_id`, `product_id`, `option`, 
			`quantity`,`date_added`) 
			VALUES ('".$customer_id."','".$product_id."','".json_encode($opArr)."','".$product_qtyi."','".$date_added."')");

			return $this->db->countAffected();

		} else {
			return false;
		}
		
		
		
	}
	/*
	 * @ Auther: Vaibhav
	 * @ Cart detail Api:
	 *
	 ***/
	public function cart_details($user_id,$currency_code){
		$this->load->model('tool/image');
		$baseUrl = $this->config->get('config_url');
		$cartdata = $this->db->query("SELECT cart_id as cart_item_id,oc_cart.product_id,oc_product_description.name as product_name,oc_product.price as product_price,oc_product.length,oc_product.width,oc_product.height, oc_cart.quantity ,`oc_product`.`image` as product_image,oc_product.tax_class_id
			FROM `oc_cart`
			 left join `oc_product` on `oc_product`.`product_id` = `oc_cart`.`product_id`
			 left join `oc_product_description` on `oc_product_description`.`product_id` = `oc_cart`.`product_id`
			  WHERE `customer_id`=$user_id");
		$customerData = $this->db->query("SELECT oc_customer.firstname as firstname,oc_customer.lastname as lastname ,
			oc_address.address_id,oc_address.address_1,oc_address.city,oc_address.postcode,oc_country.name as country ,
			oc_zone.name as zone
			FROM `oc_customer` 
			left Join oc_address on oc_customer.address_id =oc_address.address_id 
			left Join oc_country on oc_country.country_id =oc_address.country_id 
			left Join oc_zone on oc_zone.zone_id =oc_address.zone_id 
			WHERE oc_customer.`customer_id` = $user_id");

		$data["customer_default_address_detail"] = $customerData->rows[0];
		foreach($cartdata->rows as $cartprod){

			$img = $cartprod["product_image"];
			
			if (!empty($img)) {
							$image = $this->model_tool_image->resize($img, $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
						} else {
							
							$image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
						}
			$cartprod["product_price"]  =  $this->currency->format($this->tax->calculate($cartprod["product_price"], $cartprod['tax_class_id'], $this->config->get('config_tax') ? 'P' : false),$currency_code );
			$cartprod["product_image"] = $image; 
			$cartData[] =  $cartprod;
		}
		$data["cart_product_list"] = $cartData;
		return $data;
	}
	/*
	 * @ Auther: Vaibhav
	 * @ 12/23/2017
	 * @ Cart Edit Api(quantity):
	 ***/
	public function edit_cart($postData){
		$response = $this->db->query("UPDATE `oc_cart` SET `quantity`= '".$postData['product_qty']."'		
			 where product_id = '".$postData['product_id']."' and  cart_id = '".$postData['cart_item_id']."' and customer_id = '".$postData['user_id']."' ");
		
		
		if(empty($this->db->error)){
			return $response;
		}else{
		    return "error";
		}		
	}

	/*
	 * @ Auther: Vaibhav
	 * @ 12/23/2017
	 * @ (13). Cart Delete Api:
	 ***/
	public function delete_cart($postData){
		$response = $this->db->query("DELETE FROM `oc_cart` WHERE `cart_id` = '".$postData['cart_item_id']."' and customer_id = '".$postData['user_id']."' and product_id = '".$postData['product_id']."' ");
		if(empty($this->db->error)){
			return $this->db->countAffected();
		}else{
		    return "error";
		}
	}
	/*
	 * @ Auther: Vaibhav
	 * @ 12/23/2017
	 * @ Order History Api
	 * @ Parameters:user_id,gift_code
	 ***/
	public function order_history($user_id){		
			$order_history = $this->db->query("SELECT `oc_order`.`order_id`,`oc_order_status`.`name` as order_status ,`oc_order`.`currency_code`,`oc_order`.`currency_value`,`oc_order`.`total` as order_amount , DATE_FORMAT(oc_order.date_added, '%d/%m/%Y') as order_date,`oc_order_product`.`quantity` as product_numbers,`oc_order_product`.`name` as pname,`oc_customer`.`firstname`,`oc_customer`.`lastname`,`oc_currency`.`symbol_left` FROM `oc_order`			 
			 LEFT JOIN `oc_order_status` ON `oc_order_status`.`order_status_id` = `oc_order`.`order_status_id` 
			 LEFT JOIN `oc_order_product` ON `oc_order_product`.`order_id` = `oc_order`.`order_id` 
			 LEFT JOIN `oc_customer` ON  `oc_customer`.`customer_id`=`oc_order`.`customer_id` 
			 LEFT JOIN `oc_currency` ON  `oc_currency`.`currency_id`=`oc_order`.`currency_id` 
			 WHERE `oc_order`.`customer_id` = $user_id and (oc_order.order_status_id) >'0' ");

		return $order_history->rows;
	}

	/*
	 * @ Auther: Yuvraj
	 * @ 03/FEB/2017
	 * @ Get customer partner order list Api
	 * @ Parameters:user_id,gift_code
	 ***/
	public function getSellerOrders($data = array(),$customer_id){
	
		$sql = "SELECT DISTINCT o.order_id ,o.date_added,o.currency_code,o.currency_value, CONCAT(o.firstname ,' ',o.lastname) name ,os.name orderstatus  FROM " . DB_PREFIX ."order_status os LEFT JOIN ".DB_PREFIX ."order o ON (os.order_status_id = o.order_status_id) LEFT JOIN ".DB_PREFIX ."customerpartner_to_order c2o ON (o.order_id = c2o.order_id) WHERE c2o.customer_id = '".$customer_id."'  AND os.language_id = '".$this->config->get('config_language_id')."'";
		
		if (isset($data['filter_order']) && !is_null($data['filter_order'])) {
			$sql .= " AND o.order_id = '" . (int)$data['filter_order'] . "'";
		}

		if (!empty($data['filter_name'])) {
			$sql .= " AND ((o.firstname LIKE '%" . $this->db->escape($data['filter_name']) . "%') OR (o.lastname LIKE '%" . $this->db->escape($data['filter_name']) . "%') OR CONCAT(o.firstname,' ',o.lastname) like '%" . $this->db->escape($data['filter_name']) . "%') ";
		}
		
		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND os.name LIKE '%" . $data['filter_status'] . "%'";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND o.date_added LIKE '%" . $this->db->escape($data['filter_date']) . "%'";
		}

		$sort_data = array(
			'o.order_id',
			'o.firstname',
			'os.name',
			'o.date_added'
		);	

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY o.order_id";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		
		$query = $this->db->query($sql);
		
		return $query->rows;
	}


	public function getSellerOrdersTotal($data = array(),$customer_id){
					
		$sql = "SELECT COUNT(DISTINCT o.order_id) AS total ,o.date_added, CONCAT(o.firstname ,' ',o.lastname) name ,os.name orderstatus  FROM " . DB_PREFIX ."order_status os LEFT JOIN ".DB_PREFIX ."order o ON (os.order_status_id = o.order_status_id) LEFT JOIN ".DB_PREFIX ."customerpartner_to_order c2o ON (o.order_id = c2o.order_id) WHERE c2o.customer_id = '".$customer_id."'  AND os.language_id = '".$this->config->get('config_language_id')."' ";

		if (isset($data['filter_order']) && !is_null($data['filter_order'])) {
			$sql .= " AND o.order_id = '" . (int)$data['filter_order'] . "'";
		}

		if (!empty($data['filter_name'])) {
			$sql .= " AND ((o.firstname LIKE '%" . $this->db->escape($data['filter_name']) . "%') OR (o.lastname LIKE '%" . $this->db->escape($data['filter_name']) . "%') OR CONCAT(o.firstname,' ',o.lastname) like '%" . $this->db->escape($data['filter_name']) . "%') ";
		}
		
		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND os.name LIKE '%" . $data['filter_status'] . "%'";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND o.date_added LIKE '%" . $this->db->escape($data['filter_date']) . "%'";
		}

		$sort_data = array(
			'o.order_id',			
			'o.firstname',
			'os.name',
			'o.date_added'
		);	

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY o.order_id";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}


		$query = $this->db->query($sql);

		return $query->row['total'];
		
	}


	public function getTotalOrderProductsByOrderId($order_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

		return $query->row['total'];
	}

	public function getTotalOrderVouchersByOrderId($order_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order_voucher` WHERE order_id = '" . (int)$order_id . "'");

		return $query->row['total'];
	}

	/*
	 * @ Auther: Vaibhav
	 * @ 12/23/2017
	 * @ Get Reviews Api
	 * @ Parameters:user_id,gift_code
	 ***/
	public function get_reviews($postData){
		$status_enable='1';
		$user_id = $postData["user_id"];
		$product_id = $postData["product_id"];
		$reviewsData = $this->db->query("SELECT `review_id` as id,`author` as review_author_name,`text` as review_message,`rating`,DATE_FORMAT(`date_added`,'%d/%m/%Y') as review_date FROM `oc_review` WHERE `status` = $status_enable and `product_id` = $product_id");
		
		
		return $reviewsData->rows;
	}
	/*
	 * @ Auther: Vaibhav
	 * @ 12/23/2017
	 * @ Send Reviews Api
	 * @ Parameters:user_id,product_id,review_message,review_rating
	 ***/
	public function send_reviews($postData){
		$customer_id = $postData["user_id"];
		$product_id = $postData["product_id"];
		$text = $postData["review_message"];
		$author = $postData["author"];
		$review_rating = $postData["review_rating"];

		$response = $this->db->query("INSERT INTO `oc_review`(`customer_id`, `product_id`, `author`, `text`,`rating`) 
			VALUES ('".$customer_id."','".$product_id."','".$author."','".$text."','".$review_rating."')");
		
	    return $this->db->countAffected();
	}



	public function seller_registration($postData){


		//print_r($postData);die;
		$customer_id = $postData["user_id"];		
		 $this->db->query("INSERT INTO " . DB_PREFIX . "customerpartner_to_customer SET 
		 				customer_id='".$customer_id."',
		 				is_partner='1',
		 				screenname='".$postData['business_name']."',		 				
		 				country='".$postData['country_id']."',
		 				companyname='".$postData['business_name']."',
		 				
		 				vchNationalID='".$postData['national_id_no']."',
		 				vchlicense='".$postData['driver_license_no']."',
		 				vchStreet='".$postData['street']."',
		 				vchCity='".$postData['city']."',
		 				companylocality='".$postData['city']."',
		 				
		 				vchAccountName='".$postData['account_name']."',
		 				vchAccountNumber='".$postData['account_number']."',
		 				vchIFSCCode='".$postData['account_type']."',
						
						vchBankName='".$postData['bank_name']."',
						vchBankAddress='".$postData['bank_address']."'");
		 if(empty($this->db->error)){
			return $response;
		}else{
		    return "error";
		}	
	}


    /* Validate email in case for normal user register in api */
	public function getTotalCustomersByEmail($email) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row['total'];
	}


	/* Add customer here */
	public function addCustomer($data) {
		if (isset($data['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($data['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $data['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		$this->load->model('account/customer_group');

		$customer_group_info = $this->model_account_customer_group->getCustomerGroup($customer_group_id);

		$this->db->query("INSERT INTO " . DB_PREFIX . "customer SET customer_group_id = '" . (int)$customer_group_id . "', store_id = '" . (int)$this->config->get('config_store_id') . "', language_id = '" . (int)$this->config->get('config_language_id') . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', custom_field = '" . $this->db->escape(isset($data['custom_field']['account']) ? json_encode($data['custom_field']['account']) : '') . "', salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', newsletter = '" . (isset($data['newsletter']) ? (int)$data['newsletter'] : 0) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', status = '1', approved = '" . (int)!$customer_group_info['approval'] . "', date_added = NOW()");

		$customer_id = $this->db->getLastId();

		$this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int)$customer_id . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', company = '" . $this->db->escape($data['company']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', city = '" . $this->db->escape($data['city']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int)$data['country_id'] . "', zone_id = '" . (int)$data['zone_id'] . "', custom_field = '" . $this->db->escape(isset($data['custom_field']['address']) ? json_encode($data['custom_field']['address']) : '') . "'");

		$address_id = $this->db->getLastId();

		$this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");

		$this->load->language('mail/customer');

		$subject = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));

		$message = sprintf($this->language->get('text_welcome'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) . "\n\n";

		if (!$customer_group_info['approval']) {
			$message .= $this->language->get('text_login') . "\n";
		} else {
			$message .= $this->language->get('text_approval') . "\n";
		}

		$message .= $this->url->link('account/login', '', true) . "\n\n";
		$message .= $this->language->get('text_services') . "\n\n";
		$message .= $this->language->get('text_thanks') . "\n";
		$message .= html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8');

		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
		//echo $data['email'];
		//echo $this->config->get('config_email');
		//echo $message;
		$mail->setTo($data['email']);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
		$mail->setSubject($subject);
		$mail->setText($message);
		$mail->send();
		//exit;

		// Send to main admin email if new account email is enabled
		if (in_array('account', (array)$this->config->get('config_mail_alert'))) {
			$message  = $this->language->get('text_signup') . "\n\n";
			$message .= $this->language->get('text_website') . ' ' . html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8') . "\n";
			$message .= $this->language->get('text_firstname') . ' ' . $data['firstname'] . "\n";
			$message .= $this->language->get('text_lastname') . ' ' . $data['lastname'] . "\n";
			$message .= $this->language->get('text_customer_group') . ' ' . $customer_group_info['name'] . "\n";
			$message .= $this->language->get('text_email') . ' '  .  $data['email'] . "\n";
			$message .= $this->language->get('text_telephone') . ' ' . $data['telephone'] . "\n";

			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setTo($this->config->get('config_email'));
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
			$mail->setSubject(html_entity_decode($this->language->get('text_new_customer'), ENT_QUOTES, 'UTF-8'));
			$mail->setText($message);
			$mail->send();

			// Send to additional alert emails if new account email is enabled
			$emails = explode(',', $this->config->get('config_alert_email'));

			foreach ($emails as $email) {
				if (utf8_strlen($email) > 0 && filter_var($email, FILTER_VALIDATE_EMAIL)) {
					$mail->setTo($email);
					$mail->send();
				}
			}
		}

		return $customer_id;
	}


	public function getCoupon($code,$user_id=null) {
		$status = true;
		
		if(empty(@$user_id)){			
			$user_id = $this->customer->getId();
		}
		
		$coupon_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon` WHERE code = '" . $this->db->escape($code) . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) AND status = '1'");

		if ($coupon_query->num_rows) {
			if($this->cart->getSubTotal()){
				if ($coupon_query->row['total'] > $this->cart->getSubTotal()) {
					$status = false;
				}	
			}
			

			$coupon_history_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "coupon_history` ch WHERE ch.coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "'");

			if ($coupon_query->row['uses_total'] > 0 && ($coupon_history_query->row['total'] >= $coupon_query->row['uses_total'])) {
				$status = false;
			}

			if ($coupon_query->row['logged'] && !$user_id) {
				$status = false;
			}

			if ($user_id) {
				$coupon_history_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "coupon_history` ch WHERE ch.coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "' AND ch.customer_id = '" . (int)$user_id . "'");

				if ($coupon_query->row['uses_customer'] > 0 && ($coupon_history_query->row['total'] >= $coupon_query->row['uses_customer'])) {
					$status = false;
				}
			}

			// Products
			$coupon_product_data = array();

			$coupon_product_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon_product` WHERE coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "'");

			foreach ($coupon_product_query->rows as $product) {
				$coupon_product_data[] = $product['product_id'];
			}

			// Categories
			$coupon_category_data = array();

			$coupon_category_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon_category` cc LEFT JOIN `" . DB_PREFIX . "category_path` cp ON (cc.category_id = cp.path_id) WHERE cc.coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "'");

			foreach ($coupon_category_query->rows as $category) {
				$coupon_category_data[] = $category['category_id'];
			}

			$product_data = array();

			if ($coupon_product_data || $coupon_category_data) {
				foreach ($this->cart->getProducts() as $product) {
					if (in_array($product['product_id'], $coupon_product_data)) {
						$product_data[] = $product['product_id'];

						continue;
					}

					foreach ($coupon_category_data as $category_id) {
						$coupon_category_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "product_to_category` WHERE `product_id` = '" . (int)$product['product_id'] . "' AND category_id = '" . (int)$category_id . "'");

						if ($coupon_category_query->row['total']) {
							$product_data[] = $product['product_id'];

							continue;
						}
					}
				}

				if (!$product_data) {
					$status = false;
				}
			}
		} else {
			$status = false;
		}

		if ($status) {
			return array(
				'is_coupon_valid'=>true,
				'coupon_id'     => $coupon_query->row['coupon_id'],
				'code'          => $coupon_query->row['code'],
				'name'          => $coupon_query->row['name'],
				'type'          => $coupon_query->row['type'],
				'discount'      => $coupon_query->row['discount'],
				'shipping'      => $coupon_query->row['shipping'],
				'total'         => $coupon_query->row['total'],
				'product'       => $product_data,
				'date_start'    => $coupon_query->row['date_start'],
				'date_end'      => $coupon_query->row['date_end'],
				'uses_total'    => $coupon_query->row['uses_total'],
				'uses_customer' => $coupon_query->row['uses_customer'],
				'status'        => $coupon_query->row['status'],
				'date_added'    => $coupon_query->row['date_added']
			);
		}
	}


}


