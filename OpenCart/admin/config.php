<?php
// HTTP
define('HTTP_SERVER', 'http://pon.mobilytedev.com/admin/');
define('HTTP_CATALOG', 'http://pon.mobilytedev.com/');

// HTTPS
define('HTTPS_SERVER', 'http://pon.mobilytedev.com/admin/');
define('HTTPS_CATALOG', 'http://pon.mobilytedev.com/');

// DIR
define('DIR_APPLICATION', '/var/www/html/pon/trunk/admin/');
define('DIR_SYSTEM', '/var/www/html/pon/trunk/system/');
define('DIR_IMAGE', '/var/www/html/pon/trunk/image/');
define('DIR_LANGUAGE', '/var/www/html/pon/trunk/admin/language/');
define('DIR_TEMPLATE', '/var/www/html/pon/trunk/admin/view/template/');
define('DIR_CONFIG', '/var/www/html/pon/trunk/system/config/');
define('DIR_CACHE', '/var/www/html/pon/trunk/system/storage/cache/');
define('DIR_DOWNLOAD', '/var/www/html/pon/trunk/system/storage/download/');
define('DIR_LOGS', '/var/www/html/pon/trunk/system/storage/logs/');
define('DIR_MODIFICATION', '/var/www/html/pon/trunk/system/storage/modification/');
define('DIR_UPLOAD', '/var/www/html/pon/trunk/system/storage/upload/');
define('DIR_CATALOG', '/var/www/html/pon/trunk/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'pon');
define('DB_PASSWORD', '7bucq3sercuwyGBu');
define('DB_DATABASE', 'pon');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');