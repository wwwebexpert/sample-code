<div id="content" class="col-sm-12">      <div class="row">
                                <div class="col-sm-8">
                    <ul class="thumbnails">
                        </ul>
            <ul class="thumbnails list-inline">
                                    
           <li class="image-additional "><a class="thumbnail" href="http://pon.mobilytedev.com/image/cache/placeholder-279x400.png" title="Cap(Demo Product"> <img src="http://pon.mobilytedev.com/image/cache/placeholder-279x400.png" title="Cap(Demo Product" alt="Cap(Demo Product"></a></li>
                                  </ul>
                    <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-description" data-toggle="tab">Description</a></li>
                        <li><a href="#tab-specification" data-toggle="tab">Specification</a></li>
                                    <li><a href="#tab-review" data-toggle="tab">Reviews (0)</a></li>
                      </ul>
          <div class="tab-content">
     

            <div class="tab-pane active" id="tab-description"><p><span style="background-color: rgb(255, 0, 0);">Note:</span></p><p><span style="background-color: rgb(255, 0, 0);">Our DEMO PRODUCTS are listed only to assist and give seller's an idea of how to list similar products to their shops.</span></p><h2 style="font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; margin: 9pt 0in 0.0001pt;"><span style="font-size: 16pt; font-family: Arial, sans-serif; color: rgb(51, 51, 51);">Details:</span></h2><h2 style="font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; margin: 9pt 0in 0.0001pt;"><b style="font-family: inherit; color: rgb(51, 51, 51); font-size: 14px;"><span style="font-size: 12pt; line-height: 17.12px; font-family: Arial, sans-serif; color: rgb(59, 56, 56);">Adorable stylish hand-crafted baby boy hat and booties. This beautiful set would be a perfect "going home" accessory, baby shower gift or great for photos!&nbsp;<span style="background-image: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Also, these baby booties and hat are both adorable, comfortable, and cozy to keep your baby warm and in style.</span></span></b></h2><p class="MsoNormal"><b><span style="font-size: 12pt; line-height: 17.12px; font-family: Arial, sans-serif; color: rgb(59, 56, 56); background-image: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;">Cute baby loafers and hat are made with soft 100% acrylic yarn in light blue, light mocha, and brown colors.</span></b><b><span style="font-size: 12pt; line-height: 17.12px; font-family: Arial, sans-serif; color: rgb(59, 56, 56);"></span></b></p><p class="MsoNormal"><b><span style="font-size: 12pt; line-height: 17.12px; font-family: Arial, sans-serif; color: rgb(59, 56, 56);"><br>Love the set would like a different color?! Message me!&nbsp;<br><br>Set includes 1 hat and I pair of shoes.&nbsp;<br><br>Please see below for sizing!&nbsp;<br><br>Shoe sizes<br><br>Newborn fits up to 3"<br>0-3 months up to 3 1/4"<br>3-6 months up to 4"&nbsp;<br>6-12 months up to 4 1/2"<br><br><br>Hat sizes&nbsp;<br><br>Newborn 13 inches<br>0-3 Months 14 inches<br>3-6 Months. 15 inches<br>6-12 Months. 17 inches<br><br><br><br>Washing instructions- hand wash, and lay flat to dry.<br><br>All items are hand crocheted by me in a smoke free home.&nbsp;<br><br>Please note: Colors may vary slightly due to computer monitors.</span></b></p><p class="MsoNormal"><b><span style="line-height: 14.98px; font-family: Arial, sans-serif;">Ready to ship in</span></b></p><p class="MsoNormal"><span style="font-size: 12pt; line-height: 17.12px; font-family: Arial, sans-serif;">3 - 5 business days</span></p><p class="MsoNormal"><span style="font-size: 12pt; line-height: 17.12px; font-family: Arial, sans-serif;">Made to order items usually take 5-6 weeks to be delivered.<span class="apple-converted-space">&nbsp;</span><br><br>Our items ship from Nigeria and it can take 5-6 weeks to get to you. We ship via the Nipost/EMS and they provide a tracking number but the tracking doesn't update until the item leaves Nigeria if it’s an international purchase<span class="apple-converted-space">.</span><br><br>Express international shipping (3-5 days) can be provided for additional cost.</span></p><p class="MsoNormal"><span style="font-size: 12pt; line-height: 17.12px; font-family: Arial, sans-serif;">&nbsp;</span></p><p class="MsoNormal"><b><span style="font-size: 12pt; line-height: 17.12px; font-family: Arial, sans-serif;">Customs and import taxes:</span></b></p><p class="MsoNormal"><span style="font-size: 12pt; line-height: 17.12px; font-family: Arial, sans-serif;">Buyers are responsible for any customs and import taxes that may apply. I'm not responsible for delays due to customs.</span></p><p class="MsoNormal"><span style="font-size: 12pt; line-height: 17.12px; font-family: Arial, sans-serif;">&nbsp;</span></p><p class="MsoNormal"><span style="font-size: 12pt; line-height: 17.12px; font-family: Arial, sans-serif;">We want you to be happy with your purchase, please ask questions if you require more information before you place an order.</span></p><p class="MsoNormal"><span style="font-size: 12pt; line-height: 17.12px; font-family: Arial, sans-serif;">&nbsp;</span></p><p class="MsoNormal"><b><span style="font-size: 12pt; line-height: 17.12px; font-family: Arial, sans-serif;">Returns &amp; exchanges</span></b></p><p class="MsoNormal"><span style="font-size: 12pt; line-height: 17.12px; font-family: Arial, sans-serif;">I gladly accept returns and exchanges</span></p><p class="MsoNormal"><span style="font-size: 12pt; line-height: 17.12px; font-family: Arial, sans-serif;">Contact me within:<span class="apple-converted-space">&nbsp;</span><span class="text-gray-darker">3 days of delivery</span></span></p><p class="MsoNormal"><span style="font-size: 12pt; line-height: 17.12px; font-family: Arial, sans-serif;">Ship items back within:<span class="apple-converted-space">&nbsp;</span><span class="text-gray-darker">14 days of delivery</span></span></p><p class="MsoNormal"><span style="font-size: 12pt; line-height: 17.12px; font-family: Arial, sans-serif;">&nbsp;</span></p><p class="MsoNormal"><b><span style="font-size: 12pt; line-height: 17.12px; font-family: Arial, sans-serif;">I don't accept cancellations</span></b></p><p class="MsoNormal"><span style="font-size: 12pt; line-height: 17.12px; font-family: Arial, sans-serif;">But please contact me if you have any problems with your order.</span></p><p class="MsoNormal"><span style="font-size: 12pt; line-height: 17.12px; font-family: Arial, sans-serif;">The following items can't be returned or exchanged</span></p><p class="MsoNormal"><span style="font-size: 12pt; line-height: 17.12px; font-family: Arial, sans-serif;">Because of the nature of these items, unless they arrive damaged or defective, I can't accept returns for:</span></p><p class="MsoListParagraphCxSpFirst" style="text-indent: -0.25in;"><span style="font-size: 12pt; line-height: 17.12px; font-family: Wingdings;">n<span style="font-variant-numeric: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;</span></span><span style="font-size: 12pt; line-height: 17.12px; font-family: Arial, sans-serif;">Custom or personalized orders</span></p><p class="MsoListParagraphCxSpLast" style="text-indent: -0.25in;"><span style="font-size: 12pt; line-height: 17.12px; font-family: Wingdings;">n<span style="font-variant-numeric: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;</span></span><span style="font-size: 12pt; line-height: 17.12px; font-family: Arial, sans-serif;">Items on sale</span></p><p class="MsoNormal"><b><span style="font-size: 12pt; line-height: 17.12px; font-family: Arial, sans-serif;">Conditions of return</span></b></p><p></p><p class="MsoNormal"><span style="font-size: 12pt; line-height: 17.12px; font-family: Arial, sans-serif;">Buyers are responsible for return shipping costs. If the item is not returned in its original condition, the buyer is responsible for any loss in value.</span></p>




</div>
                               
                      </div>
        </div>
                               
            
            
            
            
            </div>
                      </div>
                    
          
                            <style>
                #wk_seller_info_container{
                  width: 100%;
                  margin: 10px 0;
                  /*padding: 20px;*/
                  /*background-color: #eee;*/
                }

                #wk_seller_info_profpic{
                  margin-right: 20px;
                  display: inline-block;
                  width: 80px;
                  height: 80px;
                }

                #wk_seller_info_box{
                  display: inline-block;
                }

                #wk_seller_info_rating{
                  position: relative;
                  cursor: pointer;
                }

                #wk_seller_info_rating > .fa{
                  font-size: 10px;
                }

                #wk_seller_info_rating > .fa > .fa-stack-1x {
                    color:#3B9911;
                }

                #wk_seller_info_rating_details{
                  position: absolute;
                  min-width: 250px;
                  height: auto;
                  background: #FFFFFF;
                  top: 23px;
                  padding: 20px;
                  box-shadow: 0 2px 8px 2px #868282;
                  display: none;
                  z-index: 100000;
                }

                .wk_rating_details_box > .wk_rating_details > .fa{
                  font-size: 10px;
                }

                .wk_rating_details_box > .wk_rating_details > .fa > .fa-stack-1x {
                    color:#3B9911;
                }

                .wk_rating_details{
                  padding-left: 5px;
                }

                @media only screen and (max-width: 700px) {
                    
                    #wk_seller_info_rating_details{

                        min-width: 200px;
                        /*height: 100px;*/
                    }
                }

              </style>
              
            
              
              <script>
                $("#wk_seller_info_rating, #wk_seller_info_rating_details").on('mouseover', function(){
                    $("#wk_seller_info_rating_details").show();
                });

                $("#wk_seller_info_rating, #wk_seller_info_rating_details").on('mouseout', function(){
                    $("#wk_seller_info_rating_details").hide();
                });
              </script>
                                            
      

          